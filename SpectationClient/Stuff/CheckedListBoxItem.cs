﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectationClient.Stuff {
    public class BoxItem {
        private String disp;
        private Object value;
        public BoxItem(String display, Object value) {
            this.disp = display;
            this.value = value;
        }

        public Object Value {
            get { return value; }
        }
        public Object Display {
            get { return disp; }
        }

        public override string ToString() {
            return disp;
        }
    }
}
