﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Windows.Forms;
using System.IO;

namespace SpectationClient {
    partial class SpectationClientInfo: Form {
        public SpectationClientInfo() {
            InitializeComponent();
            Properties.Settings settings = Properties.Settings.Default;
            this.Text = String.Format("Info über {0}", Application.ProductName );
            labelProductName.Text = Application.ProductName;
            labelProductVersion.Text = String.Format("Version {0}", Application.ProductVersion);
            this.RTB.Text = Resources.About; ;
            
            this.logoPictureBox.Image = Resources.Logo;
        }

        private void ll_3rdParties_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            GUI.RTBForm RF = new GUI.RTBForm("Third Party Software and Licences");
            RF.addResourceText("SpectationClient.Licence.ThirdPartyLicences.txt");
            RF.ShowDialog();
        }

        private void ll_ChangeLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            GUI.RTBForm RF = new GUI.RTBForm("Change Log");
            RF.addResourceText("SpectationClient.Docs.ChangeLog.txt");
            RF.ShowDialog();
        }

        private void linkCopyRight_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            GUI.RTBForm RF = new GUI.RTBForm("Licence SPECTATION Client");
            RF.addResourceText("SpectationClient.Licence.LGPL3.txt");
            RF.ShowDialog();
        }

      
        

        #region Assemblyattributaccessoren
        /*
        public string AssemblyTitle {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if(attributes.Length > 0) {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if(titleAttribute.Title != "") {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion {
            get {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if(attributes.Length == 0) {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if(attributes.Length == 0) {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if(attributes.Length == 0) {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if(attributes.Length == 0) {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
         */
        #endregion
    }
}
