﻿namespace SpectationClient
{
    partial class SPECTATION_Client
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SPECTATION_Client));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmiConnect = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSearchComplex = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSearchViaCORE_SPECTRA = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSearchViaCGFZ_OBSERVATION_AREAS = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiInsert = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAddBaseInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAddObsArea = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAddGFZMessprotokoll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAddPhotos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTools = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiToolsShowDBTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiToolsSQLConsole = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiToolsOpenSpectralFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsiShowDBSchema = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiShowManual = new System.Windows.Forms.ToolStripMenuItem();
            this.infoÜberSPECTATIONClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.TT_INFO = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiConnect,
            this.tsmiSearch,
            this.tsmiInsert,
            this.tsmiTools,
            this.tsmiHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(363, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "mStrip";
            // 
            // tsmiConnect
            // 
            this.tsmiConnect.AutoToolTip = true;
            this.tsmiConnect.Name = "tsmiConnect";
            this.tsmiConnect.Size = new System.Drawing.Size(112, 20);
            this.tsmiConnect.Text = "Verbinden mit DB";
            this.tsmiConnect.ToolTipText = "Legt Parameter zur Datenbankverbindung fest und\result\nlädt benötigte Informatione" +
    "n.\result\n";
            this.tsmiConnect.Click += new System.EventHandler(this.tslConnect_Click);
            // 
            // tsmiSearch
            // 
            this.tsmiSearch.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSearchComplex,
            this.tsmiSearchViaCORE_SPECTRA,
            this.tsmiSearchViaCGFZ_OBSERVATION_AREAS});
            this.tsmiSearch.Name = "tsmiSearch";
            this.tsmiSearch.Size = new System.Drawing.Size(58, 20);
            this.tsmiSearch.Text = "Suchen";
            this.tsmiSearch.ToolTipText = "Verschiedene Suchmasken um in der Datenbank\result\nnach Spektren zu suchen und sic" +
    "h beschreibende Informationen\result\nanzeigen zu lassen.";
            // 
            // tsmiSearchComplex
            // 
            this.tsmiSearchComplex.Name = "tsmiSearchComplex";
            this.tsmiSearchComplex.Size = new System.Drawing.Size(332, 22);
            this.tsmiSearchComplex.Text = "Komplexe Suche nach Spektren";
            this.tsmiSearchComplex.ToolTipText = "Spektrensuche durch Eingrenzung verschiedener Eigenschaften";
            this.tsmiSearchComplex.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // tsmiSearchViaCORE_SPECTRA
            // 
            this.tsmiSearchViaCORE_SPECTRA.Name = "tsmiSearchViaCORE_SPECTRA";
            this.tsmiSearchViaCORE_SPECTRA.Size = new System.Drawing.Size(332, 22);
            this.tsmiSearchViaCORE_SPECTRA.Text = "Suche über Tabelle CORE.SPECTRA";
            this.tsmiSearchViaCORE_SPECTRA.ToolTipText = "Wählen Sie Spektren aus der Tabelle \"CORE.SPECTRA\" aus und lassen\r\nSie sich die d" +
    "amit verbundenen Informationen anzeigen.";
            this.tsmiSearchViaCORE_SPECTRA.Click += new System.EventHandler(this.sucheInTabelleCORESPECTRAToolStripMenuItem_Click);
            // 
            // tsmiSearchViaCGFZ_OBSERVATION_AREAS
            // 
            this.tsmiSearchViaCGFZ_OBSERVATION_AREAS.Name = "tsmiSearchViaCGFZ_OBSERVATION_AREAS";
            this.tsmiSearchViaCGFZ_OBSERVATION_AREAS.Size = new System.Drawing.Size(332, 22);
            this.tsmiSearchViaCGFZ_OBSERVATION_AREAS.Text = "Suche über Tabelle C_GFZ.OBSERVATION_AREAS";
            this.tsmiSearchViaCGFZ_OBSERVATION_AREAS.ToolTipText = "Wählen Sie Beobachtungsflächen aus der Tabelle C_GFZ.OBSERVATION_AREAS\r\naus und l" +
    "assen Sie sich die damit verbundenen Spektren und deren Informationen anzeigen.";
            this.tsmiSearchViaCGFZ_OBSERVATION_AREAS.Click += new System.EventHandler(this.sucheInTabelleCGFZOBSERVATIONAREASToolStripMenuItem_Click);
            // 
            // tsmiInsert
            // 
            this.tsmiInsert.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAddBaseInfo,
            this.tsmiAddObsArea,
            this.tsmiAddGFZMessprotokoll,
            this.tsmiAddPhotos});
            this.tsmiInsert.Name = "tsmiInsert";
            this.tsmiInsert.Size = new System.Drawing.Size(66, 20);
            this.tsmiInsert.Text = "Einfügen";
            this.tsmiInsert.ToolTipText = "Verschiedene Möglichkeiten um Informationen in \result\ndie Datenbank einzufügen.";
            // 
            // tsmiAddBaseInfo
            // 
            this.tsmiAddBaseInfo.Name = "tsmiAddBaseInfo";
            this.tsmiAddBaseInfo.Size = new System.Drawing.Size(176, 22);
            this.tsmiAddBaseInfo.Text = "Basisinformationen";
            this.tsmiAddBaseInfo.ToolTipText = "Eingabemasken zum vereinfachten Einfügen von Basisdaten im Schema CORE.";
            this.tsmiAddBaseInfo.Click += new System.EventHandler(this.einzelnerSensorToolStripMenuItem_Click);
            // 
            // tsmiAddObsArea
            // 
            this.tsmiAddObsArea.Name = "tsmiAddObsArea";
            this.tsmiAddObsArea.Size = new System.Drawing.Size(176, 22);
            this.tsmiAddObsArea.Text = "Observationsgebiet";
            this.tsmiAddObsArea.Click += new System.EventHandler(this.observationsgebietToolStripMenuItem_Click);
            // 
            // tsmiAddGFZMessprotokoll
            // 
            this.tsmiAddGFZMessprotokoll.Name = "tsmiAddGFZMessprotokoll";
            this.tsmiAddGFZMessprotokoll.Size = new System.Drawing.Size(176, 22);
            this.tsmiAddGFZMessprotokoll.Text = "Messprotokoll GFZ";
            this.tsmiAddGFZMessprotokoll.ToolTipText = "Eingabe von Messdaten gemäß Messmethodik GFZ (Schemata CORE und C_GFZ)";
            this.tsmiAddGFZMessprotokoll.Click += new System.EventHandler(this.gesamtesProtokollToolStripMenuItem_Click);
            // 
            // tsmiAddPhotos
            // 
            this.tsmiAddPhotos.Name = "tsmiAddPhotos";
            this.tsmiAddPhotos.Size = new System.Drawing.Size(176, 22);
            this.tsmiAddPhotos.Text = "Fotos";
            this.tsmiAddPhotos.Click += new System.EventHandler(this.fotosHinzufügenToolStripMenuItem_Click);
            // 
            // tsmiTools
            // 
            this.tsmiTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiToolsShowDBTables,
            this.tsmiToolsSQLConsole,
            this.tsmiToolsOpenSpectralFiles});
            this.tsmiTools.Name = "tsmiTools";
            this.tsmiTools.Size = new System.Drawing.Size(48, 20);
            this.tsmiTools.Text = "Tools";
            // 
            // tsmiToolsShowDBTables
            // 
            this.tsmiToolsShowDBTables.Name = "tsmiToolsShowDBTables";
            this.tsmiToolsShowDBTables.Size = new System.Drawing.Size(193, 22);
            this.tsmiToolsShowDBTables.Text = "Tabellen anzeigen";
            this.tsmiToolsShowDBTables.Click += new System.EventHandler(this.tsmiShow_Click);
            // 
            // tsmiToolsSQLConsole
            // 
            this.tsmiToolsSQLConsole.Name = "tsmiToolsSQLConsole";
            this.tsmiToolsSQLConsole.Size = new System.Drawing.Size(193, 22);
            this.tsmiToolsSQLConsole.Text = "SQL Konsole";
            this.tsmiToolsSQLConsole.Click += new System.EventHandler(this.tsmi_SQLConsole_Click);
            // 
            // tsmiToolsOpenSpectralFiles
            // 
            this.tsmiToolsOpenSpectralFiles.Name = "tsmiToolsOpenSpectralFiles";
            this.tsmiToolsOpenSpectralFiles.Size = new System.Drawing.Size(193, 22);
            this.tsmiToolsOpenSpectralFiles.Text = "Spektraldateien öffnen";
            this.tsmiToolsOpenSpectralFiles.Click += new System.EventHandler(this.spektraldateienÖffnenToolStripMenuItem_Click);
            // 
            // tsmiHelp
            // 
            this.tsmiHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmsiShowDBSchema,
            this.tsmiShowManual,
            this.infoÜberSPECTATIONClientToolStripMenuItem});
            this.tsmiHelp.Name = "tsmiHelp";
            this.tsmiHelp.Size = new System.Drawing.Size(44, 20);
            this.tsmiHelp.Text = "Hilfe";
            // 
            // tmsiShowDBSchema
            // 
            this.tmsiShowDBSchema.Name = "tmsiShowDBSchema";
            this.tmsiShowDBSchema.Size = new System.Drawing.Size(245, 22);
            this.tmsiShowDBSchema.Text = "SPECTATION Datenbankschema";
            this.tmsiShowDBSchema.Click += new System.EventHandler(this.tsmiDBSchema_Click);
            // 
            // tsmiShowManual
            // 
            this.tsmiShowManual.Name = "tsmiShowManual";
            this.tsmiShowManual.Size = new System.Drawing.Size(245, 22);
            this.tsmiShowManual.Text = "SPECTATION Manual";
            this.tsmiShowManual.Click += new System.EventHandler(this.tsmiManual_Click);
            // 
            // infoÜberSPECTATIONClientToolStripMenuItem
            // 
            this.infoÜberSPECTATIONClientToolStripMenuItem.Name = "infoÜberSPECTATIONClientToolStripMenuItem";
            this.infoÜberSPECTATIONClientToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.infoÜberSPECTATIONClientToolStripMenuItem.Text = "Info über SPECTATION Client";
            this.infoÜberSPECTATIONClientToolStripMenuItem.Click += new System.EventHandler(this.infoÜberSPECTATIONClientToolStripMenuItem_Click);
            // 
            // tsmiAbout
            // 
            this.tsmiAbout.Name = "tsmiAbout";
            this.tsmiAbout.Size = new System.Drawing.Size(245, 22);
            this.tsmiAbout.Text = "Info über SPECTATION Client";
            this.tsmiAbout.Click += new System.EventHandler(this.tsmiAbout_Click);
            // 
            // TT_INFO
            // 
            this.TT_INFO.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // SPECTATION_Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 24);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "SPECTATION_Client";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "SPECTATION Client (Version 2012.1)";
            this.TT_INFO.SetToolTip(this, "TT on TT");
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SPECLIB_Client_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiSearch;
        private System.Windows.Forms.ToolStripMenuItem tsmiInsert;
        private System.Windows.Forms.ToolStripMenuItem tsmiConnect;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddBaseInfo;
        private System.Windows.Forms.ToolStripMenuItem tsmiSearchComplex;
        private System.Windows.Forms.ToolStripMenuItem tsmiSearchViaCORE_SPECTRA;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddGFZMessprotokoll;
        private System.Windows.Forms.ToolStripMenuItem tsmiSearchViaCGFZ_OBSERVATION_AREAS;
        private System.Windows.Forms.ToolStripMenuItem tsmiHelp;
        private System.Windows.Forms.ToolTip TT_INFO;
        private System.Windows.Forms.ToolStripMenuItem tmsiShowDBSchema;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbout;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddObsArea;
        private System.Windows.Forms.ToolStripMenuItem tsmiShowManual;
        private System.Windows.Forms.ToolStripMenuItem tsmiTools;
        private System.Windows.Forms.ToolStripMenuItem tsmiToolsShowDBTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiToolsSQLConsole;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddPhotos;
        private System.Windows.Forms.ToolStripMenuItem tsmiToolsOpenSpectralFiles;
        private System.Windows.Forms.ToolStripMenuItem infoÜberSPECTATIONClientToolStripMenuItem;


    }
}

