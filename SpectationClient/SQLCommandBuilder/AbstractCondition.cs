﻿using System;
using Npgsql;

namespace SpectationClient.SQLCommandBuilder {
    public abstract class AbstractCondition {
        public abstract bool IsEmpty { get; }
        //public enum Op: byte { };//; //: byte { }
        //public abstract Op Operator { get; }
        public abstract NpgsqlCommand getCmd();
        public abstract NpgsqlCommand getCmd(String prefix);
    }

}
