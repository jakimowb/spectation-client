﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace SpectationClient.SpectralTools {
    class BinaryBuilder:BinaryWriter  {

        public long Position {
            get { return this.BaseStream.Position; }
            set { this.BaseStream.Position = value ; }
        }

        public static BinaryBuilder CreateBinaryBuilder(long numberOfBytes) {
            return BinaryBuilder.CreateBinaryBuilder(new Byte[numberOfBytes]);
        }
        
        public static BinaryBuilder CreateBinaryBuilder(byte[] byteArray){
            return new BinaryBuilder(new MemoryStream(byteArray, true), ASCIIEncoding.ASCII);
        }

        public Byte[] getByteArray(){
           return ((MemoryStream)this.OutStream).ToArray();
        }
        
        private BinaryBuilder(Stream output):base(output) {
                
        }
        
        private BinaryBuilder(Stream output, Encoding encoding): base(output, encoding) {

        }

        #region Write single values
        public void Write(long position, String value) {
            this.Position = position;
            Char[] c = value.ToCharArray();

            this.Write(c);
        }

        public void Write(long position, Int16 value) {
            this.Position = position;
            this.Write(value);
        }

        public void Write(long position, Int32 value) {
            this.Position = position;
            this.Write(value);
        }

        public void Write(long position, Int64 value) {
            this.Position = position;
            this.Write(value);
        }

        public void Write(long position, UInt16 value) {
            this.Position = position;
            this.Write(value);
        }

        public void Write(long position, UInt32 value) {
            this.Position = position;
            this.Write(value);
        }

        public void Write(long position, UInt64 value) {
            this.Position = position;
            this.Write(value);
        }

        public void Write(long position, Boolean value) {
            this.Position = position;
            this.Write(value);
        }

        public void Write(long position, Char value) {
            this.Position = position;
            this.Write(value);
        }

        public void Write(long position, Single value) {
            this.Position = position;
            this.Write(value);
        }

        public void Write(long position, Double value) {
            this.Position = position;
            this.Write(value);
        }
        #endregion

        #region Write Arrays
        public void WriteArray(Byte[] values) {
            foreach(Byte value in values) this.Write(value);
        }
        public void WriteArray(long position, Byte[] values) {
            this.Position = position;
            this.WriteArray(values);
        }

        public void WriteArray(Int16[] values) {
            foreach(Int16 value in values) this.Write(value);
        }
        public void WriteArray(long position, Int16[] values) {
            this.Position = position;
            this.WriteArray(values);
        }


        public void WriteArray(Int32[] values) {
            foreach(Int32 value in values) this.Write(value);
        }
        public void WriteArray(long position, Int32[] values) {
            this.Position = position;
            this.WriteArray(values);
        }


        public void WriteArray(Int64[] values) {
            foreach(Int64 value in values) this.Write(value);
        }
        public void WriteArray(long position, Int64[] values) {
            this.Position = position;
            this.WriteArray(values);
        }


        public void WriteArray(UInt16[] values) {
            foreach(UInt16 value in values) this.Write(value);
        }
        public void WriteArray(long position, UInt16[] values) {
            this.Position = position;
            this.WriteArray(values);
        }


        public void WriteArray(UInt32[] values) {
            foreach(UInt32 value in values) this.Write(value);
        }
        public void WriteArray(long position, UInt32[] values) {
            this.Position = position;
            this.WriteArray(values);
        }


        public void WriteArray(UInt64[] values) {
            foreach(UInt64 value in values) this.Write(value);
        }
        public void WriteArray(long position, UInt64[] values) {
            this.Position = position;
            this.WriteArray(values);
        }


        public void WriteArray(Boolean[] values) {
            foreach(Boolean value in values) this.Write(value);
        }
        public void WriteArray(long position, Boolean[] values) {
            this.Position = position;
            this.WriteArray(values);
        }


        public void WriteArray(Char[] values) {
            foreach(Char value in values) this.Write(value);
        }
        public void WriteArray(long position, Char[] values) {
            this.Position = position;
            this.WriteArray(values);
        }


        public void WriteArray(Single[] values) {
            foreach(Single value in values) this.Write(value);
        }
        public void WriteArray(long position, Single[] values) {
            this.Position = position;
            this.WriteArray(values);
        }

        public void WriteArray(Double[] values) {
            foreach(Double value in values) this.Write(value);
        }
        public void WriteArray(long position, Double[] values) {
            this.Position = position;
            this.WriteArray(values);
        }

        #endregion


    }
}
