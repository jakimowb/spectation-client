﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
//using ToolLibrary.Forms;
//using SpectationClient.SpectralTools;
using SpectationClient.SpectralTools;

namespace SpectationClient.DataBaseDescription {
    [Serializable()]
    public class ColumnInfoSpectralFile : ColumnInfoFile {

        public ColumnInfoFile HeaderFileColumn { get; set; }

        #region Constructors
        public ColumnInfoSpectralFile(String mainBlobColumn):base(mainBlobColumn) {
           
        }
        #endregion

        override public string ToString() {
            return String.Format("ColumnInfoSpectralFile:{0}", this.Name);
        }

        override public void setRelatedColumn(String ColumnName, RelatedColumnTypes type) {
            base.setRelatedColumn(ColumnName, type);
            switch(type) {
                case RelatedColumnTypes.cfile_hdr: this.HeaderFileColumn = (ColumnInfoFile) this.Table[ColumnName]; break;

            }

        }

        /*
        protected virtual Dictionary<String, Object> getValuesForColumns(SPECLIB_Spectrum spectrum) {
            Dictionary<String, Object> ret = new Dictionary<string, object>();//base.getValuesForColumns(fi_main);
            foreach(String column in this.relatedColumns.Keys) {
                RelatedColumnTypes t = this.relatedColumns[column];
                switch (t){
                    case RelatedColumnTypes.std_DataType : throw new NotImplementedException("std_DataType");
                    case RelatedColumnTypes.std_DateTime :ret.Add(column, spectrum.TimeSaved);break;
                    case RelatedColumnTypes.std_FileExt : throw new NotImplementedException("std_FileExt");
                    case RelatedColumnTypes.std_FileName :ret.Add(column, spectrum.FileName);break;
                    case RelatedColumnTypes.std_FileType :ret.Add(column, spectrum.FileType);break;
                    case RelatedColumnTypes.std_HdrBLOB :throw new NotImplementedException("std_HdrBLOB");
                    case RelatedColumnTypes.std_MainBLOB :throw new NotImplementedException("std_MainBLOB");
                    case RelatedColumnTypes.std_unknown :ret.Add(column, null);break;
                    case RelatedColumnTypes.spec_fwhm : throw new NotImplementedException("spec_fwhm");
                    case RelatedColumnTypes.spec_n_bands :ret.Add(column, spectrum.NumberOfBands);break;
                    case RelatedColumnTypes.spec_sample_count : throw new NotImplementedException("spec_sample_count");
                    case RelatedColumnTypes.spec_spectype :ret.Add(column, spectrum.SpecType2String);break;
                    case RelatedColumnTypes.spec_type :ret.Add(column, spectrum.SpecType2String );break;
                    case RelatedColumnTypes.spec_wl_max :ret.Add(column, spectrum.WL_Min);break;
                    case RelatedColumnTypes.spec_wl_min : ret.Add(column, spectrum.WL_Min);break;

                
                }
            }
            return ret;

        }
        protected override Dictionary<String, Object> getValuesForColumns(FileInfo spectralFile) {
            //Check if this is a spectrum
            Dictionary<String, Object> ret = new Dictionary<string, object>();//base.getValuesForColumns(fi_main);

            String hdr_text;
            FileInfo fi_main;
            Byte[] BA_hdr;
            Byte[] BA_main;


            if(ESL_Reader.getESL_Files(spectralFile, out BA_main, out BA_hdr, out hdr_text, out fi_main)){
                SubSelectESLSpectrum.SpecFilesandInfo SI = new SubSelectESLSpectrum.SpecFilesandInfo();
                SubSelectESLSpectrum SSES = new SubSelectESLSpectrum(hdr_text, BA_main, fi_main.Name, false);
                if (SSES.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    int sline = SSES.getSelectedRowNumber() + 1;
                    SI = SSES.getSelectedSpectrum();
                }
               
                foreach (String columnsInRow in this.relatedColumns.Keys) {
                    RelatedColumnTypes t = this.relatedColumns[columnsInRow];
                    //this is the main column_dgv => use main BLOB
                    switch (t) {
                        case RelatedColumnTypes.std_FileName: ret.Add(columnsInRow, SI.filename); break;
                        case RelatedColumnTypes.envi_spectrumname: ret.Add(columnsInRow, SI.spectraname); break;
                        case RelatedColumnTypes.envi_BandNames: ret.Add(columnsInRow, ESL_Reader.getEHFI_Information(hdr_text, ESL_Reader.MetaTag.band_names)); break;
                        case RelatedColumnTypes.envi_ByteOrder: ret.Add(columnsInRow, ESL_Reader.getEHFI_Information(hdr_text, ESL_Reader.MetaTag.byte_order)); break;
                        case RelatedColumnTypes.std_MainBLOB: ret.Add(columnsInRow, BA_main); break;
                        case RelatedColumnTypes.std_HdrBLOB: ret.Add(columnsInRow, BA_hdr); break;
                        case RelatedColumnTypes.std_FileType:
                        case RelatedColumnTypes.envi_FileType:
                            if (ret.ContainsKey(columnsInRow)) ret.Remove(columnsInRow);
                            ret.Add(columnsInRow, ESL_Reader.getEHFI_FileType_ShortString(ESL_Reader.getEHFI_FileType(hdr_text)));
                            break;
                        case RelatedColumnTypes.asd_data_type:
                        case RelatedColumnTypes.envi_DataType: ret.Add(columnsInRow, ESL_Reader.getDataType(hdr_text).Name); break;
                        case RelatedColumnTypes.envi_HeaderOffset: ret.Add(columnsInRow, ESL_Reader.getEHFI_Information(hdr_text, ESL_Reader.MetaTag.header_offset)); break;
                        case RelatedColumnTypes.envi_Interleave: ret.Add(columnsInRow, ESL_Reader.getEHFI_Information(hdr_text, ESL_Reader.MetaTag.interleave)); break;
                        case RelatedColumnTypes.envi_Nbands: ret.Add(columnsInRow, ESL_Reader.getEHFI_Information(hdr_text, ESL_Reader.MetaTag.bands)); break;
                        case RelatedColumnTypes.envi_NLines: ret.Add(columnsInRow, ESL_Reader.getEHFI_Information(hdr_text, ESL_Reader.MetaTag.lines)); break;
                        case RelatedColumnTypes.envi_NSamples: ret.Add(columnsInRow, ESL_Reader.getEHFI_Information(hdr_text, ESL_Reader.MetaTag.samples)); break;
                        case RelatedColumnTypes.envi_Description: ret.Add(columnsInRow, ESL_Reader.getEHFI_Information(hdr_text, ESL_Reader.MetaTag.description)); break;
                    }
                }               
            } else if(ASD_Reader.isValidASDSpectrum(spectralFile)){
                ret = base.getValuesForColumns(spectralFile);
                //Add ASD specific informations
                byte[] BA = File.ReadAllBytes(spectralFile.FullName);
                
                foreach (String columnsInRow in this.relatedColumns.Keys) {
                    RelatedColumnTypes t = relatedColumns[columnsInRow];
                    switch (t) {
                        case RelatedColumnTypes.asd_spectrum_type: ret.Add(columnsInRow, ToolLibrary.SpectralTools.SPECLIB_Spectrum.getSpectrumDataType_asString(ASD_Reader.getSpectrumType(ref BA))); break;
                        case RelatedColumnTypes.asd_data_type: ret.Add(columnsInRow, ASD_Reader.getDataType(ref BA).Name); break;
                        case RelatedColumnTypes.asd_ch1_wavel: ret.Add(columnsInRow, ASD_Reader.getFirstWaveLength(ref BA)); break;
                        case RelatedColumnTypes.asd_ch_last_wavel: ret.Add(columnsInRow, ASD_Reader.getLastWaveLength(ref BA)); break;
                        case RelatedColumnTypes.asd_channels: ret.Add(columnsInRow, ASD_Reader.getChannelCount(ref BA)); break;
                        case RelatedColumnTypes.asd_data_format: ret.Add(columnsInRow, ASD_Reader.getWaveLengthStep(ref BA)); break;
                        case RelatedColumnTypes.asd_sample_count: ret.Add(columnsInRow, ASD_Reader.getASDsample_count(ref BA)); break;
                        case RelatedColumnTypes.asd_when: ret.Add(columnsInRow, ASD_Reader.getTimeSpectrumSaved(ref BA)); break;
                    }
                }
            }
            

            return ret;
        }
        */
        /*
        private String getKeyForValue(Dictionary<String, RelatedColumnTypes> dic, RelatedColumnTypes value){
            foreach (String columnsInRow in dic.Keys) {
                if (dic[columnsInRow] == value) return columnsInRow;
            }
            return null;
        }*/ 
    }
}
