﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeoAPI.Geometries;

namespace SpectationClient.DataBaseDescription {
    
    
    [Serializable()]
    public class ColumnInfoGeometry : ColumnInfo {
        private OgcGeometryType geometryType;
        private int srid;
        public int SRID { get { return this.srid; } }
        public OgcGeometryType GeometryType { get { return this.geometryType; } }

        public ColumnInfoGeometry(String columName, OgcGeometryType GeometryType, int SRID)
            : base(columName) {
            this.srid = SRID;
            this.geometryType = GeometryType;
        }
        public override string ToString() {
            return String.Format("ColumnInfoGeometry{0}", this.Name);
        }
    }
}
