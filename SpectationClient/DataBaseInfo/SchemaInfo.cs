﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectationClient.DataBaseDescription {
    [Serializable()]
    public class SchemaInfo : Dictionary<String, TableInfo> {
        private String name;
        public String Name { get { return this.name; } }

        /// <summary>
        /// Gets or sets the DataBase this schemaName is related to.
        /// </summary>
        public DataBaseInfo DataBase { get; set; }
        
        public SchemaInfo(String SchemaName) {
            this.name = SchemaName;
        }

        new public void Add(String tableName, TableInfo tableInfo){
            if(tableName != tableInfo.Name) throw new Exception("Table key and SchemaInfo Name do not match.");
            this.Add(tableInfo);
        }

        public void Add(TableInfo tableInfo) {
            if(tableInfo.Schema != null) {
                throw new Exception(
                    String.Format("Table {0} already belongs to SchemaInfo {1}",
                    tableInfo.Name, tableInfo.Schema.Name));
            } else {
                tableInfo.Schema = this;
            }
            base.Add(tableInfo.Name, tableInfo);
        }

        public override string ToString() {
            return String.Format("SchemaInfo:{0}", this.Name);
        }
    }

}
