﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace SpectationClient.DataBaseDescription {
  
    public interface IColumnInfo {
        String Name {get; }
        TableInfo Table { get; set; }
        Key Key { get; set; }
        bool IsKey { get; }
        bool IsPrimaryKey { get; }

        ForeignKey ForeignKey { get; set; }
        bool IsForeignKeyTarget { get; }
        bool IsAutogenerated { get; }

    }

    public class ColumnInfo : IColumnInfo{
   
        

        protected String name;
        public String Name { get { return name; } }

        /// <summary>
        /// Gets or sets the Table this column is related to.
        /// </summary>
        public TableInfo Table { get; set; }
        protected bool allowNULL = true;
        protected bool hasForeignKey = false;
        protected bool isAutogenerated = false;
        protected Type valueType;


        public List<ConstrainedValue> possibleValues { get; set; }
        public struct ConstrainedValue {
            public ConstrainedValue(Object value, String tooltip) {
                this.value = value;
                this.tooltip = tooltip;
            }
            public object value;
            public String tooltip;
        }


        public Key Key { get; set; }
        public bool IsKey { get { return this.Key != null; } }
        public bool IsPrimaryKey { get { return this.IsKey && this.Key.IsPrimaryKey; } }
        
        public ForeignKey ForeignKey { get; set; }
        public bool IsForeignKeyTarget { get { return this.ForeignKey != null; } }
        public bool IsAutogenerated { get { return this.isAutogenerated; } }

        #region Constructors
        public ColumnInfo(String name) {
            this.name = name;
        }

        public ColumnInfo(String columname, bool isautogenerated, Type valueType) {
            this.init(columname, isautogenerated, valueType);
        }
        #endregion


        public bool Equals(ColumnInfo ci) {
            return this.name == ci.Name && this.Table == ci.Table;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
        public Type ValueType {
            get { return this.valueType; }
        }
        

        public override bool Equals(object obj) {
            return base.Equals(obj);
        }

        protected void init(String columnName, bool isAutogenerated, Type valueType) {
            this.name = columnName;
            this.isAutogenerated = isAutogenerated;
            this.valueType = valueType;
        }

        public bool AllowDBNull { get { return this.allowNULL; } 
                                  set { this.allowNULL = value; }
                                }

        public override string ToString() {
            return String.Format("ColumnInfo:{0}", this.Name);
        }

    }


}
