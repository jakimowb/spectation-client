﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectationClient.DataBaseDescription {
    public class Key:List<ColumnInfo> {
        private bool isPrimaryKey = false;
        public bool IsPrimaryKey { get { return this.isPrimaryKey; } }

        public Key(List<ColumnInfo> keyColumns, bool isPrimaryKey) {
            this.isPrimaryKey = isPrimaryKey;
            this.AddRange(keyColumns);
        }

        public List<String> Names {
            get {
                return (from x in this
                        select x.Name).ToList();
            }
        }

        /// <summary>
        /// Returns true when both keys have attributes least one ColumnInfo in common.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Intersects(Key key) {
            foreach(ColumnInfo c in key) {
                if(this.Contains(c)) return true;
            }
            return false;
        }

        public override string ToString() {
            return String.Format("Key:{0}",TextHelper.combine(this.Names) );
        }

    }
}
