﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using SpectationClient.Stuff;

namespace SpectationClient.DataBaseDescription {
    [Serializable()]
    public class ColumnInfoImageFile : ColumnInfoFile {
        public ColumnInfoImageFile mainImageFile {get; set;}
        public ColumnInfoImageFile previewImageFile { get; set; }
       

        #region Constructors
        public ColumnInfoImageFile(String fileColumnName)
            :base(fileColumnName){
        }
        #endregion

        public override string ToString() {
            return String.Format("ColumnInfoImageFile:{0}", this.Name);
        }

        protected override Dictionary<String, Object> getValuesForColumns(FileInfo fi) {
            //Get general file information
            Dictionary<String, Object> columnValues = base.getValuesForColumns(fi);

            //Get image specific file information
            //Check if this is a known image
            Byte[] imageBLOB = File.ReadAllBytes(fi.FullName);
            BLOBInfo.BLOBType bt = BLOBInfo.getBLOBType(imageBLOB);
            if (!(bt == BLOBInfo.BLOBType.EMF ||
                  bt == BLOBInfo.BLOBType.BMP ||
                  bt == BLOBInfo.BLOBType.GIF ||
                  bt == BLOBInfo.BLOBType.JPG ||
                  bt == BLOBInfo.BLOBType.PNG ||
                  bt == BLOBInfo.BLOBType.TIFF ||
                  bt == BLOBInfo.BLOBType.WMF)) throw new Exception("Unbekanntes Bildformat: "+fi.FullName);

            //Add image-specific values
            Dictionary<RelatedColumnTypes, object> relatedValues = ColumnInfoImageFile.getImageFileInformations(fi);
            foreach (String col in relatedColumns.Keys) {
                RelatedColumnTypes relatedColumnType = relatedColumns[col];
                columnValues.Add(col, relatedValues[relatedColumnType]);
            }
            return columnValues;
        }

        public static Dictionary<RelatedColumnTypes, Object> getImageFileInformations(FileInfo fileInfo) {
            Dictionary<RelatedColumnTypes, Object> imageInformation = new Dictionary<RelatedColumnTypes, Object>();
            Image image = Image.FromFile(fileInfo.FullName);
            imageInformation.Add(RelatedColumnTypes.cimg_type, BLOBInfo.getFileExtension(image));
            imageInformation.Add(RelatedColumnTypes.cimg_rex_x, image.Size.Width);
            imageInformation.Add(RelatedColumnTypes.cimg_res_y, image.Size.Height);
            imageInformation.Add(RelatedColumnTypes.cfile, DataHelper.ImageToByteArray(image, image.RawFormat));
            imageInformation.Add(RelatedColumnTypes.cfilename, fileInfo.Name);
            imageInformation.Add(RelatedColumnTypes.cimg_dpi, image.HorizontalResolution);
            imageInformation.Add(RelatedColumnTypes.cimg_preview, DataHelper.ImageToByteArray(
                    DataHelper.makePreview(image, 300, 300), 
                    image.RawFormat)
                  );
            return imageInformation;
        }


        #region Handling of related columns

        override public void setRelatedColumn(String ColumnName, RelatedColumnTypes type) {
            if(this.Table == null) throw new Exception(String.Format("Column {0} is not connected to a TableInfo,", this.name));
            if(!this.Table.ContainsKey(ColumnName)) throw new Exception(String.Format("Column {0} does not exist in Table {1}", ColumnName, this.Table));
            this.relatedColumns.Add(ColumnName, type);

            switch(type) {
                case RelatedColumnTypes.cimg_preview: this.previewImageFile = (ColumnInfoImageFile) this.Table[ColumnName]; break;
                case RelatedColumnTypes.cfile: this.mainImageFile = (ColumnInfoImageFile)this.Table[ColumnName]; break;

            }

        }

        #endregion
    }
}
