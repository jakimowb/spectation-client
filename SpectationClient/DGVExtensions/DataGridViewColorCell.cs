﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SpectationClient.DGVExtensions {
    public class DataGridViewColorCell : DataGridViewExtensionBaseCell {

        public DataGridViewColorCell() {
            this.Value = Color.White;
        }

        public override object Clone() {
            Object ret = base.Clone();
            return ret;
        }

        public Color getColor() {
            Color c = (Color)this.Value;
            return c;
        }

        protected override bool SetValue(int rowIndex, object value) {
            return base.SetValue(rowIndex, value);
        }

        protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, System.ComponentModel.TypeConverter valueTypeConverter, System.ComponentModel.TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context) {
            return base.GetFormattedValue("", rowIndex, ref cellStyle, valueTypeConverter, formattedValueTypeConverter, context);
        }
        protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts) {

            if(value is Color) {
                Color c = (Color)value;
                cellStyle.BackColor = c;
                cellStyle.SelectionBackColor = c;
                Color fc = cellStyle.ForeColor;
                if((c.B + c.R + c.G) < 100) fc = Color.Black;
                cellStyle.ForeColor = fc;
            } else {
                cellStyle.BackColor = Color.Empty;
                cellStyle.SelectionBackColor = Color.Empty;
            }

            base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue,
                errorText, cellStyle, advancedBorderStyle, paintParts);
        }

        protected override void OnDoubleClick(DataGridViewCellEventArgs e) {
            ColorDialog cd = new ColorDialog();
            cd.AllowFullOpen = true;
            if(cd.ShowDialog() == DialogResult.OK) {
                this.Value = cd.Color;
            }
            base.OnContentDoubleClick(e);
        }
    }
}
