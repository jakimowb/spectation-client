﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;

using SpectationClient.DataBaseDescription;
using SpectationClient.GUI;
using SpectationClient.Stuff;

namespace SpectationClient.DGVExtensions {


    public class DataGridViewByteArrayColumn : DataGridViewExtensionBaseColumn {

        public override TableInfo TableInfo {
            get { return this.ColumnInfo == null? null : this.ColumnInfo.Table; }
        }


        private ColumnInfoFile columnInfoFile = null;
        public ColumnInfoFile ColumnInfoFile { get { return this.columnInfoFile; } }




        public DataGridViewByteArrayColumn() {
            this.CellTemplate = new DataGridViewByteArrayCell();
            this.ReadOnly = true;
        }
        
        public DataGridViewByteArrayColumn(ColumnInfoFile columnInfoFile) {
            this.CellTemplate = new DataGridViewByteArrayCell();
            this.columnInfoFile = columnInfoFile;
            this.ReadOnly = true;
        }

        public ContextMenuStrip setToolStripLabels(bool addSaveLabel, bool addRemoveLabel) {
            this.ContextMenuStrip = new ContextMenuStrip();
           
            if(addSaveLabel) {
                ToolStripMenuItem label = new ToolStripMenuItem("Speichere Dateien/Byte-Arrays");
                // labelShow.Width = 150;
                label.Click +=new EventHandler(label_saveClick);
                this.ContextMenuStrip.Items.Add(label);
            }

            if(addRemoveLabel) {
                ToolStripMenuItem item = new ToolStripMenuItem("Entferne Dateien/Byte-Arrays");
                //item.Width = 150;
                item.Click +=new EventHandler(label_removeClick);
                this.ContextMenuStrip.Items.Add(item);

            }

            return this.ContextMenuStrip;

        }
        void label_removeClick(object sender, EventArgs e) {
            foreach(DataGridViewByteArrayCell cell in getSelectedCells(true)) {
                cell.Value = null;
            }
        }

        void label_saveClick(object sender, EventArgs e) {
            //Open folder

            List<DataGridViewCell> cells = getSelectedCells(true);
            SaveFilesDialog SFD = new SaveFilesDialog(this.DataGridView);
            if(SFD.ShowDialog() == DialogResult.OK) {
                this.DataGridView.Cursor = Cursors.WaitCursor;
                String folder = SFD.Folder;
                
                bool useRowNumbers = SFD.UseRowNumbers;
                String[] cols = SFD.FileNameColumns;
                foreach(DataGridViewByteArrayCell c in cells) {
                    String filename;
                    if(useRowNumbers) {
                        filename = String.Format("{0}{1}", this.Name, c.RowIndex+1);

                    } else {
                        String[] columnValues = (from String column in cols
                                let otherCell = this.DataGridView[column, c.RowIndex]
                                where otherCell.Value != null
                                select otherCell.Value.ToString()).ToArray();
                        filename = String.Format("{0}_{1}",
                                        this.Name, 
                                        TextHelper.combine(columnValues, "_"));
                    }
                    FileInfo fileInfo = new FileInfo(SFD.Folder + "\\"+ filename + SFD.FileExtension);
                    fileInfo = DataHelper.createNewFilePath(fileInfo);
                    File.WriteAllBytes(fileInfo.FullName, c.Value as Byte[]);
                    
                }
                this.DataGridView.Cursor = Cursors.Default;
            }
        }
    }
}
