﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SpectationClient.DGVExtensions {
   
    
    public class DataGridViewColorColumn: DataGridViewExtensionBaseColumn {
        public DataGridViewColorColumn() {
            this.CellTemplate = new DataGridViewColorCell();
            this.ReadOnly = true;
            
        }


        public override DataBaseDescription.TableInfo TableInfo {
            get { throw new NotImplementedException(); }
        }
    }
}
