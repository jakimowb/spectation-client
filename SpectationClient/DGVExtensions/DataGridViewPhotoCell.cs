﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel;
using SpectationClient.Stuff;
using SpectationClient.DataBaseDescription;

namespace SpectationClient.DGVExtensions {
    public class DataGridViewPhotoCell : DataGridViewExtensionBaseCell {

        protected new ColumnInfoImageFile columnInfo = null;
        public new ColumnInfoImageFile ColumnInfo {
            get {
                setDatabaseDescriptions();
                return this.columnInfo;
            }
        }

        protected override void setDatabaseDescriptions() {
            if(!dataBaseDescriptionChecked) {
                DataGridViewPhotoColumn bcol = this.DataGridView.Columns[this.ColumnIndex] as DataGridViewPhotoColumn;
                if(bcol != null && bcol.ColumnInfo != null) {
                    columnInfo = bcol.ColumnInfo;
                    tableInfo = bcol.TableInfo;
                }
                dataBaseDescriptionChecked = true;
            }
        }

       

        /// <summary>
        /// Adds an image as cell value
        /// </summary>
        /// <param name="image"></param>
        /// <param name="imageFormat"></param>
        public void setImage(Image image, ImageFormat imageFormat) {
            if(this.Value is Byte[]) {
                this.Value = DataHelper.ImageToByteArray(image, imageFormat);

            } else if(this.Value is FileInfo) {
                throw new NotImplementedException();

            } else {
                this.Value = image;
            }

        }

        /// <summary>
        /// Adds an image as cell value.
        /// </summary>
        /// <param name="fileInfo"></param>
        public void setImage(FileInfo fileInfo) {
            if(this.Value is Byte[]) {
                this.Value = File.ReadAllBytes(fileInfo.FullName);
            } else if(this.Value is Image) {
                this.Value = Image.FromFile(fileInfo.FullName);
            } else {
                this.Value = fileInfo;
            }
        }

        /// <summary>
        /// Returns the image related to this column.
        /// </summary>
        /// <returns></returns>
        public Image getImage() {

            return this.getImage(this.Value);
        }





        private Image getImage(object value) {
            Image image = null;

            //Try to get image with best resolution
            if(this.ColumnInfo != null) {

                Byte[] imageBLOB = null;

                //Get from main image blob
                if(this.ColumnInfo.mainImageFile != null) {
                    imageBLOB = this.getRowValue<Byte[]>(this.ColumnInfo.Name);
                    //Get from preview image blob
                } else if(this.ColumnInfo.previewImageFile != null) {
                    imageBLOB = getRowValue<Byte[]>(this.ColumnInfo.previewImageFile.Name);
                }

                if(imageBLOB != null) image = Stuff.DataHelper.ByteArrayToImage(imageBLOB);

                //Try to read the cell value directly and interprete it as Image
            } else if(value != null) {
                if(value is Image) {
                    image = (Image)value;

                } else if(value is FileInfo) {

                    FileInfo fi = (FileInfo)value;
                    image = Image.FromFile(fi.FullName);

                } else if(value is byte[]) {

                    image = Stuff.DataHelper.ByteArrayToImage((Byte[])value);

                }
            }
            return image;
        }



        protected override object GetFormattedValue(object value, int rowIndex,
            ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context) {
            String text = String.Empty;
            if(value is byte[]) {

                text = "byte[]";

            }

            if(value is FileInfo) {

                FileInfo fi = (FileInfo)value;
                text = fi.Name;

            }

            return base.GetFormattedValue(text, rowIndex, ref cellStyle, valueTypeConverter, formattedValueTypeConverter, context);

        }

    }
}
