﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;

using SpectationClient.SpectralTools;
using SpectationClient.DataBaseDescription;
using SpectationClient.GUI;
using SpectationClient.Stuff;


namespace SpectationClient.DGVExtensions {
    public class DataGridViewSpectrumCell : DataGridViewExtensionBaseCell, IDatagridViewExtensionBaseCell {

        private String spectrumName = String.Empty;


        protected new ColumnInfoSpectralFile columnInfo = null;
        public new ColumnInfoSpectralFile ColumnInfo {
            get {
                setDatabaseDescriptions();
                return this.columnInfo;
            }
        }

        protected override void setDatabaseDescriptions() {
            if(!dataBaseDescriptionChecked) {
                DataGridViewSpectrumColumn bcol = this.DataGridView.Columns[this.ColumnIndex] as DataGridViewSpectrumColumn;
                if(bcol != null && bcol.ColumnInfo != null) {
                    columnInfo = bcol.ColumnInfo;
                    tableInfo = bcol.TableInfo;
                }
                dataBaseDescriptionChecked = true;
            }
        }

        protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context) {
            String text = String.Empty;
            if(value is Spectrum) {
                text =((Spectrum)value).SpectrumName;

                if(String.IsNullOrWhiteSpace(text)) text = "byte[]";
            }

            if(value is FileInfo) {
                text = ((FileInfo)value).Name;
            }

            if(value is Byte[] && this.ColumnInfo != null) {
                text = "byte[]";
                /*
                ColumnInfoSpectralFile CI = this.ColumnInfoSpectraFile;
                
                if(CI.FilenameColumn != null) {
                    text = getRowValue<String>(rowIndex, CI.FilenameColumn.Name);
                    
                }
                if(String.IsNullOrWhiteSpace(text)) {
                    text = "<byte[]>";
                }*/

            }

            return base.GetFormattedValue(text, rowIndex, ref cellStyle, valueTypeConverter, formattedValueTypeConverter, context);

        }


        public Spectrum getSpectrum() {
            return this.getSpectrum(this.Value);
        }

        private Spectrum getSpectrum(object value) {
            Spectrum spectrum = null;
            try {
                if(value != null) {
                    if(value is Spectrum) {
                        spectrum = (Spectrum)value;
                    } else if(value is FileInfo) {
                        FileInfo fi = (FileInfo)value;
                        if(ASD_Reader.isValidASDSpectrum(fi)) {
                            spectrum = ASD_Reader.readSpectrum(fi);
                        } else if(ESL_Reader.isValidFile(fi)) {

                            throw new NotImplementedException("TODO: Set Spectral File FileInfo to DataGrid");
                        }

                    } else if(value is byte[] && this.ColumnInfo != null) {
                        ColumnInfoSpectralFile CI = this.ColumnInfo;
                        String fileName = CI.FilenameColumn != null ? getRowValue<String>(this.RowIndex, CI.FilenameColumn.Name) : "<spectrum>";
                        Byte[] hdrBlob =  CI.HeaderFileColumn != null ? getRowValue<Byte[]>(this.RowIndex, CI.HeaderFileColumn.Name) : null;
                        Byte[] mainBlob = (Byte[])this.Value;

                        if(ASD_Reader.isValidASDSpectrum(mainBlob)) {
                            spectrum = ASD_Reader.readSpectrum(mainBlob, fileName);
                        } else if(hdrBlob != null) {
                            String hdr = ASCIIEncoding.ASCII.GetString(hdrBlob);
                            ESL_Reader ESLReader = new ESL_Reader(mainBlob, hdrBlob);
                            if(ESLReader.NumberOfBands > 0) spectrum = ESLReader.readSpectrum(0);
                        }

                    }

                }

            } catch(Exception ex) {
                FormHelper.ShowErrorBox(ex);
            }
            return spectrum;
        }

        protected override object GetValue(int rowIndex) {
            return base.GetValue(rowIndex);
        }

        protected override bool SetValue(int rowIndex, object value) {
            //Convert to spectrum

            return base.SetValue(rowIndex, value);
        }

        private T getRowValue<T>(int rowIndex, String columnName) {

            if(rowIndex < 0 || (!this.DataGridView.Columns.Contains(columnName)) || 
                this.DataGridView[columnName, rowIndex].Value == DBNull.Value) return default(T);
            return (T)this.DataGridView[columnName, rowIndex].Value;
        }

        private Object[] getRowValues(String[] columnNames) {
            Object[] values = new object[columnNames.Count()];
            int iR = this.RowIndex;
            for(int i=0; i < columnNames.Count(); i++) {
                values[i] = this.DataGridView.Rows[iR].Cells[columnNames[i]].Value;
            }
            return values;
        }
    }

}
