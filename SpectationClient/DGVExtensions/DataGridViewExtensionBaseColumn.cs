﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;

using SpectationClient.DataBaseDescription;

namespace SpectationClient.DGVExtensions {

    
    

    public abstract class DataGridViewExtensionBaseColumn : DataGridViewTextBoxColumn {

        public abstract TableInfo TableInfo { get; }
        protected IColumnInfo columnInfo = null;
        public virtual IColumnInfo ColumnInfo { get { return this.columnInfo; } }

        /// <summary>
        /// Returns the first selected Cell
        /// </summary>
        /// <returns></returns>
        protected DataGridViewCell getSelectedCell() {
            return (from DataGridViewRow x in this.DataGridView.Rows
                    where x.Cells[this.Index].Selected
                    select x.Cells[this.Index]).First() as DataGridViewCell;
         
        } 
        
        /// <summary>
        /// Returns all selected cells
        /// </summary>
        /// <param name="notEmpty">Set this on true to return selected cells with values only.</param>
        /// <returns></returns>
        protected List<DataGridViewCell> getSelectedCells(bool notEmpty=false){
            
            List<DataGridViewCell> cells;

            if(!notEmpty) {
                cells = (from DataGridViewRow x in this.DataGridView.Rows
                         where x.Cells[this.Index].Selected 
                         select x.Cells[this.Index]).ToList();
            } else {
                cells = (from DataGridViewRow x in this.DataGridView.Rows
                         where x.Cells[this.Index].Selected &&
                               x.Cells[this.Index].Value != null &&
                               x.Cells[this.Index].Value != DBNull.Value
                         select x.Cells[this.Index]).ToList();
            }
            return cells;
        } 
    }
}
