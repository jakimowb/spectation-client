﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;

using SpectationClient.DataBaseDescription;
using SpectationClient.GUI;
using SpectationClient.Stuff;

namespace SpectationClient.DGVExtensions {
    public class DataGridViewByteArrayCell : DataGridViewExtensionBaseCell {
        public byte[] File {
            get { return this.Value as Byte[]; }
            set { this.Value = value as Byte[]; }
        }
        public byte[] FileHeader { get; set; }

        private ColumnInfoFile columnInfoFile;
        public DataGridViewByteArrayCell() {

        }
        public DataGridViewByteArrayCell(ColumnInfoFile columnInfoFile) {
            this.columnInfoFile = columnInfoFile;
        }

        protected override object GetFormattedValue(object value, int rowIndex,
            ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context) {
            String text = String.Empty;
            if(value is byte[]) {

                text = "byte[]";
            }
            return base.GetFormattedValue(text, rowIndex, ref cellStyle, valueTypeConverter, formattedValueTypeConverter, context);

        }
        protected override bool SetValue(int rowIndex, object value) {
            return base.SetValue(rowIndex, value);
        }


    }
}
