﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data;
using Npgsql;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace SpectationClient.Async {

    public delegate void ExecuteNonQueryCompletedEventHandler(object sender, ExecuteNonQueryCompletedEventArgs e);
    
    public class ExecuteNonQueryCompletedEventArgs : AsyncCompletedEventArgs {
        public ExecuteNonQueryCompletedEventArgs(Exception e, bool canceled, object state, int rowsAffected)
            : base(e, canceled, state) {
            //Exception error, bool cancelled, object userState
            this.rowsAffected = rowsAffected;
        }

        
        private int rowsAffected;
        public int RowsAffected { get { return this.rowsAffected; } }

    }

    /// <summary>
    /// Class to execute SQL command asynchronously. 
    /// </summary>
    class AExecuteNonQuery : ABase {
        public event ExecuteNonQueryCompletedEventHandler ExecutionCompleted;
        private SendOrPostCallback onExecutionCompletedDelegate;

        public AExecuteNonQuery() {
            InitializeDelegates();
        }

        new protected virtual void InitializeDelegates() {
            base.InitializeDelegates();
            onExecutionCompletedDelegate = new SendOrPostCallback(RequestCompleted);
        }


        // This method is invoked via the AsyncOperation object,
        // so it is guaranteed to be executed on the correct thread.
        private void RequestCompleted(object operationState) {
            ExecuteNonQueryCompletedEventArgs e = operationState as ExecuteNonQueryCompletedEventArgs;
            OnExecutionCompleted(e);
        }


        protected void OnExecutionCompleted(ExecuteNonQueryCompletedEventArgs e) {
            if(ExecutionCompleted != null) ExecutionCompleted(this, e);
            
        }

        protected override void OnCancelAsync(AsyncOperation asyncOp) {

            GetDataTableCompletedEventArgs e = new GetDataTableCompletedEventArgs(
                                    null, null, true, asyncOp.UserSuppliedState);

            // End the task. The asyncOp object is responsible 
            // for marshaling the call.
            //asyncOp.PostOperationCompleted(onGetDataTableCompletedDelegate, e);
            throw new NotImplementedException();
        }
        

        // This is the method that the underlying, free-threaded 
        // asynchronous behavior will invoke.  This will happen on
        // an arbitrary thread.
        private void CompletionMethod(int rowsAffected, Exception exception, bool canceled, AsyncOperation asyncOp) {
            // If the task was not previously canceled,
            // remove the task from the lifetime collection.
            if(!canceled) {
                lock(userStateToLifetime.SyncRoot) {
                    userStateToLifetime.Remove(asyncOp.UserSuppliedState);
                }
            }

            // Package the results of the operation in a 
            // CalculatePrimeCompletedEventArgs.
            ExecuteNonQueryCompletedEventArgs e = new ExecuteNonQueryCompletedEventArgs(exception, canceled, asyncOp.UserSuppliedState, rowsAffected);

            // End the task. The asyncOp object is responsible 
            // for marshaling the call.
            asyncOp.PostOperationCompleted(onExecutionCompletedDelegate, e);

  
            // Note that after the call to OperationCompleted, 
            // asyncOp is no longer usable, and any attempt to use it
            // will cause an exception to be thrown.
            
        }


        // This method performs the actual work
        // It is executed on the worker thread.
        private void ExecuteCommandWorker(
            NpgsqlCommand cmd,
            AsyncOperation asyncOp) {
            Exception e = null;

            // Check that the task is still active.
            // The operation may have been canceled before
            // the thread was scheduled.
            int rowsAffected = 0;
            if(!TaskCanceled(asyncOp.UserSuppliedState)) {
                int trial = 0;
                Exception exception = null;

                //Ensure an individual connection
                cmd.Connection = cmd.Connection.Clone();
                
                bool close = cmd.Connection.State == ConnectionState.Closed;
                do {
                    try {
                        rowsAffected = cmd.ExecuteNonQuery();
                    } catch(Exception ex) {
                        exception = ex;
                        trial++;
                    } finally {
                        cmd.Connection.ClearPool();
                        cmd.Connection.Close();
                    }
                } while(exception != null && trial < 5);

                
                if(exception != null) {
                    
                   // String s = "";
                }
            }

            this.CompletionMethod(rowsAffected, e, TaskCanceled(asyncOp.UserSuppliedState), asyncOp);

            //completionMethodDelegate(calcState);
        }

        // This method starts an asynchronous calculation. 
        // First, it checks the supplied task ID for uniqueness.
        // If taskId is unique, it creates a new WorkerEventHandler 
        // and calls its BeginInvoke method to start the calculation.

        public virtual void GetDataTableAsync(NpgsqlCommand cmd) {
            this.GetDataTableAsync(cmd, cmd.CommandText);
        }

        /// <summary>
        /// Starts to get a data table anynchronously.
        /// </summary>
        /// <param name="cmd">SQL command</param>
        /// <param name="taskId">ID of SQL command</param>
        public virtual void GetDataTableAsync(NpgsqlCommand cmd, object taskId) {
            // Create an AsyncOperation for taskId.
            AsyncOperation asyncOp = AsyncOperationManager.CreateOperation(taskId);

            // Multiple threads will access the task dictionary,
            // so it must be locked to serialize access.
            lock(userStateToLifetime.SyncRoot) {
                if(userStateToLifetime.Contains(taskId)) {
                    throw new ArgumentException(
                        "Task ID parameter must be unique",
                        "taskId");
                }
                userStateToLifetime[taskId] = asyncOp;
            }

            // Start the asynchronous operation.
            WorkerEventHandler workerDelegate = new WorkerEventHandler(ExecuteCommandWorker);
            workerDelegate.BeginInvoke(cmd, asyncOp, null, null);
        }


       
      
    }
}
