﻿namespace SpectationClient.GUI {
    partial class SubSelectCoordinateSystem {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb_GEOZ = new System.Windows.Forms.RadioButton();
            this.rb_GEOD = new System.Windows.Forms.RadioButton();
            this.rb_GEOGR = new System.Windows.Forms.RadioButton();
            this.rb_ALL = new System.Windows.Forms.RadioButton();
            this.DGV = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bt_OK = new System.Windows.Forms.Button();
            this.bt_Cancel = new System.Windows.Forms.Button();
            this.gb_KEYWORD = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gb_KEYWORD_tb = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gb_KEYWORD.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_GEOZ);
            this.groupBox1.Controls.Add(this.rb_GEOD);
            this.groupBox1.Controls.Add(this.rb_GEOGR);
            this.groupBox1.Controls.Add(this.rb_ALL);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 197);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 85);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Systeme";
            // 
            // rb_GEOZ
            // 
            this.rb_GEOZ.AutoSize = true;
            this.rb_GEOZ.Location = new System.Drawing.Point(55, 62);
            this.rb_GEOZ.Name = "rb_GEOZ";
            this.rb_GEOZ.Size = new System.Drawing.Size(93, 17);
            this.rb_GEOZ.TabIndex = 3;
            this.rb_GEOZ.Text = "Geozentrische";
            this.rb_GEOZ.UseVisualStyleBackColor = true;
            // 
            // rb_GEOD
            // 
            this.rb_GEOD.AutoSize = true;
            this.rb_GEOD.Location = new System.Drawing.Point(55, 41);
            this.rb_GEOD.Name = "rb_GEOD";
            this.rb_GEOD.Size = new System.Drawing.Size(85, 17);
            this.rb_GEOD.TabIndex = 2;
            this.rb_GEOD.Text = "Geodätische";
            this.rb_GEOD.UseVisualStyleBackColor = true;
            // 
            // rb_GEOGR
            // 
            this.rb_GEOGR.AutoSize = true;
            this.rb_GEOGR.Location = new System.Drawing.Point(55, 20);
            this.rb_GEOGR.Name = "rb_GEOGR";
            this.rb_GEOGR.Size = new System.Drawing.Size(97, 17);
            this.rb_GEOGR.TabIndex = 1;
            this.rb_GEOGR.Text = "Geographische";
            this.rb_GEOGR.UseVisualStyleBackColor = true;
            // 
            // rb_ALL
            // 
            this.rb_ALL.AutoSize = true;
            this.rb_ALL.Checked = true;
            this.rb_ALL.Location = new System.Drawing.Point(7, 20);
            this.rb_ALL.Name = "rb_ALL";
            this.rb_ALL.Size = new System.Drawing.Size(42, 17);
            this.rb_ALL.TabIndex = 0;
            this.rb_ALL.TabStop = true;
            this.rb_ALL.Text = "Alle";
            this.rb_ALL.UseVisualStyleBackColor = true;
            // 
            // DGV
            // 
            this.DGV.AllowUserToAddRows = false;
            this.DGV.AllowUserToDeleteRows = false;
            this.DGV.AllowUserToResizeRows = false;
            this.DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel1.SetColumnSpan(this.DGV, 3);
            this.DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DGV.Location = new System.Drawing.Point(3, 3);
            this.DGV.MultiSelect = false;
            this.DGV.Name = "DGV";
            this.DGV.ReadOnly = true;
            this.DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV.Size = new System.Drawing.Size(505, 188);
            this.DGV.TabIndex = 0;
            this.DGV.SelectionChanged += new System.EventHandler(this.DGV_SelectionChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 175F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.DGV, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gb_KEYWORD, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(519, 285);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.bt_OK);
            this.panel1.Controls.Add(this.bt_Cancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(433, 197);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(83, 85);
            this.panel1.TabIndex = 3;
            // 
            // bt_OK
            // 
            this.bt_OK.Location = new System.Drawing.Point(3, 3);
            this.bt_OK.Name = "bt_OK";
            this.bt_OK.Size = new System.Drawing.Size(75, 23);
            this.bt_OK.TabIndex = 1;
            this.bt_OK.Text = "OK";
            this.bt_OK.UseVisualStyleBackColor = true;
            this.bt_OK.Click += new System.EventHandler(this.bt_OK_Click);
            // 
            // bt_Cancel
            // 
            this.bt_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bt_Cancel.Location = new System.Drawing.Point(3, 54);
            this.bt_Cancel.Name = "bt_Cancel";
            this.bt_Cancel.Size = new System.Drawing.Size(75, 23);
            this.bt_Cancel.TabIndex = 2;
            this.bt_Cancel.Text = "Abbruch";
            this.bt_Cancel.UseVisualStyleBackColor = true;
            // 
            // gb_KEYWORD
            // 
            this.gb_KEYWORD.Controls.Add(this.label1);
            this.gb_KEYWORD.Controls.Add(this.gb_KEYWORD_tb);
            this.gb_KEYWORD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_KEYWORD.Location = new System.Drawing.Point(178, 197);
            this.gb_KEYWORD.Name = "gb_KEYWORD";
            this.gb_KEYWORD.Size = new System.Drawing.Size(249, 85);
            this.gb_KEYWORD.TabIndex = 4;
            this.gb_KEYWORD.TabStop = false;
            this.gb_KEYWORD.Text = "Stichwort für Spalte NAME";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "z.B. \"UTM\"";
            // 
            // gb_KEYWORD_tb
            // 
            this.gb_KEYWORD_tb.Location = new System.Drawing.Point(7, 20);
            this.gb_KEYWORD_tb.Name = "gb_KEYWORD_tb";
            this.gb_KEYWORD_tb.Size = new System.Drawing.Size(236, 20);
            this.gb_KEYWORD_tb.TabIndex = 0;
            // 
            // SubSelectCoordinateSystem
            // 
            this.AcceptButton = this.bt_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bt_Cancel;
            this.ClientSize = new System.Drawing.Size(519, 285);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SubSelectCoordinateSystem";
            this.Text = "Koordinatensystem auswählen";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.gb_KEYWORD.ResumeLayout(false);
            this.gb_KEYWORD.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rb_GEOZ;
        private System.Windows.Forms.RadioButton rb_GEOD;
        private System.Windows.Forms.RadioButton rb_GEOGR;
        private System.Windows.Forms.RadioButton rb_ALL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView DGV;
        private System.Windows.Forms.Button bt_OK;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bt_Cancel;
        private System.Windows.Forms.GroupBox gb_KEYWORD;
        private System.Windows.Forms.TextBox gb_KEYWORD_tb;
        private System.Windows.Forms.Label label1;
    }
}