﻿namespace SpectationClient.GUI {
    partial class SubInsertMessprotokollGFZ {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.TT = new System.Windows.Forms.ToolTip(this.components);
            this.Veg_S_BB_btAdd = new System.Windows.Forms.Button();
            this.Veg_S_EUNIS_btAdd = new System.Windows.Forms.Button();
            this.Sensor_readSensor = new System.Windows.Forms.Button();
            this.AREA_ID_DB_tb = new System.Windows.Forms.TextBox();
            this.AREA_ID_GFZ_tb = new System.Windows.Forms.TextBox();
            this.gb_POS_newlocation_rb = new System.Windows.Forms.RadioButton();
            this.gb_POS_OBSAREA_rb = new System.Windows.Forms.RadioButton();
            this.Veg_S_BfNCode_btAdd = new System.Windows.Forms.Button();
            this.gb_COND_VEGETATION_cb = new System.Windows.Forms.CheckBox();
            this.Soil_Date = new System.Windows.Forms.DateTimePicker();
            this.gb_VEG_SP_btAdd = new System.Windows.Forms.Button();
            this.gb_VEG_SP_btRemove = new System.Windows.Forms.Button();
            this.Photos_btRemovePhotos = new System.Windows.Forms.Button();
            this.gb_SPECTRA_OBJ_btRemove = new System.Windows.Forms.Button();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.xlsEinlesenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eingabenZurücksetzenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gb_VEG = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.VEG_controlpanel = new System.Windows.Forms.Panel();
            this.gb_VEG_SP_cb = new System.Windows.Forms.CheckBox();
            this.gb_VEG_PS_cb = new System.Windows.Forms.CheckBox();
            this.gb_VEG_PS = new System.Windows.Forms.GroupBox();
            this.Veg_S_BfNCode_btRemove = new System.Windows.Forms.Button();
            this.Veg_S_EUNIS_btRemove = new System.Windows.Forms.Button();
            this.Veg_S_BB_btRemove = new System.Windows.Forms.Button();
            this.Veg_S_BfNCode_tb = new System.Windows.Forms.TextBox();
            this.Veg_S_gpSpecies = new System.Windows.Forms.GroupBox();
            this.Veg_S_NOS_Crypto = new System.Windows.Forms.TextBox();
            this.Veg_S_NOS_Herbs = new System.Windows.Forms.TextBox();
            this.Veg_S_NOS_Shrubs = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.Veg_S_NOS_Trees2 = new System.Windows.Forms.TextBox();
            this.Veg_S_NOS_Trees1 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.Veg_S_NOS_Total = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.Veg_S_Notes = new System.Windows.Forms.TextBox();
            this.Veg_S_gpCover = new System.Windows.Forms.GroupBox();
            this.Veg_S_C_Crypto = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.Veg_S_C_Herbs = new System.Windows.Forms.TextBox();
            this.Veg_S_C_oSoil = new System.Windows.Forms.TextBox();
            this.Veg_S_C_Shrubs = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.Veg_S_C_Litter = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.Veg_S_C_Total = new System.Windows.Forms.TextBox();
            this.Veg_S_C_Trees2 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.Veg_S_C_Trees1 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.Veg_S_gpHeights = new System.Windows.Forms.GroupBox();
            this.Veg_S_H_Herbs = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.Veg_S_H_Shrubs = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.Veg_S_H_Trees2 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.Veg_S_H_Trees1 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.Veg_S_EUNIS_tb = new System.Windows.Forms.TextBox();
            this.Veg_S_BB_tb = new System.Windows.Forms.TextBox();
            this.gb_VEG_SP = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Veg_P_DGV = new System.Windows.Forms.DataGridView();
            this.SPl_Col_BtSpeciesID = new System.Windows.Forms.DataGridViewButtonColumn();
            this.SPl_Col_Species = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SPl_Col_Cover = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.SPl_Col_Sociability = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.SPl_Col_PhenDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SPl_Col_PhenPhase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SPl_Col_Notes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_SPECTRA = new System.Windows.Forms.GroupBox();
            this.gb_SPECTRA_WREF = new System.Windows.Forms.GroupBox();
            this.MP_WREF_btRemoveSpectra = new System.Windows.Forms.Button();
            this.MP_WREF_DGV = new System.Windows.Forms.DataGridView();
            this.WR_Files = new DGVExtensions.DataGridViewSpectrumColumn();
            this.WR_Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MP_WREF_btReadSpectra = new System.Windows.Forms.Button();
            this.gb_SPECTRA_OBJ = new System.Windows.Forms.GroupBox();
            this.gb_SPECTRA_OBJ_btAdd = new System.Windows.Forms.Button();
            this.MP_SPECTRA_DGV = new System.Windows.Forms.DataGridView();
            this.SPEC_Field = new DGVExtensions.DataGridViewSpectrumColumn();
            this.SPEC_Jump = new DGVExtensions.DataGridViewSpectrumColumn();
            this.SPEC_HyMap = new DGVExtensions.DataGridViewSpectrumColumn();
            this.SPEC_Notes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_TITLE = new System.Windows.Forms.GroupBox();
            this.gb_POS = new System.Windows.Forms.GroupBox();
            this.POS_flp = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gb_POS_none_rb = new System.Windows.Forms.RadioButton();
            this.gb_POS_OBSAREA = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AREA_ID_INFO_tb = new System.Windows.Forms.TextBox();
            this.AREA_ID_bt = new System.Windows.Forms.Button();
            this.gb_POS_newlocation = new System.Windows.Forms.GroupBox();
            this.Loc_Coord = new System.Windows.Forms.Panel();
            this.Loc_Notes = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Loc_Aspect = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Loc_Locname = new System.Windows.Forms.TextBox();
            this.Loc_Slope = new System.Windows.Forms.TextBox();
            this.gb_TITLE_COMMON = new System.Windows.Forms.GroupBox();
            this.label65 = new System.Windows.Forms.Label();
            this.Spec_Source = new System.Windows.Forms.ComboBox();
            this.label79 = new System.Windows.Forms.Label();
            this.Ti_notes = new System.Windows.Forms.TextBox();
            this.Ti_OperatorINSERT = new System.Windows.Forms.ComboBox();
            this.Time_TimeEnd = new System.Windows.Forms.DateTimePicker();
            this.Time_TimeStart = new System.Windows.Forms.DateTimePicker();
            this.Ti_Campaign_cb = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Ti_Institute_tb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Time_Date = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.gb_INSTRUMENTS = new System.Windows.Forms.GroupBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.Spec_Optic = new System.Windows.Forms.TextBox();
            this.Sensor_type = new System.Windows.Forms.ComboBox();
            this.Spec_operator = new System.Windows.Forms.ComboBox();
            this.WRef_Panel = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.Photo_Camera = new System.Windows.Forms.ComboBox();
            this.Photo_Operator = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.gb_SITUATION = new System.Windows.Forms.GroupBox();
            this.flp_Aufnahmesituation = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gb_COND_ILLUM_cb = new System.Windows.Forms.CheckBox();
            this.gb_COND_SOIL_cb = new System.Windows.Forms.CheckBox();
            this.gb_COND_FOTOS_cb = new System.Windows.Forms.CheckBox();
            this.gb_COND_SENSOR_cb = new System.Windows.Forms.CheckBox();
            this.gb_COND_WEATHER_cb = new System.Windows.Forms.CheckBox();
            this.gb_COND_WEATHER = new System.Windows.Forms.GroupBox();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.Weather_LastPrecip = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.label50 = new System.Windows.Forms.Label();
            this.Weather_W_v = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.Weather_W_dir = new System.Windows.Forms.TextBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.labelWeather_Cloudiness = new System.Windows.Forms.Label();
            this.Weather_CloudDensity = new System.Windows.Forms.ComboBox();
            this.labelWeather_CloudType = new System.Windows.Forms.Label();
            this.Weather_CloudType = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Weather_A_H = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.Weather_A_Pa = new System.Windows.Forms.TextBox();
            this.Weather_A_T = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.Weather_notes = new System.Windows.Forms.TextBox();
            this.gb_COND_SENSOR = new System.Windows.Forms.GroupBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.Sens_Azimuth = new System.Windows.Forms.TextBox();
            this.Sens_Zenith = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.Sens_Height_AOL = new System.Windows.Forms.TextBox();
            this.Sens_Height_AGL = new System.Windows.Forms.TextBox();
            this.gb_COND_FOTOS = new System.Windows.Forms.GroupBox();
            this.Photos_PreviewBox = new System.Windows.Forms.PictureBox();
            this.Photos_btAddPhotos = new System.Windows.Forms.Button();
            this.Photos_DGV = new System.Windows.Forms.DataGridView();
            this.Photos_File = new DGVExtensions.DataGridViewPhotoColumn();
            this.Photos_Type = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Photos_Notes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_COND_SOIL = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Soil_GE_Bodenform = new System.Windows.Forms.TextBox();
            this.Soil_GE_Bezeichnung = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.Soil_WRB_Description = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Soil_WRB_Specifier = new System.Windows.Forms.ComboBox();
            this.Soil_WRB_Qualifier = new System.Windows.Forms.ComboBox();
            this.Soil_WRB_SoilGroup = new System.Windows.Forms.ComboBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.Soil_moisturetext = new System.Windows.Forms.ComboBox();
            this.Soil_colortext = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Soil_munsell_chroma = new System.Windows.Forms.TextBox();
            this.Soil_munsell_value = new System.Windows.Forms.TextBox();
            this.Soil_munsell_hue = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.Soil_notes = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.gb_COND_ILLUM = new System.Windows.Forms.GroupBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.Illum_notesIllum = new System.Windows.Forms.TextBox();
            this.Illum_notes = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.Illum_azimuth = new System.Windows.Forms.TextBox();
            this.Illum_zenith = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.Illum_illumn = new System.Windows.Forms.TextBox();
            this.Illum_Source = new System.Windows.Forms.TextBox();
            this.flp_MAIN = new System.Windows.Forms.FlowLayoutPanel();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.Fortschritt = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSL_ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.TSL_Action = new System.Windows.Forms.ToolStripStatusLabel();
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bt_INSERT = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RTB = new System.Windows.Forms.RichTextBox();
            this.IP = new System.Windows.Forms.ErrorProvider(this.components);
            this.contextMenuSpectra = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmi_ShowSpectra = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_RemoveSpectra = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStripWRSpectra = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiWRShowSpectra = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiWRRemoveSpectra = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewSpectrumColumn1 = new DGVExtensions.DataGridViewSpectrumColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewSpectrumColumn2 = new DGVExtensions.DataGridViewSpectrumColumn();
            this.dataGridViewSpectrumColumn3 = new DGVExtensions.DataGridViewSpectrumColumn();
            this.dataGridViewSpectrumColumn4 = new DGVExtensions.DataGridViewSpectrumColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewPhotoColumn1 = new DGVExtensions.DataGridViewPhotoColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.menuStrip2.SuspendLayout();
            this.gb_VEG.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.VEG_controlpanel.SuspendLayout();
            this.gb_VEG_PS.SuspendLayout();
            this.Veg_S_gpSpecies.SuspendLayout();
            this.Veg_S_gpCover.SuspendLayout();
            this.Veg_S_gpHeights.SuspendLayout();
            this.gb_VEG_SP.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Veg_P_DGV)).BeginInit();
            this.gb_SPECTRA.SuspendLayout();
            this.gb_SPECTRA_WREF.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MP_WREF_DGV)).BeginInit();
            this.gb_SPECTRA_OBJ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MP_SPECTRA_DGV)).BeginInit();
            this.gb_TITLE.SuspendLayout();
            this.gb_POS.SuspendLayout();
            this.POS_flp.SuspendLayout();
            this.panel3.SuspendLayout();
            this.gb_POS_OBSAREA.SuspendLayout();
            this.gb_POS_newlocation.SuspendLayout();
            this.gb_TITLE_COMMON.SuspendLayout();
            this.gb_INSTRUMENTS.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.gb_SITUATION.SuspendLayout();
            this.flp_Aufnahmesituation.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gb_COND_WEATHER.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.gb_COND_SENSOR.SuspendLayout();
            this.gb_COND_FOTOS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Photos_PreviewBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Photos_DGV)).BeginInit();
            this.gb_COND_SOIL.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.gb_COND_ILLUM.SuspendLayout();
            this.flp_MAIN.SuspendLayout();
            this.StatusStrip.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IP)).BeginInit();
            this.contextMenuSpectra.SuspendLayout();
            this.contextMenuStripWRSpectra.SuspendLayout();
            this.SuspendLayout();
            // 
            // EP
            // 
            this.EP.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.EP.ContainerControl = this;
            // 
            // Veg_S_BB_btAdd
            // 
            this.Veg_S_BB_btAdd.Location = new System.Drawing.Point(9, 20);
            this.Veg_S_BB_btAdd.Name = "Veg_S_BB_btAdd";
            this.Veg_S_BB_btAdd.Size = new System.Drawing.Size(99, 22);
            this.Veg_S_BB_btAdd.TabIndex = 0;
            this.Veg_S_BB_btAdd.Text = "Biotoptyp Brdbg.";
            this.TT.SetToolTip(this.Veg_S_BB_btAdd, "Ruft Auswahlliste aus Brandenburger Biotopliste auf");
            this.Veg_S_BB_btAdd.UseVisualStyleBackColor = true;
            this.Veg_S_BB_btAdd.Click += new System.EventHandler(this.Veg_S_BB_Click);
            // 
            // Veg_S_EUNIS_btAdd
            // 
            this.Veg_S_EUNIS_btAdd.Location = new System.Drawing.Point(9, 48);
            this.Veg_S_EUNIS_btAdd.Name = "Veg_S_EUNIS_btAdd";
            this.Veg_S_EUNIS_btAdd.Size = new System.Drawing.Size(99, 22);
            this.Veg_S_EUNIS_btAdd.TabIndex = 1;
            this.Veg_S_EUNIS_btAdd.Text = "EUNIS Habitat";
            this.TT.SetToolTip(this.Veg_S_EUNIS_btAdd, "Ruf Auswahlliste aus der EUNIS Habitatliste");
            this.Veg_S_EUNIS_btAdd.UseVisualStyleBackColor = true;
            this.Veg_S_EUNIS_btAdd.Click += new System.EventHandler(this.Veg_S_EUNIS_Click);
            // 
            // Sensor_readSensor
            // 
            this.Sensor_readSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sensor_readSensor.Location = new System.Drawing.Point(340, 17);
            this.Sensor_readSensor.Name = "Sensor_readSensor";
            this.Sensor_readSensor.Size = new System.Drawing.Size(67, 47);
            this.Sensor_readSensor.TabIndex = 4;
            this.Sensor_readSensor.Text = "Sensor auslesen...";
            this.TT.SetToolTip(this.Sensor_readSensor, "Wertet eine ASD-Spektraldatei der Messungen aus um den verwendeten Sensor und sei" +
        "ne Optik automatisch zu bestimmen ");
            this.Sensor_readSensor.UseVisualStyleBackColor = true;
            this.Sensor_readSensor.Click += new System.EventHandler(this.Sensor_readSensor_Click);
            // 
            // AREA_ID_DB_tb
            // 
            this.AREA_ID_DB_tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AREA_ID_DB_tb.Location = new System.Drawing.Point(31, 21);
            this.AREA_ID_DB_tb.Name = "AREA_ID_DB_tb";
            this.AREA_ID_DB_tb.Size = new System.Drawing.Size(100, 20);
            this.AREA_ID_DB_tb.TabIndex = 1;
            this.TT.SetToolTip(this.AREA_ID_DB_tb, "Datenbank ID der jeweiligen Beobachtungsfläche (angegeben in \"C_GFZ\".\"OBSERVATION" +
        "_AREAS\" Spalte \"id\")");
            // 
            // AREA_ID_GFZ_tb
            // 
            this.AREA_ID_GFZ_tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AREA_ID_GFZ_tb.Location = new System.Drawing.Point(234, 20);
            this.AREA_ID_GFZ_tb.Name = "AREA_ID_GFZ_tb";
            this.AREA_ID_GFZ_tb.Size = new System.Drawing.Size(100, 20);
            this.AREA_ID_GFZ_tb.TabIndex = 4;
            this.TT.SetToolTip(this.AREA_ID_GFZ_tb, "GFZ ID/Kurzbezeichnung der jeweiligen Beobachtungsfläche (angegeben in \"C_GFZ\".\"O" +
        "BSERVATION_AREAS\" Spalte \"gfz_area_id\")");
            // 
            // gb_POS_newlocation_rb
            // 
            this.gb_POS_newlocation_rb.AutoSize = true;
            this.gb_POS_newlocation_rb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_POS_newlocation_rb.Location = new System.Drawing.Point(258, 3);
            this.gb_POS_newlocation_rb.Name = "gb_POS_newlocation_rb";
            this.gb_POS_newlocation_rb.Size = new System.Drawing.Size(259, 17);
            this.gb_POS_newlocation_rb.TabIndex = 2;
            this.gb_POS_newlocation_rb.TabStop = true;
            this.gb_POS_newlocation_rb.Text = "neuer Standort (keine Dauerbeobachtungsfläche)";
            this.TT.SetToolTip(this.gb_POS_newlocation_rb, "Geben Sie eine Koordinate an die nicht als GFZ Beobachtungsfläche beschrieben ist" +
        ".");
            this.gb_POS_newlocation_rb.UseVisualStyleBackColor = true;
            // 
            // gb_POS_OBSAREA_rb
            // 
            this.gb_POS_OBSAREA_rb.AutoSize = true;
            this.gb_POS_OBSAREA_rb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_POS_OBSAREA_rb.Location = new System.Drawing.Point(105, 3);
            this.gb_POS_OBSAREA_rb.Name = "gb_POS_OBSAREA_rb";
            this.gb_POS_OBSAREA_rb.Size = new System.Drawing.Size(147, 17);
            this.gb_POS_OBSAREA_rb.TabIndex = 1;
            this.gb_POS_OBSAREA_rb.TabStop = true;
            this.gb_POS_OBSAREA_rb.Text = "GFZ Beobachtungsfläche";
            this.TT.SetToolTip(this.gb_POS_OBSAREA_rb, "Geben Sie eine GFZ Beobachtungsfläche an.");
            this.gb_POS_OBSAREA_rb.UseVisualStyleBackColor = true;
            // 
            // Veg_S_BfNCode_btAdd
            // 
            this.Veg_S_BfNCode_btAdd.Location = new System.Drawing.Point(9, 77);
            this.Veg_S_BfNCode_btAdd.Name = "Veg_S_BfNCode_btAdd";
            this.Veg_S_BfNCode_btAdd.Size = new System.Drawing.Size(99, 22);
            this.Veg_S_BfNCode_btAdd.TabIndex = 57;
            this.Veg_S_BfNCode_btAdd.Text = "BfN Code";
            this.TT.SetToolTip(this.Veg_S_BfNCode_btAdd, "Ruf Auswahlliste aus der EUNIS Habitatliste");
            this.Veg_S_BfNCode_btAdd.UseVisualStyleBackColor = true;
            this.Veg_S_BfNCode_btAdd.Click += new System.EventHandler(this.Veg_S_BfNCode_bt_Click);
            // 
            // gb_COND_VEGETATION_cb
            // 
            this.gb_COND_VEGETATION_cb.AutoSize = true;
            this.gb_COND_VEGETATION_cb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_VEGETATION_cb.Location = new System.Drawing.Point(345, 26);
            this.gb_COND_VEGETATION_cb.Name = "gb_COND_VEGETATION_cb";
            this.gb_COND_VEGETATION_cb.Size = new System.Drawing.Size(77, 17);
            this.gb_COND_VEGETATION_cb.TabIndex = 5;
            this.gb_COND_VEGETATION_cb.Text = "Vegetation";
            this.TT.SetToolTip(this.gb_COND_VEGETATION_cb, "Angaben zur Vegetation.");
            this.gb_COND_VEGETATION_cb.UseVisualStyleBackColor = true;
            // 
            // Soil_Date
            // 
            this.Soil_Date.Checked = false;
            this.Soil_Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Soil_Date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Soil_Date.Location = new System.Drawing.Point(79, 141);
            this.Soil_Date.Name = "Soil_Date";
            this.Soil_Date.ShowCheckBox = true;
            this.Soil_Date.Size = new System.Drawing.Size(126, 20);
            this.Soil_Date.TabIndex = 26;
            this.TT.SetToolTip(this.Soil_Date, "Tag der Aufnahme der Bodendaten, falls abweichend vom Tag der Spektrenaufnahme");
            // 
            // gb_VEG_SP_btAdd
            // 
            this.gb_VEG_SP_btAdd.Location = new System.Drawing.Point(3, 3);
            this.gb_VEG_SP_btAdd.Name = "gb_VEG_SP_btAdd";
            this.gb_VEG_SP_btAdd.Size = new System.Drawing.Size(86, 23);
            this.gb_VEG_SP_btAdd.TabIndex = 0;
            this.gb_VEG_SP_btAdd.Text = "Hinzufügen...";
            this.TT.SetToolTip(this.gb_VEG_SP_btAdd, "Pflanzenarten hinzufügen");
            this.gb_VEG_SP_btAdd.UseVisualStyleBackColor = true;
            this.gb_VEG_SP_btAdd.Click += new System.EventHandler(this.gb_VEG_SP_btAdd_Click);
            // 
            // gb_VEG_SP_btRemove
            // 
            this.gb_VEG_SP_btRemove.Location = new System.Drawing.Point(95, 3);
            this.gb_VEG_SP_btRemove.Name = "gb_VEG_SP_btRemove";
            this.gb_VEG_SP_btRemove.Size = new System.Drawing.Size(75, 23);
            this.gb_VEG_SP_btRemove.TabIndex = 1;
            this.gb_VEG_SP_btRemove.Text = "Entfernen";
            this.TT.SetToolTip(this.gb_VEG_SP_btRemove, "Entfernt die ausgewählten Zeilen.");
            this.gb_VEG_SP_btRemove.UseVisualStyleBackColor = true;
            this.gb_VEG_SP_btRemove.Click += new System.EventHandler(this.gb_VEG_SP_btRemove_Click);
            // 
            // Photos_btRemovePhotos
            // 
            this.Photos_btRemovePhotos.Location = new System.Drawing.Point(10, 50);
            this.Photos_btRemovePhotos.Name = "Photos_btRemovePhotos";
            this.Photos_btRemovePhotos.Size = new System.Drawing.Size(87, 23);
            this.Photos_btRemovePhotos.TabIndex = 3;
            this.Photos_btRemovePhotos.Text = "Entfernen";
            this.TT.SetToolTip(this.Photos_btRemovePhotos, "Entfernt ausgewählte Fotos");
            this.Photos_btRemovePhotos.UseVisualStyleBackColor = true;
            this.Photos_btRemovePhotos.Click += new System.EventHandler(this.Photos_btRemovePhotos_Click);
            // 
            // gb_SPECTRA_OBJ_btRemove
            // 
            this.gb_SPECTRA_OBJ_btRemove.Enabled = false;
            this.gb_SPECTRA_OBJ_btRemove.Location = new System.Drawing.Point(96, 19);
            this.gb_SPECTRA_OBJ_btRemove.Name = "gb_SPECTRA_OBJ_btRemove";
            this.gb_SPECTRA_OBJ_btRemove.Size = new System.Drawing.Size(79, 23);
            this.gb_SPECTRA_OBJ_btRemove.TabIndex = 15;
            this.gb_SPECTRA_OBJ_btRemove.Text = "Entfernen";
            this.TT.SetToolTip(this.gb_SPECTRA_OBJ_btRemove, "Entfernt die ausgewählten Feldspektren und davon abgeleitete Spektren");
            this.gb_SPECTRA_OBJ_btRemove.UseVisualStyleBackColor = true;
            this.gb_SPECTRA_OBJ_btRemove.Click += new System.EventHandler(this.MP_OS_btRemoveRows_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xlsEinlesenToolStripMenuItem,
            this.eingabenZurücksetzenToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(819, 24);
            this.menuStrip2.TabIndex = 2;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // xlsEinlesenToolStripMenuItem
            // 
            this.xlsEinlesenToolStripMenuItem.Name = "xlsEinlesenToolStripMenuItem";
            this.xlsEinlesenToolStripMenuItem.Size = new System.Drawing.Size(178, 20);
            this.xlsEinlesenToolStripMenuItem.Text = "Werte aus Excel-Datei einlesen";
            this.xlsEinlesenToolStripMenuItem.ToolTipText = "Bietet die Möglichkeit die Daten den Blättern einer Exceldatei (Messprotokoll GFZ" +
    ") auszulesen.\result\nAchtung: Dateinamen (Spektraldateien, Fotos) müssen \"per Han" +
    "d\" eingegeben werden.";
            this.xlsEinlesenToolStripMenuItem.Click += new System.EventHandler(this.xlsEinlesenToolStripMenuItem_Click);
            // 
            // eingabenZurücksetzenToolStripMenuItem
            // 
            this.eingabenZurücksetzenToolStripMenuItem.Name = "eingabenZurücksetzenToolStripMenuItem";
            this.eingabenZurücksetzenToolStripMenuItem.Size = new System.Drawing.Size(139, 20);
            this.eingabenZurücksetzenToolStripMenuItem.Text = "Eingaben zurücksetzen";
            this.eingabenZurücksetzenToolStripMenuItem.ToolTipText = "Entfernt alle Eingaben";
            this.eingabenZurücksetzenToolStripMenuItem.Click += new System.EventHandler(this.eingabenZurücksetzenToolStripMenuItem_Click);
            // 
            // gb_VEG
            // 
            this.gb_VEG.AutoSize = true;
            this.gb_VEG.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_VEG.Controls.Add(this.flowLayoutPanel2);
            this.gb_VEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_VEG.Location = new System.Drawing.Point(3, 920);
            this.gb_VEG.Name = "gb_VEG";
            this.gb_VEG.Size = new System.Drawing.Size(751, 580);
            this.gb_VEG.TabIndex = 2;
            this.gb_VEG.TabStop = false;
            this.gb_VEG.Text = "Vegetation";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.Controls.Add(this.VEG_controlpanel);
            this.flowLayoutPanel2.Controls.Add(this.gb_VEG_PS);
            this.flowLayoutPanel2.Controls.Add(this.gb_VEG_SP);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(745, 561);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // VEG_controlpanel
            // 
            this.VEG_controlpanel.BackColor = System.Drawing.SystemColors.Control;
            this.VEG_controlpanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.VEG_controlpanel.Controls.Add(this.gb_VEG_SP_cb);
            this.VEG_controlpanel.Controls.Add(this.gb_VEG_PS_cb);
            this.VEG_controlpanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VEG_controlpanel.Location = new System.Drawing.Point(3, 3);
            this.VEG_controlpanel.Name = "VEG_controlpanel";
            this.VEG_controlpanel.Size = new System.Drawing.Size(739, 34);
            this.VEG_controlpanel.TabIndex = 0;
            // 
            // gb_VEG_SP_cb
            // 
            this.gb_VEG_SP_cb.AutoSize = true;
            this.gb_VEG_SP_cb.Location = new System.Drawing.Point(150, 4);
            this.gb_VEG_SP_cb.Name = "gb_VEG_SP_cb";
            this.gb_VEG_SP_cb.Size = new System.Drawing.Size(170, 17);
            this.gb_VEG_SP_cb.TabIndex = 1;
            this.gb_VEG_SP_cb.Text = "Einzelpflanzenbeschreibungen";
            this.gb_VEG_SP_cb.UseVisualStyleBackColor = true;
            // 
            // gb_VEG_PS_cb
            // 
            this.gb_VEG_PS_cb.AutoSize = true;
            this.gb_VEG_PS_cb.Checked = true;
            this.gb_VEG_PS_cb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gb_VEG_PS_cb.Location = new System.Drawing.Point(9, 4);
            this.gb_VEG_PS_cb.Name = "gb_VEG_PS_cb";
            this.gb_VEG_PS_cb.Size = new System.Drawing.Size(135, 17);
            this.gb_VEG_PS_cb.TabIndex = 0;
            this.gb_VEG_PS_cb.Text = "Pflanzengesellschaften";
            this.gb_VEG_PS_cb.UseVisualStyleBackColor = true;
            // 
            // gb_VEG_PS
            // 
            this.gb_VEG_PS.Controls.Add(this.Veg_S_BfNCode_btRemove);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_EUNIS_btRemove);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_BB_btRemove);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_BfNCode_tb);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_BfNCode_btAdd);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_gpSpecies);
            this.gb_VEG_PS.Controls.Add(this.label36);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_Notes);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_gpCover);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_gpHeights);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_EUNIS_tb);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_BB_tb);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_EUNIS_btAdd);
            this.gb_VEG_PS.Controls.Add(this.Veg_S_BB_btAdd);
            this.gb_VEG_PS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_VEG_PS.Location = new System.Drawing.Point(3, 43);
            this.gb_VEG_PS.Name = "gb_VEG_PS";
            this.gb_VEG_PS.Size = new System.Drawing.Size(739, 316);
            this.gb_VEG_PS.TabIndex = 1;
            this.gb_VEG_PS.TabStop = false;
            this.gb_VEG_PS.Text = "Pflanzengesellschaft";
            // 
            // Veg_S_BfNCode_btRemove
            // 
            this.Veg_S_BfNCode_btRemove.Enabled = false;
            this.Veg_S_BfNCode_btRemove.Location = new System.Drawing.Point(111, 76);
            this.Veg_S_BfNCode_btRemove.Name = "Veg_S_BfNCode_btRemove";
            this.Veg_S_BfNCode_btRemove.Size = new System.Drawing.Size(25, 23);
            this.Veg_S_BfNCode_btRemove.TabIndex = 61;
            this.Veg_S_BfNCode_btRemove.Text = "X";
            this.Veg_S_BfNCode_btRemove.UseVisualStyleBackColor = true;
            this.Veg_S_BfNCode_btRemove.Click += new System.EventHandler(this.Veg_S_BfNCode_btRemove_Click);
            // 
            // Veg_S_EUNIS_btRemove
            // 
            this.Veg_S_EUNIS_btRemove.Enabled = false;
            this.Veg_S_EUNIS_btRemove.Location = new System.Drawing.Point(111, 47);
            this.Veg_S_EUNIS_btRemove.Name = "Veg_S_EUNIS_btRemove";
            this.Veg_S_EUNIS_btRemove.Size = new System.Drawing.Size(25, 23);
            this.Veg_S_EUNIS_btRemove.TabIndex = 60;
            this.Veg_S_EUNIS_btRemove.Text = "X";
            this.Veg_S_EUNIS_btRemove.UseVisualStyleBackColor = true;
            this.Veg_S_EUNIS_btRemove.Click += new System.EventHandler(this.Veg_S_EUNIS_btRemove_Click);
            // 
            // Veg_S_BB_btRemove
            // 
            this.Veg_S_BB_btRemove.Enabled = false;
            this.Veg_S_BB_btRemove.Location = new System.Drawing.Point(111, 20);
            this.Veg_S_BB_btRemove.Name = "Veg_S_BB_btRemove";
            this.Veg_S_BB_btRemove.Size = new System.Drawing.Size(25, 23);
            this.Veg_S_BB_btRemove.TabIndex = 59;
            this.Veg_S_BB_btRemove.Text = "X";
            this.Veg_S_BB_btRemove.UseVisualStyleBackColor = true;
            this.Veg_S_BB_btRemove.Click += new System.EventHandler(this.Veg_S_BB_btRemove_Click);
            // 
            // Veg_S_BfNCode_tb
            // 
            this.Veg_S_BfNCode_tb.Location = new System.Drawing.Point(142, 79);
            this.Veg_S_BfNCode_tb.Name = "Veg_S_BfNCode_tb";
            this.Veg_S_BfNCode_tb.ReadOnly = true;
            this.Veg_S_BfNCode_tb.Size = new System.Drawing.Size(576, 20);
            this.Veg_S_BfNCode_tb.TabIndex = 58;
            this.Veg_S_BfNCode_tb.TextChanged += new System.EventHandler(this.Veg_S_BfNCode_tb_TextChanged);
            // 
            // Veg_S_gpSpecies
            // 
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Crypto);
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Herbs);
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Shrubs);
            this.Veg_S_gpSpecies.Controls.Add(this.label59);
            this.Veg_S_gpSpecies.Controls.Add(this.label58);
            this.Veg_S_gpSpecies.Controls.Add(this.label57);
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Trees2);
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Trees1);
            this.Veg_S_gpSpecies.Controls.Add(this.label56);
            this.Veg_S_gpSpecies.Controls.Add(this.label55);
            this.Veg_S_gpSpecies.Controls.Add(this.label37);
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Total);
            this.Veg_S_gpSpecies.Location = new System.Drawing.Point(10, 107);
            this.Veg_S_gpSpecies.Name = "Veg_S_gpSpecies";
            this.Veg_S_gpSpecies.Size = new System.Drawing.Size(189, 171);
            this.Veg_S_gpSpecies.TabIndex = 29;
            this.Veg_S_gpSpecies.TabStop = false;
            this.Veg_S_gpSpecies.Text = "Artenzahl";
            // 
            // Veg_S_NOS_Crypto
            // 
            this.Veg_S_NOS_Crypto.Location = new System.Drawing.Point(114, 141);
            this.Veg_S_NOS_Crypto.Name = "Veg_S_NOS_Crypto";
            this.Veg_S_NOS_Crypto.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Crypto.TabIndex = 38;
            // 
            // Veg_S_NOS_Herbs
            // 
            this.Veg_S_NOS_Herbs.Location = new System.Drawing.Point(114, 116);
            this.Veg_S_NOS_Herbs.Name = "Veg_S_NOS_Herbs";
            this.Veg_S_NOS_Herbs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Herbs.TabIndex = 37;
            // 
            // Veg_S_NOS_Shrubs
            // 
            this.Veg_S_NOS_Shrubs.Location = new System.Drawing.Point(114, 91);
            this.Veg_S_NOS_Shrubs.Name = "Veg_S_NOS_Shrubs";
            this.Veg_S_NOS_Shrubs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Shrubs.TabIndex = 36;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(5, 145);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(103, 13);
            this.label59.TabIndex = 35;
            this.label59.Text = "Kryptogamenschicht";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(5, 120);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(66, 13);
            this.label58.TabIndex = 34;
            this.label58.Text = "Krautschicht";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(5, 95);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(78, 13);
            this.label57.TabIndex = 33;
            this.label57.Text = "Strauchschicht";
            // 
            // Veg_S_NOS_Trees2
            // 
            this.Veg_S_NOS_Trees2.Location = new System.Drawing.Point(114, 66);
            this.Veg_S_NOS_Trees2.Name = "Veg_S_NOS_Trees2";
            this.Veg_S_NOS_Trees2.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Trees2.TabIndex = 32;
            // 
            // Veg_S_NOS_Trees1
            // 
            this.Veg_S_NOS_Trees1.Location = new System.Drawing.Point(114, 41);
            this.Veg_S_NOS_Trees1.Name = "Veg_S_NOS_Trees1";
            this.Veg_S_NOS_Trees1.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Trees1.TabIndex = 31;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(5, 70);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(77, 13);
            this.label56.TabIndex = 30;
            this.label56.Text = "Baumschicht 2";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(5, 45);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(77, 13);
            this.label55.TabIndex = 29;
            this.label55.Text = "Baumschicht 1";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(5, 20);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(43, 13);
            this.label37.TabIndex = 27;
            this.label37.Text = "Gesamt";
            // 
            // Veg_S_NOS_Total
            // 
            this.Veg_S_NOS_Total.Location = new System.Drawing.Point(114, 16);
            this.Veg_S_NOS_Total.Name = "Veg_S_NOS_Total";
            this.Veg_S_NOS_Total.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Total.TabIndex = 28;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(11, 287);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(73, 13);
            this.label36.TabIndex = 26;
            this.label36.Text = "Bemerkungen";
            // 
            // Veg_S_Notes
            // 
            this.Veg_S_Notes.Location = new System.Drawing.Point(89, 284);
            this.Veg_S_Notes.Name = "Veg_S_Notes";
            this.Veg_S_Notes.Size = new System.Drawing.Size(629, 20);
            this.Veg_S_Notes.TabIndex = 25;
            // 
            // Veg_S_gpCover
            // 
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Crypto);
            this.Veg_S_gpCover.Controls.Add(this.label35);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Herbs);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_oSoil);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Shrubs);
            this.Veg_S_gpCover.Controls.Add(this.label34);
            this.Veg_S_gpCover.Controls.Add(this.label31);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Litter);
            this.Veg_S_gpCover.Controls.Add(this.label32);
            this.Veg_S_gpCover.Controls.Add(this.label63);
            this.Veg_S_gpCover.Controls.Add(this.label33);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Total);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Trees2);
            this.Veg_S_gpCover.Controls.Add(this.label64);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Trees1);
            this.Veg_S_gpCover.Controls.Add(this.label62);
            this.Veg_S_gpCover.Location = new System.Drawing.Point(384, 107);
            this.Veg_S_gpCover.Name = "Veg_S_gpCover";
            this.Veg_S_gpCover.Size = new System.Drawing.Size(334, 171);
            this.Veg_S_gpCover.TabIndex = 5;
            this.Veg_S_gpCover.TabStop = false;
            this.Veg_S_gpCover.Text = "Deckung [%]";
            // 
            // Veg_S_C_Crypto
            // 
            this.Veg_S_C_Crypto.Location = new System.Drawing.Point(115, 139);
            this.Veg_S_C_Crypto.Name = "Veg_S_C_Crypto";
            this.Veg_S_C_Crypto.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Crypto.TabIndex = 50;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(188, 43);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(74, 13);
            this.label35.TabIndex = 11;
            this.label35.Text = "offener Boden";
            // 
            // Veg_S_C_Herbs
            // 
            this.Veg_S_C_Herbs.Location = new System.Drawing.Point(115, 114);
            this.Veg_S_C_Herbs.Name = "Veg_S_C_Herbs";
            this.Veg_S_C_Herbs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Herbs.TabIndex = 49;
            // 
            // Veg_S_C_oSoil
            // 
            this.Veg_S_C_oSoil.Location = new System.Drawing.Point(269, 39);
            this.Veg_S_C_oSoil.Name = "Veg_S_C_oSoil";
            this.Veg_S_C_oSoil.Size = new System.Drawing.Size(44, 20);
            this.Veg_S_C_oSoil.TabIndex = 10;
            // 
            // Veg_S_C_Shrubs
            // 
            this.Veg_S_C_Shrubs.Location = new System.Drawing.Point(115, 89);
            this.Veg_S_C_Shrubs.Name = "Veg_S_C_Shrubs";
            this.Veg_S_C_Shrubs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Shrubs.TabIndex = 48;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(188, 18);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(32, 13);
            this.label34.TabIndex = 9;
            this.label34.Text = "Streu";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 143);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(103, 13);
            this.label31.TabIndex = 47;
            this.label31.Text = "Kryptogamenschicht";
            // 
            // Veg_S_C_Litter
            // 
            this.Veg_S_C_Litter.Location = new System.Drawing.Point(269, 14);
            this.Veg_S_C_Litter.Name = "Veg_S_C_Litter";
            this.Veg_S_C_Litter.Size = new System.Drawing.Size(44, 20);
            this.Veg_S_C_Litter.TabIndex = 7;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 118);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(66, 13);
            this.label32.TabIndex = 46;
            this.label32.Text = "Krautschicht";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(6, 43);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(77, 13);
            this.label63.TabIndex = 41;
            this.label63.Text = "Baumschicht 1";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 93);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(78, 13);
            this.label33.TabIndex = 45;
            this.label33.Text = "Strauchschicht";
            // 
            // Veg_S_C_Total
            // 
            this.Veg_S_C_Total.Location = new System.Drawing.Point(115, 14);
            this.Veg_S_C_Total.Name = "Veg_S_C_Total";
            this.Veg_S_C_Total.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Total.TabIndex = 40;
            // 
            // Veg_S_C_Trees2
            // 
            this.Veg_S_C_Trees2.Location = new System.Drawing.Point(115, 64);
            this.Veg_S_C_Trees2.Name = "Veg_S_C_Trees2";
            this.Veg_S_C_Trees2.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Trees2.TabIndex = 44;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 18);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(43, 13);
            this.label64.TabIndex = 39;
            this.label64.Text = "Gesamt";
            // 
            // Veg_S_C_Trees1
            // 
            this.Veg_S_C_Trees1.Location = new System.Drawing.Point(115, 39);
            this.Veg_S_C_Trees1.Name = "Veg_S_C_Trees1";
            this.Veg_S_C_Trees1.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Trees1.TabIndex = 43;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 68);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(77, 13);
            this.label62.TabIndex = 42;
            this.label62.Text = "Baumschicht 2";
            // 
            // Veg_S_gpHeights
            // 
            this.Veg_S_gpHeights.Controls.Add(this.Veg_S_H_Herbs);
            this.Veg_S_gpHeights.Controls.Add(this.label61);
            this.Veg_S_gpHeights.Controls.Add(this.Veg_S_H_Shrubs);
            this.Veg_S_gpHeights.Controls.Add(this.label60);
            this.Veg_S_gpHeights.Controls.Add(this.Veg_S_H_Trees2);
            this.Veg_S_gpHeights.Controls.Add(this.label30);
            this.Veg_S_gpHeights.Controls.Add(this.Veg_S_H_Trees1);
            this.Veg_S_gpHeights.Controls.Add(this.label29);
            this.Veg_S_gpHeights.Location = new System.Drawing.Point(209, 107);
            this.Veg_S_gpHeights.Name = "Veg_S_gpHeights";
            this.Veg_S_gpHeights.Size = new System.Drawing.Size(166, 171);
            this.Veg_S_gpHeights.TabIndex = 4;
            this.Veg_S_gpHeights.TabStop = false;
            this.Veg_S_gpHeights.Text = "Höhe [m]";
            // 
            // Veg_S_H_Herbs
            // 
            this.Veg_S_H_Herbs.Location = new System.Drawing.Point(93, 115);
            this.Veg_S_H_Herbs.Name = "Veg_S_H_Herbs";
            this.Veg_S_H_Herbs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_H_Herbs.TabIndex = 42;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(6, 44);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(77, 13);
            this.label61.TabIndex = 5;
            this.label61.Text = "Baumschicht 1";
            // 
            // Veg_S_H_Shrubs
            // 
            this.Veg_S_H_Shrubs.Location = new System.Drawing.Point(93, 90);
            this.Veg_S_H_Shrubs.Name = "Veg_S_H_Shrubs";
            this.Veg_S_H_Shrubs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_H_Shrubs.TabIndex = 41;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 69);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(77, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "Baumschicht 2";
            // 
            // Veg_S_H_Trees2
            // 
            this.Veg_S_H_Trees2.Location = new System.Drawing.Point(93, 65);
            this.Veg_S_H_Trees2.Name = "Veg_S_H_Trees2";
            this.Veg_S_H_Trees2.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_H_Trees2.TabIndex = 40;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 94);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(78, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Strauchschicht";
            // 
            // Veg_S_H_Trees1
            // 
            this.Veg_S_H_Trees1.Location = new System.Drawing.Point(93, 40);
            this.Veg_S_H_Trees1.Name = "Veg_S_H_Trees1";
            this.Veg_S_H_Trees1.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_H_Trees1.TabIndex = 39;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 119);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(66, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Krautschicht";
            // 
            // Veg_S_EUNIS_tb
            // 
            this.Veg_S_EUNIS_tb.Location = new System.Drawing.Point(142, 50);
            this.Veg_S_EUNIS_tb.Name = "Veg_S_EUNIS_tb";
            this.Veg_S_EUNIS_tb.ReadOnly = true;
            this.Veg_S_EUNIS_tb.Size = new System.Drawing.Size(576, 20);
            this.Veg_S_EUNIS_tb.TabIndex = 3;
            this.Veg_S_EUNIS_tb.TextChanged += new System.EventHandler(this.Veg_S_EUNIS_tb_TextChanged);
            // 
            // Veg_S_BB_tb
            // 
            this.Veg_S_BB_tb.Location = new System.Drawing.Point(142, 22);
            this.Veg_S_BB_tb.Name = "Veg_S_BB_tb";
            this.Veg_S_BB_tb.ReadOnly = true;
            this.Veg_S_BB_tb.Size = new System.Drawing.Size(576, 20);
            this.Veg_S_BB_tb.TabIndex = 2;
            this.Veg_S_BB_tb.TextChanged += new System.EventHandler(this.Veg_S_BB_tb_TextChanged);
            // 
            // gb_VEG_SP
            // 
            this.gb_VEG_SP.Controls.Add(this.flowLayoutPanel1);
            this.gb_VEG_SP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_VEG_SP.Location = new System.Drawing.Point(3, 365);
            this.gb_VEG_SP.Name = "gb_VEG_SP";
            this.gb_VEG_SP.Size = new System.Drawing.Size(739, 193);
            this.gb_VEG_SP.TabIndex = 2;
            this.gb_VEG_SP.TabStop = false;
            this.gb_VEG_SP.Text = "Einzelpflanzenbeschreibung";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.gb_VEG_SP_btAdd);
            this.flowLayoutPanel1.Controls.Add(this.gb_VEG_SP_btRemove);
            this.flowLayoutPanel1.Controls.Add(this.Veg_P_DGV);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(733, 174);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // Veg_P_DGV
            // 
            this.Veg_P_DGV.AllowUserToAddRows = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Veg_P_DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.Veg_P_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Veg_P_DGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SPl_Col_BtSpeciesID,
            this.SPl_Col_Species,
            this.SPl_Col_Cover,
            this.SPl_Col_Sociability,
            this.SPl_Col_PhenDay,
            this.SPl_Col_PhenPhase,
            this.SPl_Col_Notes});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Veg_P_DGV.DefaultCellStyle = dataGridViewCellStyle11;
            this.Veg_P_DGV.Location = new System.Drawing.Point(3, 32);
            this.Veg_P_DGV.Name = "Veg_P_DGV";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Veg_P_DGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.Veg_P_DGV.Size = new System.Drawing.Size(727, 139);
            this.Veg_P_DGV.TabIndex = 0;
            this.Veg_P_DGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Veg_P_DGV_CellContentClick);
            this.Veg_P_DGV.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.Veg_P_DGV_DataError);
            // 
            // SPl_Col_BtSpeciesID
            // 
            this.SPl_Col_BtSpeciesID.Frozen = true;
            this.SPl_Col_BtSpeciesID.HeaderText = "Auswahl";
            this.SPl_Col_BtSpeciesID.Name = "SPl_Col_BtSpeciesID";
            this.SPl_Col_BtSpeciesID.Text = "";
            this.SPl_Col_BtSpeciesID.UseColumnTextForButtonValue = true;
            this.SPl_Col_BtSpeciesID.Width = 60;
            // 
            // SPl_Col_Species
            // 
            this.SPl_Col_Species.HeaderText = "Name";
            this.SPl_Col_Species.Name = "SPl_Col_Species";
            this.SPl_Col_Species.ReadOnly = true;
            // 
            // SPl_Col_Cover
            // 
            this.SPl_Col_Cover.HeaderText = "Deckung";
            this.SPl_Col_Cover.Name = "SPl_Col_Cover";
            // 
            // SPl_Col_Sociability
            // 
            this.SPl_Col_Sociability.HeaderText = "Soziabilität";
            this.SPl_Col_Sociability.Name = "SPl_Col_Sociability";
            // 
            // SPl_Col_PhenDay
            // 
            this.SPl_Col_PhenDay.HeaderText = "Phän. Tag";
            this.SPl_Col_PhenDay.Name = "SPl_Col_PhenDay";
            this.SPl_Col_PhenDay.ToolTipText = "Phänologischer Tag";
            // 
            // SPl_Col_PhenPhase
            // 
            this.SPl_Col_PhenPhase.HeaderText = "Phän. Phase";
            this.SPl_Col_PhenPhase.Name = "SPl_Col_PhenPhase";
            this.SPl_Col_PhenPhase.ToolTipText = "Phänologische Phase";
            // 
            // SPl_Col_Notes
            // 
            this.SPl_Col_Notes.HeaderText = "Bemerkungen";
            this.SPl_Col_Notes.Name = "SPl_Col_Notes";
            // 
            // gb_SPECTRA
            // 
            this.gb_SPECTRA.AutoSize = true;
            this.gb_SPECTRA.Controls.Add(this.gb_SPECTRA_WREF);
            this.gb_SPECTRA.Controls.Add(this.gb_SPECTRA_OBJ);
            this.gb_SPECTRA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_SPECTRA.Location = new System.Drawing.Point(3, 2289);
            this.gb_SPECTRA.Name = "gb_SPECTRA";
            this.gb_SPECTRA.Size = new System.Drawing.Size(766, 537);
            this.gb_SPECTRA.TabIndex = 4;
            this.gb_SPECTRA.TabStop = false;
            this.gb_SPECTRA.Text = "Spektren und Spektrenprozessierung";
            // 
            // gb_SPECTRA_WREF
            // 
            this.gb_SPECTRA_WREF.Controls.Add(this.MP_WREF_btRemoveSpectra);
            this.gb_SPECTRA_WREF.Controls.Add(this.MP_WREF_DGV);
            this.gb_SPECTRA_WREF.Controls.Add(this.MP_WREF_btReadSpectra);
            this.gb_SPECTRA_WREF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_SPECTRA_WREF.Location = new System.Drawing.Point(9, 23);
            this.gb_SPECTRA_WREF.Name = "gb_SPECTRA_WREF";
            this.gb_SPECTRA_WREF.Size = new System.Drawing.Size(751, 171);
            this.gb_SPECTRA_WREF.TabIndex = 3;
            this.gb_SPECTRA_WREF.TabStop = false;
            this.gb_SPECTRA_WREF.Text = "Weißreferenz(en)";
            // 
            // MP_WREF_btRemoveSpectra
            // 
            this.MP_WREF_btRemoveSpectra.Enabled = false;
            this.MP_WREF_btRemoveSpectra.Location = new System.Drawing.Point(90, 19);
            this.MP_WREF_btRemoveSpectra.Name = "MP_WREF_btRemoveSpectra";
            this.MP_WREF_btRemoveSpectra.Size = new System.Drawing.Size(75, 23);
            this.MP_WREF_btRemoveSpectra.TabIndex = 2;
            this.MP_WREF_btRemoveSpectra.Text = "Entfernen";
            this.MP_WREF_btRemoveSpectra.UseVisualStyleBackColor = true;
            this.MP_WREF_btRemoveSpectra.Click += new System.EventHandler(this.MP_WREF_btRemoveSpectra_Click);
            // 
            // MP_WREF_DGV
            // 
            this.MP_WREF_DGV.AllowUserToAddRows = false;
            this.MP_WREF_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MP_WREF_DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.MP_WREF_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MP_WREF_DGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WR_Files,
            this.WR_Description});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MP_WREF_DGV.DefaultCellStyle = dataGridViewCellStyle2;
            this.MP_WREF_DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.MP_WREF_DGV.Location = new System.Drawing.Point(8, 49);
            this.MP_WREF_DGV.Name = "MP_WREF_DGV";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MP_WREF_DGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.MP_WREF_DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MP_WREF_DGV.Size = new System.Drawing.Size(584, 115);
            this.MP_WREF_DGV.TabIndex = 1;
            this.MP_WREF_DGV.SelectionChanged += new System.EventHandler(this.MP_WREF_DGV_SelectionChanged);
            // 
            // WR_Files
            // 
            this.WR_Files.HeaderText = "WR Spektren";
            this.WR_Files.Name = "WR_Files";
            this.WR_Files.ReadOnly = true;
            this.WR_Files.ToolStripLabel_ShowSpectra = false;
            // 
            // WR_Description
            // 
            this.WR_Description.HeaderText = "Beschreibung";
            this.WR_Description.Name = "WR_Description";
            // 
            // MP_WREF_btReadSpectra
            // 
            this.MP_WREF_btReadSpectra.Location = new System.Drawing.Point(8, 19);
            this.MP_WREF_btReadSpectra.Name = "MP_WREF_btReadSpectra";
            this.MP_WREF_btReadSpectra.Size = new System.Drawing.Size(75, 23);
            this.MP_WREF_btReadSpectra.TabIndex = 0;
            this.MP_WREF_btReadSpectra.Text = "Einlesen...";
            this.MP_WREF_btReadSpectra.UseVisualStyleBackColor = true;
            this.MP_WREF_btReadSpectra.Click += new System.EventHandler(this.MP_WREF_bt_Click);
            // 
            // gb_SPECTRA_OBJ
            // 
            this.gb_SPECTRA_OBJ.Controls.Add(this.gb_SPECTRA_OBJ_btRemove);
            this.gb_SPECTRA_OBJ.Controls.Add(this.gb_SPECTRA_OBJ_btAdd);
            this.gb_SPECTRA_OBJ.Controls.Add(this.MP_SPECTRA_DGV);
            this.gb_SPECTRA_OBJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_SPECTRA_OBJ.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gb_SPECTRA_OBJ.Location = new System.Drawing.Point(9, 197);
            this.gb_SPECTRA_OBJ.Name = "gb_SPECTRA_OBJ";
            this.gb_SPECTRA_OBJ.Size = new System.Drawing.Size(751, 335);
            this.gb_SPECTRA_OBJ.TabIndex = 2;
            this.gb_SPECTRA_OBJ.TabStop = false;
            this.gb_SPECTRA_OBJ.Text = "Feldspektren + Prozessierungen (keine Weißreferenzen)";
            // 
            // gb_SPECTRA_OBJ_btAdd
            // 
            this.gb_SPECTRA_OBJ_btAdd.Location = new System.Drawing.Point(8, 19);
            this.gb_SPECTRA_OBJ_btAdd.Name = "gb_SPECTRA_OBJ_btAdd";
            this.gb_SPECTRA_OBJ_btAdd.Size = new System.Drawing.Size(81, 23);
            this.gb_SPECTRA_OBJ_btAdd.TabIndex = 14;
            this.gb_SPECTRA_OBJ_btAdd.Text = "Einlesen ...";
            this.gb_SPECTRA_OBJ_btAdd.UseVisualStyleBackColor = true;
            this.gb_SPECTRA_OBJ_btAdd.Click += new System.EventHandler(this.MP_OS_Read_Click);
            // 
            // MP_SPECTRA_DGV
            // 
            this.MP_SPECTRA_DGV.AllowUserToAddRows = false;
            this.MP_SPECTRA_DGV.AllowUserToDeleteRows = false;
            this.MP_SPECTRA_DGV.AllowUserToOrderColumns = true;
            this.MP_SPECTRA_DGV.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MP_SPECTRA_DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.MP_SPECTRA_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MP_SPECTRA_DGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SPEC_Field,
            this.SPEC_Jump,
            this.SPEC_HyMap,
            this.SPEC_Notes});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MP_SPECTRA_DGV.DefaultCellStyle = dataGridViewCellStyle5;
            this.MP_SPECTRA_DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.MP_SPECTRA_DGV.Location = new System.Drawing.Point(8, 51);
            this.MP_SPECTRA_DGV.Name = "MP_SPECTRA_DGV";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MP_SPECTRA_DGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.MP_SPECTRA_DGV.Size = new System.Drawing.Size(713, 269);
            this.MP_SPECTRA_DGV.TabIndex = 13;
            this.MP_SPECTRA_DGV.SelectionChanged += new System.EventHandler(this.MP_SPECTRA_DGV_SelectionChanged);
            // 
            // SPEC_Field
            // 
            this.SPEC_Field.HeaderText = "Feldspektren";
            this.SPEC_Field.Name = "SPEC_Field";
            this.SPEC_Field.ReadOnly = true;
            this.SPEC_Field.ToolStripLabel_ShowSpectra = false;
            // 
            // SPEC_Jump
            // 
            this.SPEC_Jump.HeaderText = "Jumpkorrektur";
            this.SPEC_Jump.Name = "SPEC_Jump";
            this.SPEC_Jump.ReadOnly = true;
            this.SPEC_Jump.ToolStripLabel_ShowSpectra = false;
            // 
            // SPEC_HyMap
            // 
            this.SPEC_HyMap.HeaderText = "HyMap resampled";
            this.SPEC_HyMap.Name = "SPEC_HyMap";
            this.SPEC_HyMap.ReadOnly = true;
            this.SPEC_HyMap.ToolStripLabel_ShowSpectra = false;
            // 
            // SPEC_Notes
            // 
            this.SPEC_Notes.HeaderText = "Bemerkungen";
            this.SPEC_Notes.Name = "SPEC_Notes";
            // 
            // gb_TITLE
            // 
            this.gb_TITLE.AutoSize = true;
            this.gb_TITLE.Controls.Add(this.gb_POS);
            this.gb_TITLE.Controls.Add(this.gb_TITLE_COMMON);
            this.gb_TITLE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_TITLE.Location = new System.Drawing.Point(3, 3);
            this.gb_TITLE.Name = "gb_TITLE";
            this.gb_TITLE.Size = new System.Drawing.Size(766, 597);
            this.gb_TITLE.TabIndex = 0;
            this.gb_TITLE.TabStop = false;
            this.gb_TITLE.Text = "Titeldaten";
            // 
            // gb_POS
            // 
            this.gb_POS.AutoSize = true;
            this.gb_POS.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_POS.Controls.Add(this.POS_flp);
            this.gb_POS.Location = new System.Drawing.Point(6, 159);
            this.gb_POS.Name = "gb_POS";
            this.gb_POS.Size = new System.Drawing.Size(754, 419);
            this.gb_POS.TabIndex = 21;
            this.gb_POS.TabStop = false;
            this.gb_POS.Text = "Position / Beobachtungsfläche";
            // 
            // POS_flp
            // 
            this.POS_flp.AutoSize = true;
            this.POS_flp.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.POS_flp.Controls.Add(this.panel3);
            this.POS_flp.Controls.Add(this.gb_POS_OBSAREA);
            this.POS_flp.Controls.Add(this.gb_POS_newlocation);
            this.POS_flp.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.POS_flp.Location = new System.Drawing.Point(8, 19);
            this.POS_flp.Name = "POS_flp";
            this.POS_flp.Size = new System.Drawing.Size(740, 381);
            this.POS_flp.TabIndex = 39;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.gb_POS_newlocation_rb);
            this.panel3.Controls.Add(this.gb_POS_OBSAREA_rb);
            this.panel3.Controls.Add(this.gb_POS_none_rb);
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(734, 23);
            this.panel3.TabIndex = 21;
            // 
            // gb_POS_none_rb
            // 
            this.gb_POS_none_rb.AutoSize = true;
            this.gb_POS_none_rb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_POS_none_rb.Location = new System.Drawing.Point(8, 3);
            this.gb_POS_none_rb.Name = "gb_POS_none_rb";
            this.gb_POS_none_rb.Size = new System.Drawing.Size(91, 17);
            this.gb_POS_none_rb.TabIndex = 0;
            this.gb_POS_none_rb.TabStop = true;
            this.gb_POS_none_rb.Text = "keine Angabe";
            this.gb_POS_none_rb.UseVisualStyleBackColor = true;
            // 
            // gb_POS_OBSAREA
            // 
            this.gb_POS_OBSAREA.BackColor = System.Drawing.SystemColors.Control;
            this.gb_POS_OBSAREA.Controls.Add(this.label5);
            this.gb_POS_OBSAREA.Controls.Add(this.AREA_ID_GFZ_tb);
            this.gb_POS_OBSAREA.Controls.Add(this.label1);
            this.gb_POS_OBSAREA.Controls.Add(this.AREA_ID_INFO_tb);
            this.gb_POS_OBSAREA.Controls.Add(this.AREA_ID_DB_tb);
            this.gb_POS_OBSAREA.Controls.Add(this.AREA_ID_bt);
            this.gb_POS_OBSAREA.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gb_POS_OBSAREA.Location = new System.Drawing.Point(3, 32);
            this.gb_POS_OBSAREA.Name = "gb_POS_OBSAREA";
            this.gb_POS_OBSAREA.Size = new System.Drawing.Size(734, 101);
            this.gb_POS_OBSAREA.TabIndex = 20;
            this.gb_POS_OBSAREA.TabStop = false;
            this.gb_POS_OBSAREA.Text = "GFZ_Beobachtungsfläche";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(152, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "GFZ_AREA_ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "ID";
            // 
            // AREA_ID_INFO_tb
            // 
            this.AREA_ID_INFO_tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AREA_ID_INFO_tb.Location = new System.Drawing.Point(31, 59);
            this.AREA_ID_INFO_tb.Name = "AREA_ID_INFO_tb";
            this.AREA_ID_INFO_tb.ReadOnly = true;
            this.AREA_ID_INFO_tb.Size = new System.Drawing.Size(546, 20);
            this.AREA_ID_INFO_tb.TabIndex = 2;
            // 
            // AREA_ID_bt
            // 
            this.AREA_ID_bt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AREA_ID_bt.Location = new System.Drawing.Point(353, 19);
            this.AREA_ID_bt.Name = "AREA_ID_bt";
            this.AREA_ID_bt.Size = new System.Drawing.Size(224, 22);
            this.AREA_ID_bt.TabIndex = 0;
            this.AREA_ID_bt.Text = "Aus Datenbank auswählen";
            this.AREA_ID_bt.UseVisualStyleBackColor = true;
            this.AREA_ID_bt.Click += new System.EventHandler(this.AREA_ID_bt_Click);
            // 
            // gb_POS_newlocation
            // 
            this.gb_POS_newlocation.AutoSize = true;
            this.gb_POS_newlocation.Controls.Add(this.Loc_Coord);
            this.gb_POS_newlocation.Controls.Add(this.Loc_Notes);
            this.gb_POS_newlocation.Controls.Add(this.label11);
            this.gb_POS_newlocation.Controls.Add(this.Loc_Aspect);
            this.gb_POS_newlocation.Controls.Add(this.label12);
            this.gb_POS_newlocation.Controls.Add(this.label13);
            this.gb_POS_newlocation.Controls.Add(this.label14);
            this.gb_POS_newlocation.Controls.Add(this.Loc_Locname);
            this.gb_POS_newlocation.Controls.Add(this.Loc_Slope);
            this.gb_POS_newlocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_POS_newlocation.Location = new System.Drawing.Point(3, 139);
            this.gb_POS_newlocation.Name = "gb_POS_newlocation";
            this.gb_POS_newlocation.Size = new System.Drawing.Size(733, 239);
            this.gb_POS_newlocation.TabIndex = 38;
            this.gb_POS_newlocation.TabStop = false;
            this.gb_POS_newlocation.Text = "Daten neu eingeben";
            // 
            // Loc_Coord
            // 
            this.Loc_Coord.AutoSize = true;
            this.Loc_Coord.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Loc_Coord.Location = new System.Drawing.Point(17, 56);
            this.Loc_Coord.Name = "Loc_Coord";
            this.Loc_Coord.Size = new System.Drawing.Size(710, 135);
            this.Loc_Coord.TabIndex = 2;
            // 
            // Loc_Notes
            // 
            this.Loc_Notes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Loc_Notes.Location = new System.Drawing.Point(82, 200);
            this.Loc_Notes.Name = "Loc_Notes";
            this.Loc_Notes.Size = new System.Drawing.Size(362, 20);
            this.Loc_Notes.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 203);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 42;
            this.label11.Text = "Bemerkungen";
            // 
            // Loc_Aspect
            // 
            this.Loc_Aspect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Loc_Aspect.Location = new System.Drawing.Point(404, 23);
            this.Loc_Aspect.Name = "Loc_Aspect";
            this.Loc_Aspect.Size = new System.Drawing.Size(100, 20);
            this.Loc_Aspect.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(363, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 40;
            this.label12.Text = "Aspect";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(254, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "Slope";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(16, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Name";
            // 
            // Loc_Locname
            // 
            this.Loc_Locname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Loc_Locname.Location = new System.Drawing.Point(56, 22);
            this.Loc_Locname.Name = "Loc_Locname";
            this.Loc_Locname.Size = new System.Drawing.Size(167, 20);
            this.Loc_Locname.TabIndex = 0;
            // 
            // Loc_Slope
            // 
            this.Loc_Slope.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Loc_Slope.Location = new System.Drawing.Point(294, 21);
            this.Loc_Slope.Name = "Loc_Slope";
            this.Loc_Slope.Size = new System.Drawing.Size(53, 20);
            this.Loc_Slope.TabIndex = 26;
            // 
            // gb_TITLE_COMMON
            // 
            this.gb_TITLE_COMMON.Controls.Add(this.label65);
            this.gb_TITLE_COMMON.Controls.Add(this.Spec_Source);
            this.gb_TITLE_COMMON.Controls.Add(this.label79);
            this.gb_TITLE_COMMON.Controls.Add(this.Ti_notes);
            this.gb_TITLE_COMMON.Controls.Add(this.Ti_OperatorINSERT);
            this.gb_TITLE_COMMON.Controls.Add(this.Time_TimeEnd);
            this.gb_TITLE_COMMON.Controls.Add(this.Time_TimeStart);
            this.gb_TITLE_COMMON.Controls.Add(this.Ti_Campaign_cb);
            this.gb_TITLE_COMMON.Controls.Add(this.label9);
            this.gb_TITLE_COMMON.Controls.Add(this.Ti_Institute_tb);
            this.gb_TITLE_COMMON.Controls.Add(this.label8);
            this.gb_TITLE_COMMON.Controls.Add(this.label2);
            this.gb_TITLE_COMMON.Controls.Add(this.label7);
            this.gb_TITLE_COMMON.Controls.Add(this.label6);
            this.gb_TITLE_COMMON.Controls.Add(this.label3);
            this.gb_TITLE_COMMON.Controls.Add(this.Time_Date);
            this.gb_TITLE_COMMON.Controls.Add(this.label4);
            this.gb_TITLE_COMMON.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_TITLE_COMMON.Location = new System.Drawing.Point(6, 19);
            this.gb_TITLE_COMMON.Name = "gb_TITLE_COMMON";
            this.gb_TITLE_COMMON.Size = new System.Drawing.Size(754, 134);
            this.gb_TITLE_COMMON.TabIndex = 19;
            this.gb_TITLE_COMMON.TabStop = false;
            this.gb_TITLE_COMMON.Text = "Allgemeines";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(7, 18);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(92, 13);
            this.label65.TabIndex = 13;
            this.label65.Text = "Spektrenquelle";
            // 
            // Spec_Source
            // 
            this.Spec_Source.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Spec_Source.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Spec_Source.FormattingEnabled = true;
            this.Spec_Source.Location = new System.Drawing.Point(104, 15);
            this.Spec_Source.Name = "Spec_Source";
            this.Spec_Source.Size = new System.Drawing.Size(252, 21);
            this.Spec_Source.TabIndex = 0;
            this.Spec_Source.SelectedValueChanged += new System.EventHandler(this.VAL_Required);
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(7, 109);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(73, 13);
            this.label79.TabIndex = 25;
            this.label79.Text = "Bemerkungen";
            // 
            // Ti_notes
            // 
            this.Ti_notes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ti_notes.Location = new System.Drawing.Point(104, 106);
            this.Ti_notes.Name = "Ti_notes";
            this.Ti_notes.Size = new System.Drawing.Size(251, 20);
            this.Ti_notes.TabIndex = 3;
            this.Ti_notes.TextChanged += new System.EventHandler(this.VAL_Required);
            // 
            // Ti_OperatorINSERT
            // 
            this.Ti_OperatorINSERT.BackColor = System.Drawing.SystemColors.Window;
            this.Ti_OperatorINSERT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Ti_OperatorINSERT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ti_OperatorINSERT.FormattingEnabled = true;
            this.Ti_OperatorINSERT.Location = new System.Drawing.Point(455, 15);
            this.Ti_OperatorINSERT.Name = "Ti_OperatorINSERT";
            this.Ti_OperatorINSERT.Size = new System.Drawing.Size(283, 21);
            this.Ti_OperatorINSERT.TabIndex = 4;
            this.Ti_OperatorINSERT.SelectedValueChanged += new System.EventHandler(this.VAL_Required);
            // 
            // Time_TimeEnd
            // 
            this.Time_TimeEnd.Checked = false;
            this.Time_TimeEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time_TimeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.Time_TimeEnd.Location = new System.Drawing.Point(638, 86);
            this.Time_TimeEnd.Name = "Time_TimeEnd";
            this.Time_TimeEnd.ShowCheckBox = true;
            this.Time_TimeEnd.ShowUpDown = true;
            this.Time_TimeEnd.Size = new System.Drawing.Size(100, 20);
            this.Time_TimeEnd.TabIndex = 7;
            // 
            // Time_TimeStart
            // 
            this.Time_TimeStart.Checked = false;
            this.Time_TimeStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time_TimeStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.Time_TimeStart.Location = new System.Drawing.Point(638, 55);
            this.Time_TimeStart.Name = "Time_TimeStart";
            this.Time_TimeStart.ShowCheckBox = true;
            this.Time_TimeStart.ShowUpDown = true;
            this.Time_TimeStart.Size = new System.Drawing.Size(100, 20);
            this.Time_TimeStart.TabIndex = 6;
            // 
            // Ti_Campaign_cb
            // 
            this.Ti_Campaign_cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Ti_Campaign_cb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ti_Campaign_cb.FormattingEnabled = true;
            this.Ti_Campaign_cb.Location = new System.Drawing.Point(104, 74);
            this.Ti_Campaign_cb.Name = "Ti_Campaign_cb";
            this.Ti_Campaign_cb.Size = new System.Drawing.Size(252, 21);
            this.Ti_Campaign_cb.TabIndex = 2;
            this.Ti_Campaign_cb.SelectedValueChanged += new System.EventHandler(this.VAL_Required);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Kampagne";
            // 
            // Ti_Institute_tb
            // 
            this.Ti_Institute_tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ti_Institute_tb.Location = new System.Drawing.Point(104, 46);
            this.Ti_Institute_tb.Name = "Ti_Institute_tb";
            this.Ti_Institute_tb.Size = new System.Drawing.Size(252, 20);
            this.Ti_Institute_tb.TabIndex = 1;
            this.Ti_Institute_tb.TextChanged += new System.EventHandler(this.VAL_Required);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(594, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Ende";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Institut";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(594, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Beginn";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(553, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Uhrzeit:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(377, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Eingabe durch";
            // 
            // Time_Date
            // 
            this.Time_Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time_Date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Time_Date.Location = new System.Drawing.Point(456, 55);
            this.Time_Date.Name = "Time_Date";
            this.Time_Date.Size = new System.Drawing.Size(89, 20);
            this.Time_Date.TabIndex = 5;
            this.Time_Date.ValueChanged += new System.EventHandler(this.VAL_Required);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(411, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Datum";
            // 
            // gb_INSTRUMENTS
            // 
            this.gb_INSTRUMENTS.AutoSize = true;
            this.gb_INSTRUMENTS.Controls.Add(this.groupBox19);
            this.gb_INSTRUMENTS.Controls.Add(this.groupBox18);
            this.gb_INSTRUMENTS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_INSTRUMENTS.Location = new System.Drawing.Point(3, 2134);
            this.gb_INSTRUMENTS.Name = "gb_INSTRUMENTS";
            this.gb_INSTRUMENTS.Size = new System.Drawing.Size(768, 149);
            this.gb_INSTRUMENTS.TabIndex = 5;
            this.gb_INSTRUMENTS.TabStop = false;
            this.gb_INSTRUMENTS.Text = "Aufnahmegeräte";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.Spec_Optic);
            this.groupBox19.Controls.Add(this.Sensor_type);
            this.groupBox19.Controls.Add(this.Spec_operator);
            this.groupBox19.Controls.Add(this.Sensor_readSensor);
            this.groupBox19.Controls.Add(this.WRef_Panel);
            this.groupBox19.Controls.Add(this.label44);
            this.groupBox19.Controls.Add(this.label40);
            this.groupBox19.Controls.Add(this.label45);
            this.groupBox19.Controls.Add(this.label39);
            this.groupBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox19.Location = new System.Drawing.Point(6, 19);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(424, 125);
            this.groupBox19.TabIndex = 12;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Sensor";
            // 
            // Spec_Optic
            // 
            this.Spec_Optic.Location = new System.Drawing.Point(73, 44);
            this.Spec_Optic.Name = "Spec_Optic";
            this.Spec_Optic.Size = new System.Drawing.Size(66, 20);
            this.Spec_Optic.TabIndex = 11;
            // 
            // Sensor_type
            // 
            this.Sensor_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Sensor_type.FormattingEnabled = true;
            this.Sensor_type.Location = new System.Drawing.Point(73, 17);
            this.Sensor_type.Name = "Sensor_type";
            this.Sensor_type.Size = new System.Drawing.Size(243, 21);
            this.Sensor_type.TabIndex = 0;
            // 
            // Spec_operator
            // 
            this.Spec_operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Spec_operator.FormattingEnabled = true;
            this.Spec_operator.Location = new System.Drawing.Point(73, 70);
            this.Spec_operator.Name = "Spec_operator";
            this.Spec_operator.Size = new System.Drawing.Size(243, 21);
            this.Spec_operator.TabIndex = 1;
            // 
            // WRef_Panel
            // 
            this.WRef_Panel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.WRef_Panel.FormattingEnabled = true;
            this.WRef_Panel.Location = new System.Drawing.Point(74, 98);
            this.WRef_Panel.Name = "WRef_Panel";
            this.WRef_Panel.Size = new System.Drawing.Size(242, 21);
            this.WRef_Panel.TabIndex = 2;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(35, 23);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(33, 13);
            this.label44.TabIndex = 1;
            this.label44.Text = "Gerät";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(21, 47);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(45, 13);
            this.label40.TabIndex = 3;
            this.label40.Text = "Optik [°]";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(6, 73);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(62, 13);
            this.label45.TabIndex = 9;
            this.label45.Text = "Bediener/in";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(13, 103);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(58, 13);
            this.label39.TabIndex = 2;
            this.label39.Text = "Spektralon";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.Photo_Camera);
            this.groupBox18.Controls.Add(this.Photo_Operator);
            this.groupBox18.Controls.Add(this.label38);
            this.groupBox18.Controls.Add(this.label43);
            this.groupBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox18.Location = new System.Drawing.Point(434, 20);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(328, 124);
            this.groupBox18.TabIndex = 11;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Fotoapparat";
            // 
            // Photo_Camera
            // 
            this.Photo_Camera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Photo_Camera.FormattingEnabled = true;
            this.Photo_Camera.Location = new System.Drawing.Point(73, 17);
            this.Photo_Camera.Name = "Photo_Camera";
            this.Photo_Camera.Size = new System.Drawing.Size(247, 21);
            this.Photo_Camera.TabIndex = 0;
            // 
            // Photo_Operator
            // 
            this.Photo_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Photo_Operator.FormattingEnabled = true;
            this.Photo_Operator.Location = new System.Drawing.Point(73, 43);
            this.Photo_Operator.Name = "Photo_Operator";
            this.Photo_Operator.Size = new System.Drawing.Size(247, 21);
            this.Photo_Operator.TabIndex = 1;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(35, 23);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(33, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "Gerät";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(6, 46);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(62, 13);
            this.label43.TabIndex = 9;
            this.label43.Text = "Bediener/in";
            // 
            // gb_SITUATION
            // 
            this.gb_SITUATION.AutoSize = true;
            this.gb_SITUATION.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_SITUATION.Controls.Add(this.flp_Aufnahmesituation);
            this.gb_SITUATION.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_SITUATION.Location = new System.Drawing.Point(3, 606);
            this.gb_SITUATION.Name = "gb_SITUATION";
            this.gb_SITUATION.Size = new System.Drawing.Size(763, 1522);
            this.gb_SITUATION.TabIndex = 1;
            this.gb_SITUATION.TabStop = false;
            this.gb_SITUATION.Text = "Aufnahmesituation";
            // 
            // flp_Aufnahmesituation
            // 
            this.flp_Aufnahmesituation.AutoSize = true;
            this.flp_Aufnahmesituation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flp_Aufnahmesituation.Controls.Add(this.panel1);
            this.flp_Aufnahmesituation.Controls.Add(this.gb_COND_WEATHER);
            this.flp_Aufnahmesituation.Controls.Add(this.gb_COND_SENSOR);
            this.flp_Aufnahmesituation.Controls.Add(this.gb_COND_FOTOS);
            this.flp_Aufnahmesituation.Controls.Add(this.gb_COND_SOIL);
            this.flp_Aufnahmesituation.Controls.Add(this.gb_COND_ILLUM);
            this.flp_Aufnahmesituation.Controls.Add(this.gb_VEG);
            this.flp_Aufnahmesituation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flp_Aufnahmesituation.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flp_Aufnahmesituation.Location = new System.Drawing.Point(3, 16);
            this.flp_Aufnahmesituation.Name = "flp_Aufnahmesituation";
            this.flp_Aufnahmesituation.Size = new System.Drawing.Size(757, 1503);
            this.flp_Aufnahmesituation.TabIndex = 10;
            this.flp_Aufnahmesituation.WrapContents = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.gb_COND_VEGETATION_cb);
            this.panel1.Controls.Add(this.gb_COND_ILLUM_cb);
            this.panel1.Controls.Add(this.gb_COND_SOIL_cb);
            this.panel1.Controls.Add(this.gb_COND_FOTOS_cb);
            this.panel1.Controls.Add(this.gb_COND_SENSOR_cb);
            this.panel1.Controls.Add(this.gb_COND_WEATHER_cb);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(751, 48);
            this.panel1.TabIndex = 0;
            // 
            // gb_COND_ILLUM_cb
            // 
            this.gb_COND_ILLUM_cb.AutoSize = true;
            this.gb_COND_ILLUM_cb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_ILLUM_cb.Location = new System.Drawing.Point(136, 26);
            this.gb_COND_ILLUM_cb.Name = "gb_COND_ILLUM_cb";
            this.gb_COND_ILLUM_cb.Size = new System.Drawing.Size(203, 17);
            this.gb_COND_ILLUM_cb.TabIndex = 4;
            this.gb_COND_ILLUM_cb.Text = "Strahlungsverhältnisse / Beleuchtung";
            this.gb_COND_ILLUM_cb.UseVisualStyleBackColor = true;
            // 
            // gb_COND_SOIL_cb
            // 
            this.gb_COND_SOIL_cb.AutoSize = true;
            this.gb_COND_SOIL_cb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_SOIL_cb.Location = new System.Drawing.Point(4, 26);
            this.gb_COND_SOIL_cb.Name = "gb_COND_SOIL_cb";
            this.gb_COND_SOIL_cb.Size = new System.Drawing.Size(57, 17);
            this.gb_COND_SOIL_cb.TabIndex = 3;
            this.gb_COND_SOIL_cb.Text = "Boden";
            this.gb_COND_SOIL_cb.UseVisualStyleBackColor = true;
            // 
            // gb_COND_FOTOS_cb
            // 
            this.gb_COND_FOTOS_cb.AutoSize = true;
            this.gb_COND_FOTOS_cb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_FOTOS_cb.Location = new System.Drawing.Point(345, 3);
            this.gb_COND_FOTOS_cb.Name = "gb_COND_FOTOS_cb";
            this.gb_COND_FOTOS_cb.Size = new System.Drawing.Size(52, 17);
            this.gb_COND_FOTOS_cb.TabIndex = 2;
            this.gb_COND_FOTOS_cb.Text = "Fotos";
            this.gb_COND_FOTOS_cb.UseVisualStyleBackColor = true;
            // 
            // gb_COND_SENSOR_cb
            // 
            this.gb_COND_SENSOR_cb.AutoSize = true;
            this.gb_COND_SENSOR_cb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_SENSOR_cb.Location = new System.Drawing.Point(136, 3);
            this.gb_COND_SENSOR_cb.Name = "gb_COND_SENSOR_cb";
            this.gb_COND_SENSOR_cb.Size = new System.Drawing.Size(179, 17);
            this.gb_COND_SENSOR_cb.TabIndex = 1;
            this.gb_COND_SENSOR_cb.Text = "Sensor Position und Ausrichtung";
            this.gb_COND_SENSOR_cb.UseVisualStyleBackColor = true;
            // 
            // gb_COND_WEATHER_cb
            // 
            this.gb_COND_WEATHER_cb.AutoSize = true;
            this.gb_COND_WEATHER_cb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_WEATHER_cb.Location = new System.Drawing.Point(4, 3);
            this.gb_COND_WEATHER_cb.Name = "gb_COND_WEATHER_cb";
            this.gb_COND_WEATHER_cb.Size = new System.Drawing.Size(128, 17);
            this.gb_COND_WEATHER_cb.TabIndex = 0;
            this.gb_COND_WEATHER_cb.Text = "Wetter und Witterung";
            this.gb_COND_WEATHER_cb.UseVisualStyleBackColor = true;
            // 
            // gb_COND_WEATHER
            // 
            this.gb_COND_WEATHER.Controls.Add(this.groupBox23);
            this.gb_COND_WEATHER.Controls.Add(this.groupBox21);
            this.gb_COND_WEATHER.Controls.Add(this.groupBox22);
            this.gb_COND_WEATHER.Controls.Add(this.groupBox3);
            this.gb_COND_WEATHER.Controls.Add(this.label16);
            this.gb_COND_WEATHER.Controls.Add(this.Weather_notes);
            this.gb_COND_WEATHER.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_WEATHER.Location = new System.Drawing.Point(3, 57);
            this.gb_COND_WEATHER.Name = "gb_COND_WEATHER";
            this.gb_COND_WEATHER.Size = new System.Drawing.Size(751, 178);
            this.gb_COND_WEATHER.TabIndex = 3;
            this.gb_COND_WEATHER.TabStop = false;
            this.gb_COND_WEATHER.Text = "Wetter und Witterung";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.Weather_LastPrecip);
            this.groupBox23.Controls.Add(this.label21);
            this.groupBox23.Location = new System.Drawing.Point(381, 99);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(359, 50);
            this.groupBox23.TabIndex = 32;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Witterung";
            // 
            // Weather_LastPrecip
            // 
            this.Weather_LastPrecip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Weather_LastPrecip.FormattingEnabled = true;
            this.Weather_LastPrecip.Location = new System.Drawing.Point(79, 19);
            this.Weather_LastPrecip.Name = "Weather_LastPrecip";
            this.Weather_LastPrecip.Size = new System.Drawing.Size(253, 21);
            this.Weather_LastPrecip.TabIndex = 6;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 26);
            this.label21.TabIndex = 0;
            this.label21.Text = "letzter \r\nNiederschlag";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.label50);
            this.groupBox21.Controls.Add(this.Weather_W_v);
            this.groupBox21.Controls.Add(this.label51);
            this.groupBox21.Controls.Add(this.Weather_W_dir);
            this.groupBox21.Location = new System.Drawing.Point(197, 19);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(158, 91);
            this.groupBox21.TabIndex = 42;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Wind";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(29, 17);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(40, 13);
            this.label50.TabIndex = 32;
            this.label50.Text = "v [m/s]";
            // 
            // Weather_W_v
            // 
            this.Weather_W_v.Location = new System.Drawing.Point(76, 14);
            this.Weather_W_v.Name = "Weather_W_v";
            this.Weather_W_v.Size = new System.Drawing.Size(52, 20);
            this.Weather_W_v.TabIndex = 17;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(7, 46);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(63, 13);
            this.label51.TabIndex = 33;
            this.label51.Text = "Richtung [°]";
            // 
            // Weather_W_dir
            // 
            this.Weather_W_dir.Location = new System.Drawing.Point(76, 42);
            this.Weather_W_dir.Name = "Weather_W_dir";
            this.Weather_W_dir.Size = new System.Drawing.Size(52, 20);
            this.Weather_W_dir.TabIndex = 34;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.labelWeather_Cloudiness);
            this.groupBox22.Controls.Add(this.Weather_CloudDensity);
            this.groupBox22.Controls.Add(this.labelWeather_CloudType);
            this.groupBox22.Controls.Add(this.Weather_CloudType);
            this.groupBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox22.Location = new System.Drawing.Point(381, 18);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(359, 75);
            this.groupBox22.TabIndex = 5;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Bewölkung";
            // 
            // labelWeather_Cloudiness
            // 
            this.labelWeather_Cloudiness.AutoSize = true;
            this.labelWeather_Cloudiness.Location = new System.Drawing.Point(27, 20);
            this.labelWeather_Cloudiness.Name = "labelWeather_Cloudiness";
            this.labelWeather_Cloudiness.Size = new System.Drawing.Size(38, 13);
            this.labelWeather_Cloudiness.TabIndex = 1;
            this.labelWeather_Cloudiness.Text = "Dichte";
            // 
            // Weather_CloudDensity
            // 
            this.Weather_CloudDensity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Weather_CloudDensity.FormattingEnabled = true;
            this.Weather_CloudDensity.Location = new System.Drawing.Point(89, 17);
            this.Weather_CloudDensity.Name = "Weather_CloudDensity";
            this.Weather_CloudDensity.Size = new System.Drawing.Size(243, 21);
            this.Weather_CloudDensity.TabIndex = 7;
            // 
            // labelWeather_CloudType
            // 
            this.labelWeather_CloudType.AutoSize = true;
            this.labelWeather_CloudType.Location = new System.Drawing.Point(7, 48);
            this.labelWeather_CloudType.Name = "labelWeather_CloudType";
            this.labelWeather_CloudType.Size = new System.Drawing.Size(80, 13);
            this.labelWeather_CloudType.TabIndex = 4;
            this.labelWeather_CloudType.Text = "Wolkengattung";
            // 
            // Weather_CloudType
            // 
            this.Weather_CloudType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Weather_CloudType.FormattingEnabled = true;
            this.Weather_CloudType.Location = new System.Drawing.Point(89, 45);
            this.Weather_CloudType.Name = "Weather_CloudType";
            this.Weather_CloudType.Size = new System.Drawing.Size(243, 21);
            this.Weather_CloudType.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Weather_A_H);
            this.groupBox3.Controls.Add(this.label52);
            this.groupBox3.Controls.Add(this.label54);
            this.groupBox3.Controls.Add(this.Weather_A_Pa);
            this.groupBox3.Controls.Add(this.Weather_A_T);
            this.groupBox3.Controls.Add(this.label53);
            this.groupBox3.Location = new System.Drawing.Point(6, 18);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(165, 92);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Luft";
            // 
            // Weather_A_H
            // 
            this.Weather_A_H.Location = new System.Drawing.Point(94, 65);
            this.Weather_A_H.Name = "Weather_A_H";
            this.Weather_A_H.Size = new System.Drawing.Size(52, 20);
            this.Weather_A_H.TabIndex = 41;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 17);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(57, 13);
            this.label52.TabIndex = 35;
            this.label52.Text = "Temp. [°C]";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(6, 67);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(80, 13);
            this.label54.TabIndex = 39;
            this.label54.Text = "rel. Feuchte [%]";
            // 
            // Weather_A_Pa
            // 
            this.Weather_A_Pa.Location = new System.Drawing.Point(94, 40);
            this.Weather_A_Pa.Name = "Weather_A_Pa";
            this.Weather_A_Pa.Size = new System.Drawing.Size(52, 20);
            this.Weather_A_Pa.TabIndex = 40;
            // 
            // Weather_A_T
            // 
            this.Weather_A_T.Location = new System.Drawing.Point(94, 16);
            this.Weather_A_T.Name = "Weather_A_T";
            this.Weather_A_T.Size = new System.Drawing.Size(52, 20);
            this.Weather_A_T.TabIndex = 36;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 42);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(64, 13);
            this.label53.TabIndex = 37;
            this.label53.Text = "Druck [hPa]";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 129);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "Bemerkung";
            // 
            // Weather_notes
            // 
            this.Weather_notes.Location = new System.Drawing.Point(78, 126);
            this.Weather_notes.Name = "Weather_notes";
            this.Weather_notes.Size = new System.Drawing.Size(291, 20);
            this.Weather_notes.TabIndex = 30;
            // 
            // gb_COND_SENSOR
            // 
            this.gb_COND_SENSOR.Controls.Add(this.label69);
            this.gb_COND_SENSOR.Controls.Add(this.label68);
            this.gb_COND_SENSOR.Controls.Add(this.Sens_Azimuth);
            this.gb_COND_SENSOR.Controls.Add(this.Sens_Zenith);
            this.gb_COND_SENSOR.Controls.Add(this.label67);
            this.gb_COND_SENSOR.Controls.Add(this.label66);
            this.gb_COND_SENSOR.Controls.Add(this.Sens_Height_AOL);
            this.gb_COND_SENSOR.Controls.Add(this.Sens_Height_AGL);
            this.gb_COND_SENSOR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_SENSOR.Location = new System.Drawing.Point(3, 241);
            this.gb_COND_SENSOR.Name = "gb_COND_SENSOR";
            this.gb_COND_SENSOR.Size = new System.Drawing.Size(751, 78);
            this.gb_COND_SENSOR.TabIndex = 5;
            this.gb_COND_SENSOR.TabStop = false;
            this.gb_COND_SENSOR.Text = "Sensor-Position und Ausrichtung";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(162, 49);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(81, 13);
            this.label69.TabIndex = 7;
            this.label69.Text = "Azimutwinkel [°]";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(162, 20);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(74, 13);
            this.label68.TabIndex = 6;
            this.label68.Text = "Zenitwinkel [°]";
            // 
            // Sens_Azimuth
            // 
            this.Sens_Azimuth.Location = new System.Drawing.Point(256, 46);
            this.Sens_Azimuth.Name = "Sens_Azimuth";
            this.Sens_Azimuth.Size = new System.Drawing.Size(43, 20);
            this.Sens_Azimuth.TabIndex = 5;
            // 
            // Sens_Zenith
            // 
            this.Sens_Zenith.Location = new System.Drawing.Point(256, 17);
            this.Sens_Zenith.Name = "Sens_Zenith";
            this.Sens_Zenith.Size = new System.Drawing.Size(43, 20);
            this.Sens_Zenith.TabIndex = 4;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(9, 46);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(91, 13);
            this.label67.TabIndex = 3;
            this.label67.Text = "Sensor-Objekt [m]";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(9, 20);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(89, 13);
            this.label66.TabIndex = 2;
            this.label66.Text = "Sensor-Grund [m]";
            // 
            // Sens_Height_AOL
            // 
            this.Sens_Height_AOL.Location = new System.Drawing.Point(103, 46);
            this.Sens_Height_AOL.Name = "Sens_Height_AOL";
            this.Sens_Height_AOL.Size = new System.Drawing.Size(43, 20);
            this.Sens_Height_AOL.TabIndex = 1;
            // 
            // Sens_Height_AGL
            // 
            this.Sens_Height_AGL.Location = new System.Drawing.Point(103, 17);
            this.Sens_Height_AGL.Name = "Sens_Height_AGL";
            this.Sens_Height_AGL.Size = new System.Drawing.Size(43, 20);
            this.Sens_Height_AGL.TabIndex = 0;
            // 
            // gb_COND_FOTOS
            // 
            this.gb_COND_FOTOS.AutoSize = true;
            this.gb_COND_FOTOS.Controls.Add(this.Photos_btRemovePhotos);
            this.gb_COND_FOTOS.Controls.Add(this.Photos_PreviewBox);
            this.gb_COND_FOTOS.Controls.Add(this.Photos_btAddPhotos);
            this.gb_COND_FOTOS.Controls.Add(this.Photos_DGV);
            this.gb_COND_FOTOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_FOTOS.Location = new System.Drawing.Point(3, 325);
            this.gb_COND_FOTOS.Name = "gb_COND_FOTOS";
            this.gb_COND_FOTOS.Size = new System.Drawing.Size(751, 210);
            this.gb_COND_FOTOS.TabIndex = 8;
            this.gb_COND_FOTOS.TabStop = false;
            this.gb_COND_FOTOS.Text = "Fotos (mit Bezug zu allen Spektren dieses Messganges)";
            // 
            // Photos_PreviewBox
            // 
            this.Photos_PreviewBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Photos_PreviewBox.Location = new System.Drawing.Point(524, 19);
            this.Photos_PreviewBox.Name = "Photos_PreviewBox";
            this.Photos_PreviewBox.Size = new System.Drawing.Size(221, 172);
            this.Photos_PreviewBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Photos_PreviewBox.TabIndex = 2;
            this.Photos_PreviewBox.TabStop = false;
            // 
            // Photos_btAddPhotos
            // 
            this.Photos_btAddPhotos.Location = new System.Drawing.Point(10, 20);
            this.Photos_btAddPhotos.Name = "Photos_btAddPhotos";
            this.Photos_btAddPhotos.Size = new System.Drawing.Size(87, 23);
            this.Photos_btAddPhotos.TabIndex = 1;
            this.Photos_btAddPhotos.Text = "Hinzufügen...";
            this.Photos_btAddPhotos.UseVisualStyleBackColor = true;
            this.Photos_btAddPhotos.Click += new System.EventHandler(this.Photos_btAddPhotos_Click);
            // 
            // Photos_DGV
            // 
            this.Photos_DGV.AllowUserToAddRows = false;
            this.Photos_DGV.AllowUserToOrderColumns = true;
            this.Photos_DGV.AllowUserToResizeRows = false;
            this.Photos_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Photos_DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.Photos_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Photos_DGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Photos_File,
            this.Photos_Type,
            this.Photos_Notes});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Photos_DGV.DefaultCellStyle = dataGridViewCellStyle8;
            this.Photos_DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.Photos_DGV.Location = new System.Drawing.Point(107, 19);
            this.Photos_DGV.MultiSelect = false;
            this.Photos_DGV.Name = "Photos_DGV";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Photos_DGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.Photos_DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Photos_DGV.Size = new System.Drawing.Size(411, 172);
            this.Photos_DGV.TabIndex = 0;
            this.Photos_DGV.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.Photos_DGV_RowsRemoved);
            this.Photos_DGV.SelectionChanged += new System.EventHandler(this.Photos_DGV_SelectionChanged);
            // 
            // Photos_File
            // 
            this.Photos_File.Connection = null;
            this.Photos_File.HeaderText = "Foto";
            this.Photos_File.Name = "Photos_File";
            this.Photos_File.ReadOnly = true;
            // 
            // Photos_Type
            // 
            this.Photos_Type.HeaderText = "Typ";
            this.Photos_Type.Name = "Photos_Type";
            // 
            // Photos_Notes
            // 
            this.Photos_Notes.HeaderText = "Beschreibung";
            this.Photos_Notes.Name = "Photos_Notes";
            // 
            // gb_COND_SOIL
            // 
            this.gb_COND_SOIL.Controls.Add(this.label23);
            this.gb_COND_SOIL.Controls.Add(this.Soil_Date);
            this.gb_COND_SOIL.Controls.Add(this.label10);
            this.gb_COND_SOIL.Controls.Add(this.Soil_GE_Bodenform);
            this.gb_COND_SOIL.Controls.Add(this.Soil_GE_Bezeichnung);
            this.gb_COND_SOIL.Controls.Add(this.label78);
            this.gb_COND_SOIL.Controls.Add(this.groupBox27);
            this.gb_COND_SOIL.Controls.Add(this.Soil_moisturetext);
            this.gb_COND_SOIL.Controls.Add(this.Soil_colortext);
            this.gb_COND_SOIL.Controls.Add(this.groupBox6);
            this.gb_COND_SOIL.Controls.Add(this.label27);
            this.gb_COND_SOIL.Controls.Add(this.label22);
            this.gb_COND_SOIL.Controls.Add(this.Soil_notes);
            this.gb_COND_SOIL.Controls.Add(this.label15);
            this.gb_COND_SOIL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_SOIL.Location = new System.Drawing.Point(3, 541);
            this.gb_COND_SOIL.Name = "gb_COND_SOIL";
            this.gb_COND_SOIL.Size = new System.Drawing.Size(751, 232);
            this.gb_COND_SOIL.TabIndex = 9;
            this.gb_COND_SOIL.TabStop = false;
            this.gb_COND_SOIL.Text = "Boden";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 135);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 26);
            this.label23.TabIndex = 36;
            this.label23.Text = "Datum der\r\nAufnahme";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "Bodenform";
            // 
            // Soil_GE_Bodenform
            // 
            this.Soil_GE_Bodenform.Location = new System.Drawing.Point(78, 49);
            this.Soil_GE_Bodenform.Name = "Soil_GE_Bodenform";
            this.Soil_GE_Bodenform.Size = new System.Drawing.Size(269, 20);
            this.Soil_GE_Bodenform.TabIndex = 34;
            // 
            // Soil_GE_Bezeichnung
            // 
            this.Soil_GE_Bezeichnung.Location = new System.Drawing.Point(78, 22);
            this.Soil_GE_Bezeichnung.Name = "Soil_GE_Bezeichnung";
            this.Soil_GE_Bezeichnung.Size = new System.Drawing.Size(269, 20);
            this.Soil_GE_Bezeichnung.TabIndex = 33;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(6, 26);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(69, 13);
            this.label78.TabIndex = 32;
            this.label78.Text = "Bezeichnung";
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.Soil_WRB_Description);
            this.groupBox27.Controls.Add(this.label20);
            this.groupBox27.Controls.Add(this.Soil_WRB_Specifier);
            this.groupBox27.Controls.Add(this.Soil_WRB_Qualifier);
            this.groupBox27.Controls.Add(this.Soil_WRB_SoilGroup);
            this.groupBox27.Controls.Add(this.label75);
            this.groupBox27.Controls.Add(this.label76);
            this.groupBox27.Controls.Add(this.label77);
            this.groupBox27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox27.Location = new System.Drawing.Point(364, 90);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(374, 137);
            this.groupBox27.TabIndex = 30;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "FAO World Reference Base";
            // 
            // Soil_WRB_Description
            // 
            this.Soil_WRB_Description.Location = new System.Drawing.Point(80, 101);
            this.Soil_WRB_Description.Name = "Soil_WRB_Description";
            this.Soil_WRB_Description.Size = new System.Drawing.Size(288, 20);
            this.Soil_WRB_Description.TabIndex = 36;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label20.Location = new System.Drawing.Point(6, 104);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(60, 13);
            this.label20.TabIndex = 14;
            this.label20.Text = "Description";
            // 
            // Soil_WRB_Specifier
            // 
            this.Soil_WRB_Specifier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Soil_WRB_Specifier.FormattingEnabled = true;
            this.Soil_WRB_Specifier.Location = new System.Drawing.Point(80, 75);
            this.Soil_WRB_Specifier.Name = "Soil_WRB_Specifier";
            this.Soil_WRB_Specifier.Size = new System.Drawing.Size(205, 21);
            this.Soil_WRB_Specifier.TabIndex = 13;
            // 
            // Soil_WRB_Qualifier
            // 
            this.Soil_WRB_Qualifier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Soil_WRB_Qualifier.FormattingEnabled = true;
            this.Soil_WRB_Qualifier.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.Soil_WRB_Qualifier.Location = new System.Drawing.Point(80, 47);
            this.Soil_WRB_Qualifier.Name = "Soil_WRB_Qualifier";
            this.Soil_WRB_Qualifier.Size = new System.Drawing.Size(205, 21);
            this.Soil_WRB_Qualifier.TabIndex = 12;
            // 
            // Soil_WRB_SoilGroup
            // 
            this.Soil_WRB_SoilGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Soil_WRB_SoilGroup.FormattingEnabled = true;
            this.Soil_WRB_SoilGroup.Location = new System.Drawing.Point(80, 19);
            this.Soil_WRB_SoilGroup.Name = "Soil_WRB_SoilGroup";
            this.Soil_WRB_SoilGroup.Size = new System.Drawing.Size(205, 21);
            this.Soil_WRB_SoilGroup.TabIndex = 11;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label75.Location = new System.Drawing.Point(6, 51);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(45, 13);
            this.label75.TabIndex = 9;
            this.label75.Text = "Qualifier";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label76.Location = new System.Drawing.Point(6, 23);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(56, 13);
            this.label76.TabIndex = 8;
            this.label76.Text = "Soil Group";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label77.Location = new System.Drawing.Point(6, 79);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(48, 13);
            this.label77.TabIndex = 10;
            this.label77.Text = "Specifier";
            // 
            // Soil_moisturetext
            // 
            this.Soil_moisturetext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Soil_moisturetext.FormattingEnabled = true;
            this.Soil_moisturetext.Location = new System.Drawing.Point(78, 75);
            this.Soil_moisturetext.Name = "Soil_moisturetext";
            this.Soil_moisturetext.Size = new System.Drawing.Size(269, 21);
            this.Soil_moisturetext.TabIndex = 31;
            // 
            // Soil_colortext
            // 
            this.Soil_colortext.Location = new System.Drawing.Point(78, 103);
            this.Soil_colortext.Name = "Soil_colortext";
            this.Soil_colortext.Size = new System.Drawing.Size(269, 20);
            this.Soil_colortext.TabIndex = 30;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.Soil_munsell_chroma);
            this.groupBox6.Controls.Add(this.Soil_munsell_value);
            this.groupBox6.Controls.Add(this.Soil_munsell_hue);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox6.Location = new System.Drawing.Point(364, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(374, 65);
            this.groupBox6.TabIndex = 29;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Munselltafel";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(56, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(81, 13);
            this.label24.TabIndex = 18;
            this.label24.Text = "Value / Chroma";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(27, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "Hue";
            // 
            // Soil_munsell_chroma
            // 
            this.Soil_munsell_chroma.Location = new System.Drawing.Point(100, 39);
            this.Soil_munsell_chroma.Name = "Soil_munsell_chroma";
            this.Soil_munsell_chroma.Size = new System.Drawing.Size(30, 20);
            this.Soil_munsell_chroma.TabIndex = 16;
            // 
            // Soil_munsell_value
            // 
            this.Soil_munsell_value.Location = new System.Drawing.Point(56, 39);
            this.Soil_munsell_value.Name = "Soil_munsell_value";
            this.Soil_munsell_value.Size = new System.Drawing.Size(26, 20);
            this.Soil_munsell_value.TabIndex = 15;
            // 
            // Soil_munsell_hue
            // 
            this.Soil_munsell_hue.Location = new System.Drawing.Point(11, 39);
            this.Soil_munsell_hue.Name = "Soil_munsell_hue";
            this.Soil_munsell_hue.Size = new System.Drawing.Size(39, 20);
            this.Soil_munsell_hue.TabIndex = 14;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label19.Location = new System.Drawing.Point(86, 42);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(12, 13);
            this.label19.TabIndex = 10;
            this.label19.Text = "/";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 173);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(61, 13);
            this.label27.TabIndex = 28;
            this.label27.Text = "Bemerkung";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 78);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 11;
            this.label22.Text = "Feuchte";
            // 
            // Soil_notes
            // 
            this.Soil_notes.Location = new System.Drawing.Point(79, 170);
            this.Soil_notes.Name = "Soil_notes";
            this.Soil_notes.Size = new System.Drawing.Size(268, 20);
            this.Soil_notes.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 105);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Farbe";
            // 
            // gb_COND_ILLUM
            // 
            this.gb_COND_ILLUM.Controls.Add(this.label81);
            this.gb_COND_ILLUM.Controls.Add(this.label80);
            this.gb_COND_ILLUM.Controls.Add(this.Illum_notesIllum);
            this.gb_COND_ILLUM.Controls.Add(this.Illum_notes);
            this.gb_COND_ILLUM.Controls.Add(this.label70);
            this.gb_COND_ILLUM.Controls.Add(this.label71);
            this.gb_COND_ILLUM.Controls.Add(this.Illum_azimuth);
            this.gb_COND_ILLUM.Controls.Add(this.Illum_zenith);
            this.gb_COND_ILLUM.Controls.Add(this.label72);
            this.gb_COND_ILLUM.Controls.Add(this.label73);
            this.gb_COND_ILLUM.Controls.Add(this.Illum_illumn);
            this.gb_COND_ILLUM.Controls.Add(this.Illum_Source);
            this.gb_COND_ILLUM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_COND_ILLUM.Location = new System.Drawing.Point(3, 779);
            this.gb_COND_ILLUM.Name = "gb_COND_ILLUM";
            this.gb_COND_ILLUM.Size = new System.Drawing.Size(345, 135);
            this.gb_COND_ILLUM.TabIndex = 8;
            this.gb_COND_ILLUM.TabStop = false;
            this.gb_COND_ILLUM.Text = "Strahlungserhältnisse / Beleuchtung";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(6, 106);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(73, 13);
            this.label81.TabIndex = 11;
            this.label81.Text = "Bemerkungen";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(11, 78);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(50, 13);
            this.label80.TabIndex = 10;
            this.label80.Text = "Luxmeter";
            // 
            // Illum_notesIllum
            // 
            this.Illum_notesIllum.Location = new System.Drawing.Point(85, 75);
            this.Illum_notesIllum.Name = "Illum_notesIllum";
            this.Illum_notesIllum.Size = new System.Drawing.Size(246, 20);
            this.Illum_notesIllum.TabIndex = 9;
            // 
            // Illum_notes
            // 
            this.Illum_notes.Location = new System.Drawing.Point(85, 103);
            this.Illum_notes.Name = "Illum_notes";
            this.Illum_notes.Size = new System.Drawing.Size(246, 20);
            this.Illum_notes.TabIndex = 8;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(194, 49);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(81, 13);
            this.label70.TabIndex = 7;
            this.label70.Text = "Azimutwinkel [°]";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(194, 20);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(74, 13);
            this.label71.TabIndex = 6;
            this.label71.Text = "Zenitwinkel [°]";
            // 
            // Illum_azimuth
            // 
            this.Illum_azimuth.Location = new System.Drawing.Point(288, 46);
            this.Illum_azimuth.Name = "Illum_azimuth";
            this.Illum_azimuth.Size = new System.Drawing.Size(43, 20);
            this.Illum_azimuth.TabIndex = 5;
            // 
            // Illum_zenith
            // 
            this.Illum_zenith.Location = new System.Drawing.Point(288, 17);
            this.Illum_zenith.Name = "Illum_zenith";
            this.Illum_zenith.Size = new System.Drawing.Size(43, 20);
            this.Illum_zenith.TabIndex = 4;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(9, 46);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(60, 13);
            this.label72.TabIndex = 3;
            this.label72.Text = "Stärke [lux]";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(9, 20);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(43, 13);
            this.label73.TabIndex = 2;
            this.label73.Text = "Quelle";
            // 
            // Illum_illumn
            // 
            this.Illum_illumn.Location = new System.Drawing.Point(85, 46);
            this.Illum_illumn.Name = "Illum_illumn";
            this.Illum_illumn.Size = new System.Drawing.Size(81, 20);
            this.Illum_illumn.TabIndex = 1;
            // 
            // Illum_Source
            // 
            this.Illum_Source.Location = new System.Drawing.Point(85, 17);
            this.Illum_Source.MaxLength = 10;
            this.Illum_Source.Name = "Illum_Source";
            this.Illum_Source.Size = new System.Drawing.Size(81, 20);
            this.Illum_Source.TabIndex = 0;
            // 
            // flp_MAIN
            // 
            this.flp_MAIN.AutoScroll = true;
            this.flp_MAIN.AutoSize = true;
            this.flp_MAIN.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flp_MAIN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flp_MAIN.Controls.Add(this.gb_SPECTRA);
            this.flp_MAIN.Controls.Add(this.gb_INSTRUMENTS);
            this.flp_MAIN.Controls.Add(this.gb_SITUATION);
            this.flp_MAIN.Controls.Add(this.gb_TITLE);
            this.flp_MAIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flp_MAIN.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flp_MAIN.Location = new System.Drawing.Point(3, 3);
            this.flp_MAIN.Name = "flp_MAIN";
            this.flp_MAIN.Size = new System.Drawing.Size(813, 436);
            this.flp_MAIN.TabIndex = 4;
            this.flp_MAIN.WrapContents = false;
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Fortschritt,
            this.TSL_ProgressBar,
            this.TSL_Action});
            this.StatusStrip.Location = new System.Drawing.Point(0, 616);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(819, 22);
            this.StatusStrip.TabIndex = 5;
            this.StatusStrip.Text = "statusStrip1";
            // 
            // Fortschritt
            // 
            this.Fortschritt.Name = "Fortschritt";
            this.Fortschritt.Size = new System.Drawing.Size(61, 17);
            this.Fortschritt.Text = "Fortschritt";
            // 
            // TSL_ProgressBar
            // 
            this.TSL_ProgressBar.Name = "TSL_ProgressBar";
            this.TSL_ProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // TSL_Action
            // 
            this.TSL_Action.Name = "TSL_Action";
            this.TSL_Action.Size = new System.Drawing.Size(0, 17);
            // 
            // OFD
            // 
            this.OFD.FileName = "openFileDialog1";
            // 
            // timer
            // 
            this.timer.Interval = 500;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flp_MAIN, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(819, 592);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bt_INSERT);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 445);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(813, 144);
            this.panel2.TabIndex = 5;
            // 
            // bt_INSERT
            // 
            this.bt_INSERT.Enabled = false;
            this.bt_INSERT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_INSERT.Location = new System.Drawing.Point(682, 51);
            this.bt_INSERT.Name = "bt_INSERT";
            this.bt_INSERT.Size = new System.Drawing.Size(122, 87);
            this.bt_INSERT.TabIndex = 36;
            this.bt_INSERT.Text = "Einfügen in die Datenbank";
            this.bt_INSERT.UseVisualStyleBackColor = true;
            this.bt_INSERT.Click += new System.EventHandler(this.bt_INSERT_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RTB);
            this.groupBox2.Location = new System.Drawing.Point(4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(672, 137);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SQL-Kommandos";
            // 
            // RTB
            // 
            this.RTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RTB.Location = new System.Drawing.Point(3, 16);
            this.RTB.Name = "RTB";
            this.RTB.ReadOnly = true;
            this.RTB.Size = new System.Drawing.Size(666, 118);
            this.RTB.TabIndex = 4;
            this.RTB.Text = "";
            // 
            // IP
            // 
            this.IP.ContainerControl = this;
            // 
            // contextMenuSpectra
            // 
            this.contextMenuSpectra.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_ShowSpectra,
            this.tsmi_RemoveSpectra});
            this.contextMenuSpectra.Name = "contextMenuSpectra";
            this.contextMenuSpectra.Size = new System.Drawing.Size(221, 48);
            // 
            // tsmi_ShowSpectra
            // 
            this.tsmi_ShowSpectra.Name = "tsmi_ShowSpectra";
            this.tsmi_ShowSpectra.Size = new System.Drawing.Size(220, 22);
            this.tsmi_ShowSpectra.Text = "Zeige markierte Spektren";
            // 
            // tsmi_RemoveSpectra
            // 
            this.tsmi_RemoveSpectra.Name = "tsmi_RemoveSpectra";
            this.tsmi_RemoveSpectra.Size = new System.Drawing.Size(220, 22);
            this.tsmi_RemoveSpectra.Text = "Entferne markierte Spektren";
            // 
            // contextMenuStripWRSpectra
            // 
            this.contextMenuStripWRSpectra.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiWRShowSpectra,
            this.tsmiWRRemoveSpectra});
            this.contextMenuStripWRSpectra.Name = "contextMenuStripWRSpectra";
            this.contextMenuStripWRSpectra.Size = new System.Drawing.Size(221, 48);
            // 
            // tsmiWRShowSpectra
            // 
            this.tsmiWRShowSpectra.Name = "tsmiWRShowSpectra";
            this.tsmiWRShowSpectra.Size = new System.Drawing.Size(220, 22);
            this.tsmiWRShowSpectra.Text = "Zeige markierte Spektren";
            // 
            // tsmiWRRemoveSpectra
            // 
            this.tsmiWRRemoveSpectra.Name = "tsmiWRRemoveSpectra";
            this.tsmiWRRemoveSpectra.Size = new System.Drawing.Size(220, 22);
            this.tsmiWRRemoveSpectra.Text = "Entferne markierte Spektren";
            // 
            // dataGridViewSpectrumColumn1
            // 
            this.dataGridViewSpectrumColumn1.HeaderText = "WR Spektren";
            this.dataGridViewSpectrumColumn1.Name = "dataGridViewSpectrumColumn1";
            this.dataGridViewSpectrumColumn1.ReadOnly = true;
            this.dataGridViewSpectrumColumn1.ToolStripLabel_ShowSpectra = false;
            this.dataGridViewSpectrumColumn1.Width = 271;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Art";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 270;
            // 
            // dataGridViewSpectrumColumn2
            // 
            this.dataGridViewSpectrumColumn2.HeaderText = "Feldspektren";
            this.dataGridViewSpectrumColumn2.Name = "dataGridViewSpectrumColumn2";
            this.dataGridViewSpectrumColumn2.ReadOnly = true;
            this.dataGridViewSpectrumColumn2.ToolStripLabel_ShowSpectra = false;
            // 
            // dataGridViewSpectrumColumn3
            // 
            this.dataGridViewSpectrumColumn3.HeaderText = "Jumpkorrektur";
            this.dataGridViewSpectrumColumn3.Name = "dataGridViewSpectrumColumn3";
            this.dataGridViewSpectrumColumn3.ReadOnly = true;
            this.dataGridViewSpectrumColumn3.ToolStripLabel_ShowSpectra = false;
            // 
            // dataGridViewSpectrumColumn4
            // 
            this.dataGridViewSpectrumColumn4.HeaderText = "HyMap resampled";
            this.dataGridViewSpectrumColumn4.Name = "dataGridViewSpectrumColumn4";
            this.dataGridViewSpectrumColumn4.ReadOnly = true;
            this.dataGridViewSpectrumColumn4.ToolStripLabel_ShowSpectra = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Phän. Tag";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ToolTipText = "Phänologischer Tag";
            // 
            // dataGridViewPhotoColumn1
            // 
            this.dataGridViewPhotoColumn1.Connection = null;
            this.dataGridViewPhotoColumn1.HeaderText = "Foto";
            this.dataGridViewPhotoColumn1.Name = "dataGridViewPhotoColumn1";
            this.dataGridViewPhotoColumn1.ReadOnly = true;
            this.dataGridViewPhotoColumn1.Width = 123;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Phän. Phase";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ToolTipText = "Phänologische Phase";
            this.dataGridViewTextBoxColumn3.Width = 123;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Bemerkungen";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Anmerkungen";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ToolTipText = "Phänologischer Tag";
            this.dataGridViewTextBoxColumn5.Width = 270;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Anmerkungen (Spektrum)";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ToolTipText = "Phänologische Phase";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Bemerkungen";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // SubInsertMessprotokollGFZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 638);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.menuStrip2);
            this.Name = "SubInsertMessprotokollGFZ";
            this.Text = "Eingabe Messprotokoll GFZ";
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.gb_VEG.ResumeLayout(false);
            this.gb_VEG.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.VEG_controlpanel.ResumeLayout(false);
            this.VEG_controlpanel.PerformLayout();
            this.gb_VEG_PS.ResumeLayout(false);
            this.gb_VEG_PS.PerformLayout();
            this.Veg_S_gpSpecies.ResumeLayout(false);
            this.Veg_S_gpSpecies.PerformLayout();
            this.Veg_S_gpCover.ResumeLayout(false);
            this.Veg_S_gpCover.PerformLayout();
            this.Veg_S_gpHeights.ResumeLayout(false);
            this.Veg_S_gpHeights.PerformLayout();
            this.gb_VEG_SP.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Veg_P_DGV)).EndInit();
            this.gb_SPECTRA.ResumeLayout(false);
            this.gb_SPECTRA_WREF.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MP_WREF_DGV)).EndInit();
            this.gb_SPECTRA_OBJ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MP_SPECTRA_DGV)).EndInit();
            this.gb_TITLE.ResumeLayout(false);
            this.gb_TITLE.PerformLayout();
            this.gb_POS.ResumeLayout(false);
            this.gb_POS.PerformLayout();
            this.POS_flp.ResumeLayout(false);
            this.POS_flp.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.gb_POS_OBSAREA.ResumeLayout(false);
            this.gb_POS_OBSAREA.PerformLayout();
            this.gb_POS_newlocation.ResumeLayout(false);
            this.gb_POS_newlocation.PerformLayout();
            this.gb_TITLE_COMMON.ResumeLayout(false);
            this.gb_TITLE_COMMON.PerformLayout();
            this.gb_INSTRUMENTS.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.gb_SITUATION.ResumeLayout(false);
            this.gb_SITUATION.PerformLayout();
            this.flp_Aufnahmesituation.ResumeLayout(false);
            this.flp_Aufnahmesituation.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gb_COND_WEATHER.ResumeLayout(false);
            this.gb_COND_WEATHER.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gb_COND_SENSOR.ResumeLayout(false);
            this.gb_COND_SENSOR.PerformLayout();
            this.gb_COND_FOTOS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Photos_PreviewBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Photos_DGV)).EndInit();
            this.gb_COND_SOIL.ResumeLayout(false);
            this.gb_COND_SOIL.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.gb_COND_ILLUM.ResumeLayout(false);
            this.gb_COND_ILLUM.PerformLayout();
            this.flp_MAIN.ResumeLayout(false);
            this.flp_MAIN.PerformLayout();
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IP)).EndInit();
            this.contextMenuSpectra.ResumeLayout(false);
            this.contextMenuStripWRSpectra.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.ToolTip TT;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.GroupBox gb_TITLE;
        private System.Windows.Forms.GroupBox gb_SITUATION;
        private System.Windows.Forms.GroupBox gb_VEG;
        private System.Windows.Forms.GroupBox gb_SPECTRA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Ti_Institute_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker Time_Date;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox gb_TITLE_COMMON;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox Ti_Campaign_cb;
        private System.Windows.Forms.GroupBox gb_COND_WEATHER;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label labelWeather_Cloudiness;
        private System.Windows.Forms.ComboBox Weather_CloudDensity;
        private System.Windows.Forms.ComboBox Weather_LastPrecip;
        private System.Windows.Forms.ComboBox Weather_CloudType;
        private System.Windows.Forms.Label labelWeather_CloudType;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Weather_notes;
        private System.Windows.Forms.GroupBox gb_VEG_SP;
        private System.Windows.Forms.GroupBox gb_VEG_PS;
        private System.Windows.Forms.Button Veg_S_EUNIS_btAdd;
        private System.Windows.Forms.Button Veg_S_BB_btAdd;
        private System.Windows.Forms.TextBox Veg_S_EUNIS_tb;
        private System.Windows.Forms.TextBox Veg_S_BB_tb;
        private System.Windows.Forms.GroupBox Veg_S_gpCover;
        private System.Windows.Forms.GroupBox Veg_S_gpHeights;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox Veg_S_C_oSoil;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox Veg_S_C_Litter;
        private System.Windows.Forms.TextBox Veg_S_NOS_Total;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox Veg_S_Notes;
        private System.Windows.Forms.DataGridView Veg_P_DGV;
        private System.Windows.Forms.GroupBox gb_INSTRUMENTS;
        private System.Windows.Forms.ComboBox Photo_Camera;
        private System.Windows.Forms.ComboBox WRef_Panel;
        private System.Windows.Forms.Button Sensor_readSensor;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox gb_SPECTRA_WREF;
        private System.Windows.Forms.DataGridView MP_WREF_DGV;
        private System.Windows.Forms.Button MP_WREF_btReadSpectra;
        private System.Windows.Forms.GroupBox gb_SPECTRA_OBJ;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.ComboBox Sensor_type;
        private System.Windows.Forms.ComboBox Spec_operator;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.ComboBox Photo_Operator;
        private System.Windows.Forms.TextBox Spec_Optic;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel Fortschritt;
        private System.Windows.Forms.ToolStripProgressBar TSL_ProgressBar;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DateTimePicker Time_TimeStart;
        private System.Windows.Forms.DateTimePicker Time_TimeEnd;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox Weather_A_Pa;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox Weather_A_T;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox Weather_W_dir;
        private System.Windows.Forms.TextBox Weather_W_v;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox Weather_A_H;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.GroupBox Veg_S_gpSpecies;
        private System.Windows.Forms.TextBox Veg_S_NOS_Trees2;
        private System.Windows.Forms.TextBox Veg_S_NOS_Trees1;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox Veg_S_NOS_Crypto;
        private System.Windows.Forms.TextBox Veg_S_NOS_Herbs;
        private System.Windows.Forms.TextBox Veg_S_NOS_Shrubs;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox Veg_S_C_Crypto;
        private System.Windows.Forms.TextBox Veg_S_C_Herbs;
        private System.Windows.Forms.TextBox Veg_S_C_Shrubs;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox Veg_S_C_Total;
        private System.Windows.Forms.TextBox Veg_S_C_Trees2;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox Veg_S_C_Trees1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox Veg_S_H_Herbs;
        private System.Windows.Forms.TextBox Veg_S_H_Shrubs;
        private System.Windows.Forms.TextBox Veg_S_H_Trees2;
        private System.Windows.Forms.TextBox Veg_S_H_Trees1;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.ComboBox Spec_Source;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.GroupBox gb_COND_SENSOR;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox Sens_Height_AOL;
        private System.Windows.Forms.TextBox Sens_Height_AGL;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox Sens_Azimuth;
        private System.Windows.Forms.TextBox Sens_Zenith;
        private System.Windows.Forms.GroupBox gb_COND_ILLUM;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox Illum_azimuth;
        private System.Windows.Forms.TextBox Illum_zenith;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox Illum_illumn;
        private System.Windows.Forms.TextBox Illum_Source;
        private System.Windows.Forms.ComboBox Ti_OperatorINSERT;
        private System.Windows.Forms.TextBox Ti_notes;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox Illum_notes;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox Illum_notesIllum;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.ToolStripStatusLabel TSL_Action;
        private System.Windows.Forms.GroupBox gb_COND_FOTOS;
        private System.Windows.Forms.DataGridView Photos_DGV;
        private System.Windows.Forms.Button Photos_btAddPhotos;
        private System.Windows.Forms.PictureBox Photos_PreviewBox;
        private System.Windows.Forms.Button gb_SPECTRA_OBJ_btAdd;
        private System.Windows.Forms.DataGridView MP_SPECTRA_DGV;
        private System.Windows.Forms.ToolStripMenuItem xlsEinlesenToolStripMenuItem;
        private System.Windows.Forms.GroupBox gb_POS_OBSAREA;
        private System.Windows.Forms.Button AREA_ID_bt;
        private System.Windows.Forms.GroupBox gb_COND_SOIL;
        private System.Windows.Forms.TextBox Soil_GE_Bezeichnung;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.ComboBox Soil_WRB_Specifier;
        private System.Windows.Forms.ComboBox Soil_WRB_Qualifier;
        private System.Windows.Forms.ComboBox Soil_WRB_SoilGroup;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.ComboBox Soil_moisturetext;
        private System.Windows.Forms.TextBox Soil_colortext;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox Soil_munsell_chroma;
        private System.Windows.Forms.TextBox Soil_munsell_value;
        private System.Windows.Forms.TextBox Soil_munsell_hue;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox Soil_notes;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox AREA_ID_DB_tb;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TextBox AREA_ID_INFO_tb;
        private System.Windows.Forms.FlowLayoutPanel flp_MAIN;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Panel VEG_controlpanel;
        private System.Windows.Forms.CheckBox gb_VEG_SP_cb;
        private System.Windows.Forms.CheckBox gb_VEG_PS_cb;
        private System.Windows.Forms.FlowLayoutPanel flp_Aufnahmesituation;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox gb_COND_FOTOS_cb;
        private System.Windows.Forms.CheckBox gb_COND_SENSOR_cb;
        private System.Windows.Forms.CheckBox gb_COND_WEATHER_cb;
        private System.Windows.Forms.CheckBox gb_COND_ILLUM_cb;
        private System.Windows.Forms.CheckBox gb_COND_SOIL_cb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox AREA_ID_GFZ_tb;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox RTB;
        private System.Windows.Forms.Button bt_INSERT;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.ErrorProvider IP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Soil_GE_Bodenform;
        private System.Windows.Forms.GroupBox gb_POS;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton gb_POS_newlocation_rb;
        private System.Windows.Forms.RadioButton gb_POS_OBSAREA_rb;
        private System.Windows.Forms.RadioButton gb_POS_none_rb;
        private System.Windows.Forms.GroupBox gb_POS_newlocation;
        private System.Windows.Forms.Panel Loc_Coord;
        private System.Windows.Forms.TextBox Loc_Notes;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Loc_Aspect;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Loc_Locname;
        private System.Windows.Forms.TextBox Loc_Slope;
        private System.Windows.Forms.FlowLayoutPanel POS_flp;
        private System.Windows.Forms.TextBox Veg_S_BfNCode_tb;
        private System.Windows.Forms.Button Veg_S_BfNCode_btAdd;
        private System.Windows.Forms.CheckBox gb_COND_VEGETATION_cb;
        private System.Windows.Forms.TextBox Soil_WRB_Description;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DateTimePicker Soil_Date;
        private System.Windows.Forms.ToolStripMenuItem eingabenZurücksetzenToolStripMenuItem;
        private System.Windows.Forms.DataGridViewButtonColumn SPl_Col_BtSpeciesID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPl_Col_Species;
        private System.Windows.Forms.DataGridViewComboBoxColumn SPl_Col_Cover;
        private System.Windows.Forms.DataGridViewComboBoxColumn SPl_Col_Sociability;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPl_Col_PhenDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPl_Col_PhenPhase;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPl_Col_Notes;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button gb_VEG_SP_btAdd;
        private System.Windows.Forms.Button gb_VEG_SP_btRemove;
        private System.Windows.Forms.Button Photos_btRemovePhotos;
        private System.Windows.Forms.Button gb_SPECTRA_OBJ_btRemove;
        private DGVExtensions.DataGridViewPhotoColumn Photos_File;
        private System.Windows.Forms.DataGridViewComboBoxColumn Photos_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Photos_Notes;
        private System.Windows.Forms.Button MP_WREF_btRemoveSpectra;
        private DGVExtensions.DataGridViewSpectrumColumn SPEC_Field;
        private DGVExtensions.DataGridViewSpectrumColumn SPEC_Jump;
        private DGVExtensions.DataGridViewSpectrumColumn SPEC_HyMap;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPEC_Notes;
        private DGVExtensions.DataGridViewSpectrumColumn WR_Files;
        private System.Windows.Forms.DataGridViewTextBoxColumn WR_Description;
        private System.Windows.Forms.ContextMenuStrip contextMenuSpectra;
        private System.Windows.Forms.ToolStripMenuItem tsmi_ShowSpectra;
        private System.Windows.Forms.ToolStripMenuItem tsmi_RemoveSpectra;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripWRSpectra;
        private System.Windows.Forms.ToolStripMenuItem tsmiWRShowSpectra;
        private System.Windows.Forms.ToolStripMenuItem tsmiWRRemoveSpectra;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label18;
        private DGVExtensions.DataGridViewSpectrumColumn dataGridViewSpectrumColumn1;
        private DGVExtensions.DataGridViewSpectrumColumn dataGridViewSpectrumColumn2;
        private DGVExtensions.DataGridViewSpectrumColumn dataGridViewSpectrumColumn3;
        private DGVExtensions.DataGridViewSpectrumColumn dataGridViewSpectrumColumn4;
        private DGVExtensions.DataGridViewPhotoColumn dataGridViewPhotoColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Button Veg_S_BfNCode_btRemove;
        private System.Windows.Forms.Button Veg_S_EUNIS_btRemove;
        private System.Windows.Forms.Button Veg_S_BB_btRemove;
    }
}