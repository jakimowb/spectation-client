﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SpectationClient.DGVExtensions;
using SpectationClient.Stuff;
using SpectationClient.SpectralTools;

namespace SpectationClient.GUI {
    public partial class SelectSpectraForm : Form {



        private Random random = new Random(42);

        public SelectSpectraForm() {
            InitializeComponent();
            init();
        }

       

        /// <summary>
        /// Gets or sets the form title.
        /// </summary>
        public String Title {
            set { this.Title = value; }
            get { return this.Title; }
        }

        private void init() {

            this.Icon = Resources.Icon;
            DGV.SelectionChanged +=new EventHandler(DGV_SelectionChanged);
            UC_SpectralViewer.linkToSpectrumColumn(SPEC, COLOR);
        }

        private void DGV_SelectionChanged(object sender, EventArgs e) {
            this.btOK.Enabled = DGV.SelectedRows.Count > 0;
        }

        public List<Spectrum> getSelectedSpectra() {
            List<Spectrum> spectra = new List<Spectrum>();
            int iSpec = SPEC.Index;
            
            foreach(DataGridViewRow row in DGV.Rows) {
                if(row.Selected) {
                    DGVExtensions.DataGridViewSpectrumCell cell = row.Cells[iSpec] as DGVExtensions.DataGridViewSpectrumCell;
                    if(cell != null && cell.Value != null) spectra.Add(cell.getSpectrum());
                }
            }
            return spectra;
        }

        public bool addSpectra(IEnumerable<String> fileNames) {

            var spectralFiles = (from path in fileNames
                                 let isASD = ASD_Reader.isValidASDSpectrum(path)
                                 let isESL = ESL_Reader.isValidFile(path)
                                 where File.Exists(path) && (isASD || isESL)
                                 select new {
                                     fileInfo = new FileInfo(path),
                                     fileType = isASD ? Spectrum.SpectrumFileType.ASD :
                                                        Spectrum.SpectrumFileType.ESL
                                 }
                                );



            List<Spectrum> newSpectra = new List<Spectrum>();
            foreach(var info in spectralFiles) {
                String fileName = info.fileInfo.Name;
                switch(info.fileType) {
                    case Spectrum.SpectrumFileType.ESL:
                        FileInfo headerFile;
                        FileInfo binaryFile;
                        bool b = ENVI_FileReader.findFileInfos(info.fileInfo, out binaryFile, out headerFile);
                        if(ESL_Reader.isValidFile(binaryFile)) {
                            ESL_Reader eslReader2 = new ESL_Reader(binaryFile, headerFile);
                            newSpectra.AddRange(eslReader2.readSpectra());
                        }

                        break;
                    case Spectrum.SpectrumFileType.ASD:
                        newSpectra.Add(ASD_Reader.readSpectrum(info.fileInfo));
                        break;
                }
            }

            return this.addSpectra(newSpectra);
        }

        private bool addSpectra(IEnumerable<Spectrum> spectra) {
            foreach(Spectrum spectrum in spectra) {
                int nextRow = this.DGV.Rows.Add(new Object[] { spectrum, getColor(), spectrum.FileName });
            }
            return spectra.Count() > 0;
        }

        private Color getColor() {
            Color x = Color.ForestGreen;
            return Color.FromArgb(255,
                this.random.Next(0, 255),
                this.random.Next(0, 255),
                this.random.Next(0, 255)
                );

        }

        private void btOK_Click(object sender, EventArgs e) {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
