﻿namespace SpectationClient.GUI
{
    partial class SubReadMessprotokollGFZXLS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sheetlist_label = new System.Windows.Forms.Label();
            this.sheetlist_cb = new System.Windows.Forms.ComboBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.Optionen = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbDecimalPoints = new System.Windows.Forms.ComboBox();
            this.btCancel = new System.Windows.Forms.Button();
            this.btReady2Read = new System.Windows.Forms.Button();
            this.RTB = new System.Windows.Forms.RichTextBox();
            this.DGVDockPanel = new System.Windows.Forms.Panel();
            this.DGV = new System.Windows.Forms.DataGridView();
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Optionen.SuspendLayout();
            this.DGVDockPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 175F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.RTB, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.DGVDockPanel, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 149F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(920, 449);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.groupBox1);
            this.flowLayoutPanel1.Controls.Add(this.Optionen);
            this.flowLayoutPanel1.Controls.Add(this.btCancel);
            this.flowLayoutPanel1.Controls.Add(this.btReady2Read);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(748, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(169, 294);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sheetlist_label);
            this.groupBox1.Controls.Add(this.sheetlist_cb);
            this.groupBox1.Controls.Add(this.btnOpen);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(161, 100);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datenquelle";
            // 
            // sheetlist_label
            // 
            this.sheetlist_label.AutoSize = true;
            this.sheetlist_label.Enabled = false;
            this.sheetlist_label.Location = new System.Drawing.Point(6, 45);
            this.sheetlist_label.Name = "sheetlist_label";
            this.sheetlist_label.Size = new System.Drawing.Size(85, 13);
            this.sheetlist_label.TabIndex = 6;
            this.sheetlist_label.Text = "Blatt auswählen:";
            // 
            // sheetlist_cb
            // 
            this.sheetlist_cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sheetlist_cb.Enabled = false;
            this.sheetlist_cb.FormattingEnabled = true;
            this.sheetlist_cb.Location = new System.Drawing.Point(6, 61);
            this.sheetlist_cb.Name = "sheetlist_cb";
            this.sheetlist_cb.Size = new System.Drawing.Size(136, 21);
            this.sheetlist_cb.TabIndex = 5;
            this.sheetlist_cb.SelectedIndexChanged += new System.EventHandler(this.cbSheets_SelectedIndexChanged);
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(6, 19);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(149, 23);
            this.btnOpen.TabIndex = 1;
            this.btnOpen.Text = "Excel Datei öffnen";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // Optionen
            // 
            this.Optionen.Controls.Add(this.label1);
            this.Optionen.Controls.Add(this.cbDecimalPoints);
            this.Optionen.Location = new System.Drawing.Point(3, 109);
            this.Optionen.Name = "Optionen";
            this.Optionen.Size = new System.Drawing.Size(161, 79);
            this.Optionen.TabIndex = 8;
            this.Optionen.TabStop = false;
            this.Optionen.Text = "Optionen";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Dezimalstellen durch:";
            // 
            // cbDecimalPoints
            // 
            this.cbDecimalPoints.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDecimalPoints.FormattingEnabled = true;
            this.cbDecimalPoints.Location = new System.Drawing.Point(9, 36);
            this.cbDecimalPoints.Name = "cbDecimalPoints";
            this.cbDecimalPoints.Size = new System.Drawing.Size(108, 21);
            this.cbDecimalPoints.TabIndex = 0;
            this.cbDecimalPoints.SelectedIndexChanged += new System.EventHandler(this.cbDecimalPoints_SelectedIndexChanged);
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(3, 194);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(161, 23);
            this.btCancel.TabIndex = 4;
            this.btCancel.Text = "Abbruch";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btReady2Read
            // 
            this.btReady2Read.Location = new System.Drawing.Point(3, 223);
            this.btReady2Read.Name = "btReady2Read";
            this.btReady2Read.Size = new System.Drawing.Size(161, 52);
            this.btReady2Read.TabIndex = 4;
            this.btReady2Read.Text = "Werte einlesen";
            this.btReady2Read.UseVisualStyleBackColor = true;
            this.btReady2Read.Click += new System.EventHandler(this.btOK_Click);
            // 
            // RTB
            // 
            this.RTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RTB.Location = new System.Drawing.Point(3, 303);
            this.RTB.Name = "RTB";
            this.RTB.ReadOnly = true;
            this.RTB.Size = new System.Drawing.Size(739, 143);
            this.RTB.TabIndex = 4;
            this.RTB.Text = "";
            // 
            // DGVDockPanel
            // 
            this.DGVDockPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DGVDockPanel.Controls.Add(this.DGV);
            this.DGVDockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVDockPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.DGVDockPanel.Location = new System.Drawing.Point(3, 3);
            this.DGVDockPanel.Name = "DGVDockPanel";
            this.DGVDockPanel.Size = new System.Drawing.Size(739, 294);
            this.DGVDockPanel.TabIndex = 5;
            // 
            // DGV
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV.Location = new System.Drawing.Point(0, 0);
            this.DGV.Name = "DGV";
            this.DGV.Size = new System.Drawing.Size(737, 292);
            this.DGV.TabIndex = 0;
            // 
            // OFD
            // 
            this.OFD.FileName = "openFileDialog1";
            // 
            // SubReadMessprotokollGFZXLS
            // 
            this.AcceptButton = this.btReady2Read;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(920, 449);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SubReadMessprotokollGFZXLS";
            this.Text = "Messprotokoll einlesen...";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Optionen.ResumeLayout(false);
            this.Optionen.PerformLayout();
            this.DGVDockPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView DGV;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btReady2Read;
        private System.Windows.Forms.ComboBox sheetlist_cb;
        private System.Windows.Forms.Label sheetlist_label;
        private System.Windows.Forms.RichTextBox RTB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox Optionen;
        private System.Windows.Forms.ComboBox cbDecimalPoints;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel DGVDockPanel;
    }
}