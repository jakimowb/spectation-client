﻿namespace SpectationClient {
    partial class SaveDGV {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.FBD = new System.Windows.Forms.FolderBrowserDialog();
            this.rBt_all = new System.Windows.Forms.RadioButton();
            this.rB_selected = new System.Windows.Forms.RadioButton();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.lPath = new System.Windows.Forms.Label();
            this.btSetPath = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.cB_sep = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rBt_all
            // 
            this.rBt_all.AutoSize = true;
            this.rBt_all.Checked = true;
            this.rBt_all.Location = new System.Drawing.Point(16, 60);
            this.rBt_all.Name = "rBt_all";
            this.rBt_all.Size = new System.Drawing.Size(60, 17);
            this.rBt_all.TabIndex = 0;
            this.rBt_all.TabStop = true;
            this.rBt_all.Text = "all rows";
            this.rBt_all.UseVisualStyleBackColor = true;
            // 
            // rB_selected
            // 
            this.rB_selected.AutoSize = true;
            this.rB_selected.Location = new System.Drawing.Point(16, 83);
            this.rB_selected.Name = "rB_selected";
            this.rB_selected.Size = new System.Drawing.Size(112, 17);
            this.rB_selected.TabIndex = 1;
            this.rB_selected.Text = "selected rows only";
            this.rB_selected.UseVisualStyleBackColor = true;
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(48, 25);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(213, 20);
            this.tbPath.TabIndex = 2;
            // 
            // lPath
            // 
            this.lPath.AutoSize = true;
            this.lPath.Location = new System.Drawing.Point(6, 28);
            this.lPath.Name = "lPath";
            this.lPath.Size = new System.Drawing.Size(36, 13);
            this.lPath.TabIndex = 3;
            this.lPath.Text = "Folder";
            // 
            // btSetPath
            // 
            this.btSetPath.Location = new System.Drawing.Point(155, 51);
            this.btSetPath.Name = "btSetPath";
            this.btSetPath.Size = new System.Drawing.Size(106, 23);
            this.btSetPath.TabIndex = 4;
            this.btSetPath.Text = "Set Directory";
            this.btSetPath.UseVisualStyleBackColor = true;
            this.btSetPath.Click += new System.EventHandler(this.btSetPath_Click);
            // 
            // btSave
            // 
            this.btSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.Location = new System.Drawing.Point(186, 121);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(75, 23);
            this.btSave.TabIndex = 5;
            this.btSave.Text = "Save Table";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(16, 121);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 6;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // cB_sep
            // 
            this.cB_sep.FormattingEnabled = true;
            this.cB_sep.Location = new System.Drawing.Point(210, 82);
            this.cB_sep.Name = "cB_sep";
            this.cB_sep.Size = new System.Drawing.Size(51, 21);
            this.cB_sep.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(151, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Seperator";
            // 
            // SaveDGV
            // 
            this.AcceptButton = this.btSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(325, 156);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cB_sep);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btSetPath);
            this.Controls.Add(this.lPath);
            this.Controls.Add(this.tbPath);
            this.Controls.Add(this.rB_selected);
            this.Controls.Add(this.rBt_all);
            this.Name = "SaveDGV";
            this.Text = "Save Dialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog FBD;
        private System.Windows.Forms.RadioButton rBt_all;
        private System.Windows.Forms.RadioButton rB_selected;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Label lPath;
        private System.Windows.Forms.Button btSetPath;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.ComboBox cB_sep;
        private System.Windows.Forms.Label label1;
    }
}