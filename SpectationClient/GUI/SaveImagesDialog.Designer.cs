﻿namespace SpectationClient.GUI {
    partial class SaveImagesDialog {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.tbFolder = new System.Windows.Forms.TextBox();
            this.btFolder = new System.Windows.Forms.Button();
            this.labelFolder = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbFileName_ColumnValues = new System.Windows.Forms.RadioButton();
            this.rbFileName_RowNumber = new System.Windows.Forms.RadioButton();
            this.btOK = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.clbColumns = new System.Windows.Forms.CheckedListBox();
            this.labelPreviewText = new System.Windows.Forms.Label();
            this.FBD = new System.Windows.Forms.FolderBrowserDialog();
            this.TT = new System.Windows.Forms.ToolTip(this.components);
            this.cbImageFormat = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbFolder
            // 
            this.tbFolder.Location = new System.Drawing.Point(70, 9);
            this.tbFolder.Name = "tbFolder";
            this.tbFolder.Size = new System.Drawing.Size(287, 20);
            this.tbFolder.TabIndex = 0;
            this.tbFolder.TextChanged += new System.EventHandler(this.validation_event);
            // 
            // btFolder
            // 
            this.btFolder.Location = new System.Drawing.Point(363, 8);
            this.btFolder.Name = "btFolder";
            this.btFolder.Size = new System.Drawing.Size(75, 23);
            this.btFolder.TabIndex = 1;
            this.btFolder.Text = "Auswählen...";
            this.btFolder.UseVisualStyleBackColor = true;
            this.btFolder.Click += new System.EventHandler(this.btFolder_Click);
            // 
            // labelFolder
            // 
            this.labelFolder.AutoSize = true;
            this.labelFolder.Location = new System.Drawing.Point(6, 13);
            this.labelFolder.Name = "labelFolder";
            this.labelFolder.Size = new System.Drawing.Size(61, 13);
            this.labelFolder.TabIndex = 4;
            this.labelFolder.Text = "Speicherort";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Format";
            // 
            // rbFileName_ColumnValues
            // 
            this.rbFileName_ColumnValues.AutoSize = true;
            this.rbFileName_ColumnValues.Location = new System.Drawing.Point(9, 67);
            this.rbFileName_ColumnValues.Name = "rbFileName_ColumnValues";
            this.rbFileName_ColumnValues.Size = new System.Drawing.Size(164, 17);
            this.rbFileName_ColumnValues.TabIndex = 6;
            this.rbFileName_ColumnValues.Text = "Verwende Werte aus Spalten";
            this.rbFileName_ColumnValues.UseVisualStyleBackColor = true;
            this.rbFileName_ColumnValues.CheckedChanged += new System.EventHandler(this.rbFileName_ColumnValues_CheckedChanged);
            // 
            // rbFileName_RowNumber
            // 
            this.rbFileName_RowNumber.AutoSize = true;
            this.rbFileName_RowNumber.Checked = true;
            this.rbFileName_RowNumber.Location = new System.Drawing.Point(9, 44);
            this.rbFileName_RowNumber.Name = "rbFileName_RowNumber";
            this.rbFileName_RowNumber.Size = new System.Drawing.Size(148, 17);
            this.rbFileName_RowNumber.TabIndex = 7;
            this.rbFileName_RowNumber.TabStop = true;
            this.rbFileName_RowNumber.Text = "Verwende Zeilennummern";
            this.rbFileName_RowNumber.UseVisualStyleBackColor = true;
            // 
            // btOK
            // 
            this.btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOK.Enabled = false;
            this.btOK.Location = new System.Drawing.Point(363, 260);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(75, 23);
            this.btOK.TabIndex = 9;
            this.btOK.Text = "Speichern";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(282, 260);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 10;
            this.btCancel.Text = "Abbruch";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbImageFormat);
            this.groupBox1.Controls.Add(this.clbColumns);
            this.groupBox1.Controls.Add(this.labelPreviewText);
            this.groupBox1.Controls.Add(this.rbFileName_RowNumber);
            this.groupBox1.Controls.Add(this.rbFileName_ColumnValues);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(3, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(435, 220);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dateinamen";
            // 
            // clbColumns
            // 
            this.clbColumns.CheckOnClick = true;
            this.clbColumns.Enabled = false;
            this.clbColumns.FormattingEnabled = true;
            this.clbColumns.Location = new System.Drawing.Point(10, 91);
            this.clbColumns.Name = "clbColumns";
            this.clbColumns.Size = new System.Drawing.Size(419, 124);
            this.clbColumns.TabIndex = 13;
            // 
            // labelPreviewText
            // 
            this.labelPreviewText.AutoSize = true;
            this.labelPreviewText.Location = new System.Drawing.Point(64, 190);
            this.labelPreviewText.Name = "labelPreviewText";
            this.labelPreviewText.Size = new System.Drawing.Size(0, 13);
            this.labelPreviewText.TabIndex = 12;
            // 
            // cbImageFormat
            // 
            this.cbImageFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbImageFormat.FormattingEnabled = true;
            this.cbImageFormat.Location = new System.Drawing.Point(52, 19);
            this.cbImageFormat.Name = "cbImageFormat";
            this.cbImageFormat.Size = new System.Drawing.Size(121, 21);
            this.cbImageFormat.TabIndex = 14;
            // 
            // SaveImagesDialog
            // 
            this.AcceptButton = this.btOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(448, 290);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btOK);
            this.Controls.Add(this.labelFolder);
            this.Controls.Add(this.btFolder);
            this.Controls.Add(this.tbFolder);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaveImagesDialog";
            this.Text = "Bilder speichern";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFolder;
        private System.Windows.Forms.Button btFolder;
        private System.Windows.Forms.Label labelFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbFileName_ColumnValues;
        private System.Windows.Forms.RadioButton rbFileName_RowNumber;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelPreviewText;
        private System.Windows.Forms.FolderBrowserDialog FBD;
        private System.Windows.Forms.CheckedListBox clbColumns;
        private System.Windows.Forms.ToolTip TT;
        private System.Windows.Forms.ComboBox cbImageFormat;
    }
}