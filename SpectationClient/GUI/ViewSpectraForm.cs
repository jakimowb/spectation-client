﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SpectationClient.DGVExtensions;
using SpectationClient.GUI.UC;

namespace SpectationClient.GUI {
    public partial class ViewSpectraForm : Form {
        public ViewSpectraForm() {
            InitializeComponent();

            this.Icon = Resources.Icon;
            this.Disposed += new EventHandler(ViewSpectraForm_Disposed);
            this.FormClosed +=new FormClosedEventHandler(ViewSpectraForm_FormClosed);
            this.Shown +=new EventHandler(ViewSpectraForm_Shown);
        }

        void ViewSpectraForm_Shown(object sender, EventArgs e) {
            this.uC_SpectralViewer1.RefreshPlot();
        }



        /// <summary>
        /// Connects the ViewSpectraForm with an DataGirdViewSpectrum column and reacts on changes in its selection.
        /// </summary>
        /// <param name="specColumn"></param>
        /// <param name="colorColumn"></param>
        public void linkToSpectrumColumn(DataGridViewSpectrumColumn specColumn, DataGridViewColorColumn colorColumn=null) {
            this.uC_SpectralViewer1.linkToSpectrumColumn(specColumn, colorColumn);
            specColumn.Disposed +=new EventHandler(column_Disposed);
        }

        void ViewSpectraForm_FormClosed(object sender, FormClosedEventArgs e) {
            this.uC_SpectralViewer1.Dispose();
        }

        void ViewSpectraForm_Disposed(object sender, EventArgs e) {
            this.uC_SpectralViewer1.Dispose();
        }

        void column_Disposed(object sender, EventArgs e) {
            this.uC_SpectralViewer1.Dispose();
            this.Close();
        }
    }
}
