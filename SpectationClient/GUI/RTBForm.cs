﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SpectationClient.GUI {
    public partial class RTBForm : Form {
        public RTBForm() {
            InitializeComponent();
        }
        public RTBForm(String text, String boxname) {
            InitializeComponent();
            this.Icon = Resources.Icon;
            RTB.Text = text;
            this.Text = boxname;
        }

        public RTBForm(String boxname) {
            InitializeComponent();
            this.Icon = Resources.Icon;
            this.Text = boxname;
            
        }

        public void addResourceText(String manifestResource){
            StringBuilder SB = new StringBuilder();
            try {
                Assembly assembly = Assembly.GetExecutingAssembly();
                TextReader textStreamReader = new StreamReader(assembly.GetManifestResourceStream(manifestResource));
                SB.AppendLine(textStreamReader.ReadToEnd());
            } catch(Exception e) {
                Stuff.FormHelper.ShowErrorBox(e);
            }
            RTB.Text = SB.ToString();
            
        }

        private void btSaveText_Click(object sender, EventArgs e) {
            if (SFD.ShowDialog() == DialogResult.OK) {
                File.WriteAllText(SFD.FileName, RTB.Text, Encoding.UTF8);
            }
        }

        private void btOK_Click(object sender, EventArgs e) {
            this.Close();
        }

       
    }
}
