﻿namespace SpectationClient.GUI {
    partial class SubSearch {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsl_ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.tsl_Message = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.gb_CORE = new System.Windows.Forms.GroupBox();
            this.gb_CORE_flp = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gb_CORE_SPECTRA_cb = new System.Windows.Forms.CheckBox();
            this.gb_CORE_LOCATIONS_cb = new System.Windows.Forms.CheckBox();
            this.gb_CORE_SENSORS_WREF_cb = new System.Windows.Forms.CheckBox();
            this.gb_CORE_CAMPAIGN_USER_cb = new System.Windows.Forms.CheckBox();
            this.gb_CORE_SPECTRA = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CORE_ID_PANEL = new System.Windows.Forms.Panel();
            this.CORE_Date_Begin = new System.Windows.Forms.DateTimePicker();
            this.CORE_Date_End = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CORE_SpecType = new System.Windows.Forms.ComboBox();
            this.CORE_Source = new System.Windows.Forms.ComboBox();
            this.CORE_Datatype = new System.Windows.Forms.ComboBox();
            this.gb_CORE_CAMPAIGN_USER = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.gb_CORE_USER_clb = new System.Windows.Forms.CheckedListBox();
            this.gb_CORE_CAMP_clb = new System.Windows.Forms.CheckedListBox();
            this.gb_CORE_SENSORS_WREF = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gb_CORE_WREF_PANELS_clb = new System.Windows.Forms.CheckedListBox();
            this.gb_CORE_SENS_clb = new System.Windows.Forms.CheckedListBox();
            this.gb_CORE_LOCATION = new System.Windows.Forms.GroupBox();
            this.gbLOC_ShapeFile = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.gbLOC_Buffer = new System.Windows.Forms.GroupBox();
            this.LOC_BUFF_CSPanel = new System.Windows.Forms.Panel();
            this.LOC_LAT_Y = new System.Windows.Forms.TextBox();
            this.LOC_LONG_X = new System.Windows.Forms.TextBox();
            this.LOC_label_LAT_Y = new System.Windows.Forms.Label();
            this.LOC_label_LONG_X = new System.Windows.Forms.Label();
            this.LOC_Buffer = new System.Windows.Forms.TextBox();
            this.LOC_label_Buffer = new System.Windows.Forms.Label();
            this.LOC_matchAll = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LOC_name = new System.Windows.Forms.TextBox();
            this.gb_CGFZ = new System.Windows.Forms.GroupBox();
            this.gb_CGFZ_flp = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gb_CGFZ_SP_cb = new System.Windows.Forms.CheckBox();
            this.gb_CGFZ_PS_cb = new System.Windows.Forms.CheckBox();
            this.gb_CGFZ_OBSAREA_cb = new System.Windows.Forms.CheckBox();
            this.gb_CGFZ_OBSAREA = new System.Windows.Forms.GroupBox();
            this.gb_CGFZ_OBSAREA_removeRow_bt = new System.Windows.Forms.Button();
            this.gb_CGFZ_OBSAREA_add_bt = new System.Windows.Forms.Button();
            this.OBSAREA_DGV = new System.Windows.Forms.DataGridView();
            this.gb_CGFZ_PS = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.VGFZ_PS_RemoveRowBB_bt = new System.Windows.Forms.Button();
            this.VGFZ_PS_AddBB_bt = new System.Windows.Forms.Button();
            this.VGFZ_PS_BB_DGV = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.VGFZ_PS_RemoveRowBfN_bt = new System.Windows.Forms.Button();
            this.VGFZ_PS_AddBfN_bt = new System.Windows.Forms.Button();
            this.VGFZ_PS_BfN_DGV = new System.Windows.Forms.DataGridView();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.VGFZ_PS_RemoveRowEUNIS_bt = new System.Windows.Forms.Button();
            this.VGFZ_PS_AddEUNISCode_bt = new System.Windows.Forms.Button();
            this.VGFZ_PS_EUNIS_DGV = new System.Windows.Forms.DataGridView();
            this.gb_CGFZ_SP = new System.Windows.Forms.GroupBox();
            this.VGFZ_SP_BtRemove = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.VGFZ_SP_oneofall = new System.Windows.Forms.RadioButton();
            this.VGFZ_SP_musthaveall = new System.Windows.Forms.RadioButton();
            this.VGFZ_SP_DGV = new System.Windows.Forms.DataGridView();
            this.VGFZ_SP_BtAdd = new System.Windows.Forms.Button();
            this.gbPSEUDO = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.RTB_CMD = new System.Windows.Forms.RichTextBox();
            this.SEARCH_bt = new System.Windows.Forms.Button();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.TT = new System.Windows.Forms.ToolTip(this.components);
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.gb_CORE.SuspendLayout();
            this.gb_CORE_flp.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gb_CORE_SPECTRA.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gb_CORE_CAMPAIGN_USER.SuspendLayout();
            this.gb_CORE_SENSORS_WREF.SuspendLayout();
            this.gb_CORE_LOCATION.SuspendLayout();
            this.gbLOC_ShapeFile.SuspendLayout();
            this.gbLOC_Buffer.SuspendLayout();
            this.gb_CGFZ.SuspendLayout();
            this.gb_CGFZ_flp.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gb_CGFZ_OBSAREA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OBSAREA_DGV)).BeginInit();
            this.gb_CGFZ_PS.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VGFZ_PS_BB_DGV)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VGFZ_PS_BfN_DGV)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VGFZ_PS_EUNIS_DGV)).BeginInit();
            this.gb_CGFZ_SP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VGFZ_SP_DGV)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(793, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tsl_ProgressBar,
            this.tsl_Message});
            this.statusStrip1.Location = new System.Drawing.Point(0, 553);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(793, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(55, 17);
            this.toolStripStatusLabel1.Text = "Progress:";
            // 
            // tsl_ProgressBar
            // 
            this.tsl_ProgressBar.Name = "tsl_ProgressBar";
            this.tsl_ProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // tsl_Message
            // 
            this.tsl_Message.Name = "tsl_Message";
            this.tsl_Message.Size = new System.Drawing.Size(57, 17);
            this.tsl_Message.Text = "<empty>";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Size = new System.Drawing.Size(793, 529);
            this.splitContainer1.SplitterDistance = 441;
            this.splitContainer1.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.gb_CORE);
            this.flowLayoutPanel1.Controls.Add(this.gb_CGFZ);
            this.flowLayoutPanel1.Controls.Add(this.gbPSEUDO);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(789, 437);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // gb_CORE
            // 
            this.gb_CORE.AutoSize = true;
            this.gb_CORE.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_CORE.Controls.Add(this.gb_CORE_flp);
            this.gb_CORE.Location = new System.Drawing.Point(3, 3);
            this.gb_CORE.Name = "gb_CORE";
            this.gb_CORE.Size = new System.Drawing.Size(751, 884);
            this.gb_CORE.TabIndex = 0;
            this.gb_CORE.TabStop = false;
            this.gb_CORE.Text = "CORE";
            // 
            // gb_CORE_flp
            // 
            this.gb_CORE_flp.AutoSize = true;
            this.gb_CORE_flp.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_CORE_flp.Controls.Add(this.panel2);
            this.gb_CORE_flp.Controls.Add(this.gb_CORE_SPECTRA);
            this.gb_CORE_flp.Controls.Add(this.gb_CORE_CAMPAIGN_USER);
            this.gb_CORE_flp.Controls.Add(this.gb_CORE_SENSORS_WREF);
            this.gb_CORE_flp.Controls.Add(this.gb_CORE_LOCATION);
            this.gb_CORE_flp.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.gb_CORE_flp.Location = new System.Drawing.Point(11, 24);
            this.gb_CORE_flp.Name = "gb_CORE_flp";
            this.gb_CORE_flp.Size = new System.Drawing.Size(734, 841);
            this.gb_CORE_flp.TabIndex = 20;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.gb_CORE_SPECTRA_cb);
            this.panel2.Controls.Add(this.gb_CORE_LOCATIONS_cb);
            this.panel2.Controls.Add(this.gb_CORE_SENSORS_WREF_cb);
            this.panel2.Controls.Add(this.gb_CORE_CAMPAIGN_USER_cb);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(728, 50);
            this.panel2.TabIndex = 0;
            // 
            // gb_CORE_SPECTRA_cb
            // 
            this.gb_CORE_SPECTRA_cb.AutoSize = true;
            this.gb_CORE_SPECTRA_cb.Checked = true;
            this.gb_CORE_SPECTRA_cb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gb_CORE_SPECTRA_cb.Location = new System.Drawing.Point(7, 3);
            this.gb_CORE_SPECTRA_cb.Name = "gb_CORE_SPECTRA_cb";
            this.gb_CORE_SPECTRA_cb.Size = new System.Drawing.Size(136, 17);
            this.gb_CORE_SPECTRA_cb.TabIndex = 3;
            this.gb_CORE_SPECTRA_cb.Text = "Spektreneigenschaften";
            this.gb_CORE_SPECTRA_cb.UseVisualStyleBackColor = true;
            // 
            // gb_CORE_LOCATIONS_cb
            // 
            this.gb_CORE_LOCATIONS_cb.AutoSize = true;
            this.gb_CORE_LOCATIONS_cb.Location = new System.Drawing.Point(272, 27);
            this.gb_CORE_LOCATIONS_cb.Name = "gb_CORE_LOCATIONS_cb";
            this.gb_CORE_LOCATIONS_cb.Size = new System.Drawing.Size(125, 17);
            this.gb_CORE_LOCATIONS_cb.TabIndex = 2;
            this.gb_CORE_LOCATIONS_cb.Text = "Position / Koordinate";
            this.gb_CORE_LOCATIONS_cb.UseVisualStyleBackColor = true;
            // 
            // gb_CORE_SENSORS_WREF_cb
            // 
            this.gb_CORE_SENSORS_WREF_cb.AutoSize = true;
            this.gb_CORE_SENSORS_WREF_cb.Location = new System.Drawing.Point(7, 27);
            this.gb_CORE_SENSORS_WREF_cb.Name = "gb_CORE_SENSORS_WREF_cb";
            this.gb_CORE_SENSORS_WREF_cb.Size = new System.Drawing.Size(254, 17);
            this.gb_CORE_SENSORS_WREF_cb.TabIndex = 1;
            this.gb_CORE_SENSORS_WREF_cb.Text = "Sensoren und Weißreferenzflächen/Spektralons";
            this.gb_CORE_SENSORS_WREF_cb.UseVisualStyleBackColor = true;
            // 
            // gb_CORE_CAMPAIGN_USER_cb
            // 
            this.gb_CORE_CAMPAIGN_USER_cb.AutoSize = true;
            this.gb_CORE_CAMPAIGN_USER_cb.Location = new System.Drawing.Point(272, 4);
            this.gb_CORE_CAMPAIGN_USER_cb.Name = "gb_CORE_CAMPAIGN_USER_cb";
            this.gb_CORE_CAMPAIGN_USER_cb.Size = new System.Drawing.Size(149, 17);
            this.gb_CORE_CAMPAIGN_USER_cb.TabIndex = 0;
            this.gb_CORE_CAMPAIGN_USER_cb.Text = "Kampagnen und Benutzer";
            this.gb_CORE_CAMPAIGN_USER_cb.UseVisualStyleBackColor = true;
            // 
            // gb_CORE_SPECTRA
            // 
            this.gb_CORE_SPECTRA.Controls.Add(this.label8);
            this.gb_CORE_SPECTRA.Controls.Add(this.label5);
            this.gb_CORE_SPECTRA.Controls.Add(this.label9);
            this.gb_CORE_SPECTRA.Controls.Add(this.groupBox1);
            this.gb_CORE_SPECTRA.Controls.Add(this.CORE_Date_Begin);
            this.gb_CORE_SPECTRA.Controls.Add(this.CORE_Date_End);
            this.gb_CORE_SPECTRA.Controls.Add(this.label6);
            this.gb_CORE_SPECTRA.Controls.Add(this.label7);
            this.gb_CORE_SPECTRA.Controls.Add(this.CORE_SpecType);
            this.gb_CORE_SPECTRA.Controls.Add(this.CORE_Source);
            this.gb_CORE_SPECTRA.Controls.Add(this.CORE_Datatype);
            this.gb_CORE_SPECTRA.Location = new System.Drawing.Point(3, 59);
            this.gb_CORE_SPECTRA.Name = "gb_CORE_SPECTRA";
            this.gb_CORE_SPECTRA.Size = new System.Drawing.Size(728, 180);
            this.gb_CORE_SPECTRA.TabIndex = 19;
            this.gb_CORE_SPECTRA.TabStop = false;
            this.gb_CORE_SPECTRA.Text = "CORE.SPECTRA";
            this.TT.SetToolTip(this.gb_CORE_SPECTRA, "Eingrenzen der Spektren nach spezifischen Dateieigenschaften");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Datum Begin";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Spektrum-Typ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(52, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Ende";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CORE_ID_PANEL);
            this.groupBox1.Location = new System.Drawing.Point(350, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(286, 144);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Spektrum-IDs";
            this.TT.SetToolTip(this.groupBox1, "Geben Sie hier gezielt spectraIDs aus der Spalte CORE.SPECTRA(id) an um nur diese" +
        " Spektren und zugehörige Informationen zu erhalten.");
            // 
            // CORE_ID_PANEL
            // 
            this.CORE_ID_PANEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CORE_ID_PANEL.Location = new System.Drawing.Point(3, 16);
            this.CORE_ID_PANEL.Name = "CORE_ID_PANEL";
            this.CORE_ID_PANEL.Size = new System.Drawing.Size(280, 125);
            this.CORE_ID_PANEL.TabIndex = 0;
            // 
            // CORE_Date_Begin
            // 
            this.CORE_Date_Begin.Checked = false;
            this.CORE_Date_Begin.Location = new System.Drawing.Point(90, 110);
            this.CORE_Date_Begin.Name = "CORE_Date_Begin";
            this.CORE_Date_Begin.ShowCheckBox = true;
            this.CORE_Date_Begin.Size = new System.Drawing.Size(200, 20);
            this.CORE_Date_Begin.TabIndex = 10;
            this.CORE_Date_Begin.ValueChanged += new System.EventHandler(this.CORE_Date_Begin_ValueChanged);
            // 
            // CORE_Date_End
            // 
            this.CORE_Date_End.Checked = false;
            this.CORE_Date_End.Location = new System.Drawing.Point(90, 136);
            this.CORE_Date_End.Name = "CORE_Date_End";
            this.CORE_Date_End.ShowCheckBox = true;
            this.CORE_Date_End.Size = new System.Drawing.Size(200, 20);
            this.CORE_Date_End.TabIndex = 11;
            this.CORE_Date_End.ValueChanged += new System.EventHandler(this.CORE_Date_End_ValueChanged);
            this.CORE_Date_End.Validating += new System.ComponentModel.CancelEventHandler(this.CORE_Date_End_Validating);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Datentyp";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Herkunft";
            // 
            // CORE_SpecType
            // 
            this.CORE_SpecType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CORE_SpecType.FormattingEnabled = true;
            this.CORE_SpecType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CORE_SpecType.Location = new System.Drawing.Point(90, 16);
            this.CORE_SpecType.Name = "CORE_SpecType";
            this.CORE_SpecType.Size = new System.Drawing.Size(121, 21);
            this.CORE_SpecType.TabIndex = 12;
            // 
            // CORE_Source
            // 
            this.CORE_Source.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CORE_Source.FormattingEnabled = true;
            this.CORE_Source.Location = new System.Drawing.Point(90, 69);
            this.CORE_Source.Name = "CORE_Source";
            this.CORE_Source.Size = new System.Drawing.Size(121, 21);
            this.CORE_Source.TabIndex = 14;
            // 
            // CORE_Datatype
            // 
            this.CORE_Datatype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CORE_Datatype.FormattingEnabled = true;
            this.CORE_Datatype.Location = new System.Drawing.Point(90, 42);
            this.CORE_Datatype.Name = "CORE_Datatype";
            this.CORE_Datatype.Size = new System.Drawing.Size(121, 21);
            this.CORE_Datatype.TabIndex = 13;
            // 
            // gb_CORE_CAMPAIGN_USER
            // 
            this.gb_CORE_CAMPAIGN_USER.AutoSize = true;
            this.gb_CORE_CAMPAIGN_USER.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_CORE_CAMPAIGN_USER.Controls.Add(this.label3);
            this.gb_CORE_CAMPAIGN_USER.Controls.Add(this.label12);
            this.gb_CORE_CAMPAIGN_USER.Controls.Add(this.gb_CORE_USER_clb);
            this.gb_CORE_CAMPAIGN_USER.Controls.Add(this.gb_CORE_CAMP_clb);
            this.gb_CORE_CAMPAIGN_USER.Location = new System.Drawing.Point(3, 245);
            this.gb_CORE_CAMPAIGN_USER.Name = "gb_CORE_CAMPAIGN_USER";
            this.gb_CORE_CAMPAIGN_USER.Size = new System.Drawing.Size(728, 195);
            this.gb_CORE_CAMPAIGN_USER.TabIndex = 3;
            this.gb_CORE_CAMPAIGN_USER.TabStop = false;
            this.gb_CORE_CAMPAIGN_USER.Text = "Kampagne/Projekt der Spektralmessungen";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(368, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "CORE.OPERATORS";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "CORE.CAMPAIGNS";
            // 
            // gb_CORE_USER_clb
            // 
            this.gb_CORE_USER_clb.CheckOnClick = true;
            this.gb_CORE_USER_clb.FormattingEnabled = true;
            this.gb_CORE_USER_clb.Location = new System.Drawing.Point(366, 37);
            this.gb_CORE_USER_clb.Name = "gb_CORE_USER_clb";
            this.gb_CORE_USER_clb.Size = new System.Drawing.Size(356, 139);
            this.gb_CORE_USER_clb.TabIndex = 1;
            // 
            // gb_CORE_CAMP_clb
            // 
            this.gb_CORE_CAMP_clb.CheckOnClick = true;
            this.gb_CORE_CAMP_clb.FormattingEnabled = true;
            this.gb_CORE_CAMP_clb.Location = new System.Drawing.Point(7, 37);
            this.gb_CORE_CAMP_clb.Name = "gb_CORE_CAMP_clb";
            this.gb_CORE_CAMP_clb.Size = new System.Drawing.Size(353, 139);
            this.gb_CORE_CAMP_clb.TabIndex = 0;
            // 
            // gb_CORE_SENSORS_WREF
            // 
            this.gb_CORE_SENSORS_WREF.AutoSize = true;
            this.gb_CORE_SENSORS_WREF.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_CORE_SENSORS_WREF.Controls.Add(this.label2);
            this.gb_CORE_SENSORS_WREF.Controls.Add(this.label1);
            this.gb_CORE_SENSORS_WREF.Controls.Add(this.gb_CORE_WREF_PANELS_clb);
            this.gb_CORE_SENSORS_WREF.Controls.Add(this.gb_CORE_SENS_clb);
            this.gb_CORE_SENSORS_WREF.Location = new System.Drawing.Point(3, 446);
            this.gb_CORE_SENSORS_WREF.Name = "gb_CORE_SENSORS_WREF";
            this.gb_CORE_SENSORS_WREF.Size = new System.Drawing.Size(728, 192);
            this.gb_CORE_SENSORS_WREF.TabIndex = 2;
            this.gb_CORE_SENSORS_WREF.TabStop = false;
            this.gb_CORE_SENSORS_WREF.Text = "Sensor und Weißreferenz";
            this.TT.SetToolTip(this.gb_CORE_SENSORS_WREF, "Suchergebnis auf Spektren eines bestimmten Sensors beschränken");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(368, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "CORE.WREF_PANELS";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "CORE.SENSORS";
            // 
            // gb_CORE_WREF_PANELS_clb
            // 
            this.gb_CORE_WREF_PANELS_clb.CheckOnClick = true;
            this.gb_CORE_WREF_PANELS_clb.FormattingEnabled = true;
            this.gb_CORE_WREF_PANELS_clb.Location = new System.Drawing.Point(366, 34);
            this.gb_CORE_WREF_PANELS_clb.Name = "gb_CORE_WREF_PANELS_clb";
            this.gb_CORE_WREF_PANELS_clb.Size = new System.Drawing.Size(356, 139);
            this.gb_CORE_WREF_PANELS_clb.TabIndex = 0;
            // 
            // gb_CORE_SENS_clb
            // 
            this.gb_CORE_SENS_clb.CheckOnClick = true;
            this.gb_CORE_SENS_clb.FormattingEnabled = true;
            this.gb_CORE_SENS_clb.Location = new System.Drawing.Point(7, 34);
            this.gb_CORE_SENS_clb.Name = "gb_CORE_SENS_clb";
            this.gb_CORE_SENS_clb.Size = new System.Drawing.Size(353, 139);
            this.gb_CORE_SENS_clb.TabIndex = 0;
            // 
            // gb_CORE_LOCATION
            // 
            this.gb_CORE_LOCATION.Controls.Add(this.gbLOC_ShapeFile);
            this.gb_CORE_LOCATION.Controls.Add(this.gbLOC_Buffer);
            this.gb_CORE_LOCATION.Controls.Add(this.LOC_matchAll);
            this.gb_CORE_LOCATION.Controls.Add(this.label4);
            this.gb_CORE_LOCATION.Controls.Add(this.LOC_name);
            this.gb_CORE_LOCATION.Location = new System.Drawing.Point(3, 644);
            this.gb_CORE_LOCATION.Name = "gb_CORE_LOCATION";
            this.gb_CORE_LOCATION.Size = new System.Drawing.Size(728, 194);
            this.gb_CORE_LOCATION.TabIndex = 1;
            this.gb_CORE_LOCATION.TabStop = false;
            this.gb_CORE_LOCATION.Text = "CORE.LOCATION";
            this.TT.SetToolTip(this.gb_CORE_LOCATION, "Suche auf Spektren beschränken, die in der Nähe einer bestimmten Koordinate aufge" +
        "nommen wurden.");
            // 
            // gbLOC_ShapeFile
            // 
            this.gbLOC_ShapeFile.Controls.Add(this.label10);
            this.gbLOC_ShapeFile.Location = new System.Drawing.Point(532, 43);
            this.gbLOC_ShapeFile.Name = "gbLOC_ShapeFile";
            this.gbLOC_ShapeFile.Size = new System.Drawing.Size(148, 108);
            this.gbLOC_ShapeFile.TabIndex = 10;
            this.gbLOC_ShapeFile.TabStop = false;
            this.gbLOC_ShapeFile.Text = "Definierter Bereich aus ShapeFile";
            this.gbLOC_ShapeFile.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(56, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "//TODO";
            // 
            // gbLOC_Buffer
            // 
            this.gbLOC_Buffer.AutoSize = true;
            this.gbLOC_Buffer.Controls.Add(this.LOC_BUFF_CSPanel);
            this.gbLOC_Buffer.Controls.Add(this.LOC_LAT_Y);
            this.gbLOC_Buffer.Controls.Add(this.LOC_LONG_X);
            this.gbLOC_Buffer.Controls.Add(this.LOC_label_LAT_Y);
            this.gbLOC_Buffer.Controls.Add(this.LOC_label_LONG_X);
            this.gbLOC_Buffer.Controls.Add(this.LOC_Buffer);
            this.gbLOC_Buffer.Controls.Add(this.LOC_label_Buffer);
            this.gbLOC_Buffer.Location = new System.Drawing.Point(13, 43);
            this.gbLOC_Buffer.Name = "gbLOC_Buffer";
            this.gbLOC_Buffer.Size = new System.Drawing.Size(501, 143);
            this.gbLOC_Buffer.TabIndex = 9;
            this.gbLOC_Buffer.TabStop = false;
            this.gbLOC_Buffer.Text = "Sucher per Buffer";
            // 
            // LOC_BUFF_CSPanel
            // 
            this.LOC_BUFF_CSPanel.AutoSize = true;
            this.LOC_BUFF_CSPanel.Location = new System.Drawing.Point(7, 20);
            this.LOC_BUFF_CSPanel.Name = "LOC_BUFF_CSPanel";
            this.LOC_BUFF_CSPanel.Size = new System.Drawing.Size(488, 62);
            this.LOC_BUFF_CSPanel.TabIndex = 6;
            // 
            // LOC_LAT_Y
            // 
            this.LOC_LAT_Y.Location = new System.Drawing.Point(115, 104);
            this.LOC_LAT_Y.Name = "LOC_LAT_Y";
            this.LOC_LAT_Y.Size = new System.Drawing.Size(100, 20);
            this.LOC_LAT_Y.TabIndex = 0;
            // 
            // LOC_LONG_X
            // 
            this.LOC_LONG_X.Location = new System.Drawing.Point(9, 104);
            this.LOC_LONG_X.Name = "LOC_LONG_X";
            this.LOC_LONG_X.Size = new System.Drawing.Size(100, 20);
            this.LOC_LONG_X.TabIndex = 3;
            // 
            // LOC_label_LAT_Y
            // 
            this.LOC_label_LAT_Y.AutoSize = true;
            this.LOC_label_LAT_Y.Location = new System.Drawing.Point(112, 86);
            this.LOC_label_LAT_Y.Name = "LOC_label_LAT_Y";
            this.LOC_label_LAT_Y.Size = new System.Drawing.Size(45, 13);
            this.LOC_label_LAT_Y.TabIndex = 2;
            this.LOC_label_LAT_Y.Text = "Latitude";
            // 
            // LOC_label_LONG_X
            // 
            this.LOC_label_LONG_X.AutoSize = true;
            this.LOC_label_LONG_X.Location = new System.Drawing.Point(6, 85);
            this.LOC_label_LONG_X.Name = "LOC_label_LONG_X";
            this.LOC_label_LONG_X.Size = new System.Drawing.Size(54, 13);
            this.LOC_label_LONG_X.TabIndex = 4;
            this.LOC_label_LONG_X.Text = "Longitude";
            this.TT.SetToolTip(this.LOC_label_LONG_X, "Die dem Koordinatensystem zugrunde liegende Längeneinheit");
            // 
            // LOC_Buffer
            // 
            this.LOC_Buffer.Location = new System.Drawing.Point(224, 104);
            this.LOC_Buffer.Name = "LOC_Buffer";
            this.LOC_Buffer.Size = new System.Drawing.Size(100, 20);
            this.LOC_Buffer.TabIndex = 1;
            this.LOC_Buffer.Text = "3";
            // 
            // LOC_label_Buffer
            // 
            this.LOC_label_Buffer.AutoSize = true;
            this.LOC_label_Buffer.Location = new System.Drawing.Point(226, 85);
            this.LOC_label_Buffer.Name = "LOC_label_Buffer";
            this.LOC_label_Buffer.Size = new System.Drawing.Size(35, 13);
            this.LOC_label_Buffer.TabIndex = 5;
            this.LOC_label_Buffer.Text = "Buffer";
            // 
            // LOC_matchAll
            // 
            this.LOC_matchAll.AutoSize = true;
            this.LOC_matchAll.Checked = true;
            this.LOC_matchAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.LOC_matchAll.Location = new System.Drawing.Point(162, 19);
            this.LOC_matchAll.Name = "LOC_matchAll";
            this.LOC_matchAll.Size = new System.Drawing.Size(198, 17);
            this.LOC_matchAll.TabIndex = 8;
            this.LOC_matchAll.Text = "suche sämtliche Übereinstimmungen";
            this.LOC_matchAll.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Name";
            // 
            // LOC_name
            // 
            this.LOC_name.Location = new System.Drawing.Point(52, 17);
            this.LOC_name.Name = "LOC_name";
            this.LOC_name.Size = new System.Drawing.Size(100, 20);
            this.LOC_name.TabIndex = 6;
            this.LOC_name.TextChanged += new System.EventHandler(this.EVENT_PrepareCommand);
            // 
            // gb_CGFZ
            // 
            this.gb_CGFZ.AutoSize = true;
            this.gb_CGFZ.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_CGFZ.Controls.Add(this.gb_CGFZ_flp);
            this.gb_CGFZ.Location = new System.Drawing.Point(3, 893);
            this.gb_CGFZ.Name = "gb_CGFZ";
            this.gb_CGFZ.Size = new System.Drawing.Size(752, 1038);
            this.gb_CGFZ.TabIndex = 4;
            this.gb_CGFZ.TabStop = false;
            this.gb_CGFZ.Text = "C_GFZ";
            // 
            // gb_CGFZ_flp
            // 
            this.gb_CGFZ_flp.AutoSize = true;
            this.gb_CGFZ_flp.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_CGFZ_flp.Controls.Add(this.panel1);
            this.gb_CGFZ_flp.Controls.Add(this.gb_CGFZ_OBSAREA);
            this.gb_CGFZ_flp.Controls.Add(this.gb_CGFZ_PS);
            this.gb_CGFZ_flp.Controls.Add(this.gb_CGFZ_SP);
            this.gb_CGFZ_flp.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.gb_CGFZ_flp.Location = new System.Drawing.Point(11, 19);
            this.gb_CGFZ_flp.Name = "gb_CGFZ_flp";
            this.gb_CGFZ_flp.Size = new System.Drawing.Size(735, 1000);
            this.gb_CGFZ_flp.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.gb_CGFZ_SP_cb);
            this.panel1.Controls.Add(this.gb_CGFZ_PS_cb);
            this.panel1.Controls.Add(this.gb_CGFZ_OBSAREA_cb);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(728, 43);
            this.panel1.TabIndex = 0;
            // 
            // gb_CGFZ_SP_cb
            // 
            this.gb_CGFZ_SP_cb.AutoSize = true;
            this.gb_CGFZ_SP_cb.Location = new System.Drawing.Point(8, 23);
            this.gb_CGFZ_SP_cb.Name = "gb_CGFZ_SP_cb";
            this.gb_CGFZ_SP_cb.Size = new System.Drawing.Size(221, 17);
            this.gb_CGFZ_SP_cb.TabIndex = 3;
            this.gb_CGFZ_SP_cb.Text = "PLANTS - Einzelpflanzenbeschreibungen";
            this.gb_CGFZ_SP_cb.UseVisualStyleBackColor = true;
            // 
            // gb_CGFZ_PS_cb
            // 
            this.gb_CGFZ_PS_cb.AutoSize = true;
            this.gb_CGFZ_PS_cb.Location = new System.Drawing.Point(272, 4);
            this.gb_CGFZ_PS_cb.Name = "gb_CGFZ_PS_cb";
            this.gb_CGFZ_PS_cb.Size = new System.Drawing.Size(278, 17);
            this.gb_CGFZ_PS_cb.TabIndex = 2;
            this.gb_CGFZ_PS_cb.Text = "C_GFZ.PLANT_SOCIETIES - Pflanzengesellschaften";
            this.gb_CGFZ_PS_cb.UseVisualStyleBackColor = true;
            // 
            // gb_CGFZ_OBSAREA_cb
            // 
            this.gb_CGFZ_OBSAREA_cb.AutoSize = true;
            this.gb_CGFZ_OBSAREA_cb.Location = new System.Drawing.Point(8, 4);
            this.gb_CGFZ_OBSAREA_cb.Name = "gb_CGFZ_OBSAREA_cb";
            this.gb_CGFZ_OBSAREA_cb.Size = new System.Drawing.Size(258, 17);
            this.gb_CGFZ_OBSAREA_cb.TabIndex = 0;
            this.gb_CGFZ_OBSAREA_cb.Text = "OBSERVATION_AREAS - Beobachtungsflächen";
            this.gb_CGFZ_OBSAREA_cb.UseVisualStyleBackColor = true;
            // 
            // gb_CGFZ_OBSAREA
            // 
            this.gb_CGFZ_OBSAREA.AutoSize = true;
            this.gb_CGFZ_OBSAREA.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_CGFZ_OBSAREA.Controls.Add(this.gb_CGFZ_OBSAREA_removeRow_bt);
            this.gb_CGFZ_OBSAREA.Controls.Add(this.gb_CGFZ_OBSAREA_add_bt);
            this.gb_CGFZ_OBSAREA.Controls.Add(this.OBSAREA_DGV);
            this.gb_CGFZ_OBSAREA.Location = new System.Drawing.Point(3, 52);
            this.gb_CGFZ_OBSAREA.Name = "gb_CGFZ_OBSAREA";
            this.gb_CGFZ_OBSAREA.Size = new System.Drawing.Size(728, 223);
            this.gb_CGFZ_OBSAREA.TabIndex = 3;
            this.gb_CGFZ_OBSAREA.TabStop = false;
            this.gb_CGFZ_OBSAREA.Text = "C_GFZ.OBSERVATION_AREAS - Beobachtungsflächen";
            // 
            // gb_CGFZ_OBSAREA_removeRow_bt
            // 
            this.gb_CGFZ_OBSAREA_removeRow_bt.Enabled = false;
            this.gb_CGFZ_OBSAREA_removeRow_bt.Location = new System.Drawing.Point(98, 17);
            this.gb_CGFZ_OBSAREA_removeRow_bt.Name = "gb_CGFZ_OBSAREA_removeRow_bt";
            this.gb_CGFZ_OBSAREA_removeRow_bt.Size = new System.Drawing.Size(84, 23);
            this.gb_CGFZ_OBSAREA_removeRow_bt.TabIndex = 3;
            this.gb_CGFZ_OBSAREA_removeRow_bt.Text = "Entfernen";
            this.TT.SetToolTip(this.gb_CGFZ_OBSAREA_removeRow_bt, "Entfernt markierte Zeilen");
            this.gb_CGFZ_OBSAREA_removeRow_bt.UseVisualStyleBackColor = true;
            this.gb_CGFZ_OBSAREA_removeRow_bt.Click += new System.EventHandler(this.gb_CGFZ_OBSAREA_removeRow_bt_Click);
            // 
            // gb_CGFZ_OBSAREA_add_bt
            // 
            this.gb_CGFZ_OBSAREA_add_bt.Location = new System.Drawing.Point(8, 17);
            this.gb_CGFZ_OBSAREA_add_bt.Name = "gb_CGFZ_OBSAREA_add_bt";
            this.gb_CGFZ_OBSAREA_add_bt.Size = new System.Drawing.Size(84, 23);
            this.gb_CGFZ_OBSAREA_add_bt.TabIndex = 2;
            this.gb_CGFZ_OBSAREA_add_bt.Text = "Hinzufügen...";
            this.gb_CGFZ_OBSAREA_add_bt.UseVisualStyleBackColor = true;
            this.gb_CGFZ_OBSAREA_add_bt.Click += new System.EventHandler(this.OBSAREA_bt_Click);
            // 
            // OBSAREA_DGV
            // 
            this.OBSAREA_DGV.AllowUserToAddRows = false;
            this.OBSAREA_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OBSAREA_DGV.Location = new System.Drawing.Point(8, 46);
            this.OBSAREA_DGV.Name = "OBSAREA_DGV";
            this.OBSAREA_DGV.Size = new System.Drawing.Size(714, 158);
            this.OBSAREA_DGV.TabIndex = 0;
            this.OBSAREA_DGV.SelectionChanged += new System.EventHandler(this.OBSAREA_DGV_SelectionChanged);
            // 
            // gb_CGFZ_PS
            // 
            this.gb_CGFZ_PS.AutoSize = true;
            this.gb_CGFZ_PS.Controls.Add(this.flowLayoutPanel2);
            this.gb_CGFZ_PS.Location = new System.Drawing.Point(3, 281);
            this.gb_CGFZ_PS.Name = "gb_CGFZ_PS";
            this.gb_CGFZ_PS.Size = new System.Drawing.Size(729, 509);
            this.gb_CGFZ_PS.TabIndex = 1;
            this.gb_CGFZ_PS.TabStop = false;
            this.gb_CGFZ_PS.Text = "C_GFZ.PLANT_SOCIETIES - Pflanzengesellschaften";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.Controls.Add(this.groupBox7);
            this.flowLayoutPanel2.Controls.Add(this.groupBox2);
            this.flowLayoutPanel2.Controls.Add(this.groupBox8);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(723, 490);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // groupBox7
            // 
            this.groupBox7.AutoSize = true;
            this.groupBox7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox7.Controls.Add(this.VGFZ_PS_RemoveRowBB_bt);
            this.groupBox7.Controls.Add(this.VGFZ_PS_AddBB_bt);
            this.groupBox7.Controls.Add(this.VGFZ_PS_BB_DGV);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(717, 136);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Biotope gem. Brandenburger Liste";
            // 
            // VGFZ_PS_RemoveRowBB_bt
            // 
            this.VGFZ_PS_RemoveRowBB_bt.Enabled = false;
            this.VGFZ_PS_RemoveRowBB_bt.Location = new System.Drawing.Point(97, 21);
            this.VGFZ_PS_RemoveRowBB_bt.Name = "VGFZ_PS_RemoveRowBB_bt";
            this.VGFZ_PS_RemoveRowBB_bt.Size = new System.Drawing.Size(84, 23);
            this.VGFZ_PS_RemoveRowBB_bt.TabIndex = 4;
            this.VGFZ_PS_RemoveRowBB_bt.Text = "Entfernen";
            this.TT.SetToolTip(this.VGFZ_PS_RemoveRowBB_bt, "Entfernt markierte Zeilen");
            this.VGFZ_PS_RemoveRowBB_bt.UseVisualStyleBackColor = true;
            this.VGFZ_PS_RemoveRowBB_bt.Click += new System.EventHandler(this.VGFZ_PS_RemoveRowBB_bt_Click);
            // 
            // VGFZ_PS_AddBB_bt
            // 
            this.VGFZ_PS_AddBB_bt.Location = new System.Drawing.Point(7, 20);
            this.VGFZ_PS_AddBB_bt.Name = "VGFZ_PS_AddBB_bt";
            this.VGFZ_PS_AddBB_bt.Size = new System.Drawing.Size(84, 23);
            this.VGFZ_PS_AddBB_bt.TabIndex = 1;
            this.VGFZ_PS_AddBB_bt.Text = "Hinzufügen...";
            this.VGFZ_PS_AddBB_bt.UseVisualStyleBackColor = true;
            this.VGFZ_PS_AddBB_bt.Click += new System.EventHandler(this.VGFZ_PS_BtAddBB_Click);
            // 
            // VGFZ_PS_BB_DGV
            // 
            this.VGFZ_PS_BB_DGV.AllowUserToAddRows = false;
            this.VGFZ_PS_BB_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.VGFZ_PS_BB_DGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.VGFZ_PS_BB_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VGFZ_PS_BB_DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.VGFZ_PS_BB_DGV.Location = new System.Drawing.Point(6, 50);
            this.VGFZ_PS_BB_DGV.Name = "VGFZ_PS_BB_DGV";
            this.VGFZ_PS_BB_DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.VGFZ_PS_BB_DGV.Size = new System.Drawing.Size(705, 67);
            this.VGFZ_PS_BB_DGV.TabIndex = 0;
            this.VGFZ_PS_BB_DGV.SelectionChanged += new System.EventHandler(this.VGFZ_PS_BB_DGV_SelectionChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.VGFZ_PS_RemoveRowBfN_bt);
            this.groupBox2.Controls.Add(this.VGFZ_PS_AddBfN_bt);
            this.groupBox2.Controls.Add(this.VGFZ_PS_BfN_DGV);
            this.groupBox2.Location = new System.Drawing.Point(3, 145);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(711, 168);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "BfN-Code";
            // 
            // VGFZ_PS_RemoveRowBfN_bt
            // 
            this.VGFZ_PS_RemoveRowBfN_bt.Enabled = false;
            this.VGFZ_PS_RemoveRowBfN_bt.Location = new System.Drawing.Point(97, 20);
            this.VGFZ_PS_RemoveRowBfN_bt.Name = "VGFZ_PS_RemoveRowBfN_bt";
            this.VGFZ_PS_RemoveRowBfN_bt.Size = new System.Drawing.Size(84, 23);
            this.VGFZ_PS_RemoveRowBfN_bt.TabIndex = 5;
            this.VGFZ_PS_RemoveRowBfN_bt.Text = "Entfernen";
            this.TT.SetToolTip(this.VGFZ_PS_RemoveRowBfN_bt, "Entfernt markierte Zeilen");
            this.VGFZ_PS_RemoveRowBfN_bt.UseVisualStyleBackColor = true;
            this.VGFZ_PS_RemoveRowBfN_bt.Click += new System.EventHandler(this.VGFZ_PS_RemoveRowBfN_bt_Click);
            // 
            // VGFZ_PS_AddBfN_bt
            // 
            this.VGFZ_PS_AddBfN_bt.Location = new System.Drawing.Point(6, 20);
            this.VGFZ_PS_AddBfN_bt.Name = "VGFZ_PS_AddBfN_bt";
            this.VGFZ_PS_AddBfN_bt.Size = new System.Drawing.Size(84, 23);
            this.VGFZ_PS_AddBfN_bt.TabIndex = 2;
            this.VGFZ_PS_AddBfN_bt.Text = "Hinzufügen...";
            this.VGFZ_PS_AddBfN_bt.UseVisualStyleBackColor = true;
            this.VGFZ_PS_AddBfN_bt.Click += new System.EventHandler(this.VGFZ_PS_AddBfN_bt_Click);
            // 
            // VGFZ_PS_BfN_DGV
            // 
            this.VGFZ_PS_BfN_DGV.AllowUserToAddRows = false;
            this.VGFZ_PS_BfN_DGV.AllowUserToOrderColumns = true;
            this.VGFZ_PS_BfN_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.VGFZ_PS_BfN_DGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.VGFZ_PS_BfN_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VGFZ_PS_BfN_DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.VGFZ_PS_BfN_DGV.Location = new System.Drawing.Point(6, 49);
            this.VGFZ_PS_BfN_DGV.MaximumSize = new System.Drawing.Size(699, 300);
            this.VGFZ_PS_BfN_DGV.MinimumSize = new System.Drawing.Size(699, 100);
            this.VGFZ_PS_BfN_DGV.Name = "VGFZ_PS_BfN_DGV";
            this.VGFZ_PS_BfN_DGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.VGFZ_PS_BfN_DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.VGFZ_PS_BfN_DGV.Size = new System.Drawing.Size(699, 100);
            this.VGFZ_PS_BfN_DGV.TabIndex = 1;
            this.VGFZ_PS_BfN_DGV.SelectionChanged += new System.EventHandler(this.VGFZ_PS_BfN_DGV_SelectionChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.AutoSize = true;
            this.groupBox8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox8.Controls.Add(this.VGFZ_PS_RemoveRowEUNIS_bt);
            this.groupBox8.Controls.Add(this.VGFZ_PS_AddEUNISCode_bt);
            this.groupBox8.Controls.Add(this.VGFZ_PS_EUNIS_DGV);
            this.groupBox8.Location = new System.Drawing.Point(3, 319);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(711, 168);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "EUNIS Habitate";
            // 
            // VGFZ_PS_RemoveRowEUNIS_bt
            // 
            this.VGFZ_PS_RemoveRowEUNIS_bt.Enabled = false;
            this.VGFZ_PS_RemoveRowEUNIS_bt.Location = new System.Drawing.Point(97, 20);
            this.VGFZ_PS_RemoveRowEUNIS_bt.Name = "VGFZ_PS_RemoveRowEUNIS_bt";
            this.VGFZ_PS_RemoveRowEUNIS_bt.Size = new System.Drawing.Size(84, 23);
            this.VGFZ_PS_RemoveRowEUNIS_bt.TabIndex = 5;
            this.VGFZ_PS_RemoveRowEUNIS_bt.Text = "Entfernen";
            this.TT.SetToolTip(this.VGFZ_PS_RemoveRowEUNIS_bt, "Entfernt markierte Zeilen");
            this.VGFZ_PS_RemoveRowEUNIS_bt.UseVisualStyleBackColor = true;
            this.VGFZ_PS_RemoveRowEUNIS_bt.Click += new System.EventHandler(this.VGFZ_PS_RemoveRowEUNIS_bt_Click);
            // 
            // VGFZ_PS_AddEUNISCode_bt
            // 
            this.VGFZ_PS_AddEUNISCode_bt.Location = new System.Drawing.Point(6, 20);
            this.VGFZ_PS_AddEUNISCode_bt.Name = "VGFZ_PS_AddEUNISCode_bt";
            this.VGFZ_PS_AddEUNISCode_bt.Size = new System.Drawing.Size(84, 23);
            this.VGFZ_PS_AddEUNISCode_bt.TabIndex = 2;
            this.VGFZ_PS_AddEUNISCode_bt.Text = "Hinzufügen...";
            this.VGFZ_PS_AddEUNISCode_bt.UseVisualStyleBackColor = true;
            this.VGFZ_PS_AddEUNISCode_bt.Click += new System.EventHandler(this.VGFZ_PS_AddEUNIS_bt_Click);
            // 
            // VGFZ_PS_EUNIS_DGV
            // 
            this.VGFZ_PS_EUNIS_DGV.AllowUserToAddRows = false;
            this.VGFZ_PS_EUNIS_DGV.AllowUserToOrderColumns = true;
            this.VGFZ_PS_EUNIS_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.VGFZ_PS_EUNIS_DGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.VGFZ_PS_EUNIS_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VGFZ_PS_EUNIS_DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.VGFZ_PS_EUNIS_DGV.Location = new System.Drawing.Point(6, 49);
            this.VGFZ_PS_EUNIS_DGV.MaximumSize = new System.Drawing.Size(699, 300);
            this.VGFZ_PS_EUNIS_DGV.MinimumSize = new System.Drawing.Size(699, 100);
            this.VGFZ_PS_EUNIS_DGV.Name = "VGFZ_PS_EUNIS_DGV";
            this.VGFZ_PS_EUNIS_DGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.VGFZ_PS_EUNIS_DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.VGFZ_PS_EUNIS_DGV.Size = new System.Drawing.Size(699, 100);
            this.VGFZ_PS_EUNIS_DGV.TabIndex = 1;
            this.VGFZ_PS_EUNIS_DGV.SelectionChanged += new System.EventHandler(this.VGFZ_PS_EUNIS_DGV_SelectionChanged);
            // 
            // gb_CGFZ_SP
            // 
            this.gb_CGFZ_SP.AutoSize = true;
            this.gb_CGFZ_SP.Controls.Add(this.VGFZ_SP_BtRemove);
            this.gb_CGFZ_SP.Controls.Add(this.label11);
            this.gb_CGFZ_SP.Controls.Add(this.VGFZ_SP_oneofall);
            this.gb_CGFZ_SP.Controls.Add(this.VGFZ_SP_musthaveall);
            this.gb_CGFZ_SP.Controls.Add(this.VGFZ_SP_DGV);
            this.gb_CGFZ_SP.Controls.Add(this.VGFZ_SP_BtAdd);
            this.gb_CGFZ_SP.Location = new System.Drawing.Point(3, 796);
            this.gb_CGFZ_SP.Name = "gb_CGFZ_SP";
            this.gb_CGFZ_SP.Size = new System.Drawing.Size(728, 201);
            this.gb_CGFZ_SP.TabIndex = 0;
            this.gb_CGFZ_SP.TabStop = false;
            this.gb_CGFZ_SP.Text = "C_GFZ.PLANTS - Einzelpflanzenbeschreibungen";
            // 
            // VGFZ_SP_BtRemove
            // 
            this.VGFZ_SP_BtRemove.Enabled = false;
            this.VGFZ_SP_BtRemove.Location = new System.Drawing.Point(132, 21);
            this.VGFZ_SP_BtRemove.Name = "VGFZ_SP_BtRemove";
            this.VGFZ_SP_BtRemove.Size = new System.Drawing.Size(84, 23);
            this.VGFZ_SP_BtRemove.TabIndex = 6;
            this.VGFZ_SP_BtRemove.Text = "Entfernen";
            this.TT.SetToolTip(this.VGFZ_SP_BtRemove, "Entfernt markierte Zeilen");
            this.VGFZ_SP_BtRemove.UseVisualStyleBackColor = true;
            this.VGFZ_SP_BtRemove.Click += new System.EventHandler(this.VGFZ_SP_BtRemove_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(270, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Enthält:";
            // 
            // VGFZ_SP_oneofall
            // 
            this.VGFZ_SP_oneofall.AutoSize = true;
            this.VGFZ_SP_oneofall.Location = new System.Drawing.Point(364, 24);
            this.VGFZ_SP_oneofall.Name = "VGFZ_SP_oneofall";
            this.VGFZ_SP_oneofall.Size = new System.Drawing.Size(163, 17);
            this.VGFZ_SP_oneofall.TabIndex = 3;
            this.VGFZ_SP_oneofall.Text = "mindestens eine der Pflanzen";
            this.TT.SetToolTip(this.VGFZ_SP_oneofall, "Spektrum muss mindestens mit einer der angegebenen Pflanzen verlinkt sein");
            this.VGFZ_SP_oneofall.UseVisualStyleBackColor = true;
            // 
            // VGFZ_SP_musthaveall
            // 
            this.VGFZ_SP_musthaveall.AutoSize = true;
            this.VGFZ_SP_musthaveall.Checked = true;
            this.VGFZ_SP_musthaveall.Location = new System.Drawing.Point(319, 24);
            this.VGFZ_SP_musthaveall.Name = "VGFZ_SP_musthaveall";
            this.VGFZ_SP_musthaveall.Size = new System.Drawing.Size(41, 17);
            this.VGFZ_SP_musthaveall.TabIndex = 2;
            this.VGFZ_SP_musthaveall.TabStop = true;
            this.VGFZ_SP_musthaveall.Text = "alle";
            this.TT.SetToolTip(this.VGFZ_SP_musthaveall, "Spektren müssen mit allen angegebenen Pflanzen verlinkt sein");
            this.VGFZ_SP_musthaveall.UseVisualStyleBackColor = true;
            // 
            // VGFZ_SP_DGV
            // 
            this.VGFZ_SP_DGV.AllowUserToAddRows = false;
            this.VGFZ_SP_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.VGFZ_SP_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VGFZ_SP_DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.VGFZ_SP_DGV.Location = new System.Drawing.Point(9, 49);
            this.VGFZ_SP_DGV.Name = "VGFZ_SP_DGV";
            this.VGFZ_SP_DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.VGFZ_SP_DGV.Size = new System.Drawing.Size(713, 133);
            this.VGFZ_SP_DGV.TabIndex = 1;
            this.VGFZ_SP_DGV.SelectionChanged += new System.EventHandler(this.VGFZ_SP_DGV_SelectionChanged);
            // 
            // VGFZ_SP_BtAdd
            // 
            this.VGFZ_SP_BtAdd.Location = new System.Drawing.Point(9, 20);
            this.VGFZ_SP_BtAdd.Name = "VGFZ_SP_BtAdd";
            this.VGFZ_SP_BtAdd.Size = new System.Drawing.Size(117, 23);
            this.VGFZ_SP_BtAdd.TabIndex = 0;
            this.VGFZ_SP_BtAdd.Text = "Pflanze hinzufügen";
            this.VGFZ_SP_BtAdd.UseVisualStyleBackColor = true;
            this.VGFZ_SP_BtAdd.Click += new System.EventHandler(this.VGFZ_SP_BtAdd_Click);
            // 
            // gbPSEUDO
            // 
            this.gbPSEUDO.Location = new System.Drawing.Point(3, 1937);
            this.gbPSEUDO.Name = "gbPSEUDO";
            this.gbPSEUDO.Size = new System.Drawing.Size(143, 91);
            this.gbPSEUDO.TabIndex = 6;
            this.gbPSEUDO.TabStop = false;
            this.gbPSEUDO.Text = "//PSEUDO";
            this.gbPSEUDO.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.26996F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.73004F));
            this.tableLayoutPanel1.Controls.Add(this.RTB_CMD, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.SEARCH_bt, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(789, 80);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // RTB_CMD
            // 
            this.RTB_CMD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RTB_CMD.Location = new System.Drawing.Point(3, 3);
            this.RTB_CMD.Name = "RTB_CMD";
            this.RTB_CMD.ReadOnly = true;
            this.RTB_CMD.Size = new System.Drawing.Size(650, 74);
            this.RTB_CMD.TabIndex = 1;
            this.RTB_CMD.Text = "";
            // 
            // SEARCH_bt
            // 
            this.SEARCH_bt.Dock = System.Windows.Forms.DockStyle.Top;
            this.SEARCH_bt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SEARCH_bt.Location = new System.Drawing.Point(659, 3);
            this.SEARCH_bt.Name = "SEARCH_bt";
            this.SEARCH_bt.Size = new System.Drawing.Size(127, 30);
            this.SEARCH_bt.TabIndex = 0;
            this.SEARCH_bt.Text = "Suchen!";
            this.SEARCH_bt.UseVisualStyleBackColor = true;
            this.SEARCH_bt.Click += new System.EventHandler(this.SEARCH_bt_Click);
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // timer
            // 
            this.timer.Interval = 500;
            // 
            // SubSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 575);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "SubSearch";
            this.Text = "Kompexe Suchanfragen";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.gb_CORE.ResumeLayout(false);
            this.gb_CORE.PerformLayout();
            this.gb_CORE_flp.ResumeLayout(false);
            this.gb_CORE_flp.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gb_CORE_SPECTRA.ResumeLayout(false);
            this.gb_CORE_SPECTRA.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.gb_CORE_CAMPAIGN_USER.ResumeLayout(false);
            this.gb_CORE_CAMPAIGN_USER.PerformLayout();
            this.gb_CORE_SENSORS_WREF.ResumeLayout(false);
            this.gb_CORE_SENSORS_WREF.PerformLayout();
            this.gb_CORE_LOCATION.ResumeLayout(false);
            this.gb_CORE_LOCATION.PerformLayout();
            this.gbLOC_ShapeFile.ResumeLayout(false);
            this.gbLOC_ShapeFile.PerformLayout();
            this.gbLOC_Buffer.ResumeLayout(false);
            this.gbLOC_Buffer.PerformLayout();
            this.gb_CGFZ.ResumeLayout(false);
            this.gb_CGFZ.PerformLayout();
            this.gb_CGFZ_flp.ResumeLayout(false);
            this.gb_CGFZ_flp.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gb_CGFZ_OBSAREA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OBSAREA_DGV)).EndInit();
            this.gb_CGFZ_PS.ResumeLayout(false);
            this.gb_CGFZ_PS.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VGFZ_PS_BB_DGV)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VGFZ_PS_BfN_DGV)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VGFZ_PS_EUNIS_DGV)).EndInit();
            this.gb_CGFZ_SP.ResumeLayout(false);
            this.gb_CGFZ_SP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VGFZ_SP_DGV)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar tsl_ProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel tsl_Message;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox gb_CORE;
        private System.Windows.Forms.GroupBox gb_CORE_LOCATION;
        private System.Windows.Forms.Button SEARCH_bt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox LOC_name;
        private System.Windows.Forms.Label LOC_label_Buffer;
        private System.Windows.Forms.Label LOC_label_LONG_X;
        private System.Windows.Forms.TextBox LOC_LONG_X;
        private System.Windows.Forms.Label LOC_label_LAT_Y;
        private System.Windows.Forms.TextBox LOC_Buffer;
        private System.Windows.Forms.TextBox LOC_LAT_Y;
        private System.Windows.Forms.GroupBox gbLOC_ShapeFile;
        private System.Windows.Forms.GroupBox gbLOC_Buffer;
        private System.Windows.Forms.CheckBox LOC_matchAll;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.ToolTip TT;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker CORE_Date_End;
        private System.Windows.Forms.DateTimePicker CORE_Date_Begin;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox gb_CORE_SENSORS_WREF;
        private System.Windows.Forms.CheckedListBox gb_CORE_SENS_clb;
        private System.Windows.Forms.GroupBox gb_CORE_CAMPAIGN_USER;
        private System.Windows.Forms.CheckedListBox gb_CORE_CAMP_clb;
        private System.Windows.Forms.ComboBox CORE_Source;
        private System.Windows.Forms.ComboBox CORE_Datatype;
        private System.Windows.Forms.ComboBox CORE_SpecType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gb_CGFZ;
        private System.Windows.Forms.GroupBox gbPSEUDO;
        private System.Windows.Forms.GroupBox gb_CGFZ_PS;
        private System.Windows.Forms.GroupBox gb_CGFZ_SP;
        private System.Windows.Forms.Button VGFZ_SP_BtAdd;
        private System.Windows.Forms.DataGridView VGFZ_PS_BB_DGV;
        private System.Windows.Forms.DataGridView VGFZ_SP_DGV;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView VGFZ_PS_EUNIS_DGV;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button VGFZ_PS_AddBB_bt;
        private System.Windows.Forms.Button VGFZ_PS_AddEUNISCode_bt;
        private System.Windows.Forms.CheckedListBox gb_CORE_WREF_PANELS_clb;
        private System.Windows.Forms.RichTextBox RTB_CMD;
        private System.Windows.Forms.RadioButton VGFZ_SP_oneofall;
        private System.Windows.Forms.RadioButton VGFZ_SP_musthaveall;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel LOC_BUFF_CSPanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel CORE_ID_PANEL;
        private System.Windows.Forms.GroupBox gb_CORE_SPECTRA;
        private System.Windows.Forms.FlowLayoutPanel gb_CGFZ_flp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox gb_CGFZ_OBSAREA;
        private System.Windows.Forms.Button gb_CGFZ_OBSAREA_add_bt;
        private System.Windows.Forms.DataGridView OBSAREA_DGV;
        private System.Windows.Forms.CheckBox gb_CGFZ_SP_cb;
        private System.Windows.Forms.CheckBox gb_CGFZ_PS_cb;
        private System.Windows.Forms.CheckBox gb_CGFZ_OBSAREA_cb;
        private System.Windows.Forms.FlowLayoutPanel gb_CORE_flp;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox gb_CORE_USER_clb;
        private System.Windows.Forms.CheckBox gb_CORE_LOCATIONS_cb;
        private System.Windows.Forms.CheckBox gb_CORE_SENSORS_WREF_cb;
        private System.Windows.Forms.CheckBox gb_CORE_CAMPAIGN_USER_cb;
        private System.Windows.Forms.CheckBox gb_CORE_SPECTRA_cb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button gb_CGFZ_OBSAREA_removeRow_bt;
        private System.Windows.Forms.Button VGFZ_PS_RemoveRowEUNIS_bt;
        private System.Windows.Forms.Button VGFZ_PS_RemoveRowBB_bt;
        private System.Windows.Forms.Button VGFZ_SP_BtRemove;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button VGFZ_PS_RemoveRowBfN_bt;
        private System.Windows.Forms.Button VGFZ_PS_AddBfN_bt;
        private System.Windows.Forms.DataGridView VGFZ_PS_BfN_DGV;
    }
}