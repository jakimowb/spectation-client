﻿namespace SpectationClient.GUI {
    partial class SubInsertGFZPhotos {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.uC_ImageViewer1 = new GUI.UC.UC_ImageViewer();
            this.btRemovePhoto = new System.Windows.Forms.Button();
            this.Photos_btAddPhotos = new System.Windows.Forms.Button();
            this.Photos_DGV = new System.Windows.Forms.DataGridView();
            this.ColPhotos = new DGVExtensions.DataGridViewPhotoColumn();
            this.ColType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColNotes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SPECIDs_PANEL = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.Photo_Camera = new System.Windows.Forms.ComboBox();
            this.Photo_Operator = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Time_TimeStart = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.Time_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.bt_INSERT_PHOTOS = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RTB_SQL = new System.Windows.Forms.RichTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ts_progressbar = new System.Windows.Forms.ToolStripProgressBar();
            this.ts_statuslabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.TT = new System.Windows.Forms.ToolTip(this.components);
            this.dataGridViewImage2Column1 = new DGVExtensions.DataGridViewPhotoColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Photos_DGV)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox7
            // 
            this.groupBox7.AutoSize = true;
            this.groupBox7.Controls.Add(this.uC_ImageViewer1);
            this.groupBox7.Controls.Add(this.btRemovePhoto);
            this.groupBox7.Controls.Add(this.Photos_btAddPhotos);
            this.groupBox7.Controls.Add(this.Photos_DGV);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(3, 109);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(752, 196);
            this.groupBox7.TabIndex = 9;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Neue Fotos";
            this.TT.SetToolTip(this.groupBox7, "Geben Sie hier die Fotos an, welche in die Datenbank eingefügt werden sollen. ");
            // 
            // uC_ImageViewer1
            // 
            this.uC_ImageViewer1.Location = new System.Drawing.Point(521, 19);
            this.uC_ImageViewer1.Name = "uC_ImageViewer1";
            this.uC_ImageViewer1.Size = new System.Drawing.Size(225, 172);
            this.uC_ImageViewer1.TabIndex = 4;
            // 
            // btRemovePhoto
            // 
            this.btRemovePhoto.Enabled = false;
            this.btRemovePhoto.Location = new System.Drawing.Point(10, 50);
            this.btRemovePhoto.Name = "btRemovePhoto";
            this.btRemovePhoto.Size = new System.Drawing.Size(87, 23);
            this.btRemovePhoto.TabIndex = 3;
            this.btRemovePhoto.Text = "Entfernen";
            this.btRemovePhoto.UseVisualStyleBackColor = true;
            this.btRemovePhoto.Click += new System.EventHandler(this.btRemovePhoto_Click);
            // 
            // Photos_btAddPhotos
            // 
            this.Photos_btAddPhotos.Location = new System.Drawing.Point(10, 20);
            this.Photos_btAddPhotos.Name = "Photos_btAddPhotos";
            this.Photos_btAddPhotos.Size = new System.Drawing.Size(87, 23);
            this.Photos_btAddPhotos.TabIndex = 1;
            this.Photos_btAddPhotos.Text = "Hinzufügen...";
            this.Photos_btAddPhotos.UseVisualStyleBackColor = true;
            this.Photos_btAddPhotos.Click += new System.EventHandler(this.Photos_btAddPhotos_Click);
            // 
            // Photos_DGV
            // 
            this.Photos_DGV.AllowUserToAddRows = false;
            this.Photos_DGV.AllowUserToOrderColumns = true;
            this.Photos_DGV.AllowUserToResizeRows = false;
            this.Photos_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Photos_DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Photos_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Photos_DGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColPhotos,
            this.ColType,
            this.ColNotes});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Photos_DGV.DefaultCellStyle = dataGridViewCellStyle2;
            this.Photos_DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.Photos_DGV.Location = new System.Drawing.Point(107, 19);
            this.Photos_DGV.MultiSelect = false;
            this.Photos_DGV.Name = "Photos_DGV";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Photos_DGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.Photos_DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Photos_DGV.Size = new System.Drawing.Size(411, 172);
            this.Photos_DGV.TabIndex = 0;
            this.Photos_DGV.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.checkCommand);
            this.Photos_DGV.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.Photos_DGV_RowsRemoved);
            this.Photos_DGV.SelectionChanged += new System.EventHandler(this.Photos_DGV_SelectionChanged);
            // 
            // ColPhotos
            // 
            this.ColPhotos.HeaderText = "Photo";
            this.ColPhotos.Name = "ColPhotos";
            this.ColPhotos.ReadOnly = true;
            // 
            // ColType
            // 
            this.ColType.HeaderText = "Typ";
            this.ColType.Name = "ColType";
            // 
            // ColNotes
            // 
            this.ColNotes.HeaderText = "Beschreibung";
            this.ColNotes.Name = "ColNotes";
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.SPECIDs_PANEL);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.MinimumSize = new System.Drawing.Size(200, 100);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Spektren IDs";
            this.TT.SetToolTip(this.groupBox1, "Hier die bereits existierenden Spektren IDs angeben um \r\njedes Foto mit jeder ID " +
        "zu verknüpfen.");
            // 
            // SPECIDs_PANEL
            // 
            this.SPECIDs_PANEL.AutoSize = true;
            this.SPECIDs_PANEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPECIDs_PANEL.Location = new System.Drawing.Point(5, 18);
            this.SPECIDs_PANEL.Margin = new System.Windows.Forms.Padding(5);
            this.SPECIDs_PANEL.Name = "SPECIDs_PANEL";
            this.SPECIDs_PANEL.Size = new System.Drawing.Size(190, 77);
            this.SPECIDs_PANEL.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.bt_INSERT_PHOTOS);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(793, 512);
            this.splitContainer1.SplitterDistance = 355;
            this.splitContainer1.TabIndex = 11;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.groupBox1);
            this.flowLayoutPanel1.Controls.Add(this.groupBox18);
            this.flowLayoutPanel1.Controls.Add(this.groupBox3);
            this.flowLayoutPanel1.Controls.Add(this.groupBox7);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(791, 353);
            this.flowLayoutPanel1.TabIndex = 11;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.Photo_Camera);
            this.groupBox18.Controls.Add(this.Photo_Operator);
            this.groupBox18.Controls.Add(this.label38);
            this.groupBox18.Controls.Add(this.label43);
            this.groupBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox18.Location = new System.Drawing.Point(209, 3);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(308, 100);
            this.groupBox18.TabIndex = 12;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Fotoapparat";
            // 
            // Photo_Camera
            // 
            this.Photo_Camera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Photo_Camera.FormattingEnabled = true;
            this.Photo_Camera.Location = new System.Drawing.Point(73, 17);
            this.Photo_Camera.Name = "Photo_Camera";
            this.Photo_Camera.Size = new System.Drawing.Size(229, 21);
            this.Photo_Camera.TabIndex = 0;
            this.Photo_Camera.SelectedValueChanged += new System.EventHandler(this.checkCommand);
            // 
            // Photo_Operator
            // 
            this.Photo_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Photo_Operator.FormattingEnabled = true;
            this.Photo_Operator.Location = new System.Drawing.Point(73, 43);
            this.Photo_Operator.Name = "Photo_Operator";
            this.Photo_Operator.Size = new System.Drawing.Size(229, 21);
            this.Photo_Operator.TabIndex = 1;
            this.Photo_Operator.SelectedValueChanged += new System.EventHandler(this.checkCommand);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(35, 23);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(33, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "Gerät";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(6, 46);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(62, 13);
            this.label43.TabIndex = 9;
            this.label43.Text = "Bediener/in";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Time_TimeStart);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.Time_dateTimePicker);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(523, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(162, 100);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Datum-Uhrzeit";
            // 
            // Time_TimeStart
            // 
            this.Time_TimeStart.Checked = false;
            this.Time_TimeStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time_TimeStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.Time_TimeStart.Location = new System.Drawing.Point(53, 44);
            this.Time_TimeStart.Name = "Time_TimeStart";
            this.Time_TimeStart.ShowUpDown = true;
            this.Time_TimeStart.Size = new System.Drawing.Size(89, 20);
            this.Time_TimeStart.TabIndex = 13;
            this.Time_TimeStart.ValueChanged += new System.EventHandler(this.checkCommand);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Uhrzeit:";
            // 
            // Time_dateTimePicker
            // 
            this.Time_dateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time_dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Time_dateTimePicker.Location = new System.Drawing.Point(53, 20);
            this.Time_dateTimePicker.Name = "Time_dateTimePicker";
            this.Time_dateTimePicker.Size = new System.Drawing.Size(89, 20);
            this.Time_dateTimePicker.TabIndex = 12;
            this.Time_dateTimePicker.ValueChanged += new System.EventHandler(this.checkCommand);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Datum";
            // 
            // bt_INSERT_PHOTOS
            // 
            this.bt_INSERT_PHOTOS.Enabled = false;
            this.bt_INSERT_PHOTOS.Location = new System.Drawing.Point(646, 19);
            this.bt_INSERT_PHOTOS.Name = "bt_INSERT_PHOTOS";
            this.bt_INSERT_PHOTOS.Size = new System.Drawing.Size(118, 47);
            this.bt_INSERT_PHOTOS.TabIndex = 2;
            this.bt_INSERT_PHOTOS.Text = "Fotos einfügen";
            this.bt_INSERT_PHOTOS.UseVisualStyleBackColor = true;
            this.bt_INSERT_PHOTOS.Click += new System.EventHandler(this.bt_INSERT_PHOTOS_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RTB_SQL);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(637, 106);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SQL-Kommando";
            // 
            // RTB_SQL
            // 
            this.RTB_SQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RTB_SQL.Location = new System.Drawing.Point(3, 16);
            this.RTB_SQL.Name = "RTB_SQL";
            this.RTB_SQL.ReadOnly = true;
            this.RTB_SQL.Size = new System.Drawing.Size(631, 87);
            this.RTB_SQL.TabIndex = 0;
            this.RTB_SQL.Text = "";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ts_progressbar,
            this.ts_statuslabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 490);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(793, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ts_progressbar
            // 
            this.ts_progressbar.Name = "ts_progressbar";
            this.ts_progressbar.Size = new System.Drawing.Size(100, 16);
            // 
            // ts_statuslabel
            // 
            this.ts_statuslabel.Name = "ts_statuslabel";
            this.ts_statuslabel.Size = new System.Drawing.Size(0, 17);
            // 
            // dataGridViewImage2Column1
            // 
            this.dataGridViewImage2Column1.HeaderText = "Photo";
            this.dataGridViewImage2Column1.Name = "dataGridViewImage2Column1";
            this.dataGridViewImage2Column1.ReadOnly = true;
            this.dataGridViewImage2Column1.Width = 123;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Beschreibung";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 123;
            // 
            // SubInsertGFZPhotos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 512);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Name = "SubInsertGFZPhotos";
            this.Text = "Nachträgliches Einfügen von Fotos";
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Photos_DGV)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button Photos_btAddPhotos;
        private System.Windows.Forms.DataGridView Photos_DGV;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel SPECIDs_PANEL;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox RTB_SQL;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bt_INSERT_PHOTOS;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.ComboBox Photo_Camera;
        private System.Windows.Forms.ComboBox Photo_Operator;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker Time_TimeStart;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker Time_dateTimePicker;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar ts_progressbar;
        private System.Windows.Forms.ToolStripStatusLabel ts_statuslabel;
        private System.Windows.Forms.ToolTip TT;
        private DGVExtensions.DataGridViewPhotoColumn ColPhotos;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNotes;
        private System.Windows.Forms.Button btRemovePhoto;
        private GUI.UC.UC_ImageViewer uC_ImageViewer1;
        private DGVExtensions.DataGridViewPhotoColumn dataGridViewImage2Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    }
}