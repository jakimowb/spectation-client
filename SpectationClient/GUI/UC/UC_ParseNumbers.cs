﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using SpectationClient.DataBaseDescription;
using SpectationClient.Stuff;
using Npgsql;

namespace SpectationClient {
    public partial class UC_ParseNumbers : UserControl {
        public event EventHandler LabelsChanged;

        ErrorList EL = new ErrorList();
        List<Int64> NL = new List<Int64>();
        public UC_ParseNumbers() {
            InitializeComponent();
        }

        private void RTB_TextChanged(object sender, EventArgs e) {
            NL.Clear();
            String  text = RTB.Text;
            Regex r = new Regex(@"[0123456789]+");
            foreach (Match m in r.Matches(text)) {

                Int64 val;
                if(Int64.TryParse(m.Value, out val) && !NL.Contains(val)) {
                    NL.Add(val);   
                }
            }
           
            if (LabelsChanged != null) LabelsChanged(null, new EventArgs());
            textBox1.Text = this.NumberString;
            //Regex r_nesw_ge = new Regex(@"[NOSWnosw]*");
        }

        public List<Int64> Numbers{
            get { return NL; }
        }
        public bool hasNumbers {
            get { return NL.Count > 0; }
        }
        public Int64 Count {
            get { return NL.Count; }
        }

        public String NumberString {
            get { return TextHelper.combine(NL, ","); }
        }

        public void clear() {
            this.RTB.Clear();
        }
    }
}
