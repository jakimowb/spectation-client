﻿namespace SpectationClient {
    partial class UC_CoordinateSystem {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.SRIDlabel = new System.Windows.Forms.Label();
            this.CS_SRID = new System.Windows.Forms.TextBox();
            this.CS_SELECT = new System.Windows.Forms.Button();
            this.CS_NAME = new System.Windows.Forms.TextBox();
            this.CS_UNIT = new System.Windows.Forms.TextBox();
            this.UNITLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SRIDlabel
            // 
            this.SRIDlabel.AutoSize = true;
            this.SRIDlabel.Location = new System.Drawing.Point(132, 33);
            this.SRIDlabel.Name = "SRIDlabel";
            this.SRIDlabel.Size = new System.Drawing.Size(33, 13);
            this.SRIDlabel.TabIndex = 15;
            this.SRIDlabel.Text = "SRID";
            // 
            // CS_SRID
            // 
            this.CS_SRID.Location = new System.Drawing.Point(171, 30);
            this.CS_SRID.Name = "CS_SRID";
            this.CS_SRID.ReadOnly = true;
            this.CS_SRID.Size = new System.Drawing.Size(65, 20);
            this.CS_SRID.TabIndex = 14;
            // 
            // CS_SELECT
            // 
            this.CS_SELECT.Location = new System.Drawing.Point(3, 3);
            this.CS_SELECT.Name = "CS_SELECT";
            this.CS_SELECT.Size = new System.Drawing.Size(119, 23);
            this.CS_SELECT.TabIndex = 13;
            this.CS_SELECT.Text = "Koordinaten-System";
            this.CS_SELECT.UseVisualStyleBackColor = true;
            this.CS_SELECT.Click += new System.EventHandler(this.CS_SELECT_Click);
            // 
            // CS_NAME
            // 
            this.CS_NAME.Location = new System.Drawing.Point(132, 3);
            this.CS_NAME.Name = "CS_NAME";
            this.CS_NAME.ReadOnly = true;
            this.CS_NAME.Size = new System.Drawing.Size(340, 20);
            this.CS_NAME.TabIndex = 12;
            // 
            // CS_UNIT
            // 
            this.CS_UNIT.Location = new System.Drawing.Point(348, 30);
            this.CS_UNIT.Name = "CS_UNIT";
            this.CS_UNIT.ReadOnly = true;
            this.CS_UNIT.Size = new System.Drawing.Size(95, 20);
            this.CS_UNIT.TabIndex = 16;
            // 
            // UNITLabel
            // 
            this.UNITLabel.AutoSize = true;
            this.UNITLabel.Location = new System.Drawing.Point(268, 33);
            this.UNITLabel.Name = "UNITLabel";
            this.UNITLabel.Size = new System.Drawing.Size(74, 13);
            this.UNITLabel.TabIndex = 17;
            this.UNITLabel.Text = "Längeneinheit";
            // 
            // UC_CoordinateSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.UNITLabel);
            this.Controls.Add(this.CS_UNIT);
            this.Controls.Add(this.SRIDlabel);
            this.Controls.Add(this.CS_SRID);
            this.Controls.Add(this.CS_SELECT);
            this.Controls.Add(this.CS_NAME);
            this.Name = "UC_CoordinateSystem";
            this.Size = new System.Drawing.Size(475, 59);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SRIDlabel;
        private System.Windows.Forms.TextBox CS_SRID;
        private System.Windows.Forms.Button CS_SELECT;
        private System.Windows.Forms.TextBox CS_NAME;
        private System.Windows.Forms.TextBox CS_UNIT;
        private System.Windows.Forms.Label UNITLabel;
    }
}
