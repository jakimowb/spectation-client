﻿namespace SpectationClient.GUI.UC {
    partial class UC_ImageViewer {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.PictureBox = new System.Windows.Forms.PictureBox();
            this.CMS = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsms_SaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.bildKopierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
            this.CMS.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureBox
            // 
            this.PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox.Location = new System.Drawing.Point(0, 0);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(336, 275);
            this.PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PictureBox.TabIndex = 0;
            this.PictureBox.TabStop = false;
            // 
            // CMS
            // 
            this.CMS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsms_SaveAs,
            this.bildKopierenToolStripMenuItem});
            this.CMS.Name = "CMS";
            this.CMS.Size = new System.Drawing.Size(191, 70);
            // 
            // tsms_SaveAs
            // 
            this.tsms_SaveAs.Name = "tsms_SaveAs";
            this.tsms_SaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsms_SaveAs.Size = new System.Drawing.Size(190, 22);
            this.tsms_SaveAs.Text = "Bild speichern";
            this.tsms_SaveAs.Click += new System.EventHandler(this.label_saveClick);
            // 
            // bildKopierenToolStripMenuItem
            // 
            this.bildKopierenToolStripMenuItem.Name = "bildKopierenToolStripMenuItem";
            this.bildKopierenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.bildKopierenToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.bildKopierenToolStripMenuItem.Text = "Bild kopieren";
            this.bildKopierenToolStripMenuItem.Click += new System.EventHandler(this.bildKopierenToolStripMenuItem_Click);
            // 
            // UC_ImageViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PictureBox);
            this.Name = "UC_ImageViewer";
            this.Size = new System.Drawing.Size(336, 275);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
            this.CMS.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBox;
        private System.Windows.Forms.ContextMenuStrip CMS;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.ToolStripMenuItem tsms_SaveAs;
        private System.Windows.Forms.ToolStripMenuItem bildKopierenToolStripMenuItem;
    }
}
