﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;
using SpectationClient.Stuff;

namespace SpectationClient.GUI.UC {
    public partial class UC_ImageViewer : UserControl {

        public UC_ImageViewer() {
            InitializeComponent();
            init();
        }

        private void init() {
            this.PictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            this.PictureBox.ContextMenuStrip = this.CMS;
        }

        void label_saveClick(object sender, EventArgs e) {

            SFD.Filter = DataHelper.ImageFormatFilter;
            SFD.AddExtension = true;
            
            if(SFD.ShowDialog() == DialogResult.OK) {
                
                DataHelper.saveImage(SFD.FileName, this.PictureBox.Image);
            }
        }

        public void showImage(Image image){
            if(image != null) {
                this.PictureBox.Image = image;
                

            } else {

                this.PictureBox.Image = null;
            }
            
        }

        private void bildKopierenToolStripMenuItem_Click(object sender, EventArgs e) {
            Clipboard.SetImage(this.PictureBox.Image);
        }
    }
}
