﻿namespace SpectationClient {
    partial class UC_SetCoordinate {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.tb_X = new System.Windows.Forms.TextBox();
            this.tb_Y = new System.Windows.Forms.TextBox();
            this.tb_Height = new System.Windows.Forms.TextBox();
            this.LabelX = new System.Windows.Forms.Label();
            this.LabelY = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb_X
            // 
            this.tb_X.Location = new System.Drawing.Point(82, 71);
            this.tb_X.Name = "tb_X";
            this.tb_X.Size = new System.Drawing.Size(100, 20);
            this.tb_X.TabIndex = 18;
            // 
            // tb_Y
            // 
            this.tb_Y.Location = new System.Drawing.Point(82, 102);
            this.tb_Y.Name = "tb_Y";
            this.tb_Y.Size = new System.Drawing.Size(100, 20);
            this.tb_Y.TabIndex = 19;
            // 
            // tb_Height
            // 
            this.tb_Height.Location = new System.Drawing.Point(302, 68);
            this.tb_Height.Name = "tb_Height";
            this.tb_Height.Size = new System.Drawing.Size(111, 20);
            this.tb_Height.TabIndex = 20;
            // 
            // LabelX
            // 
            this.LabelX.AutoSize = true;
            this.LabelX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LabelX.Location = new System.Drawing.Point(13, 75);
            this.LabelX.Name = "LabelX";
            this.LabelX.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelX.Size = new System.Drawing.Size(35, 13);
            this.LabelX.TabIndex = 21;
            this.LabelX.Text = "label1";
            this.LabelX.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // LabelY
            // 
            this.LabelY.AutoSize = true;
            this.LabelY.Location = new System.Drawing.Point(13, 105);
            this.LabelY.Name = "LabelY";
            this.LabelY.Size = new System.Drawing.Size(35, 13);
            this.LabelY.TabIndex = 22;
            this.LabelY.Text = "label2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(227, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Höhe ü. NN";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(417, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "m";
            // 
            // UC_SetCoordinate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LabelY);
            this.Controls.Add(this.tb_Height);
            this.Controls.Add(this.tb_Y);
            this.Controls.Add(this.tb_X);
            this.Controls.Add(this.LabelX);
            this.Name = "UC_SetCoordinate";
            this.Size = new System.Drawing.Size(478, 135);
            this.Controls.SetChildIndex(this.LabelX, 0);
            this.Controls.SetChildIndex(this.tb_X, 0);
            this.Controls.SetChildIndex(this.tb_Y, 0);
            this.Controls.SetChildIndex(this.tb_Height, 0);
            this.Controls.SetChildIndex(this.LabelY, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_X;
        private System.Windows.Forms.TextBox tb_Y;
        private System.Windows.Forms.TextBox tb_Height;
        private System.Windows.Forms.Label LabelX;
        private System.Windows.Forms.Label LabelY;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
