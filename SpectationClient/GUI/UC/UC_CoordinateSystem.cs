﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SpectationClient.GUI;
using GeoAPI.CoordinateSystems;
//using ProjNet.CoordinateSystems;
//using ProjNet.CoordinateSystems;

namespace SpectationClient {
    public partial class UC_CoordinateSystem : UserControl {
        
        protected DBManager DBM;
        
        protected const String C_LOC_LATY_LAT = "Latitude";
        protected const String C_LOC_LATY_Y = "Hochwert";
        protected const String C_LOC_LONGX_LONG = "Longitude";
        protected const String C_LOC_LONGX_X = "Rechtswert";

        protected float _C_LATY_MIN = 0;
        protected float _C_LATY_MAX = -1;
        protected float _C_LONGX_MIN = 0;
        protected float _C_LONGX_MAX = -1;
        protected long _SRID;
        protected long _DEF_SRID = 4326; //Default columnSRID GCS Lat Long WGS 84
        protected String _LABEL_LATY;
        protected String _LABEL_LONGX;
        protected String _TYPE;
        public event EventHandler LabelsChanged;
        public event EventHandler SRID_Changed;

        public UC_CoordinateSystem() {
            InitializeComponent();
        }
        public UC_CoordinateSystem(ref DBManager dbm, long initSRID) {
            InitializeComponent();
            this.DBM = dbm;
            this._DEF_SRID = initSRID;
            initnewSRID(initSRID);
            this.CS_SRID.TextChanged += new EventHandler(CS_SRID_TextChanged);
        }

        void CS_SRID_TextChanged(object sender, EventArgs e) {
            if(SRID_Changed != null) this.SRID_Changed(this, new EventArgs());
        }

        /// <summary>
        /// Returns the used columnSRID
        /// </summary>
        public long SRID { get { return _SRID; } }
        public String COORD_LABEL_LATY { get { return _LABEL_LATY; } }
        public String COORD_LABEL_LONGX { get { return _LABEL_LONGX; } }
        public String COORD_UNIT { get { return CS_UNIT.Text; } }
        public String COORD_NAME { get { return CS_NAME.Text; } }
        public String COORD_TYPE { get { return _TYPE; } }

        public float COORD_LATY_MIN { get { return _C_LATY_MIN; } }
        public float COORD_LATY_MAX { get { return _C_LATY_MAX; } }
        public float COORD_LONGX_MIN { get { return _C_LONGX_MIN; } }
        public float COORD_LONGX_MAX { get { return _C_LONGX_MAX; } }

        protected void initnewSRID(long srid){
            this._SRID = srid;
            initnewSRID(srid, DBM.getCOORD_SYS_INFO(srid));
        }

        protected void initnewSRID(long srid, DataRow SRID_INFO_Row) {
            if (Convert.ToInt64(SRID_INFO_Row["SRID"]) != srid) throw new Exception("Class: UC_CoordinateSystem\nConstructor\nError: SRID not retrieved from DBMananger!");
            initnewSRID(SRID_INFO_Row);
        }

        protected void initnewSRID(DataRow SRID_INFO_Row) {
            initnewSRID(_SRID,
                    SRID_INFO_Row["NAME"].ToString(),
                    SRID_INFO_Row["UNIT"].ToString(),
                    SRID_INFO_Row["TYPE"].ToString()
                    );
        }

        public void setSRID(long srid){
            initnewSRID(srid);
        }

        protected void initnewSRID(long srid, String name, String unit, String typename) {
            _SRID = srid;
            this.CS_SRID.Text = srid.ToString();
            this.CS_UNIT.Text = unit;
            this.CS_NAME.Text = name;
            String t_old = _TYPE;
            _TYPE = typename;
            if (t_old != _TYPE) {
                if (typename == "ProjectedCoordinateSystem" ||
                    typename == "GeocentricCoordinateSystem") {
                    _LABEL_LATY = C_LOC_LATY_Y;
                    _LABEL_LONGX = C_LOC_LONGX_X;
                    _C_LATY_MAX = 10000000;
                    _C_LATY_MIN = 0;
                    _C_LONGX_MAX = 10000000;
                    _C_LONGX_MIN = 0;                    
                } else if (typename == "GeographicCoordinateSystem") {
                    _LABEL_LATY = C_LOC_LATY_LAT;
                    _LABEL_LONGX = C_LOC_LONGX_LONG;
                    _C_LATY_MAX = 90;
                    _C_LATY_MIN = -90;
                    _C_LONGX_MAX = 180;
                    _C_LONGX_MIN = -180;
                } else {
                    throw new Exception("Class: UC_CoordinateSystem\nFunction: initnewSRID(...)\nError: unknown Coordinate System Type: " + typename);
                }
                
                if(LabelsChanged != null) LabelsChanged(null, new EventArgs());
            }
        }

        protected void CS_SELECT_Click(object sender, EventArgs e) {
            SubSelectCoordinateSystem SS = new SubSelectCoordinateSystem(ref DBM);
            this.Cursor = Cursors.WaitCursor;
            if (SS.ShowDialog() == DialogResult.OK) {
                initnewSRID(SS.SRID, SS.NAME, SS.UNIT, SS.TYPE);
            }
            this.Cursor = Cursors.Default;
        }

        
    }
}
