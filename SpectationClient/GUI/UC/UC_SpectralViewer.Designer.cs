﻿namespace SpectationClient.GUI.UC {
    partial class UC_SpectralViewer {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.CMS = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmi_setYRange_minmax = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_setYRange_01 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmi_saveChart = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_saveClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmi_saveASD = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_saveESL = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_spectraToClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_YRange_01 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_YRangeInf = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuItem_SavePNG = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_SaveSpectra = new System.Windows.Forms.ToolStripMenuItem();
            this.SVD = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.Chart)).BeginInit();
            this.CMS.SuspendLayout();
            this.SuspendLayout();
            // 
            // Chart
            // 
            chartArea1.AlignmentOrientation = ((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical | System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal)));
            chartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.AxisX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisX.LabelStyle.Interval = 0D;
            chartArea1.AxisX.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisX.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea1.AxisX.Title = "Wavelength";
            chartArea1.AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisY.LabelStyle.Format = "F3";
            chartArea1.AxisY.Title = "Value";
            chartArea1.BackColor = System.Drawing.Color.White;
            chartArea1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea1.CursorX.IsUserEnabled = true;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.CursorY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.CursorY.IsUserEnabled = true;
            chartArea1.CursorY.IsUserSelectionEnabled = true;
            chartArea1.Name = "ChartArea1";
            this.Chart.ChartAreas.Add(chartArea1);
            this.Chart.ContextMenuStrip = this.CMS;
            this.Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            legend1.Title = "Spektren";
            this.Chart.Legends.Add(legend1);
            this.Chart.Location = new System.Drawing.Point(0, 0);
            this.Chart.Name = "Chart";
            this.Chart.Size = new System.Drawing.Size(726, 341);
            this.Chart.TabIndex = 0;
            this.Chart.Text = "chart1";
            this.Chart.GetToolTipText += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs>(this.Chart_GetToolTipText_1);
            // 
            // CMS
            // 
            this.CMS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_setYRange_minmax,
            this.tsmi_setYRange_01,
            this.toolStripSeparator2,
            this.tsmi_saveChart,
            this.tsmi_saveClipboard,
            this.toolStripSeparator3,
            this.tsmi_saveASD,
            this.tsmi_saveESL,
            this.tsmi_spectraToClipboard});
            this.CMS.Name = "contextMenuStrip1";
            this.CMS.Size = new System.Drawing.Size(255, 170);
            // 
            // tsmi_setYRange_minmax
            // 
            this.tsmi_setYRange_minmax.Name = "tsmi_setYRange_minmax";
            this.tsmi_setYRange_minmax.Size = new System.Drawing.Size(254, 22);
            this.tsmi_setYRange_minmax.Text = "Y-Range [min, max]";
            this.tsmi_setYRange_minmax.Click += new System.EventHandler(this.menuItem_YRangeInf_Click);
            // 
            // tsmi_setYRange_01
            // 
            this.tsmi_setYRange_01.Name = "tsmi_setYRange_01";
            this.tsmi_setYRange_01.Size = new System.Drawing.Size(254, 22);
            this.tsmi_setYRange_01.Text = "Y-Range [0.0, 1.0]";
            this.tsmi_setYRange_01.Click += new System.EventHandler(this.menuItem_YRange_01_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(251, 6);
            // 
            // tsmi_saveChart
            // 
            this.tsmi_saveChart.Name = "tsmi_saveChart";
            this.tsmi_saveChart.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmi_saveChart.Size = new System.Drawing.Size(254, 22);
            this.tsmi_saveChart.Text = "Grafik speichern";
            this.tsmi_saveChart.ToolTipText = "Speichert den Plot";
            this.tsmi_saveChart.Click += new System.EventHandler(this.menuItem_SaveAsImage_Click);
            // 
            // tsmi_saveClipboard
            // 
            this.tsmi_saveClipboard.Name = "tsmi_saveClipboard";
            this.tsmi_saveClipboard.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.tsmi_saveClipboard.Size = new System.Drawing.Size(254, 22);
            this.tsmi_saveClipboard.Text = "Grafik kopieren";
            this.tsmi_saveClipboard.ToolTipText = "Kopiert den Plot in die Zwischenablage";
            this.tsmi_saveClipboard.Click += new System.EventHandler(this.tsmi_saveClipboard_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(251, 6);
            // 
            // tsmi_saveASD
            // 
            this.tsmi_saveASD.Name = "tsmi_saveASD";
            this.tsmi_saveASD.Size = new System.Drawing.Size(254, 22);
            this.tsmi_saveASD.Text = "Speichere als ASD FieldSpec";
            this.tsmi_saveASD.ToolTipText = "Speichert angezeigte Spektren im ASD FielSpec Format";
            this.tsmi_saveASD.Click += new System.EventHandler(this.tsmi_saveASD_Click);
            // 
            // tsmi_saveESL
            // 
            this.tsmi_saveESL.Name = "tsmi_saveESL";
            this.tsmi_saveESL.Size = new System.Drawing.Size(254, 22);
            this.tsmi_saveESL.Text = "Speichere als ENVI Spectral Library";
            this.tsmi_saveESL.ToolTipText = "Speichert angezeigte Spektren im ENVI Spectral Library Format";
            this.tsmi_saveESL.Click += new System.EventHandler(this.tsmi_saveESL_Click);
            // 
            // tsmi_spectraToClipboard
            // 
            this.tsmi_spectraToClipboard.Name = "tsmi_spectraToClipboard";
            this.tsmi_spectraToClipboard.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.C)));
            this.tsmi_spectraToClipboard.Size = new System.Drawing.Size(254, 22);
            this.tsmi_spectraToClipboard.Text = "Spektren kopieren";
            this.tsmi_spectraToClipboard.ToolTipText = "Kopiert angezeigte Spektren in die Zwischenablage";
            this.tsmi_spectraToClipboard.Click += new System.EventHandler(this.tsmi_spectraToClipboard_Click);
            // 
            // menuItem_YRange_01
            // 
            this.menuItem_YRange_01.Name = "menuItem_YRange_01";
            this.menuItem_YRange_01.Size = new System.Drawing.Size(32, 19);
            // 
            // menuItem_YRangeInf
            // 
            this.menuItem_YRangeInf.Name = "menuItem_YRangeInf";
            this.menuItem_YRangeInf.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // menuItem_SavePNG
            // 
            this.menuItem_SavePNG.Name = "menuItem_SavePNG";
            this.menuItem_SavePNG.Size = new System.Drawing.Size(32, 19);
            // 
            // menuItem_SaveSpectra
            // 
            this.menuItem_SaveSpectra.Name = "menuItem_SaveSpectra";
            this.menuItem_SaveSpectra.Size = new System.Drawing.Size(32, 19);
            // 
            // UC_SpectralViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Chart);
            this.Name = "UC_SpectralViewer";
            this.Size = new System.Drawing.Size(726, 341);
            ((System.ComponentModel.ISupportInitialize)(this.Chart)).EndInit();
            this.CMS.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart Chart;
        private System.Windows.Forms.ContextMenuStrip CMS;
        private System.Windows.Forms.ToolStripMenuItem menuItem_YRangeInf;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuItem_SavePNG;
        private System.Windows.Forms.ToolStripMenuItem menuItem_SaveSpectra;
        private System.Windows.Forms.ToolStripMenuItem menuItem_YRange_01;
        private System.Windows.Forms.SaveFileDialog SVD;
        private System.Windows.Forms.ToolStripMenuItem tsmi_setYRange_minmax;
        private System.Windows.Forms.ToolStripMenuItem tsmi_setYRange_01;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tsmi_saveChart;
        private System.Windows.Forms.ToolStripMenuItem tsmi_saveASD;
        private System.Windows.Forms.ToolStripMenuItem tsmi_saveClipboard;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem tsmi_saveESL;
        private System.Windows.Forms.ToolStripMenuItem tsmi_spectraToClipboard;
    }
}
