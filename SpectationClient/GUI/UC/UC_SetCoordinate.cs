﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpectationClient.Stuff;
using SpectationClient.GUI;

namespace SpectationClient {
    public partial class UC_SetCoordinate: UC_CoordinateSystem {
        protected ErrorList EL;
        
        public event EventHandler ErrorListIsEmpty;
        public event EventHandler ErrorAdded;
        public event EventHandler ValueChanged;

        public UC_SetCoordinate() {
            InitializeComponent();
        }

        public UC_SetCoordinate(ref DBManager dbm, long initSRID, ref ErrorList el) {
            InitializeComponent();
            this.DBM = dbm;
            this.EL = (el == null)? new ErrorList() : el;
            this.EL.ErrorListIsEmpty += new EventHandler(EL_ErrorListIsEmpty);
            this.EL.ErrorAdded += new EventHandler(EL_ErrorAdded);
            this.LabelsChanged += new EventHandler(UC_SetCoordinate_LabelsChanged);
            this.tb_X.TextChanged += new EventHandler(tb_XY_TextChanged);
            this.tb_Y.TextChanged += new EventHandler(tb_XY_TextChanged);
            this.tb_Height.TextChanged += new EventHandler(tb_Height_TextChanged);
            this.initnewSRID(initSRID);
            
        }

        void EL_ErrorAdded(object sender, EventArgs e) {
            if(this.ErrorAdded != null) this.ErrorAdded(null, new EventArgs());
        }

        void EL_ErrorListIsEmpty(object sender, EventArgs e) {
            if(this.ErrorListIsEmpty != null) this.ErrorListIsEmpty(null, new EventArgs());
        }

        void tb_Height_TextChanged(object sender, EventArgs e) {
            if(this.ValueChanged != null) this.ValueChanged(this, new EventArgs());
        }

        public void validate() {
            bool need_XY = tb_X.TextLength > 0 || tb_Y.TextLength > 0;
            EL.validation_Number(tb_Y, need_XY, _C_LATY_MIN, _C_LATY_MAX);
            EL.validation_Number(tb_X, need_XY, _C_LONGX_MIN, _C_LONGX_MAX);
            EL.validation_Number(tb_Height, false);
        }
        
        public void re_init(ref DBManager dbm, long initSRID, ErrorList el) {
            this.DBM = dbm;
            this.EL = (el == null) ? new ErrorList() : el;
            this.LabelsChanged += new EventHandler(UC_SetCoordinate_LabelsChanged);
            this.tb_X.TextChanged += new EventHandler(tb_XY_TextChanged);
            this.tb_Y.TextChanged += new EventHandler(tb_XY_TextChanged);
            this.initnewSRID(initSRID);
        
        }
        void tb_XY_TextChanged(object sender, EventArgs e) {
            //throw new NotImplementedException();
            if(this.ValueChanged != null) this.ValueChanged(this, new EventArgs());
        }

        /// <summary>
        /// Cleans all Forms, except the used columnSRID
        /// </summary>
        public void clear() {
            this.tb_X.Clear();
            this.tb_Y.Clear();
            this.tb_Height.Clear();
        }

        /// <summary>
        /// Cleans all Forms and sets the columnSRID back to a required or the default one.
        /// </summary>
        /// <param name="columnSRID">Required columnSRID. Use -1 to get back to the default one.</param>
        public void clear(long srid) {
            this.clear();
            this.setSRID((srid > 0)? srid : this._DEF_SRID);     
        }
        

        void UC_SetCoordinate_LabelsChanged(object sender, EventArgs e) {
            this.LabelX.Text = this._LABEL_LONGX;
            this.LabelY.Text = this._LABEL_LATY;
            this.validate();
            if(this.ValueChanged != null) this.ValueChanged(this, new EventArgs());
        }

        public bool isValid {
            get {return EL.isEmpty; }
        }
        public bool hasXYValues {
            get {
                float temp; 
                return float.TryParse(this.tb_X.Text, out temp) && float.TryParse(this.tb_Y.Text, out temp); }
        }
        public float XValue {
            get { return float.Parse(this.tb_X.Text); }
            set { this.tb_X.Text = value.ToString(); }
        }

        public float YValue {
            get { return float.Parse(this.tb_Y.Text); }
            set { this.tb_Y.Text = value.ToString(); }
        }

        public String XText {
            get { return this.tb_X.Text; }
            set { this.tb_X.Text = value; }
        }

        public String YText {
            get { return this.tb_Y.Text; }
            set { this.tb_Y.Text = value; }
        }

        public bool hasHeightValue {
            get {
                float temp;
                return float.TryParse(this.tb_Height.Text, out temp);
            }
        }
        public float HeightValue {
            get { return float.Parse(this.tb_Height.Text); }
            set { this.tb_Height.Text = value.ToString(); }
        }

       
    }
}
