﻿namespace SpectationClient.GUI {
    partial class SubInsertBaseInfo {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.TT = new System.Windows.Forms.ToolTip(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.SENS_bt_ReadSerialAndCalNo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.AREA_btINSERT = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.Loc_Notes = new System.Windows.Forms.TextBox();
            this.Loc_Pointname = new System.Windows.Forms.TextBox();
            this.Loc_Locname = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.CAMERA_INSERT_BT = new System.Windows.Forms.Button();
            this.CAMERA_NAME_TB = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.CAMERA_OWNER_TB = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.CAMERA_DESCR_TB = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.gB_Operator = new System.Windows.Forms.GroupBox();
            this.OP_INSERT_BT = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.OP_SURNAME_TB = new System.Windows.Forms.TextBox();
            this.OP_DESCR_TB = new System.Windows.Forms.TextBox();
            this.OP_NAME_TB = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.OP_WORKGROUP_TB = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.OP_INSTITUTE_TB = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.CAMP_Insert_bt = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.CAMP_End = new System.Windows.Forms.DateTimePicker();
            this.CAMP_Begin = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CAMP_Notes = new System.Windows.Forms.TextBox();
            this.CAMP_Area = new System.Windows.Forms.TextBox();
            this.CAMP_Name = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.WREF_INSERT_BTN = new System.Windows.Forms.Button();
            this.WREF_CALDAT_DTP = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.WREF_DESCR_TB = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.WREF_CAL = new System.Windows.Forms.TextBox();
            this.WREF_Name = new System.Windows.Forms.ComboBox();
            this.WREF_ID = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.WREF_CALFILE_TB = new System.Windows.Forms.TextBox();
            this.WREF_setFileBt = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SENS_btINSERT = new System.Windows.Forms.Button();
            this.SENS_SCalDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SENS_SNotes = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SENS_SCalNo = new System.Windows.Forms.TextBox();
            this.SENS_SName = new System.Windows.Forms.ComboBox();
            this.SENS_SSerial = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SENS_SCalFilePath = new System.Windows.Forms.TextBox();
            this.SENS_FilePathbtn = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.gB_Operator.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // EP
            // 
            this.EP.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.EP.ContainerControl = this;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 35);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Name";
            this.TT.SetToolTip(this.label12, "z.B. ASD FieldSpec");
            // 
            // SENS_bt_ReadSerialAndCalNo
            // 
            this.SENS_bt_ReadSerialAndCalNo.Location = new System.Drawing.Point(333, 36);
            this.SENS_bt_ReadSerialAndCalNo.Name = "SENS_bt_ReadSerialAndCalNo";
            this.SENS_bt_ReadSerialAndCalNo.Size = new System.Drawing.Size(75, 69);
            this.SENS_bt_ReadSerialAndCalNo.TabIndex = 12;
            this.SENS_bt_ReadSerialAndCalNo.Text = "Auslesen aus ASD Datei";
            this.TT.SetToolTip(this.SENS_bt_ReadSerialAndCalNo, "Ließt aus einem mit dem Sensor erstellten ASd-Spektrum den Sensortyp, die Herstll" +
        "er-ID sowie die Nummer der letzten Kalibrierung heraus.");
            this.SENS_bt_ReadSerialAndCalNo.UseVisualStyleBackColor = true;
            this.SENS_bt_ReadSerialAndCalNo.Click += new System.EventHandler(this.bt_ReadSerialAndCalNo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            this.TT.SetToolTip(this.label1, "z.B. ASD FieldSpec");
            // 
            // OFD
            // 
            this.OFD.FileName = "openFileDialog1";
            // 
            // AREA_btINSERT
            // 
            this.AREA_btINSERT.Location = new System.Drawing.Point(331, 15);
            this.AREA_btINSERT.Name = "AREA_btINSERT";
            this.AREA_btINSERT.Size = new System.Drawing.Size(75, 23);
            this.AREA_btINSERT.TabIndex = 32;
            this.AREA_btINSERT.Text = "Einfügen";
            this.AREA_btINSERT.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(10, 342);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(73, 13);
            this.label37.TabIndex = 31;
            this.label37.Text = "Bemerkungen";
            // 
            // Loc_Notes
            // 
            this.Loc_Notes.Location = new System.Drawing.Point(89, 339);
            this.Loc_Notes.Name = "Loc_Notes";
            this.Loc_Notes.Size = new System.Drawing.Size(303, 20);
            this.Loc_Notes.TabIndex = 27;
            // 
            // Loc_Pointname
            // 
            this.Loc_Pointname.Location = new System.Drawing.Point(49, 89);
            this.Loc_Pointname.Name = "Loc_Pointname";
            this.Loc_Pointname.Size = new System.Drawing.Size(172, 20);
            this.Loc_Pointname.TabIndex = 26;
            // 
            // Loc_Locname
            // 
            this.Loc_Locname.Location = new System.Drawing.Point(49, 61);
            this.Loc_Locname.Name = "Loc_Locname";
            this.Loc_Locname.Size = new System.Drawing.Size(167, 20);
            this.Loc_Locname.TabIndex = 25;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(10, 62);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(24, 13);
            this.label38.TabIndex = 29;
            this.label38.Text = "Ort";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(8, 90);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "Punkt";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(82, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 20);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(68, 13);
            this.label36.TabIndex = 2;
            this.label36.Text = "Übernehmen";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(552, 271);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Kamera";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.CAMERA_INSERT_BT);
            this.groupBox4.Controls.Add(this.CAMERA_NAME_TB);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.CAMERA_OWNER_TB);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.CAMERA_DESCR_TB);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(546, 265);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Einfügen in CORE.CAMERAS";
            // 
            // CAMERA_INSERT_BT
            // 
            this.CAMERA_INSERT_BT.Location = new System.Drawing.Point(11, 118);
            this.CAMERA_INSERT_BT.Name = "CAMERA_INSERT_BT";
            this.CAMERA_INSERT_BT.Size = new System.Drawing.Size(122, 72);
            this.CAMERA_INSERT_BT.TabIndex = 6;
            this.CAMERA_INSERT_BT.Text = "Einfügen";
            this.CAMERA_INSERT_BT.UseVisualStyleBackColor = true;
            this.CAMERA_INSERT_BT.Click += new System.EventHandler(this.CAMERA_INSERT_BT_Click);
            // 
            // CAMERA_NAME_TB
            // 
            this.CAMERA_NAME_TB.Location = new System.Drawing.Point(136, 25);
            this.CAMERA_NAME_TB.Name = "CAMERA_NAME_TB";
            this.CAMERA_NAME_TB.Size = new System.Drawing.Size(213, 20);
            this.CAMERA_NAME_TB.TabIndex = 0;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(60, 77);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(73, 13);
            this.label25.TabIndex = 5;
            this.label25.Text = "Bemerkungen";
            // 
            // CAMERA_OWNER_TB
            // 
            this.CAMERA_OWNER_TB.Location = new System.Drawing.Point(136, 51);
            this.CAMERA_OWNER_TB.Name = "CAMERA_OWNER_TB";
            this.CAMERA_OWNER_TB.Size = new System.Drawing.Size(213, 20);
            this.CAMERA_OWNER_TB.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(81, 51);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "Besitzer";
            // 
            // CAMERA_DESCR_TB
            // 
            this.CAMERA_DESCR_TB.Location = new System.Drawing.Point(136, 77);
            this.CAMERA_DESCR_TB.Name = "CAMERA_DESCR_TB";
            this.CAMERA_DESCR_TB.Size = new System.Drawing.Size(213, 20);
            this.CAMERA_DESCR_TB.TabIndex = 2;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(7, 28);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(126, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "Name / Bezeichnung";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.gB_Operator);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(552, 271);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Operator";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // gB_Operator
            // 
            this.gB_Operator.Controls.Add(this.OP_INSERT_BT);
            this.gB_Operator.Controls.Add(this.label22);
            this.gB_Operator.Controls.Add(this.OP_SURNAME_TB);
            this.gB_Operator.Controls.Add(this.OP_DESCR_TB);
            this.gB_Operator.Controls.Add(this.OP_NAME_TB);
            this.gB_Operator.Controls.Add(this.label21);
            this.gB_Operator.Controls.Add(this.label18);
            this.gB_Operator.Controls.Add(this.OP_WORKGROUP_TB);
            this.gB_Operator.Controls.Add(this.label19);
            this.gB_Operator.Controls.Add(this.label20);
            this.gB_Operator.Controls.Add(this.OP_INSTITUTE_TB);
            this.gB_Operator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gB_Operator.Location = new System.Drawing.Point(3, 3);
            this.gB_Operator.Name = "gB_Operator";
            this.gB_Operator.Size = new System.Drawing.Size(546, 265);
            this.gB_Operator.TabIndex = 11;
            this.gB_Operator.TabStop = false;
            this.gB_Operator.Text = "Einfügen in CORE.OPERATORS";
            // 
            // OP_INSERT_BT
            // 
            this.OP_INSERT_BT.Location = new System.Drawing.Point(18, 179);
            this.OP_INSERT_BT.Name = "OP_INSERT_BT";
            this.OP_INSERT_BT.Size = new System.Drawing.Size(211, 41);
            this.OP_INSERT_BT.TabIndex = 10;
            this.OP_INSERT_BT.Text = "Operator / Benutzer \r\neinfügen";
            this.OP_INSERT_BT.UseVisualStyleBackColor = true;
            this.OP_INSERT_BT.Click += new System.EventHandler(this.OP_INSERT_BT_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(15, 140);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 9;
            this.label22.Text = "Bemerkungen";
            // 
            // OP_SURNAME_TB
            // 
            this.OP_SURNAME_TB.Location = new System.Drawing.Point(92, 54);
            this.OP_SURNAME_TB.Name = "OP_SURNAME_TB";
            this.OP_SURNAME_TB.Size = new System.Drawing.Size(155, 20);
            this.OP_SURNAME_TB.TabIndex = 2;
            // 
            // OP_DESCR_TB
            // 
            this.OP_DESCR_TB.Location = new System.Drawing.Point(92, 137);
            this.OP_DESCR_TB.Name = "OP_DESCR_TB";
            this.OP_DESCR_TB.Size = new System.Drawing.Size(211, 20);
            this.OP_DESCR_TB.TabIndex = 8;
            // 
            // OP_NAME_TB
            // 
            this.OP_NAME_TB.Location = new System.Drawing.Point(92, 26);
            this.OP_NAME_TB.Name = "OP_NAME_TB";
            this.OP_NAME_TB.Size = new System.Drawing.Size(155, 20);
            this.OP_NAME_TB.TabIndex = 0;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 111);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 13);
            this.label21.TabIndex = 7;
            this.label21.Text = "Arbeitsgruppe";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(21, 29);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Vorname";
            // 
            // OP_WORKGROUP_TB
            // 
            this.OP_WORKGROUP_TB.Location = new System.Drawing.Point(92, 108);
            this.OP_WORKGROUP_TB.Name = "OP_WORKGROUP_TB";
            this.OP_WORKGROUP_TB.Size = new System.Drawing.Size(211, 20);
            this.OP_WORKGROUP_TB.TabIndex = 6;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(21, 57);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(67, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "Nachname";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(21, 82);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "Institut";
            // 
            // OP_INSTITUTE_TB
            // 
            this.OP_INSTITUTE_TB.Location = new System.Drawing.Point(92, 79);
            this.OP_INSTITUTE_TB.Name = "OP_INSTITUTE_TB";
            this.OP_INSTITUTE_TB.Size = new System.Drawing.Size(155, 20);
            this.OP_INSTITUTE_TB.TabIndex = 4;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(552, 271);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Kampagne";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.CAMP_Insert_bt);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.CAMP_End);
            this.groupBox2.Controls.Add(this.CAMP_Begin);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.CAMP_Notes);
            this.groupBox2.Controls.Add(this.CAMP_Area);
            this.groupBox2.Controls.Add(this.CAMP_Name);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(465, 214);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Einfügen in CORE.CAMPAIGNS";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(6, 78);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(36, 13);
            this.label35.TabIndex = 17;
            this.label35.Text = "Dauer";
            // 
            // CAMP_Insert_bt
            // 
            this.CAMP_Insert_bt.BackColor = System.Drawing.SystemColors.Control;
            this.CAMP_Insert_bt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.CAMP_Insert_bt.Location = new System.Drawing.Point(6, 159);
            this.CAMP_Insert_bt.Name = "CAMP_Insert_bt";
            this.CAMP_Insert_bt.Size = new System.Drawing.Size(146, 36);
            this.CAMP_Insert_bt.TabIndex = 16;
            this.CAMP_Insert_bt.Text = "Kampagne Einfügen";
            this.CAMP_Insert_bt.UseVisualStyleBackColor = false;
            this.CAMP_Insert_bt.Click += new System.EventHandler(this.CAMP_Insert_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(44, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "bis";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(44, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "von";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Bemerkungen";
            // 
            // CAMP_End
            // 
            this.CAMP_End.Location = new System.Drawing.Point(85, 98);
            this.CAMP_End.Name = "CAMP_End";
            this.CAMP_End.ShowCheckBox = true;
            this.CAMP_End.Size = new System.Drawing.Size(200, 20);
            this.CAMP_End.TabIndex = 7;
            // 
            // CAMP_Begin
            // 
            this.CAMP_Begin.Location = new System.Drawing.Point(85, 72);
            this.CAMP_Begin.Name = "CAMP_Begin";
            this.CAMP_Begin.ShowCheckBox = true;
            this.CAMP_Begin.Size = new System.Drawing.Size(200, 20);
            this.CAMP_Begin.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Gebiet";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Name";
            // 
            // CAMP_Notes
            // 
            this.CAMP_Notes.Location = new System.Drawing.Point(85, 128);
            this.CAMP_Notes.Name = "CAMP_Notes";
            this.CAMP_Notes.Size = new System.Drawing.Size(374, 20);
            this.CAMP_Notes.TabIndex = 2;
            // 
            // CAMP_Area
            // 
            this.CAMP_Area.Location = new System.Drawing.Point(85, 46);
            this.CAMP_Area.Name = "CAMP_Area";
            this.CAMP_Area.Size = new System.Drawing.Size(200, 20);
            this.CAMP_Area.TabIndex = 1;
            // 
            // CAMP_Name
            // 
            this.CAMP_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CAMP_Name.Location = new System.Drawing.Point(85, 20);
            this.CAMP_Name.Name = "CAMP_Name";
            this.CAMP_Name.Size = new System.Drawing.Size(200, 20);
            this.CAMP_Name.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(552, 271);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Spectralon";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.AutoSize = true;
            this.groupBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox3.Controls.Add(this.WREF_INSERT_BTN);
            this.groupBox3.Controls.Add(this.WREF_CALDAT_DTP);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.WREF_DESCR_TB);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.WREF_CAL);
            this.groupBox3.Controls.Add(this.WREF_Name);
            this.groupBox3.Controls.Add(this.WREF_ID);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.WREF_CALFILE_TB);
            this.groupBox3.Controls.Add(this.WREF_setFileBt);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(534, 254);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Einfügen in CORE.WREF_PANELS";
            // 
            // WREF_INSERT_BTN
            // 
            this.WREF_INSERT_BTN.Location = new System.Drawing.Point(6, 199);
            this.WREF_INSERT_BTN.Name = "WREF_INSERT_BTN";
            this.WREF_INSERT_BTN.Size = new System.Drawing.Size(146, 36);
            this.WREF_INSERT_BTN.TabIndex = 7;
            this.WREF_INSERT_BTN.Text = "Spectralon Einfügen";
            this.WREF_INSERT_BTN.UseVisualStyleBackColor = true;
            this.WREF_INSERT_BTN.Click += new System.EventHandler(this.WREF_INSERT_BTN_Click);
            // 
            // WREF_CALDAT_DTP
            // 
            this.WREF_CALDAT_DTP.Checked = false;
            this.WREF_CALDAT_DTP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.WREF_CALDAT_DTP.Location = new System.Drawing.Point(131, 115);
            this.WREF_CALDAT_DTP.Name = "WREF_CALDAT_DTP";
            this.WREF_CALDAT_DTP.ShowCheckBox = true;
            this.WREF_CALDAT_DTP.Size = new System.Drawing.Size(146, 20);
            this.WREF_CALDAT_DTP.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 118);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Letzte Kalibrierung";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 144);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Kalibrierdatei";
            // 
            // WREF_DESCR_TB
            // 
            this.WREF_DESCR_TB.Location = new System.Drawing.Point(108, 169);
            this.WREF_DESCR_TB.Name = "WREF_DESCR_TB";
            this.WREF_DESCR_TB.Size = new System.Drawing.Size(420, 20);
            this.WREF_DESCR_TB.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 91);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(117, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Nr. der Kalibrierung";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 61);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Hersteller-ID";
            // 
            // WREF_CAL
            // 
            this.WREF_CAL.Location = new System.Drawing.Point(131, 87);
            this.WREF_CAL.Name = "WREF_CAL";
            this.WREF_CAL.Size = new System.Drawing.Size(180, 20);
            this.WREF_CAL.TabIndex = 2;
            // 
            // WREF_Name
            // 
            this.WREF_Name.FormattingEnabled = true;
            this.WREF_Name.Location = new System.Drawing.Point(131, 32);
            this.WREF_Name.Name = "WREF_Name";
            this.WREF_Name.Size = new System.Drawing.Size(180, 21);
            this.WREF_Name.TabIndex = 0;
            // 
            // WREF_ID
            // 
            this.WREF_ID.Location = new System.Drawing.Point(131, 61);
            this.WREF_ID.Name = "WREF_ID";
            this.WREF_ID.Size = new System.Drawing.Size(180, 20);
            this.WREF_ID.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 172);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "Bemerkungen";
            // 
            // WREF_CALFILE_TB
            // 
            this.WREF_CALFILE_TB.Location = new System.Drawing.Point(161, 141);
            this.WREF_CALFILE_TB.Name = "WREF_CALFILE_TB";
            this.WREF_CALFILE_TB.ReadOnly = true;
            this.WREF_CALFILE_TB.Size = new System.Drawing.Size(367, 20);
            this.WREF_CALFILE_TB.TabIndex = 5;
            // 
            // WREF_setFileBt
            // 
            this.WREF_setFileBt.Location = new System.Drawing.Point(108, 139);
            this.WREF_setFileBt.Name = "WREF_setFileBt";
            this.WREF_setFileBt.Size = new System.Drawing.Size(47, 23);
            this.WREF_setFileBt.TabIndex = 4;
            this.WREF_setFileBt.Text = "...";
            this.WREF_setFileBt.UseVisualStyleBackColor = true;
            this.WREF_setFileBt.Click += new System.EventHandler(this.WREF_setFileBt_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(552, 327);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Sensor";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.SENS_btINSERT);
            this.groupBox1.Controls.Add(this.SENS_SCalDate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.SENS_SNotes);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.SENS_bt_ReadSerialAndCalNo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.SENS_SCalNo);
            this.groupBox1.Controls.Add(this.SENS_SName);
            this.groupBox1.Controls.Add(this.SENS_SSerial);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.SENS_SCalFilePath);
            this.groupBox1.Controls.Add(this.SENS_FilePathbtn);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(545, 254);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Einfügen in CORE.SENSORS";
            // 
            // SENS_btINSERT
            // 
            this.SENS_btINSERT.Location = new System.Drawing.Point(6, 199);
            this.SENS_btINSERT.Name = "SENS_btINSERT";
            this.SENS_btINSERT.Size = new System.Drawing.Size(146, 36);
            this.SENS_btINSERT.TabIndex = 7;
            this.SENS_btINSERT.Text = "Sensor Einfügen";
            this.SENS_btINSERT.UseVisualStyleBackColor = true;
            this.SENS_btINSERT.Click += new System.EventHandler(this.SENS_btINSERT_Click);
            // 
            // SENS_SCalDate
            // 
            this.SENS_SCalDate.Checked = false;
            this.SENS_SCalDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.SENS_SCalDate.Location = new System.Drawing.Point(131, 114);
            this.SENS_SCalDate.Name = "SENS_SCalDate";
            this.SENS_SCalDate.ShowCheckBox = true;
            this.SENS_SCalDate.Size = new System.Drawing.Size(146, 20);
            this.SENS_SCalDate.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Letzte Kalibrierung";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Kalibrierdatei";
            // 
            // SENS_SNotes
            // 
            this.SENS_SNotes.Location = new System.Drawing.Point(108, 169);
            this.SENS_SNotes.Name = "SENS_SNotes";
            this.SENS_SNotes.Size = new System.Drawing.Size(431, 20);
            this.SENS_SNotes.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nr. der Kalibrierung";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Hersteller-ID";
            // 
            // SENS_SCalNo
            // 
            this.SENS_SCalNo.Location = new System.Drawing.Point(131, 87);
            this.SENS_SCalNo.Name = "SENS_SCalNo";
            this.SENS_SCalNo.Size = new System.Drawing.Size(180, 20);
            this.SENS_SCalNo.TabIndex = 11;
            // 
            // SENS_SName
            // 
            this.SENS_SName.FormattingEnabled = true;
            this.SENS_SName.Location = new System.Drawing.Point(131, 32);
            this.SENS_SName.Name = "SENS_SName";
            this.SENS_SName.Size = new System.Drawing.Size(180, 21);
            this.SENS_SName.TabIndex = 4;
            // 
            // SENS_SSerial
            // 
            this.SENS_SSerial.Location = new System.Drawing.Point(131, 61);
            this.SENS_SSerial.Name = "SENS_SSerial";
            this.SENS_SSerial.Size = new System.Drawing.Size(180, 20);
            this.SENS_SSerial.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Bemerkungen";
            // 
            // SENS_SCalFilePath
            // 
            this.SENS_SCalFilePath.Location = new System.Drawing.Point(161, 140);
            this.SENS_SCalFilePath.Name = "SENS_SCalFilePath";
            this.SENS_SCalFilePath.ReadOnly = true;
            this.SENS_SCalFilePath.Size = new System.Drawing.Size(378, 20);
            this.SENS_SCalFilePath.TabIndex = 9;
            // 
            // SENS_FilePathbtn
            // 
            this.SENS_FilePathbtn.Location = new System.Drawing.Point(108, 139);
            this.SENS_FilePathbtn.Name = "SENS_FilePathbtn";
            this.SENS_FilePathbtn.Size = new System.Drawing.Size(47, 23);
            this.SENS_FilePathbtn.TabIndex = 8;
            this.SENS_FilePathbtn.Text = "...";
            this.SENS_FilePathbtn.UseVisualStyleBackColor = true;
            this.SENS_FilePathbtn.Click += new System.EventHandler(this.FilePathbtn_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(560, 353);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 0;
            // 
            // SubInsertBaseInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 353);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SubInsertBaseInfo";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Basisinformationen in die Datenbank einfügen";
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.gB_Operator.ResumeLayout(false);
            this.gB_Operator.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip TT;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox Loc_Notes;
        private System.Windows.Forms.TextBox Loc_Pointname;
        private System.Windows.Forms.TextBox Loc_Locname;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button AREA_btINSERT;
        //private UC_SetCoordinate uC_SetCoordinate1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button SENS_btINSERT;
        private System.Windows.Forms.DateTimePicker SENS_SCalDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SENS_SNotes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SENS_bt_ReadSerialAndCalNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox SENS_SCalNo;
        private System.Windows.Forms.ComboBox SENS_SName;
        private System.Windows.Forms.TextBox SENS_SSerial;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox SENS_SCalFilePath;
        private System.Windows.Forms.Button SENS_FilePathbtn;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button WREF_INSERT_BTN;
        private System.Windows.Forms.DateTimePicker WREF_CALDAT_DTP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox WREF_DESCR_TB;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox WREF_CAL;
        private System.Windows.Forms.ComboBox WREF_Name;
        private System.Windows.Forms.TextBox WREF_ID;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox WREF_CALFILE_TB;
        private System.Windows.Forms.Button WREF_setFileBt;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button CAMP_Insert_bt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker CAMP_End;
        private System.Windows.Forms.DateTimePicker CAMP_Begin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox CAMP_Notes;
        private System.Windows.Forms.TextBox CAMP_Area;
        private System.Windows.Forms.TextBox CAMP_Name;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox gB_Operator;
        private System.Windows.Forms.Button OP_INSERT_BT;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox OP_SURNAME_TB;
        private System.Windows.Forms.TextBox OP_DESCR_TB;
        private System.Windows.Forms.TextBox OP_NAME_TB;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox OP_WORKGROUP_TB;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox OP_INSTITUTE_TB;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button CAMERA_INSERT_BT;
        private System.Windows.Forms.TextBox CAMERA_NAME_TB;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox CAMERA_OWNER_TB;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox CAMERA_DESCR_TB;
        private System.Windows.Forms.Label label23;
    }
}