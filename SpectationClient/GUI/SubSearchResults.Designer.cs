﻿namespace SpectationClient.GUI {
    partial class SubSearchResults {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubSearchResults));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.prog_bar = new System.Windows.Forms.ToolStripProgressBar();
            this.prog_statuslabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.SPEC_dgv = new System.Windows.Forms.DataGridView();
            this.SPEC_gb = new System.Windows.Forms.GroupBox();
            this.SPEC_waitlabel = new System.Windows.Forms.Label();
            this.SPEC_bt_view = new System.Windows.Forms.Button();
            this.gb_CONNECTED = new System.Windows.Forms.GroupBox();
            this.gB_CONNECTED_flp = new System.Windows.Forms.FlowLayoutPanel();
            this.gb_WREF = new System.Windows.Forms.GroupBox();
            this.WR_showBt = new System.Windows.Forms.Button();
            this.WR_waitlabel = new System.Windows.Forms.Label();
            this.WR_dgv = new System.Windows.Forms.DataGridView();
            this.gb_LOCATIONS = new System.Windows.Forms.GroupBox();
            this.LOC_waitlabel = new System.Windows.Forms.Label();
            this.LOC_dgv = new System.Windows.Forms.DataGridView();
            this.gb_OBSAREA = new System.Windows.Forms.GroupBox();
            this.OBSAREA_wl = new System.Windows.Forms.Label();
            this.OBSAREA_dgv = new System.Windows.Forms.DataGridView();
            this.PHOTOS_gb = new System.Windows.Forms.GroupBox();
            this.PHOTOS_showBt = new System.Windows.Forms.Button();
            this.PHOTOS_waitlabel = new System.Windows.Forms.Label();
            this.PHOTOS_dgv = new System.Windows.Forms.DataGridView();
            this.gb_VEG_SP = new System.Windows.Forms.GroupBox();
            this.VEG_SP_waitlabel = new System.Windows.Forms.Label();
            this.VEG_SP_dgv = new System.Windows.Forms.DataGridView();
            this.gb_VEG_PS = new System.Windows.Forms.GroupBox();
            this.VEG_PS_waitlabel = new System.Windows.Forms.Label();
            this.VEG_PS_dgv = new System.Windows.Forms.DataGridView();
            this.gb_SOILS = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SOILS_dgv = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RTB = new System.Windows.Forms.RichTextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btSaveALL = new System.Windows.Forms.Button();
            this.Save_FileFormat_ESL = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.Save_SEP = new System.Windows.Forms.ComboBox();
            this.cbSaveSelectedOnly = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btSaveFotosFromDB = new System.Windows.Forms.CheckBox();
            this.Save_FileFormat_ASD = new System.Windows.Forms.RadioButton();
            this.gb_CheckBoxes = new System.Windows.Forms.GroupBox();
            this.cb_SOILS = new System.Windows.Forms.CheckBox();
            this.cb_SPEC = new System.Windows.Forms.CheckBox();
            this.rbShowSelectedOnly = new System.Windows.Forms.RadioButton();
            this.rbShowALL = new System.Windows.Forms.RadioButton();
            this.cb_LOCATIONS = new System.Windows.Forms.CheckBox();
            this.cb_PHOTOS = new System.Windows.Forms.CheckBox();
            this.cb_WREF = new System.Windows.Forms.CheckBox();
            this.cb_VEG_PS = new System.Windows.Forms.CheckBox();
            this.cb_OBSAREA = new System.Windows.Forms.CheckBox();
            this.cb_VEG_SP = new System.Windows.Forms.CheckBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmSearchParameters = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSQLCommandString = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsTBLIMIT = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tstbOFFSET = new System.Windows.Forms.ToolStripTextBox();
            this.tsbOFFSET_BACK = new System.Windows.Forms.ToolStripButton();
            this.tsbOFFSET_FORWARD = new System.Windows.Forms.ToolStripButton();
            this.FBD = new System.Windows.Forms.FolderBrowserDialog();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.TT = new System.Windows.Forms.ToolTip(this.components);
            this.TIMER = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPEC_dgv)).BeginInit();
            this.SPEC_gb.SuspendLayout();
            this.gb_CONNECTED.SuspendLayout();
            this.gB_CONNECTED_flp.SuspendLayout();
            this.gb_WREF.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WR_dgv)).BeginInit();
            this.gb_LOCATIONS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LOC_dgv)).BeginInit();
            this.gb_OBSAREA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OBSAREA_dgv)).BeginInit();
            this.PHOTOS_gb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PHOTOS_dgv)).BeginInit();
            this.gb_VEG_SP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VEG_SP_dgv)).BeginInit();
            this.gb_VEG_PS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VEG_PS_dgv)).BeginInit();
            this.gb_SOILS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SOILS_dgv)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gb_CheckBoxes.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.prog_bar,
            this.prog_statuslabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 687);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1138, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(119, 17);
            this.toolStripStatusLabel1.Text = "Datenbankaktivitäten";
            // 
            // prog_bar
            // 
            this.prog_bar.Name = "prog_bar";
            this.prog_bar.Size = new System.Drawing.Size(100, 16);
            // 
            // prog_statuslabel
            // 
            this.prog_statuslabel.Name = "prog_statuslabel";
            this.prog_statuslabel.Size = new System.Drawing.Size(37, 17);
            this.prog_statuslabel.Text = "Bereit";
            // 
            // SPEC_dgv
            // 
            this.SPEC_dgv.AllowUserToAddRows = false;
            this.SPEC_dgv.AllowUserToDeleteRows = false;
            this.SPEC_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SPEC_dgv.Location = new System.Drawing.Point(12, 55);
            this.SPEC_dgv.Name = "SPEC_dgv";
            this.SPEC_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SPEC_dgv.Size = new System.Drawing.Size(842, 68);
            this.SPEC_dgv.TabIndex = 1;
            this.SPEC_dgv.SelectionChanged += new System.EventHandler(this.SPEC_dgv_SelectionChanged);
            // 
            // SPEC_gb
            // 
            this.SPEC_gb.AutoSize = true;
            this.SPEC_gb.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SPEC_gb.Controls.Add(this.SPEC_waitlabel);
            this.SPEC_gb.Controls.Add(this.SPEC_bt_view);
            this.SPEC_gb.Controls.Add(this.SPEC_dgv);
            this.SPEC_gb.Location = new System.Drawing.Point(2, 2);
            this.SPEC_gb.Name = "SPEC_gb";
            this.SPEC_gb.Size = new System.Drawing.Size(860, 142);
            this.SPEC_gb.TabIndex = 0;
            this.SPEC_gb.TabStop = false;
            this.SPEC_gb.Text = "Spektren";
            // 
            // SPEC_waitlabel
            // 
            this.SPEC_waitlabel.AutoSize = true;
            this.SPEC_waitlabel.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.SPEC_waitlabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SPEC_waitlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SPEC_waitlabel.ForeColor = System.Drawing.Color.YellowGreen;
            this.SPEC_waitlabel.Location = new System.Drawing.Point(22, 94);
            this.SPEC_waitlabel.Name = "SPEC_waitlabel";
            this.SPEC_waitlabel.Size = new System.Drawing.Size(0, 29);
            this.SPEC_waitlabel.TabIndex = 3;
            // 
            // SPEC_bt_view
            // 
            this.SPEC_bt_view.Location = new System.Drawing.Point(12, 19);
            this.SPEC_bt_view.Name = "SPEC_bt_view";
            this.SPEC_bt_view.Size = new System.Drawing.Size(174, 23);
            this.SPEC_bt_view.TabIndex = 2;
            this.SPEC_bt_view.Text = "Ausgewählte Spektren anzeigen";
            this.SPEC_bt_view.UseVisualStyleBackColor = true;
            this.SPEC_bt_view.Click += new System.EventHandler(this.SPEC_showBt_Click);
            // 
            // gb_CONNECTED
            // 
            this.gb_CONNECTED.AutoSize = true;
            this.gb_CONNECTED.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_CONNECTED.Controls.Add(this.gB_CONNECTED_flp);
            this.gb_CONNECTED.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_CONNECTED.Location = new System.Drawing.Point(0, 0);
            this.gb_CONNECTED.MinimumSize = new System.Drawing.Size(860, 50);
            this.gb_CONNECTED.Name = "gb_CONNECTED";
            this.gb_CONNECTED.Size = new System.Drawing.Size(897, 488);
            this.gb_CONNECTED.TabIndex = 7;
            this.gb_CONNECTED.TabStop = false;
            this.gb_CONNECTED.Text = "Mit den Spektren verbundene Informationen";
            // 
            // gB_CONNECTED_flp
            // 
            this.gB_CONNECTED_flp.AutoScroll = true;
            this.gB_CONNECTED_flp.AutoSize = true;
            this.gB_CONNECTED_flp.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gB_CONNECTED_flp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gB_CONNECTED_flp.Controls.Add(this.gb_WREF);
            this.gB_CONNECTED_flp.Controls.Add(this.gb_LOCATIONS);
            this.gB_CONNECTED_flp.Controls.Add(this.gb_OBSAREA);
            this.gB_CONNECTED_flp.Controls.Add(this.PHOTOS_gb);
            this.gB_CONNECTED_flp.Controls.Add(this.gb_VEG_SP);
            this.gB_CONNECTED_flp.Controls.Add(this.gb_VEG_PS);
            this.gB_CONNECTED_flp.Controls.Add(this.gb_SOILS);
            this.gB_CONNECTED_flp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gB_CONNECTED_flp.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.gB_CONNECTED_flp.Location = new System.Drawing.Point(3, 16);
            this.gB_CONNECTED_flp.Margin = new System.Windows.Forms.Padding(5);
            this.gB_CONNECTED_flp.Name = "gB_CONNECTED_flp";
            this.gB_CONNECTED_flp.Padding = new System.Windows.Forms.Padding(5);
            this.gB_CONNECTED_flp.Size = new System.Drawing.Size(891, 469);
            this.gB_CONNECTED_flp.TabIndex = 0;
            this.gB_CONNECTED_flp.WrapContents = false;
            // 
            // gb_WREF
            // 
            this.gb_WREF.AutoSize = true;
            this.gb_WREF.Controls.Add(this.WR_showBt);
            this.gb_WREF.Controls.Add(this.WR_waitlabel);
            this.gb_WREF.Controls.Add(this.WR_dgv);
            this.gb_WREF.Location = new System.Drawing.Point(8, 8);
            this.gb_WREF.Name = "gb_WREF";
            this.gb_WREF.Size = new System.Drawing.Size(848, 119);
            this.gb_WREF.TabIndex = 1;
            this.gb_WREF.TabStop = false;
            this.gb_WREF.Text = "Weißreferenzspektren";
            // 
            // WR_showBt
            // 
            this.WR_showBt.Enabled = false;
            this.WR_showBt.Location = new System.Drawing.Point(6, 19);
            this.WR_showBt.Name = "WR_showBt";
            this.WR_showBt.Size = new System.Drawing.Size(195, 23);
            this.WR_showBt.TabIndex = 6;
            this.WR_showBt.Text = "Ausgewählte Spektren anzeigen";
            this.WR_showBt.UseVisualStyleBackColor = true;
            this.WR_showBt.Click += new System.EventHandler(this.WR_showBt_Click);
            // 
            // WR_waitlabel
            // 
            this.WR_waitlabel.AutoSize = true;
            this.WR_waitlabel.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.WR_waitlabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WR_waitlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WR_waitlabel.ForeColor = System.Drawing.Color.YellowGreen;
            this.WR_waitlabel.Location = new System.Drawing.Point(22, 71);
            this.WR_waitlabel.Name = "WR_waitlabel";
            this.WR_waitlabel.Size = new System.Drawing.Size(0, 29);
            this.WR_waitlabel.TabIndex = 5;
            // 
            // WR_dgv
            // 
            this.WR_dgv.AllowUserToAddRows = false;
            this.WR_dgv.AllowUserToDeleteRows = false;
            this.WR_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WR_dgv.Location = new System.Drawing.Point(6, 48);
            this.WR_dgv.Name = "WR_dgv";
            this.WR_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.WR_dgv.Size = new System.Drawing.Size(836, 52);
            this.WR_dgv.TabIndex = 4;
            this.WR_dgv.SelectionChanged += new System.EventHandler(this.WR_dgv_SelectionChanged);
            // 
            // gb_LOCATIONS
            // 
            this.gb_LOCATIONS.AutoSize = true;
            this.gb_LOCATIONS.Controls.Add(this.LOC_waitlabel);
            this.gb_LOCATIONS.Controls.Add(this.LOC_dgv);
            this.gb_LOCATIONS.Location = new System.Drawing.Point(8, 133);
            this.gb_LOCATIONS.Name = "gb_LOCATIONS";
            this.gb_LOCATIONS.Size = new System.Drawing.Size(848, 92);
            this.gb_LOCATIONS.TabIndex = 2;
            this.gb_LOCATIONS.TabStop = false;
            this.gb_LOCATIONS.Text = "Koordinaten/Orte";
            // 
            // LOC_waitlabel
            // 
            this.LOC_waitlabel.AutoSize = true;
            this.LOC_waitlabel.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.LOC_waitlabel.Enabled = false;
            this.LOC_waitlabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LOC_waitlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LOC_waitlabel.ForeColor = System.Drawing.Color.YellowGreen;
            this.LOC_waitlabel.Location = new System.Drawing.Point(7, 33);
            this.LOC_waitlabel.Name = "LOC_waitlabel";
            this.LOC_waitlabel.Size = new System.Drawing.Size(0, 29);
            this.LOC_waitlabel.TabIndex = 5;
            // 
            // LOC_dgv
            // 
            this.LOC_dgv.AllowUserToAddRows = false;
            this.LOC_dgv.AllowUserToDeleteRows = false;
            this.LOC_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LOC_dgv.Location = new System.Drawing.Point(6, 19);
            this.LOC_dgv.Name = "LOC_dgv";
            this.LOC_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.LOC_dgv.Size = new System.Drawing.Size(836, 54);
            this.LOC_dgv.TabIndex = 4;
            // 
            // gb_OBSAREA
            // 
            this.gb_OBSAREA.AutoSize = true;
            this.gb_OBSAREA.Controls.Add(this.OBSAREA_wl);
            this.gb_OBSAREA.Controls.Add(this.OBSAREA_dgv);
            this.gb_OBSAREA.Location = new System.Drawing.Point(8, 231);
            this.gb_OBSAREA.Name = "gb_OBSAREA";
            this.gb_OBSAREA.Size = new System.Drawing.Size(848, 95);
            this.gb_OBSAREA.TabIndex = 6;
            this.gb_OBSAREA.TabStop = false;
            this.gb_OBSAREA.Text = "Beobachtungsflächen";
            this.TT.SetToolTip(this.gb_OBSAREA, "Inhalte aus der Tabelle C_GFZ.OBSERVATION_AREAS");
            // 
            // OBSAREA_wl
            // 
            this.OBSAREA_wl.AutoSize = true;
            this.OBSAREA_wl.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.OBSAREA_wl.Enabled = false;
            this.OBSAREA_wl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OBSAREA_wl.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OBSAREA_wl.ForeColor = System.Drawing.Color.YellowGreen;
            this.OBSAREA_wl.Location = new System.Drawing.Point(6, 33);
            this.OBSAREA_wl.Name = "OBSAREA_wl";
            this.OBSAREA_wl.Size = new System.Drawing.Size(0, 29);
            this.OBSAREA_wl.TabIndex = 6;
            // 
            // OBSAREA_dgv
            // 
            this.OBSAREA_dgv.AllowUserToAddRows = false;
            this.OBSAREA_dgv.AllowUserToDeleteRows = false;
            this.OBSAREA_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OBSAREA_dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.OBSAREA_dgv.Location = new System.Drawing.Point(5, 19);
            this.OBSAREA_dgv.Name = "OBSAREA_dgv";
            this.OBSAREA_dgv.ReadOnly = true;
            this.OBSAREA_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.OBSAREA_dgv.Size = new System.Drawing.Size(837, 57);
            this.OBSAREA_dgv.TabIndex = 7;
            // 
            // PHOTOS_gb
            // 
            this.PHOTOS_gb.AutoSize = true;
            this.PHOTOS_gb.Controls.Add(this.PHOTOS_showBt);
            this.PHOTOS_gb.Controls.Add(this.PHOTOS_waitlabel);
            this.PHOTOS_gb.Controls.Add(this.PHOTOS_dgv);
            this.PHOTOS_gb.Location = new System.Drawing.Point(8, 332);
            this.PHOTOS_gb.Name = "PHOTOS_gb";
            this.PHOTOS_gb.Size = new System.Drawing.Size(848, 105);
            this.PHOTOS_gb.TabIndex = 2;
            this.PHOTOS_gb.TabStop = false;
            this.PHOTOS_gb.Text = "Fotos";
            // 
            // PHOTOS_showBt
            // 
            this.PHOTOS_showBt.Enabled = false;
            this.PHOTOS_showBt.Location = new System.Drawing.Point(5, 17);
            this.PHOTOS_showBt.Name = "PHOTOS_showBt";
            this.PHOTOS_showBt.Size = new System.Drawing.Size(174, 23);
            this.PHOTOS_showBt.TabIndex = 7;
            this.PHOTOS_showBt.Text = "Ausgewählte Fotos anzeigen";
            this.PHOTOS_showBt.UseVisualStyleBackColor = true;
            this.PHOTOS_showBt.Click += new System.EventHandler(this.PHOTOS_showBt_Click);
            // 
            // PHOTOS_waitlabel
            // 
            this.PHOTOS_waitlabel.AutoSize = true;
            this.PHOTOS_waitlabel.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.PHOTOS_waitlabel.Enabled = false;
            this.PHOTOS_waitlabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PHOTOS_waitlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PHOTOS_waitlabel.ForeColor = System.Drawing.Color.YellowGreen;
            this.PHOTOS_waitlabel.Location = new System.Drawing.Point(22, 57);
            this.PHOTOS_waitlabel.Name = "PHOTOS_waitlabel";
            this.PHOTOS_waitlabel.Size = new System.Drawing.Size(0, 29);
            this.PHOTOS_waitlabel.TabIndex = 6;
            // 
            // PHOTOS_dgv
            // 
            this.PHOTOS_dgv.AllowUserToAddRows = false;
            this.PHOTOS_dgv.AllowUserToDeleteRows = false;
            this.PHOTOS_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PHOTOS_dgv.Location = new System.Drawing.Point(6, 46);
            this.PHOTOS_dgv.Name = "PHOTOS_dgv";
            this.PHOTOS_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PHOTOS_dgv.Size = new System.Drawing.Size(836, 40);
            this.PHOTOS_dgv.TabIndex = 5;
            this.PHOTOS_dgv.SelectionChanged += new System.EventHandler(this.PHOTOS_dgv_SelectionChanged);
            // 
            // gb_VEG_SP
            // 
            this.gb_VEG_SP.AutoSize = true;
            this.gb_VEG_SP.Controls.Add(this.VEG_SP_waitlabel);
            this.gb_VEG_SP.Controls.Add(this.VEG_SP_dgv);
            this.gb_VEG_SP.Location = new System.Drawing.Point(8, 443);
            this.gb_VEG_SP.Name = "gb_VEG_SP";
            this.gb_VEG_SP.Size = new System.Drawing.Size(848, 94);
            this.gb_VEG_SP.TabIndex = 4;
            this.gb_VEG_SP.TabStop = false;
            this.gb_VEG_SP.Text = "Einzelpflanzen";
            // 
            // VEG_SP_waitlabel
            // 
            this.VEG_SP_waitlabel.AutoSize = true;
            this.VEG_SP_waitlabel.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.VEG_SP_waitlabel.Enabled = false;
            this.VEG_SP_waitlabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.VEG_SP_waitlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VEG_SP_waitlabel.ForeColor = System.Drawing.Color.YellowGreen;
            this.VEG_SP_waitlabel.Location = new System.Drawing.Point(33, 46);
            this.VEG_SP_waitlabel.Name = "VEG_SP_waitlabel";
            this.VEG_SP_waitlabel.Size = new System.Drawing.Size(0, 29);
            this.VEG_SP_waitlabel.TabIndex = 4;
            // 
            // VEG_SP_dgv
            // 
            this.VEG_SP_dgv.AllowUserToAddRows = false;
            this.VEG_SP_dgv.AllowUserToDeleteRows = false;
            this.VEG_SP_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VEG_SP_dgv.Location = new System.Drawing.Point(6, 23);
            this.VEG_SP_dgv.Name = "VEG_SP_dgv";
            this.VEG_SP_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.VEG_SP_dgv.Size = new System.Drawing.Size(836, 52);
            this.VEG_SP_dgv.TabIndex = 3;
            // 
            // gb_VEG_PS
            // 
            this.gb_VEG_PS.AutoSize = true;
            this.gb_VEG_PS.Controls.Add(this.VEG_PS_waitlabel);
            this.gb_VEG_PS.Controls.Add(this.VEG_PS_dgv);
            this.gb_VEG_PS.Location = new System.Drawing.Point(8, 543);
            this.gb_VEG_PS.Name = "gb_VEG_PS";
            this.gb_VEG_PS.Size = new System.Drawing.Size(848, 90);
            this.gb_VEG_PS.TabIndex = 5;
            this.gb_VEG_PS.TabStop = false;
            this.gb_VEG_PS.Text = "Pflanzengesellschaften";
            // 
            // VEG_PS_waitlabel
            // 
            this.VEG_PS_waitlabel.AutoSize = true;
            this.VEG_PS_waitlabel.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.VEG_PS_waitlabel.Enabled = false;
            this.VEG_PS_waitlabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.VEG_PS_waitlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VEG_PS_waitlabel.ForeColor = System.Drawing.Color.YellowGreen;
            this.VEG_PS_waitlabel.Location = new System.Drawing.Point(16, 42);
            this.VEG_PS_waitlabel.Name = "VEG_PS_waitlabel";
            this.VEG_PS_waitlabel.Size = new System.Drawing.Size(0, 29);
            this.VEG_PS_waitlabel.TabIndex = 4;
            // 
            // VEG_PS_dgv
            // 
            this.VEG_PS_dgv.AllowUserToAddRows = false;
            this.VEG_PS_dgv.AllowUserToDeleteRows = false;
            this.VEG_PS_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VEG_PS_dgv.Location = new System.Drawing.Point(6, 24);
            this.VEG_PS_dgv.Name = "VEG_PS_dgv";
            this.VEG_PS_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.VEG_PS_dgv.Size = new System.Drawing.Size(836, 47);
            this.VEG_PS_dgv.TabIndex = 2;
            // 
            // gb_SOILS
            // 
            this.gb_SOILS.AutoSize = true;
            this.gb_SOILS.Controls.Add(this.label3);
            this.gb_SOILS.Controls.Add(this.SOILS_dgv);
            this.gb_SOILS.Location = new System.Drawing.Point(8, 639);
            this.gb_SOILS.Name = "gb_SOILS";
            this.gb_SOILS.Size = new System.Drawing.Size(848, 90);
            this.gb_SOILS.TabIndex = 6;
            this.gb_SOILS.TabStop = false;
            this.gb_SOILS.Text = "Böden";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label3.Enabled = false;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.YellowGreen;
            this.label3.Location = new System.Drawing.Point(16, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 29);
            this.label3.TabIndex = 4;
            // 
            // SOILS_dgv
            // 
            this.SOILS_dgv.AllowUserToAddRows = false;
            this.SOILS_dgv.AllowUserToDeleteRows = false;
            this.SOILS_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SOILS_dgv.Location = new System.Drawing.Point(6, 24);
            this.SOILS_dgv.Name = "SOILS_dgv";
            this.SOILS_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SOILS_dgv.Size = new System.Drawing.Size(836, 47);
            this.SOILS_dgv.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.flowLayoutPanel1);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.gb_CheckBoxes);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(908, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(227, 656);
            this.panel2.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RTB);
            this.groupBox3.Location = new System.Drawing.Point(3, 462);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(214, 193);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Info";
            // 
            // RTB
            // 
            this.RTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RTB.Location = new System.Drawing.Point(3, 16);
            this.RTB.Name = "RTB";
            this.RTB.ReadOnly = true;
            this.RTB.Size = new System.Drawing.Size(208, 174);
            this.RTB.TabIndex = 0;
            this.RTB.Text = "";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(233, 22);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(0, 0);
            this.flowLayoutPanel1.TabIndex = 7;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.Controls.Add(this.btSaveALL);
            this.groupBox2.Controls.Add(this.Save_FileFormat_ESL);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.Save_SEP);
            this.groupBox2.Controls.Add(this.cbSaveSelectedOnly);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.btSaveFotosFromDB);
            this.groupBox2.Controls.Add(this.Save_FileFormat_ASD);
            this.groupBox2.Location = new System.Drawing.Point(3, 251);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(214, 205);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ergebnisse Speichern";
            // 
            // btSaveALL
            // 
            this.btSaveALL.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btSaveALL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSaveALL.Location = new System.Drawing.Point(6, 152);
            this.btSaveALL.Name = "btSaveALL";
            this.btSaveALL.Size = new System.Drawing.Size(202, 34);
            this.btSaveALL.TabIndex = 6;
            this.btSaveALL.Text = "Angaben speichern";
            this.btSaveALL.UseVisualStyleBackColor = true;
            this.btSaveALL.Click += new System.EventHandler(this.btSaveAll_Click);
            // 
            // Save_FileFormat_ESL
            // 
            this.Save_FileFormat_ESL.AutoSize = true;
            this.Save_FileFormat_ESL.Checked = true;
            this.Save_FileFormat_ESL.Location = new System.Drawing.Point(70, 129);
            this.Save_FileFormat_ESL.Name = "Save_FileFormat_ESL";
            this.Save_FileFormat_ESL.Size = new System.Drawing.Size(45, 17);
            this.Save_FileFormat_ESL.TabIndex = 13;
            this.Save_FileFormat_ESL.TabStop = true;
            this.Save_FileFormat_ESL.Text = "ESL";
            this.TT.SetToolTip(this.Save_FileFormat_ESL, "Abspeichern als ENVI-Spectral Library");
            this.Save_FileFormat_ESL.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Separator für Text Tabellen";
            // 
            // Save_SEP
            // 
            this.Save_SEP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Save_SEP.FormattingEnabled = true;
            this.Save_SEP.Location = new System.Drawing.Point(6, 89);
            this.Save_SEP.Name = "Save_SEP";
            this.Save_SEP.Size = new System.Drawing.Size(121, 21);
            this.Save_SEP.TabIndex = 11;
            // 
            // cbSaveSelectedOnly
            // 
            this.cbSaveSelectedOnly.AutoSize = true;
            this.cbSaveSelectedOnly.Location = new System.Drawing.Point(6, 45);
            this.cbSaveSelectedOnly.Name = "cbSaveSelectedOnly";
            this.cbSaveSelectedOnly.Size = new System.Drawing.Size(195, 17);
            this.cbSaveSelectedOnly.TabIndex = 9;
            this.cbSaveSelectedOnly.Text = "nur selektierte Einträge abspeichern";
            this.cbSaveSelectedOnly.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Dateiformat für Spektren";
            // 
            // btSaveFotosFromDB
            // 
            this.btSaveFotosFromDB.AutoSize = true;
            this.btSaveFotosFromDB.Location = new System.Drawing.Point(6, 22);
            this.btSaveFotosFromDB.Name = "btSaveFotosFromDB";
            this.btSaveFotosFromDB.Size = new System.Drawing.Size(157, 17);
            this.btSaveFotosFromDB.TabIndex = 7;
            this.btSaveFotosFromDB.Text = "Fotos aus Datenbank laden";
            this.btSaveFotosFromDB.UseVisualStyleBackColor = true;
            // 
            // Save_FileFormat_ASD
            // 
            this.Save_FileFormat_ASD.AutoSize = true;
            this.Save_FileFormat_ASD.Location = new System.Drawing.Point(17, 129);
            this.Save_FileFormat_ASD.Name = "Save_FileFormat_ASD";
            this.Save_FileFormat_ASD.Size = new System.Drawing.Size(47, 17);
            this.Save_FileFormat_ASD.TabIndex = 12;
            this.Save_FileFormat_ASD.Text = "ASD";
            this.TT.SetToolTip(this.Save_FileFormat_ASD, "Abspeichern im ASD-FieldSpec Format");
            this.Save_FileFormat_ASD.UseVisualStyleBackColor = true;
            // 
            // gb_CheckBoxes
            // 
            this.gb_CheckBoxes.Controls.Add(this.cb_SOILS);
            this.gb_CheckBoxes.Controls.Add(this.cb_SPEC);
            this.gb_CheckBoxes.Controls.Add(this.rbShowSelectedOnly);
            this.gb_CheckBoxes.Controls.Add(this.rbShowALL);
            this.gb_CheckBoxes.Controls.Add(this.cb_LOCATIONS);
            this.gb_CheckBoxes.Controls.Add(this.cb_PHOTOS);
            this.gb_CheckBoxes.Controls.Add(this.cb_WREF);
            this.gb_CheckBoxes.Controls.Add(this.cb_VEG_PS);
            this.gb_CheckBoxes.Controls.Add(this.cb_OBSAREA);
            this.gb_CheckBoxes.Controls.Add(this.cb_VEG_SP);
            this.gb_CheckBoxes.Location = new System.Drawing.Point(3, 3);
            this.gb_CheckBoxes.Name = "gb_CheckBoxes";
            this.gb_CheckBoxes.Size = new System.Drawing.Size(215, 242);
            this.gb_CheckBoxes.TabIndex = 18;
            this.gb_CheckBoxes.TabStop = false;
            this.gb_CheckBoxes.Text = "Ergebnisstabellen";
            // 
            // cb_SOILS
            // 
            this.cb_SOILS.AutoSize = true;
            this.cb_SOILS.Location = new System.Drawing.Point(6, 212);
            this.cb_SOILS.Name = "cb_SOILS";
            this.cb_SOILS.Size = new System.Drawing.Size(57, 17);
            this.cb_SOILS.TabIndex = 19;
            this.cb_SOILS.Text = "Böden";
            this.cb_SOILS.UseVisualStyleBackColor = true;
            // 
            // cb_SPEC
            // 
            this.cb_SPEC.AutoSize = true;
            this.cb_SPEC.Checked = true;
            this.cb_SPEC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_SPEC.Location = new System.Drawing.Point(6, 19);
            this.cb_SPEC.Name = "cb_SPEC";
            this.cb_SPEC.Size = new System.Drawing.Size(153, 17);
            this.cb_SPEC.TabIndex = 18;
            this.cb_SPEC.Text = "Spektren aus Suchanfrage";
            this.cb_SPEC.UseVisualStyleBackColor = true;
            // 
            // rbShowSelectedOnly
            // 
            this.rbShowSelectedOnly.AutoSize = true;
            this.rbShowSelectedOnly.Location = new System.Drawing.Point(6, 63);
            this.rbShowSelectedOnly.Name = "rbShowSelectedOnly";
            this.rbShowSelectedOnly.Size = new System.Drawing.Size(151, 17);
            this.rbShowSelectedOnly.TabIndex = 16;
            this.rbShowSelectedOnly.Text = "zu ausgewählten Spektren";
            this.rbShowSelectedOnly.UseVisualStyleBackColor = true;
            // 
            // rbShowALL
            // 
            this.rbShowALL.AutoSize = true;
            this.rbShowALL.Checked = true;
            this.rbShowALL.Location = new System.Drawing.Point(6, 42);
            this.rbShowALL.Name = "rbShowALL";
            this.rbShowALL.Size = new System.Drawing.Size(107, 17);
            this.rbShowALL.TabIndex = 15;
            this.rbShowALL.TabStop = true;
            this.rbShowALL.Text = "zu allen Spektren";
            this.rbShowALL.UseVisualStyleBackColor = true;
            this.rbShowALL.CheckedChanged += new System.EventHandler(this.rbShowResults_checkedChanged);
            // 
            // cb_LOCATIONS
            // 
            this.cb_LOCATIONS.AutoSize = true;
            this.cb_LOCATIONS.Location = new System.Drawing.Point(6, 107);
            this.cb_LOCATIONS.Name = "cb_LOCATIONS";
            this.cb_LOCATIONS.Size = new System.Drawing.Size(108, 17);
            this.cb_LOCATIONS.TabIndex = 1;
            this.cb_LOCATIONS.Text = "Koordinaten/Orte";
            this.cb_LOCATIONS.UseVisualStyleBackColor = true;
            // 
            // cb_PHOTOS
            // 
            this.cb_PHOTOS.AutoSize = true;
            this.cb_PHOTOS.Location = new System.Drawing.Point(6, 149);
            this.cb_PHOTOS.Name = "cb_PHOTOS";
            this.cb_PHOTOS.Size = new System.Drawing.Size(52, 17);
            this.cb_PHOTOS.TabIndex = 17;
            this.cb_PHOTOS.Text = "Fotos";
            this.cb_PHOTOS.UseVisualStyleBackColor = true;
            // 
            // cb_WREF
            // 
            this.cb_WREF.AutoSize = true;
            this.cb_WREF.Location = new System.Drawing.Point(6, 86);
            this.cb_WREF.Name = "cb_WREF";
            this.cb_WREF.Size = new System.Drawing.Size(134, 17);
            this.cb_WREF.TabIndex = 0;
            this.cb_WREF.Text = "Weissreferenzspektren";
            this.cb_WREF.UseVisualStyleBackColor = true;
            // 
            // cb_VEG_PS
            // 
            this.cb_VEG_PS.AutoSize = true;
            this.cb_VEG_PS.Location = new System.Drawing.Point(6, 191);
            this.cb_VEG_PS.Name = "cb_VEG_PS";
            this.cb_VEG_PS.Size = new System.Drawing.Size(135, 17);
            this.cb_VEG_PS.TabIndex = 4;
            this.cb_VEG_PS.Text = "Pflanzengesellschaften";
            this.cb_VEG_PS.UseVisualStyleBackColor = true;
            // 
            // cb_OBSAREA
            // 
            this.cb_OBSAREA.AutoSize = true;
            this.cb_OBSAREA.Location = new System.Drawing.Point(6, 128);
            this.cb_OBSAREA.Name = "cb_OBSAREA";
            this.cb_OBSAREA.Size = new System.Drawing.Size(130, 17);
            this.cb_OBSAREA.TabIndex = 2;
            this.cb_OBSAREA.Text = "Beobachtungsflächen";
            this.cb_OBSAREA.UseVisualStyleBackColor = true;
            // 
            // cb_VEG_SP
            // 
            this.cb_VEG_SP.AutoSize = true;
            this.cb_VEG_SP.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cb_VEG_SP.Location = new System.Drawing.Point(6, 170);
            this.cb_VEG_SP.Name = "cb_VEG_SP";
            this.cb_VEG_SP.Size = new System.Drawing.Size(94, 17);
            this.cb_VEG_SP.TabIndex = 3;
            this.cb_VEG_SP.Text = "Einzelpflanzen";
            this.cb_VEG_SP.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.tsbSearch,
            this.toolStripLabel1,
            this.tsTBLIMIT,
            this.toolStripLabel2,
            this.tstbOFFSET,
            this.tsbOFFSET_BACK,
            this.tsbOFFSET_FORWARD});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1138, 25);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmSearchParameters,
            this.tsmSQLCommandString});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(115, 22);
            this.toolStripDropDownButton1.Text = "Infos Suchanfrage";
            // 
            // tsmSearchParameters
            // 
            this.tsmSearchParameters.Name = "tsmSearchParameters";
            this.tsmSearchParameters.Size = new System.Drawing.Size(218, 22);
            this.tsmSearchParameters.Text = "verwendete Suchparameter";
            this.tsmSearchParameters.Click += new System.EventHandler(this.tsmSearchParameters_Click);
            // 
            // tsmSQLCommandString
            // 
            this.tsmSQLCommandString.Name = "tsmSQLCommandString";
            this.tsmSQLCommandString.Size = new System.Drawing.Size(218, 22);
            this.tsmSQLCommandString.Text = "verwendete SQL-Anfrage";
            this.tsmSQLCommandString.Click += new System.EventHandler(this.tsmSQLCommandString_Click);
            // 
            // tsbSearch
            // 
            this.tsbSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbSearch.Image = ((System.Drawing.Image)(resources.GetObject("tsbSearch.Image")));
            this.tsbSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSearch.Name = "tsbSearch";
            this.tsbSearch.Size = new System.Drawing.Size(87, 22);
            this.tsbSearch.Text = "Erneut Suchen";
            this.tsbSearch.ToolTipText = "Startet erneut die vordefinierte Suchanfrage.";
            this.tsbSearch.Click += new System.EventHandler(this.tsbSearch_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(67, 22);
            this.toolStripLabel1.Text = "max. Zeilen";
            // 
            // tsTBLIMIT
            // 
            this.tsTBLIMIT.Name = "tsTBLIMIT";
            this.tsTBLIMIT.Size = new System.Drawing.Size(50, 25);
            this.tsTBLIMIT.Text = "200";
            this.tsTBLIMIT.TextChanged += new System.EventHandler(this.tstbOFFSETLIMIT_TextChanged);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(54, 22);
            this.toolStripLabel2.Text = "Startzeile";
            // 
            // tstbOFFSET
            // 
            this.tstbOFFSET.Name = "tstbOFFSET";
            this.tstbOFFSET.Size = new System.Drawing.Size(50, 25);
            this.tstbOFFSET.Text = "0";
            this.tstbOFFSET.TextChanged += new System.EventHandler(this.tstbOFFSETLIMIT_TextChanged);
            // 
            // tsbOFFSET_BACK
            // 
            this.tsbOFFSET_BACK.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbOFFSET_BACK.Image = ((System.Drawing.Image)(resources.GetObject("tsbOFFSET_BACK.Image")));
            this.tsbOFFSET_BACK.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOFFSET_BACK.Name = "tsbOFFSET_BACK";
            this.tsbOFFSET_BACK.Size = new System.Drawing.Size(54, 22);
            this.tsbOFFSET_BACK.Text = "<zurück";
            this.tsbOFFSET_BACK.Click += new System.EventHandler(this.tsbOFFSET_BACK_Click);
            // 
            // tsbOFFSET_FORWARD
            // 
            this.tsbOFFSET_FORWARD.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbOFFSET_FORWARD.Image = ((System.Drawing.Image)(resources.GetObject("tsbOFFSET_FORWARD.Image")));
            this.tsbOFFSET_FORWARD.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOFFSET_FORWARD.Name = "tsbOFFSET_FORWARD";
            this.tsbOFFSET_FORWARD.Size = new System.Drawing.Size(51, 22);
            this.tsbOFFSET_FORWARD.Text = "weiter>";
            this.tsbOFFSET_FORWARD.Click += new System.EventHandler(this.tsbOFFSET_FORWARD_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 905F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.splitContainer1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1138, 662);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.SPEC_gb);
            this.splitContainer1.Panel1MinSize = 120;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gb_CONNECTED);
            this.splitContainer1.Size = new System.Drawing.Size(899, 656);
            this.splitContainer1.SplitterDistance = 162;
            this.splitContainer1.TabIndex = 1;
            // 
            // TIMER
            // 
            this.TIMER.Interval = 500;
            // 
            // SubSearchResults
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 709);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "SubSearchResults";
            this.Text = "Suchergebnisse";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPEC_dgv)).EndInit();
            this.SPEC_gb.ResumeLayout(false);
            this.SPEC_gb.PerformLayout();
            this.gb_CONNECTED.ResumeLayout(false);
            this.gb_CONNECTED.PerformLayout();
            this.gB_CONNECTED_flp.ResumeLayout(false);
            this.gB_CONNECTED_flp.PerformLayout();
            this.gb_WREF.ResumeLayout(false);
            this.gb_WREF.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WR_dgv)).EndInit();
            this.gb_LOCATIONS.ResumeLayout(false);
            this.gb_LOCATIONS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LOC_dgv)).EndInit();
            this.gb_OBSAREA.ResumeLayout(false);
            this.gb_OBSAREA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OBSAREA_dgv)).EndInit();
            this.PHOTOS_gb.ResumeLayout(false);
            this.PHOTOS_gb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PHOTOS_dgv)).EndInit();
            this.gb_VEG_SP.ResumeLayout(false);
            this.gb_VEG_SP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VEG_SP_dgv)).EndInit();
            this.gb_VEG_PS.ResumeLayout(false);
            this.gb_VEG_PS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VEG_PS_dgv)).EndInit();
            this.gb_SOILS.ResumeLayout(false);
            this.gb_SOILS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SOILS_dgv)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gb_CheckBoxes.ResumeLayout(false);
            this.gb_CheckBoxes.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridView SPEC_dgv;
        private System.Windows.Forms.GroupBox SPEC_gb;
        private System.Windows.Forms.GroupBox gb_WREF;
        private System.Windows.Forms.GroupBox gb_LOCATIONS;
        private System.Windows.Forms.GroupBox gb_VEG_PS;
        private System.Windows.Forms.DataGridView VEG_PS_dgv;
        private System.Windows.Forms.GroupBox gb_VEG_SP;
        private System.Windows.Forms.DataGridView VEG_SP_dgv;
        private System.Windows.Forms.DataGridView LOC_dgv;
        private System.Windows.Forms.Button SPEC_bt_view;
        private System.Windows.Forms.Label SPEC_waitlabel;
        private System.Windows.Forms.Label LOC_waitlabel;
        private System.Windows.Forms.Label VEG_SP_waitlabel;
        private System.Windows.Forms.Label VEG_PS_waitlabel;
        private System.Windows.Forms.GroupBox PHOTOS_gb;
        private System.Windows.Forms.Label WR_waitlabel;
        private System.Windows.Forms.DataGridView WR_dgv;
        private System.Windows.Forms.Label PHOTOS_waitlabel;
        private System.Windows.Forms.DataGridView PHOTOS_dgv;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.FolderBrowserDialog FBD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ToolStripProgressBar prog_bar;
        private System.Windows.Forms.ToolStripStatusLabel prog_statuslabel;
        private System.Windows.Forms.CheckBox cbSaveSelectedOnly;
        private System.Windows.Forms.CheckBox btSaveFotosFromDB;
        private System.Windows.Forms.Button btSaveALL;
        private System.Windows.Forms.ToolTip TT;
        private System.Windows.Forms.ComboBox Save_SEP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button PHOTOS_showBt;
        private System.Windows.Forms.Button WR_showBt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton Save_FileFormat_ESL;
        private System.Windows.Forms.RadioButton Save_FileFormat_ASD;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem tsmSearchParameters;
        private System.Windows.Forms.ToolStripMenuItem tsmSQLCommandString;
        private System.Windows.Forms.GroupBox gb_OBSAREA;
        private System.Windows.Forms.Label OBSAREA_wl;
        private System.Windows.Forms.DataGridView OBSAREA_dgv;
        private System.Windows.Forms.RadioButton rbShowSelectedOnly;
        private System.Windows.Forms.RadioButton rbShowALL;
        private System.Windows.Forms.ToolStripButton tsbSearch;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox tsTBLIMIT;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox tstbOFFSET;
        private System.Windows.Forms.ToolStripButton tsbOFFSET_BACK;
        private System.Windows.Forms.ToolStripButton tsbOFFSET_FORWARD;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Timer TIMER;
        private System.Windows.Forms.GroupBox gb_CONNECTED;
        private System.Windows.Forms.FlowLayoutPanel gB_CONNECTED_flp;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox cb_VEG_SP;
        private System.Windows.Forms.CheckBox cb_OBSAREA;
        private System.Windows.Forms.CheckBox cb_LOCATIONS;
        private System.Windows.Forms.CheckBox cb_WREF;
        private System.Windows.Forms.CheckBox cb_VEG_PS;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox RTB;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.CheckBox cb_PHOTOS;
        private System.Windows.Forms.GroupBox gb_CheckBoxes;
        private System.Windows.Forms.CheckBox cb_SPEC;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.CheckBox cb_SOILS;
        private System.Windows.Forms.GroupBox gb_SOILS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView SOILS_dgv;
    }
}