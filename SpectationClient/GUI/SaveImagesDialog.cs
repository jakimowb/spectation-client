﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using SpectationClient.Stuff;


namespace SpectationClient.GUI {
    public partial class SaveImagesDialog : Form {

        DataGridView dgv;
        ErrorList EL;

        /// <summary>
        /// Returns a specified file extension, e.g. "jpg" or "png". Removes leading/trailing blanks or dots.
        /// </summary>
        public ImageFormat ImageFormat { 
            get {
                ImageFormat format = cbImageFormat.SelectedValue as ImageFormat;
                BoxItem item = cbImageFormat.SelectedItem as BoxItem;
                return format;
               
            }
            
        }
        public String[] FileNameColumns {
            get { return getSelectedColumns(); } 
        }
        public String Folder { get { return this.tbFolder.Text; } }

        public bool UseRowNumbers { get { return rbFileName_RowNumber.Checked; } }

        public SaveImagesDialog() {
            InitializeComponent();
            init();
        }

        public SaveImagesDialog(DataGridView dgv) {
            InitializeComponent();
            this.dgv = dgv;
            init();
        }

        private void init() {
            this.Icon = Resources.Icon;
            EL = new ErrorList();
            cbImageFormat.DataSource = new BindingSource(DataHelper.ImageFormatFilters, null);
            cbImageFormat.DisplayMember = "Display";
            cbImageFormat.ValueMember = "Value";
            cbImageFormat.SelectedIndex = 0;

            if(this.dgv != null) {
                foreach(DataGridViewColumn c in this.dgv.Columns) {
                    if(c.ValueType != typeof(Byte[])) {
                        String display = c.Name;
                        BoxItem item = new BoxItem(display, c.Name);
                        clbColumns.Items.Add(item, false);
                    }
                }
            }
        }

        private void btFolder_Click(object sender, EventArgs e) {
            FBD.Description = "Bitte Speicherort angeben";
            if(FBD.ShowDialog() == System.Windows.Forms.DialogResult.OK){
                tbFolder.Text = FBD.SelectedPath;
            }
        }

        //Returns all checked DGV column names
        private String[] getSelectedColumns() {
            return (from BoxItem item in clbColumns.CheckedItems
                             select item.Value as String).ToArray();
        }
        private void validation_event(object sender, EventArgs e) {
            validation();
        }
        private void validation() {
            if(!Directory.Exists(tbFolder.Text)) {
                EL.addError(tbFolder, "Ordner existiert nicht");
            } else {
                EL.removeError(tbFolder);
            }

            //Preview
            btOK.Enabled = EL.Count == 0 && tbFolder.Text.Trim().Length > 0;
        }

        private void btOK_Click(object sender, EventArgs e) {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void rbFileName_ColumnValues_CheckedChanged(object sender, EventArgs e) {
            this.clbColumns.Enabled = rbFileName_ColumnValues.Checked;
        }
    }
}
