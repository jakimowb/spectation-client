﻿namespace SpectationClient.GUI.UC {
    partial class ViewImageForm {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.uC_ImageViewer1 = new GUI.UC.UC_ImageViewer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.FLP_PreviewChoice = new System.Windows.Forms.FlowLayoutPanel();
            this.rbUsePreview = new System.Windows.Forms.RadioButton();
            this.rbUseOriginal = new System.Windows.Forms.RadioButton();
            this.labelInfoText = new System.Windows.Forms.Label();
            this.TT = new System.Windows.Forms.ToolTip(this.components);
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.FLP_PreviewChoice.SuspendLayout();
            this.SuspendLayout();
            // 
            // uC_ImageViewer1
            // 
            this.uC_ImageViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uC_ImageViewer1.Location = new System.Drawing.Point(3, 3);
            this.uC_ImageViewer1.Name = "uC_ImageViewer1";
            this.uC_ImageViewer1.Size = new System.Drawing.Size(473, 364);
            this.uC_ImageViewer1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.uC_ImageViewer1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.FLP_PreviewChoice, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.88482F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.115183F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(479, 403);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // FLP_PreviewChoice
            // 
            this.FLP_PreviewChoice.Controls.Add(this.rbUsePreview);
            this.FLP_PreviewChoice.Controls.Add(this.rbUseOriginal);
            this.FLP_PreviewChoice.Controls.Add(this.labelInfoText);
            this.FLP_PreviewChoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FLP_PreviewChoice.Location = new System.Drawing.Point(3, 373);
            this.FLP_PreviewChoice.Name = "FLP_PreviewChoice";
            this.FLP_PreviewChoice.Size = new System.Drawing.Size(473, 27);
            this.FLP_PreviewChoice.TabIndex = 1;
            this.FLP_PreviewChoice.Visible = false;
            // 
            // rbUsePreview
            // 
            this.rbUsePreview.AutoSize = true;
            this.rbUsePreview.Checked = true;
            this.rbUsePreview.Location = new System.Drawing.Point(3, 3);
            this.rbUsePreview.Name = "rbUsePreview";
            this.rbUsePreview.Size = new System.Drawing.Size(70, 17);
            this.rbUsePreview.TabIndex = 0;
            this.rbUsePreview.TabStop = true;
            this.rbUsePreview.Text = "Vorschau";
            this.rbUsePreview.UseVisualStyleBackColor = true;
            this.rbUsePreview.CheckedChanged += new System.EventHandler(this.rbUsePreview_CheckedChanged);
            // 
            // rbUseOriginal
            // 
            this.rbUseOriginal.AutoSize = true;
            this.rbUseOriginal.Location = new System.Drawing.Point(79, 3);
            this.rbUseOriginal.Name = "rbUseOriginal";
            this.rbUseOriginal.Size = new System.Drawing.Size(80, 17);
            this.rbUseOriginal.TabIndex = 1;
            this.rbUseOriginal.Text = "Original Bild";
            this.rbUseOriginal.UseVisualStyleBackColor = true;
            this.rbUseOriginal.CheckedChanged += new System.EventHandler(this.rbUsePreview_CheckedChanged);
            // 
            // labelInfoText
            // 
            this.labelInfoText.AutoSize = true;
            this.labelInfoText.Location = new System.Drawing.Point(165, 6);
            this.labelInfoText.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.labelInfoText.Name = "labelInfoText";
            this.labelInfoText.Size = new System.Drawing.Size(0, 13);
            this.labelInfoText.TabIndex = 1;
            // 
            // timer
            // 
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // ViewImageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 403);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ViewImageForm";
            this.Text = "Bild Anzeige";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.FLP_PreviewChoice.ResumeLayout(false);
            this.FLP_PreviewChoice.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private UC_ImageViewer uC_ImageViewer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel FLP_PreviewChoice;
        private System.Windows.Forms.RadioButton rbUsePreview;
        private System.Windows.Forms.RadioButton rbUseOriginal;
        private System.Windows.Forms.ToolTip TT;
        private System.Windows.Forms.Label labelInfoText;
        private System.Windows.Forms.Timer timer;
    }
}