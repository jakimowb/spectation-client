﻿using SpectationClient.GUI.UC;

namespace SpectationClient.GUI {
    partial class ViewSpectraForm {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.uC_SpectralViewer1 = new UC_SpectralViewer();
            this.SuspendLayout();
            // 
            // uC_SpectralViewer1
            // 
            this.uC_SpectralViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uC_SpectralViewer1.Location = new System.Drawing.Point(0, 0);
            this.uC_SpectralViewer1.Name = "uC_SpectralViewer1";
            this.uC_SpectralViewer1.Size = new System.Drawing.Size(696, 437);
            this.uC_SpectralViewer1.TabIndex = 0;
            // 
            // ViewSpectraForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 437);
            this.Controls.Add(this.uC_SpectralViewer1);
            this.Name = "ViewSpectraForm";
            this.Text = "Spektren Anzeige";
            this.ResumeLayout(false);

        }

        #endregion

        private UC_SpectralViewer uC_SpectralViewer1;
    }
}