﻿namespace SpectationClient.GUI {
    partial class SubInsertObservationArea {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbLOC_New = new System.Windows.Forms.RadioButton();
            this.rbLOC_none = new System.Windows.Forms.RadioButton();
            this.rbLOC_select = new System.Windows.Forms.RadioButton();
            this.gbLOC_new = new System.Windows.Forms.GroupBox();
            this.Loc_Coord = new System.Windows.Forms.Panel();
            this.Loc_Notes = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Loc_Aspect = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.Loc_Locname = new System.Windows.Forms.TextBox();
            this.Loc_Slope = new System.Windows.Forms.TextBox();
            this.gbLOC_select = new System.Windows.Forms.GroupBox();
            this.Loc_ID_description = new System.Windows.Forms.TextBox();
            this.Loc_ID = new System.Windows.Forms.TextBox();
            this.Loc_ID_Select_bt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.bt_Bio_BB_remove = new System.Windows.Forms.Button();
            this.bt_Bio_EUNIS_btRemove = new System.Windows.Forms.Button();
            this.AREA_BFNCode_bt_remove = new System.Windows.Forms.Button();
            this.AREA_BFNCode_tb = new System.Windows.Forms.TextBox();
            this.AREA_BFNCode_bt = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.area_name_tb = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.area_cb_Reliefeinheit_KA5 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.area_tb_Bio_EUNIS = new System.Windows.Forms.TextBox();
            this.area_tb_Bio_BB = new System.Windows.Forms.TextBox();
            this.bt_Bio_EUNIS = new System.Windows.Forms.Button();
            this.bt_Bio_BB = new System.Windows.Forms.Button();
            this.area_tb_Methodic = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.area_cb_LU_CLC = new System.Windows.Forms.ComboBox();
            this.label49 = new System.Windows.Forms.Label();
            this.area_cb_LU_KA5 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.area_tb_Area_Notes = new System.Windows.Forms.TextBox();
            this.area_aspect_nosw = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.area_tb_area = new System.Windows.Forms.TextBox();
            this.area_tb_width = new System.Windows.Forms.TextBox();
            this.area_tb_length = new System.Windows.Forms.TextBox();
            this.area_gfz_id_tb = new System.Windows.Forms.TextBox();
            this.gb_SOIL = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gb_SOIL_rb_new = new System.Windows.Forms.RadioButton();
            this.gb_SOIL_rb_none = new System.Windows.Forms.RadioButton();
            this.gb_SOIL_rb_select = new System.Windows.Forms.RadioButton();
            this.gb_SOIL_select = new System.Windows.Forms.GroupBox();
            this.soil_tb_iddescription = new System.Windows.Forms.TextBox();
            this.soil_tb_id = new System.Windows.Forms.TextBox();
            this.soil_bt_select_id = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.gb_SOIL_new = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.soil_monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.label12 = new System.Windows.Forms.Label();
            this.Soil_ge_bodenform_tb = new System.Windows.Forms.TextBox();
            this.Soil_GE_Bezeichnung = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.Soil_WRB_Specifier = new System.Windows.Forms.ComboBox();
            this.Soil_WRB_Qualifier = new System.Windows.Forms.ComboBox();
            this.Soil_WRB_SoilGroup = new System.Windows.Forms.ComboBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.Soil_moisturetext_cb = new System.Windows.Forms.ComboBox();
            this.Soil_colortext = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Soil_munsell_chroma = new System.Windows.Forms.TextBox();
            this.Soil_munsell_value = new System.Windows.Forms.TextBox();
            this.Soil_munsell_hue = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.Soil_notes = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.gb_PlantSociety = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.gb_PS_rb_new = new System.Windows.Forms.RadioButton();
            this.gb_PS_rb_none = new System.Windows.Forms.RadioButton();
            this.gb_PS_rb_select = new System.Windows.Forms.RadioButton();
            this.gb_PS_select = new System.Windows.Forms.GroupBox();
            this.gb_PS_select_tbIDdescription = new System.Windows.Forms.TextBox();
            this.gb_PS_select_tbID = new System.Windows.Forms.TextBox();
            this.gb_PS_bt_select = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.gb_PS_new = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gb_PS_dateCalendar = new System.Windows.Forms.MonthCalendar();
            this.label41 = new System.Windows.Forms.Label();
            this.Veg_S_gpSpecies = new System.Windows.Forms.GroupBox();
            this.Veg_S_NOS_Crypto = new System.Windows.Forms.TextBox();
            this.Veg_S_NOS_Herbs = new System.Windows.Forms.TextBox();
            this.Veg_S_NOS_Shrubs = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.Veg_S_NOS_Trees2 = new System.Windows.Forms.TextBox();
            this.Veg_S_NOS_Trees1 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.Veg_S_NOS_Total = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.Veg_S_Notes = new System.Windows.Forms.TextBox();
            this.Veg_S_gpCover = new System.Windows.Forms.GroupBox();
            this.Veg_S_C_Crypto = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.Veg_S_C_Herbs = new System.Windows.Forms.TextBox();
            this.Veg_S_C_oSoil = new System.Windows.Forms.TextBox();
            this.Veg_S_C_Shrubs = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Veg_S_C_Litter = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Veg_S_C_Total = new System.Windows.Forms.TextBox();
            this.Veg_S_C_Trees2 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.Veg_S_C_Trees1 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.Veg_S_gpHeights = new System.Windows.Forms.GroupBox();
            this.Veg_S_H_Herbs = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.Veg_S_H_Shrubs = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.Veg_S_H_Trees2 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.Veg_S_H_Trees1 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RTB = new System.Windows.Forms.RichTextBox();
            this.btCANCEL = new System.Windows.Forms.Button();
            this.btINSERT = new System.Windows.Forms.Button();
            this.TT = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSL_Progressbar = new System.Windows.Forms.ToolStripProgressBar();
            this.TSL_StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.groupBox6.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbLOC_new.SuspendLayout();
            this.gbLOC_select.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.gb_SOIL.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.gb_SOIL_select.SuspendLayout();
            this.gb_SOIL_new.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gb_PlantSociety.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.gb_PS_select.SuspendLayout();
            this.gb_PS_new.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Veg_S_gpSpecies.SuspendLayout();
            this.Veg_S_gpCover.SuspendLayout();
            this.Veg_S_gpHeights.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.AutoSize = true;
            this.groupBox6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox6.Controls.Add(this.flowLayoutPanel2);
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(625, 437);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Basis-Informationen in CORE.LOCATIONS";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.Controls.Add(this.panel2);
            this.flowLayoutPanel2.Controls.Add(this.gbLOC_new);
            this.flowLayoutPanel2.Controls.Add(this.gbLOC_select);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 23);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(613, 395);
            this.flowLayoutPanel2.TabIndex = 37;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.rbLOC_New);
            this.panel2.Controls.Add(this.rbLOC_none);
            this.panel2.Controls.Add(this.rbLOC_select);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(548, 30);
            this.panel2.TabIndex = 0;
            // 
            // rbLOC_New
            // 
            this.rbLOC_New.AutoSize = true;
            this.rbLOC_New.Location = new System.Drawing.Point(200, 3);
            this.rbLOC_New.Name = "rbLOC_New";
            this.rbLOC_New.Size = new System.Drawing.Size(43, 17);
            this.rbLOC_New.TabIndex = 2;
            this.rbLOC_New.Text = "neu";
            this.rbLOC_New.UseVisualStyleBackColor = true;
            // 
            // rbLOC_none
            // 
            this.rbLOC_none.AutoSize = true;
            this.rbLOC_none.Checked = true;
            this.rbLOC_none.Location = new System.Drawing.Point(3, 3);
            this.rbLOC_none.Name = "rbLOC_none";
            this.rbLOC_none.Size = new System.Drawing.Size(51, 17);
            this.rbLOC_none.TabIndex = 0;
            this.rbLOC_none.TabStop = true;
            this.rbLOC_none.Text = "keine";
            this.rbLOC_none.UseVisualStyleBackColor = true;
            // 
            // rbLOC_select
            // 
            this.rbLOC_select.AutoSize = true;
            this.rbLOC_select.Location = new System.Drawing.Point(60, 3);
            this.rbLOC_select.Name = "rbLOC_select";
            this.rbLOC_select.Size = new System.Drawing.Size(136, 17);
            this.rbLOC_select.TabIndex = 1;
            this.rbLOC_select.Text = "vorhandene auswählen";
            this.rbLOC_select.UseVisualStyleBackColor = true;
            // 
            // gbLOC_new
            // 
            this.gbLOC_new.AutoSize = true;
            this.gbLOC_new.Controls.Add(this.Loc_Coord);
            this.gbLOC_new.Controls.Add(this.Loc_Notes);
            this.gbLOC_new.Controls.Add(this.label4);
            this.gbLOC_new.Controls.Add(this.Loc_Aspect);
            this.gbLOC_new.Controls.Add(this.label3);
            this.gbLOC_new.Controls.Add(this.label2);
            this.gbLOC_new.Controls.Add(this.label38);
            this.gbLOC_new.Controls.Add(this.Loc_Locname);
            this.gbLOC_new.Controls.Add(this.Loc_Slope);
            this.gbLOC_new.Location = new System.Drawing.Point(3, 39);
            this.gbLOC_new.Name = "gbLOC_new";
            this.gbLOC_new.Size = new System.Drawing.Size(607, 257);
            this.gbLOC_new.TabIndex = 37;
            this.gbLOC_new.TabStop = false;
            this.gbLOC_new.Text = "Daten neu eingeben";
            // 
            // Loc_Coord
            // 
            this.Loc_Coord.AutoSize = true;
            this.Loc_Coord.Location = new System.Drawing.Point(17, 77);
            this.Loc_Coord.Name = "Loc_Coord";
            this.Loc_Coord.Size = new System.Drawing.Size(525, 135);
            this.Loc_Coord.TabIndex = 2;
            // 
            // Loc_Notes
            // 
            this.Loc_Notes.Location = new System.Drawing.Point(82, 218);
            this.Loc_Notes.Name = "Loc_Notes";
            this.Loc_Notes.Size = new System.Drawing.Size(519, 20);
            this.Loc_Notes.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 221);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "Bemerkungen";
            // 
            // Loc_Aspect
            // 
            this.Loc_Aspect.Location = new System.Drawing.Point(166, 50);
            this.Loc_Aspect.Name = "Loc_Aspect";
            this.Loc_Aspect.Size = new System.Drawing.Size(100, 20);
            this.Loc_Aspect.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(125, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Aspect";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "Slope";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(16, 23);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(39, 13);
            this.label38.TabIndex = 29;
            this.label38.Text = "Name";
            // 
            // Loc_Locname
            // 
            this.Loc_Locname.Location = new System.Drawing.Point(56, 22);
            this.Loc_Locname.Name = "Loc_Locname";
            this.Loc_Locname.Size = new System.Drawing.Size(167, 20);
            this.Loc_Locname.TabIndex = 0;
            // 
            // Loc_Slope
            // 
            this.Loc_Slope.Location = new System.Drawing.Point(56, 48);
            this.Loc_Slope.Name = "Loc_Slope";
            this.Loc_Slope.Size = new System.Drawing.Size(53, 20);
            this.Loc_Slope.TabIndex = 26;
            // 
            // gbLOC_select
            // 
            this.gbLOC_select.AutoSize = true;
            this.gbLOC_select.Controls.Add(this.Loc_ID_description);
            this.gbLOC_select.Controls.Add(this.Loc_ID);
            this.gbLOC_select.Controls.Add(this.Loc_ID_Select_bt);
            this.gbLOC_select.Controls.Add(this.label1);
            this.gbLOC_select.Location = new System.Drawing.Point(3, 302);
            this.gbLOC_select.Name = "gbLOC_select";
            this.gbLOC_select.Size = new System.Drawing.Size(607, 90);
            this.gbLOC_select.TabIndex = 38;
            this.gbLOC_select.TabStop = false;
            this.gbLOC_select.Text = "Vorhandene Position auswählen";
            // 
            // Loc_ID_description
            // 
            this.Loc_ID_description.Location = new System.Drawing.Point(14, 51);
            this.Loc_ID_description.Name = "Loc_ID_description";
            this.Loc_ID_description.ReadOnly = true;
            this.Loc_ID_description.Size = new System.Drawing.Size(587, 20);
            this.Loc_ID_description.TabIndex = 39;
            // 
            // Loc_ID
            // 
            this.Loc_ID.Location = new System.Drawing.Point(35, 19);
            this.Loc_ID.Name = "Loc_ID";
            this.Loc_ID.Size = new System.Drawing.Size(92, 20);
            this.Loc_ID.TabIndex = 0;
            // 
            // Loc_ID_Select_bt
            // 
            this.Loc_ID_Select_bt.Location = new System.Drawing.Point(137, 16);
            this.Loc_ID_Select_bt.Name = "Loc_ID_Select_bt";
            this.Loc_ID_Select_bt.Size = new System.Drawing.Size(75, 23);
            this.Loc_ID_Select_bt.TabIndex = 1;
            this.Loc_ID_Select_bt.Text = "Auswählen";
            this.TT.SetToolTip(this.Loc_ID_Select_bt, "Eine bereit in der Datenbank eingebenene Koordinate verwenden");
            this.Loc_ID_Select_bt.UseVisualStyleBackColor = true;
            this.Loc_ID_Select_bt.Click += new System.EventHandler(this.Loc_ID_Select_bt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "ID";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 834F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(834, 616);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.groupBox6);
            this.flowLayoutPanel1.Controls.Add(this.groupBox5);
            this.flowLayoutPanel1.Controls.Add(this.gb_SOIL);
            this.flowLayoutPanel1.Controls.Add(this.gb_PlantSociety);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(828, 460);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.AutoSize = true;
            this.groupBox5.Controls.Add(this.bt_Bio_BB_remove);
            this.groupBox5.Controls.Add(this.bt_Bio_EUNIS_btRemove);
            this.groupBox5.Controls.Add(this.AREA_BFNCode_bt_remove);
            this.groupBox5.Controls.Add(this.AREA_BFNCode_tb);
            this.groupBox5.Controls.Add(this.AREA_BFNCode_bt);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.area_name_tb);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.area_cb_Reliefeinheit_KA5);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.area_tb_Bio_EUNIS);
            this.groupBox5.Controls.Add(this.area_tb_Bio_BB);
            this.groupBox5.Controls.Add(this.bt_Bio_EUNIS);
            this.groupBox5.Controls.Add(this.bt_Bio_BB);
            this.groupBox5.Controls.Add(this.area_tb_Methodic);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.area_cb_LU_CLC);
            this.groupBox5.Controls.Add(this.label49);
            this.groupBox5.Controls.Add(this.area_cb_LU_KA5);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.area_tb_Area_Notes);
            this.groupBox5.Controls.Add(this.area_aspect_nosw);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.area_tb_area);
            this.groupBox5.Controls.Add(this.area_tb_width);
            this.groupBox5.Controls.Add(this.area_tb_length);
            this.groupBox5.Controls.Add(this.area_gfz_id_tb);
            this.groupBox5.Location = new System.Drawing.Point(3, 446);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(566, 387);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ergänzende Informationen in C_GFZ.OBSERVATION_AREAS";
            // 
            // bt_Bio_BB_remove
            // 
            this.bt_Bio_BB_remove.Enabled = false;
            this.bt_Bio_BB_remove.Location = new System.Drawing.Point(109, 233);
            this.bt_Bio_BB_remove.Name = "bt_Bio_BB_remove";
            this.bt_Bio_BB_remove.Size = new System.Drawing.Size(22, 23);
            this.bt_Bio_BB_remove.TabIndex = 59;
            this.bt_Bio_BB_remove.Text = "X";
            this.bt_Bio_BB_remove.UseVisualStyleBackColor = true;
            this.bt_Bio_BB_remove.Click += new System.EventHandler(this.bt_Bio_BB_remove_Click);
            // 
            // bt_Bio_EUNIS_btRemove
            // 
            this.bt_Bio_EUNIS_btRemove.Enabled = false;
            this.bt_Bio_EUNIS_btRemove.Location = new System.Drawing.Point(109, 260);
            this.bt_Bio_EUNIS_btRemove.Name = "bt_Bio_EUNIS_btRemove";
            this.bt_Bio_EUNIS_btRemove.Size = new System.Drawing.Size(22, 23);
            this.bt_Bio_EUNIS_btRemove.TabIndex = 58;
            this.bt_Bio_EUNIS_btRemove.Text = "X";
            this.bt_Bio_EUNIS_btRemove.UseVisualStyleBackColor = true;
            this.bt_Bio_EUNIS_btRemove.Click += new System.EventHandler(this.bt_Bio_EUNIS_btRemove_Click);
            // 
            // AREA_BFNCode_bt_remove
            // 
            this.AREA_BFNCode_bt_remove.Enabled = false;
            this.AREA_BFNCode_bt_remove.Location = new System.Drawing.Point(109, 290);
            this.AREA_BFNCode_bt_remove.Name = "AREA_BFNCode_bt_remove";
            this.AREA_BFNCode_bt_remove.Size = new System.Drawing.Size(22, 23);
            this.AREA_BFNCode_bt_remove.TabIndex = 57;
            this.AREA_BFNCode_bt_remove.Text = "X";
            this.AREA_BFNCode_bt_remove.UseVisualStyleBackColor = true;
            this.AREA_BFNCode_bt_remove.Click += new System.EventHandler(this.AREA_BFNCode_bt_remove_Click);
            // 
            // AREA_BFNCode_tb
            // 
            this.AREA_BFNCode_tb.Location = new System.Drawing.Point(137, 291);
            this.AREA_BFNCode_tb.Name = "AREA_BFNCode_tb";
            this.AREA_BFNCode_tb.ReadOnly = true;
            this.AREA_BFNCode_tb.Size = new System.Drawing.Size(423, 20);
            this.AREA_BFNCode_tb.TabIndex = 56;
            this.AREA_BFNCode_tb.TextChanged += new System.EventHandler(this.AREA_BFNCode_tb_TextChanged);
            // 
            // AREA_BFNCode_bt
            // 
            this.AREA_BFNCode_bt.Location = new System.Drawing.Point(8, 290);
            this.AREA_BFNCode_bt.Name = "AREA_BFNCode_bt";
            this.AREA_BFNCode_bt.Size = new System.Drawing.Size(99, 22);
            this.AREA_BFNCode_bt.TabIndex = 55;
            this.AREA_BFNCode_bt.Text = "BfN Code";
            this.TT.SetToolTip(this.AREA_BFNCode_bt, "Ruf Auswahlliste aus der EUNIS Habitatliste");
            this.AREA_BFNCode_bt.UseVisualStyleBackColor = true;
            this.AREA_BFNCode_bt.Click += new System.EventHandler(this.AREA_BFNCode_bt_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(152, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 54;
            this.label10.Text = "Gebietsname";
            // 
            // area_name_tb
            // 
            this.area_name_tb.Location = new System.Drawing.Point(226, 28);
            this.area_name_tb.Name = "area_name_tb";
            this.area_name_tb.Size = new System.Drawing.Size(325, 20);
            this.area_name_tb.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(42, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 52;
            this.label9.Text = "Reliefeinheit";
            // 
            // area_cb_Reliefeinheit_KA5
            // 
            this.area_cb_Reliefeinheit_KA5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.area_cb_Reliefeinheit_KA5.FormattingEnabled = true;
            this.area_cb_Reliefeinheit_KA5.Location = new System.Drawing.Point(137, 144);
            this.area_cb_Reliefeinheit_KA5.Name = "area_cb_Reliefeinheit_KA5";
            this.area_cb_Reliefeinheit_KA5.Size = new System.Drawing.Size(267, 21);
            this.area_cb_Reliefeinheit_KA5.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(332, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 50;
            this.label6.Text = "NOSW";
            // 
            // area_tb_Bio_EUNIS
            // 
            this.area_tb_Bio_EUNIS.Location = new System.Drawing.Point(137, 263);
            this.area_tb_Bio_EUNIS.Name = "area_tb_Bio_EUNIS";
            this.area_tb_Bio_EUNIS.ReadOnly = true;
            this.area_tb_Bio_EUNIS.Size = new System.Drawing.Size(423, 20);
            this.area_tb_Bio_EUNIS.TabIndex = 49;
            this.area_tb_Bio_EUNIS.TextChanged += new System.EventHandler(this.area_tb_Bio_EUNIS_TextChanged);
            // 
            // area_tb_Bio_BB
            // 
            this.area_tb_Bio_BB.Location = new System.Drawing.Point(137, 235);
            this.area_tb_Bio_BB.Name = "area_tb_Bio_BB";
            this.area_tb_Bio_BB.ReadOnly = true;
            this.area_tb_Bio_BB.Size = new System.Drawing.Size(423, 20);
            this.area_tb_Bio_BB.TabIndex = 48;
            this.area_tb_Bio_BB.TextChanged += new System.EventHandler(this.area_tb_Bio_BB_TextChanged);
            // 
            // bt_Bio_EUNIS
            // 
            this.bt_Bio_EUNIS.Location = new System.Drawing.Point(7, 261);
            this.bt_Bio_EUNIS.Name = "bt_Bio_EUNIS";
            this.bt_Bio_EUNIS.Size = new System.Drawing.Size(99, 22);
            this.bt_Bio_EUNIS.TabIndex = 11;
            this.bt_Bio_EUNIS.Text = "EUNIS Habitat";
            this.TT.SetToolTip(this.bt_Bio_EUNIS, "Ruf Auswahlliste aus der EUNIS Habitatliste");
            this.bt_Bio_EUNIS.UseVisualStyleBackColor = true;
            this.bt_Bio_EUNIS.Click += new System.EventHandler(this.bt_Bio_EUNIS_Click);
            // 
            // bt_Bio_BB
            // 
            this.bt_Bio_BB.Location = new System.Drawing.Point(7, 233);
            this.bt_Bio_BB.Name = "bt_Bio_BB";
            this.bt_Bio_BB.Size = new System.Drawing.Size(99, 22);
            this.bt_Bio_BB.TabIndex = 10;
            this.bt_Bio_BB.Text = "Biotoptyp Brdbg.";
            this.TT.SetToolTip(this.bt_Bio_BB, "Ruft Auswahlliste aus Brandenburger Biotopliste auf");
            this.bt_Bio_BB.UseVisualStyleBackColor = true;
            this.bt_Bio_BB.Click += new System.EventHandler(this.bt_Bio_BB_Click);
            // 
            // area_tb_Methodic
            // 
            this.area_tb_Methodic.Location = new System.Drawing.Point(137, 320);
            this.area_tb_Methodic.Name = "area_tb_Methodic";
            this.area_tb_Methodic.Size = new System.Drawing.Size(423, 20);
            this.area_tb_Methodic.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Messmethodik";
            // 
            // area_cb_LU_CLC
            // 
            this.area_cb_LU_CLC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.area_cb_LU_CLC.FormattingEnabled = true;
            this.area_cb_LU_CLC.Location = new System.Drawing.Point(137, 208);
            this.area_cb_LU_CLC.Name = "area_cb_LU_CLC";
            this.area_cb_LU_CLC.Size = new System.Drawing.Size(267, 21);
            this.area_cb_LU_CLC.TabIndex = 9;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(13, 207);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(102, 13);
            this.label49.TabIndex = 43;
            this.label49.Text = "Nutzung (Corine)";
            this.TT.SetToolTip(this.label49, "Nutzung gem. KA5 oder Corine Land Cover Classification muss angegeben sein!");
            // 
            // area_cb_LU_KA5
            // 
            this.area_cb_LU_KA5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.area_cb_LU_KA5.FormattingEnabled = true;
            this.area_cb_LU_KA5.Location = new System.Drawing.Point(137, 176);
            this.area_cb_LU_KA5.Name = "area_cb_LU_KA5";
            this.area_cb_LU_KA5.Size = new System.Drawing.Size(267, 21);
            this.area_cb_LU_KA5.TabIndex = 8;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(23, 179);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(89, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "Nutzung (KA5)";
            this.TT.SetToolTip(this.label20, "Nutzung gem. KA5 oder Corine Land Cover Classification muss angegeben sein!");
            // 
            // area_tb_Area_Notes
            // 
            this.area_tb_Area_Notes.Location = new System.Drawing.Point(137, 348);
            this.area_tb_Area_Notes.Name = "area_tb_Area_Notes";
            this.area_tb_Area_Notes.Size = new System.Drawing.Size(423, 20);
            this.area_tb_Area_Notes.TabIndex = 13;
            // 
            // area_aspect_nosw
            // 
            this.area_aspect_nosw.Location = new System.Drawing.Point(227, 62);
            this.area_aspect_nosw.Name = "area_aspect_nosw";
            this.area_aspect_nosw.Size = new System.Drawing.Size(99, 20);
            this.area_aspect_nosw.TabIndex = 6;
            this.TT.SetToolTip(this.area_aspect_nosw, "Textangabe, z.B. NNW für Nord-Nord-West");
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 117);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(39, 13);
            this.label34.TabIndex = 13;
            this.label34.Text = "Fläche";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(104, 117);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(18, 13);
            this.label33.TabIndex = 12;
            this.label33.Text = "m²";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(104, 91);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(15, 13);
            this.label32.TabIndex = 11;
            this.label32.Text = "m";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 91);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 13);
            this.label31.TabIndex = 10;
            this.label31.Text = "Breite";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(103, 65);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(15, 13);
            this.label30.TabIndex = 9;
            this.label30.Text = "m";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(138, 65);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(84, 13);
            this.label29.TabIndex = 8;
            this.label29.Text = "Himmelsrichtung";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(41, 349);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(73, 13);
            this.label28.TabIndex = 7;
            this.label28.Text = "Bemerkungen";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 65);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(37, 13);
            this.label27.TabIndex = 6;
            this.label27.Text = "Länge";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(6, 31);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(51, 13);
            this.label26.TabIndex = 5;
            this.label26.Text = "GFZ_ID";
            // 
            // area_tb_area
            // 
            this.area_tb_area.Location = new System.Drawing.Point(56, 113);
            this.area_tb_area.Name = "area_tb_area";
            this.area_tb_area.Size = new System.Drawing.Size(46, 20);
            this.area_tb_area.TabIndex = 5;
            // 
            // area_tb_width
            // 
            this.area_tb_width.Location = new System.Drawing.Point(56, 87);
            this.area_tb_width.Name = "area_tb_width";
            this.area_tb_width.Size = new System.Drawing.Size(46, 20);
            this.area_tb_width.TabIndex = 4;
            // 
            // area_tb_length
            // 
            this.area_tb_length.Location = new System.Drawing.Point(56, 61);
            this.area_tb_length.Name = "area_tb_length";
            this.area_tb_length.Size = new System.Drawing.Size(46, 20);
            this.area_tb_length.TabIndex = 3;
            // 
            // area_gfz_id_tb
            // 
            this.area_gfz_id_tb.Location = new System.Drawing.Point(58, 28);
            this.area_gfz_id_tb.Name = "area_gfz_id_tb";
            this.area_gfz_id_tb.Size = new System.Drawing.Size(80, 20);
            this.area_gfz_id_tb.TabIndex = 1;
            // 
            // gb_SOIL
            // 
            this.gb_SOIL.AutoSize = true;
            this.gb_SOIL.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_SOIL.Controls.Add(this.flowLayoutPanel3);
            this.gb_SOIL.Location = new System.Drawing.Point(3, 839);
            this.gb_SOIL.Name = "gb_SOIL";
            this.gb_SOIL.Size = new System.Drawing.Size(780, 467);
            this.gb_SOIL.TabIndex = 51;
            this.gb_SOIL.TabStop = false;
            this.gb_SOIL.Text = "Bodenbeschreibung in C_GFZ.SOILS";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.AutoSize = true;
            this.flowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel3.Controls.Add(this.panel3);
            this.flowLayoutPanel3.Controls.Add(this.gb_SOIL_select);
            this.flowLayoutPanel3.Controls.Add(this.gb_SOIL_new);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(774, 448);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.gb_SOIL_rb_new);
            this.panel3.Controls.Add(this.gb_SOIL_rb_none);
            this.panel3.Controls.Add(this.gb_SOIL_rb_select);
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(768, 32);
            this.panel3.TabIndex = 1;
            // 
            // gb_SOIL_rb_new
            // 
            this.gb_SOIL_rb_new.AutoSize = true;
            this.gb_SOIL_rb_new.Location = new System.Drawing.Point(200, 3);
            this.gb_SOIL_rb_new.Name = "gb_SOIL_rb_new";
            this.gb_SOIL_rb_new.Size = new System.Drawing.Size(43, 17);
            this.gb_SOIL_rb_new.TabIndex = 35;
            this.gb_SOIL_rb_new.Text = "neu";
            this.gb_SOIL_rb_new.UseVisualStyleBackColor = true;
            // 
            // gb_SOIL_rb_none
            // 
            this.gb_SOIL_rb_none.AutoSize = true;
            this.gb_SOIL_rb_none.Checked = true;
            this.gb_SOIL_rb_none.Location = new System.Drawing.Point(3, 3);
            this.gb_SOIL_rb_none.Name = "gb_SOIL_rb_none";
            this.gb_SOIL_rb_none.Size = new System.Drawing.Size(51, 17);
            this.gb_SOIL_rb_none.TabIndex = 33;
            this.gb_SOIL_rb_none.TabStop = true;
            this.gb_SOIL_rb_none.Text = "keine";
            this.gb_SOIL_rb_none.UseVisualStyleBackColor = true;
            // 
            // gb_SOIL_rb_select
            // 
            this.gb_SOIL_rb_select.AutoSize = true;
            this.gb_SOIL_rb_select.Location = new System.Drawing.Point(60, 3);
            this.gb_SOIL_rb_select.Name = "gb_SOIL_rb_select";
            this.gb_SOIL_rb_select.Size = new System.Drawing.Size(136, 17);
            this.gb_SOIL_rb_select.TabIndex = 34;
            this.gb_SOIL_rb_select.Text = "vorhandene auswählen";
            this.gb_SOIL_rb_select.UseVisualStyleBackColor = true;
            // 
            // gb_SOIL_select
            // 
            this.gb_SOIL_select.AutoSize = true;
            this.gb_SOIL_select.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_SOIL_select.Controls.Add(this.soil_tb_iddescription);
            this.gb_SOIL_select.Controls.Add(this.soil_tb_id);
            this.gb_SOIL_select.Controls.Add(this.soil_bt_select_id);
            this.gb_SOIL_select.Controls.Add(this.label7);
            this.gb_SOIL_select.Location = new System.Drawing.Point(3, 41);
            this.gb_SOIL_select.Name = "gb_SOIL_select";
            this.gb_SOIL_select.Size = new System.Drawing.Size(768, 91);
            this.gb_SOIL_select.TabIndex = 3;
            this.gb_SOIL_select.TabStop = false;
            this.gb_SOIL_select.Text = "Boden auswählen";
            // 
            // soil_tb_iddescription
            // 
            this.soil_tb_iddescription.Location = new System.Drawing.Point(6, 52);
            this.soil_tb_iddescription.Name = "soil_tb_iddescription";
            this.soil_tb_iddescription.ReadOnly = true;
            this.soil_tb_iddescription.Size = new System.Drawing.Size(756, 20);
            this.soil_tb_iddescription.TabIndex = 43;
            // 
            // soil_tb_id
            // 
            this.soil_tb_id.Location = new System.Drawing.Point(31, 24);
            this.soil_tb_id.Name = "soil_tb_id";
            this.soil_tb_id.Size = new System.Drawing.Size(92, 20);
            this.soil_tb_id.TabIndex = 41;
            // 
            // soil_bt_select_id
            // 
            this.soil_bt_select_id.Location = new System.Drawing.Point(151, 24);
            this.soil_bt_select_id.Name = "soil_bt_select_id";
            this.soil_bt_select_id.Size = new System.Drawing.Size(75, 23);
            this.soil_bt_select_id.TabIndex = 42;
            this.soil_bt_select_id.Text = "Auswählen";
            this.TT.SetToolTip(this.soil_bt_select_id, "Eine bereit in der Datenbank eingebenene Koordinate verwenden");
            this.soil_bt_select_id.UseVisualStyleBackColor = true;
            this.soil_bt_select_id.Click += new System.EventHandler(this.soil_bt_select_id_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "ID";
            // 
            // gb_SOIL_new
            // 
            this.gb_SOIL_new.AutoSize = true;
            this.gb_SOIL_new.Controls.Add(this.groupBox3);
            this.gb_SOIL_new.Controls.Add(this.label12);
            this.gb_SOIL_new.Controls.Add(this.Soil_ge_bodenform_tb);
            this.gb_SOIL_new.Controls.Add(this.Soil_GE_Bezeichnung);
            this.gb_SOIL_new.Controls.Add(this.label78);
            this.gb_SOIL_new.Controls.Add(this.groupBox27);
            this.gb_SOIL_new.Controls.Add(this.Soil_moisturetext_cb);
            this.gb_SOIL_new.Controls.Add(this.Soil_colortext);
            this.gb_SOIL_new.Controls.Add(this.groupBox4);
            this.gb_SOIL_new.Controls.Add(this.label8);
            this.gb_SOIL_new.Controls.Add(this.label22);
            this.gb_SOIL_new.Controls.Add(this.Soil_notes);
            this.gb_SOIL_new.Controls.Add(this.label15);
            this.gb_SOIL_new.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_SOIL_new.Location = new System.Drawing.Point(3, 138);
            this.gb_SOIL_new.Name = "gb_SOIL_new";
            this.gb_SOIL_new.Size = new System.Drawing.Size(768, 307);
            this.gb_SOIL_new.TabIndex = 5;
            this.gb_SOIL_new.TabStop = false;
            this.gb_SOIL_new.Text = "Boden";
            // 
            // groupBox3
            // 
            this.groupBox3.AutoSize = true;
            this.groupBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox3.Controls.Add(this.soil_monthCalendar1);
            this.groupBox3.Location = new System.Drawing.Point(511, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(198, 190);
            this.groupBox3.TabIndex = 46;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Datum der Aufnahme";
            // 
            // soil_monthCalendar1
            // 
            this.soil_monthCalendar1.Location = new System.Drawing.Point(8, 24);
            this.soil_monthCalendar1.MaxSelectionCount = 1;
            this.soil_monthCalendar1.Name = "soil_monthCalendar1";
            this.soil_monthCalendar1.TabIndex = 36;
            this.soil_monthCalendar1.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateSelected);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 45;
            this.label12.Text = "Bodenform";
            // 
            // Soil_ge_bodenform_tb
            // 
            this.Soil_ge_bodenform_tb.Location = new System.Drawing.Point(83, 50);
            this.Soil_ge_bodenform_tb.Name = "Soil_ge_bodenform_tb";
            this.Soil_ge_bodenform_tb.Size = new System.Drawing.Size(353, 20);
            this.Soil_ge_bodenform_tb.TabIndex = 44;
            // 
            // Soil_GE_Bezeichnung
            // 
            this.Soil_GE_Bezeichnung.Location = new System.Drawing.Point(83, 22);
            this.Soil_GE_Bezeichnung.Name = "Soil_GE_Bezeichnung";
            this.Soil_GE_Bezeichnung.Size = new System.Drawing.Size(353, 20);
            this.Soil_GE_Bezeichnung.TabIndex = 33;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(8, 26);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(69, 13);
            this.label78.TabIndex = 32;
            this.label78.Text = "Bezeichnung";
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.Soil_WRB_Specifier);
            this.groupBox27.Controls.Add(this.Soil_WRB_Qualifier);
            this.groupBox27.Controls.Add(this.Soil_WRB_SoilGroup);
            this.groupBox27.Controls.Add(this.label75);
            this.groupBox27.Controls.Add(this.label76);
            this.groupBox27.Controls.Add(this.label77);
            this.groupBox27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox27.Location = new System.Drawing.Point(12, 145);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(280, 110);
            this.groupBox27.TabIndex = 30;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "FAO World Reference Base";
            // 
            // Soil_WRB_Specifier
            // 
            this.Soil_WRB_Specifier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Soil_WRB_Specifier.FormattingEnabled = true;
            this.Soil_WRB_Specifier.Location = new System.Drawing.Point(77, 81);
            this.Soil_WRB_Specifier.Name = "Soil_WRB_Specifier";
            this.Soil_WRB_Specifier.Size = new System.Drawing.Size(180, 21);
            this.Soil_WRB_Specifier.TabIndex = 42;
            // 
            // Soil_WRB_Qualifier
            // 
            this.Soil_WRB_Qualifier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Soil_WRB_Qualifier.FormattingEnabled = true;
            this.Soil_WRB_Qualifier.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.Soil_WRB_Qualifier.Location = new System.Drawing.Point(77, 52);
            this.Soil_WRB_Qualifier.Name = "Soil_WRB_Qualifier";
            this.Soil_WRB_Qualifier.Size = new System.Drawing.Size(180, 21);
            this.Soil_WRB_Qualifier.TabIndex = 41;
            // 
            // Soil_WRB_SoilGroup
            // 
            this.Soil_WRB_SoilGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Soil_WRB_SoilGroup.FormattingEnabled = true;
            this.Soil_WRB_SoilGroup.Location = new System.Drawing.Point(77, 20);
            this.Soil_WRB_SoilGroup.Name = "Soil_WRB_SoilGroup";
            this.Soil_WRB_SoilGroup.Size = new System.Drawing.Size(180, 21);
            this.Soil_WRB_SoilGroup.TabIndex = 40;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label75.Location = new System.Drawing.Point(17, 56);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(45, 13);
            this.label75.TabIndex = 9;
            this.label75.Text = "Qualifier";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label76.Location = new System.Drawing.Point(6, 24);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(56, 13);
            this.label76.TabIndex = 8;
            this.label76.Text = "Soil Group";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label77.Location = new System.Drawing.Point(15, 85);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(48, 13);
            this.label77.TabIndex = 10;
            this.label77.Text = "Specifier";
            // 
            // Soil_moisturetext_cb
            // 
            this.Soil_moisturetext_cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Soil_moisturetext_cb.FormattingEnabled = true;
            this.Soil_moisturetext_cb.Location = new System.Drawing.Point(83, 78);
            this.Soil_moisturetext_cb.Name = "Soil_moisturetext_cb";
            this.Soil_moisturetext_cb.Size = new System.Drawing.Size(300, 21);
            this.Soil_moisturetext_cb.TabIndex = 34;
            // 
            // Soil_colortext
            // 
            this.Soil_colortext.Location = new System.Drawing.Point(83, 107);
            this.Soil_colortext.Name = "Soil_colortext";
            this.Soil_colortext.Size = new System.Drawing.Size(353, 20);
            this.Soil_colortext.TabIndex = 35;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Soil_munsell_chroma);
            this.groupBox4.Controls.Add(this.Soil_munsell_value);
            this.groupBox4.Controls.Add(this.Soil_munsell_hue);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox4.Location = new System.Drawing.Point(302, 145);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(203, 110);
            this.groupBox4.TabIndex = 29;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Munselltafel";
            // 
            // Soil_munsell_chroma
            // 
            this.Soil_munsell_chroma.Location = new System.Drawing.Point(110, 75);
            this.Soil_munsell_chroma.Name = "Soil_munsell_chroma";
            this.Soil_munsell_chroma.Size = new System.Drawing.Size(65, 20);
            this.Soil_munsell_chroma.TabIndex = 39;
            // 
            // Soil_munsell_value
            // 
            this.Soil_munsell_value.Location = new System.Drawing.Point(110, 49);
            this.Soil_munsell_value.Name = "Soil_munsell_value";
            this.Soil_munsell_value.Size = new System.Drawing.Size(65, 20);
            this.Soil_munsell_value.TabIndex = 38;
            // 
            // Soil_munsell_hue
            // 
            this.Soil_munsell_hue.Location = new System.Drawing.Point(110, 22);
            this.Soil_munsell_hue.Name = "Soil_munsell_hue";
            this.Soil_munsell_hue.Size = new System.Drawing.Size(65, 20);
            this.Soil_munsell_hue.TabIndex = 37;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(21, 53);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Value/Helligkeit";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label17.Location = new System.Drawing.Point(28, 26);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Hue/Farbton";
            this.TT.SetToolTip(this.label17, "z.B. 2.5 YR");
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label19.Location = new System.Drawing.Point(12, 79);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(93, 13);
            this.label19.TabIndex = 10;
            this.label19.Text = "Chroma/Sättigung";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 271);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Bemerkung";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 82);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 11;
            this.label22.Text = "Feuchte";
            // 
            // Soil_notes
            // 
            this.Soil_notes.Location = new System.Drawing.Point(80, 268);
            this.Soil_notes.Name = "Soil_notes";
            this.Soil_notes.Size = new System.Drawing.Size(682, 20);
            this.Soil_notes.TabIndex = 43;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 110);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Farbe";
            // 
            // gb_PlantSociety
            // 
            this.gb_PlantSociety.AutoSize = true;
            this.gb_PlantSociety.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_PlantSociety.Controls.Add(this.flowLayoutPanel4);
            this.gb_PlantSociety.Location = new System.Drawing.Point(3, 1312);
            this.gb_PlantSociety.Name = "gb_PlantSociety";
            this.gb_PlantSociety.Size = new System.Drawing.Size(780, 481);
            this.gb_PlantSociety.TabIndex = 53;
            this.gb_PlantSociety.TabStop = false;
            this.gb_PlantSociety.Text = "Pflanzengesellschaft in C_GFZ.PLANT_SOCIETIES";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.AutoSize = true;
            this.flowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel4.Controls.Add(this.panel4);
            this.flowLayoutPanel4.Controls.Add(this.gb_PS_select);
            this.flowLayoutPanel4.Controls.Add(this.gb_PS_new);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(774, 462);
            this.flowLayoutPanel4.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.gb_PS_rb_new);
            this.panel4.Controls.Add(this.gb_PS_rb_none);
            this.panel4.Controls.Add(this.gb_PS_rb_select);
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(768, 32);
            this.panel4.TabIndex = 1;
            // 
            // gb_PS_rb_new
            // 
            this.gb_PS_rb_new.AutoSize = true;
            this.gb_PS_rb_new.Location = new System.Drawing.Point(200, 3);
            this.gb_PS_rb_new.Name = "gb_PS_rb_new";
            this.gb_PS_rb_new.Size = new System.Drawing.Size(43, 17);
            this.gb_PS_rb_new.TabIndex = 35;
            this.gb_PS_rb_new.Text = "neu";
            this.gb_PS_rb_new.UseVisualStyleBackColor = true;
            // 
            // gb_PS_rb_none
            // 
            this.gb_PS_rb_none.AutoSize = true;
            this.gb_PS_rb_none.Checked = true;
            this.gb_PS_rb_none.Location = new System.Drawing.Point(3, 3);
            this.gb_PS_rb_none.Name = "gb_PS_rb_none";
            this.gb_PS_rb_none.Size = new System.Drawing.Size(51, 17);
            this.gb_PS_rb_none.TabIndex = 33;
            this.gb_PS_rb_none.TabStop = true;
            this.gb_PS_rb_none.Text = "keine";
            this.gb_PS_rb_none.UseVisualStyleBackColor = true;
            // 
            // gb_PS_rb_select
            // 
            this.gb_PS_rb_select.AutoSize = true;
            this.gb_PS_rb_select.Location = new System.Drawing.Point(60, 3);
            this.gb_PS_rb_select.Name = "gb_PS_rb_select";
            this.gb_PS_rb_select.Size = new System.Drawing.Size(136, 17);
            this.gb_PS_rb_select.TabIndex = 34;
            this.gb_PS_rb_select.Text = "vorhandene auswählen";
            this.gb_PS_rb_select.UseVisualStyleBackColor = true;
            // 
            // gb_PS_select
            // 
            this.gb_PS_select.AutoSize = true;
            this.gb_PS_select.Controls.Add(this.gb_PS_select_tbIDdescription);
            this.gb_PS_select.Controls.Add(this.gb_PS_select_tbID);
            this.gb_PS_select.Controls.Add(this.gb_PS_bt_select);
            this.gb_PS_select.Controls.Add(this.label25);
            this.gb_PS_select.Location = new System.Drawing.Point(3, 41);
            this.gb_PS_select.Name = "gb_PS_select";
            this.gb_PS_select.Size = new System.Drawing.Size(768, 91);
            this.gb_PS_select.TabIndex = 3;
            this.gb_PS_select.TabStop = false;
            this.gb_PS_select.Text = "Pflanzengesellschaft auswählen";
            // 
            // gb_PS_select_tbIDdescription
            // 
            this.gb_PS_select_tbIDdescription.Location = new System.Drawing.Point(6, 52);
            this.gb_PS_select_tbIDdescription.Name = "gb_PS_select_tbIDdescription";
            this.gb_PS_select_tbIDdescription.ReadOnly = true;
            this.gb_PS_select_tbIDdescription.Size = new System.Drawing.Size(756, 20);
            this.gb_PS_select_tbIDdescription.TabIndex = 43;
            // 
            // gb_PS_select_tbID
            // 
            this.gb_PS_select_tbID.Location = new System.Drawing.Point(31, 24);
            this.gb_PS_select_tbID.Name = "gb_PS_select_tbID";
            this.gb_PS_select_tbID.Size = new System.Drawing.Size(92, 20);
            this.gb_PS_select_tbID.TabIndex = 41;
            // 
            // gb_PS_bt_select
            // 
            this.gb_PS_bt_select.Location = new System.Drawing.Point(151, 24);
            this.gb_PS_bt_select.Name = "gb_PS_bt_select";
            this.gb_PS_bt_select.Size = new System.Drawing.Size(75, 23);
            this.gb_PS_bt_select.TabIndex = 42;
            this.gb_PS_bt_select.Text = "Auswählen";
            this.TT.SetToolTip(this.gb_PS_bt_select, "Eine bereit in der Datenbank eingebenene Koordinate verwenden");
            this.gb_PS_bt_select.UseVisualStyleBackColor = true;
            this.gb_PS_bt_select.Click += new System.EventHandler(this.gb_PS_bt_select_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 25);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(18, 13);
            this.label25.TabIndex = 42;
            this.label25.Text = "ID";
            // 
            // gb_PS_new
            // 
            this.gb_PS_new.AutoSize = true;
            this.gb_PS_new.Controls.Add(this.groupBox1);
            this.gb_PS_new.Controls.Add(this.label41);
            this.gb_PS_new.Controls.Add(this.Veg_S_gpSpecies);
            this.gb_PS_new.Controls.Add(this.label36);
            this.gb_PS_new.Controls.Add(this.Veg_S_Notes);
            this.gb_PS_new.Controls.Add(this.Veg_S_gpCover);
            this.gb_PS_new.Controls.Add(this.Veg_S_gpHeights);
            this.gb_PS_new.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_PS_new.Location = new System.Drawing.Point(3, 138);
            this.gb_PS_new.Name = "gb_PS_new";
            this.gb_PS_new.Size = new System.Drawing.Size(768, 321);
            this.gb_PS_new.TabIndex = 53;
            this.gb_PS_new.TabStop = false;
            this.gb_PS_new.Text = "Pflanzengesellschaft";
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.gb_PS_dateCalendar);
            this.groupBox1.Location = new System.Drawing.Point(511, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(251, 208);
            this.groupBox1.TabIndex = 64;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datum der Aufnahme";
            // 
            // gb_PS_dateCalendar
            // 
            this.gb_PS_dateCalendar.Location = new System.Drawing.Point(12, 21);
            this.gb_PS_dateCalendar.MaxSelectionCount = 1;
            this.gb_PS_dateCalendar.Name = "gb_PS_dateCalendar";
            this.gb_PS_dateCalendar.TabIndex = 63;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(7, 25);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(472, 13);
            this.label41.TabIndex = 61;
            this.label41.Text = "(Werte für Biotoptyp BB, EUNIS Habitat oder BFN Code werde wie oben angegeben übe" +
    "rnommen)";
            // 
            // Veg_S_gpSpecies
            // 
            this.Veg_S_gpSpecies.AutoSize = true;
            this.Veg_S_gpSpecies.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Crypto);
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Herbs);
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Shrubs);
            this.Veg_S_gpSpecies.Controls.Add(this.label59);
            this.Veg_S_gpSpecies.Controls.Add(this.label58);
            this.Veg_S_gpSpecies.Controls.Add(this.label57);
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Trees2);
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Trees1);
            this.Veg_S_gpSpecies.Controls.Add(this.label56);
            this.Veg_S_gpSpecies.Controls.Add(this.label55);
            this.Veg_S_gpSpecies.Controls.Add(this.label37);
            this.Veg_S_gpSpecies.Controls.Add(this.Veg_S_NOS_Total);
            this.Veg_S_gpSpecies.Location = new System.Drawing.Point(10, 45);
            this.Veg_S_gpSpecies.Name = "Veg_S_gpSpecies";
            this.Veg_S_gpSpecies.Size = new System.Drawing.Size(167, 173);
            this.Veg_S_gpSpecies.TabIndex = 29;
            this.Veg_S_gpSpecies.TabStop = false;
            this.Veg_S_gpSpecies.Text = "Artenzahl";
            // 
            // Veg_S_NOS_Crypto
            // 
            this.Veg_S_NOS_Crypto.Location = new System.Drawing.Point(114, 134);
            this.Veg_S_NOS_Crypto.Name = "Veg_S_NOS_Crypto";
            this.Veg_S_NOS_Crypto.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Crypto.TabIndex = 38;
            // 
            // Veg_S_NOS_Herbs
            // 
            this.Veg_S_NOS_Herbs.Location = new System.Drawing.Point(114, 110);
            this.Veg_S_NOS_Herbs.Name = "Veg_S_NOS_Herbs";
            this.Veg_S_NOS_Herbs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Herbs.TabIndex = 37;
            // 
            // Veg_S_NOS_Shrubs
            // 
            this.Veg_S_NOS_Shrubs.Location = new System.Drawing.Point(114, 86);
            this.Veg_S_NOS_Shrubs.Name = "Veg_S_NOS_Shrubs";
            this.Veg_S_NOS_Shrubs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Shrubs.TabIndex = 36;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(5, 138);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(103, 13);
            this.label59.TabIndex = 35;
            this.label59.Text = "Kryptogamenschicht";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(5, 114);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(66, 13);
            this.label58.TabIndex = 34;
            this.label58.Text = "Krautschicht";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(5, 90);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(78, 13);
            this.label57.TabIndex = 33;
            this.label57.Text = "Strauchschicht";
            // 
            // Veg_S_NOS_Trees2
            // 
            this.Veg_S_NOS_Trees2.Location = new System.Drawing.Point(114, 62);
            this.Veg_S_NOS_Trees2.Name = "Veg_S_NOS_Trees2";
            this.Veg_S_NOS_Trees2.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Trees2.TabIndex = 32;
            // 
            // Veg_S_NOS_Trees1
            // 
            this.Veg_S_NOS_Trees1.Location = new System.Drawing.Point(114, 38);
            this.Veg_S_NOS_Trees1.Name = "Veg_S_NOS_Trees1";
            this.Veg_S_NOS_Trees1.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Trees1.TabIndex = 31;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(5, 66);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(77, 13);
            this.label56.TabIndex = 30;
            this.label56.Text = "Baumschicht 2";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(5, 42);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(77, 13);
            this.label55.TabIndex = 29;
            this.label55.Text = "Baumschicht 1";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(5, 18);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(43, 13);
            this.label37.TabIndex = 27;
            this.label37.Text = "Gesamt";
            // 
            // Veg_S_NOS_Total
            // 
            this.Veg_S_NOS_Total.Location = new System.Drawing.Point(114, 14);
            this.Veg_S_NOS_Total.Name = "Veg_S_NOS_Total";
            this.Veg_S_NOS_Total.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_NOS_Total.TabIndex = 28;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 285);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(73, 13);
            this.label36.TabIndex = 26;
            this.label36.Text = "Bemerkungen";
            // 
            // Veg_S_Notes
            // 
            this.Veg_S_Notes.Location = new System.Drawing.Point(85, 282);
            this.Veg_S_Notes.Name = "Veg_S_Notes";
            this.Veg_S_Notes.Size = new System.Drawing.Size(644, 20);
            this.Veg_S_Notes.TabIndex = 25;
            // 
            // Veg_S_gpCover
            // 
            this.Veg_S_gpCover.AutoSize = true;
            this.Veg_S_gpCover.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Crypto);
            this.Veg_S_gpCover.Controls.Add(this.label35);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Herbs);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_oSoil);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Shrubs);
            this.Veg_S_gpCover.Controls.Add(this.label13);
            this.Veg_S_gpCover.Controls.Add(this.label14);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Litter);
            this.Veg_S_gpCover.Controls.Add(this.label16);
            this.Veg_S_gpCover.Controls.Add(this.label63);
            this.Veg_S_gpCover.Controls.Add(this.label21);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Total);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Trees2);
            this.Veg_S_gpCover.Controls.Add(this.label64);
            this.Veg_S_gpCover.Controls.Add(this.Veg_S_C_Trees1);
            this.Veg_S_gpCover.Controls.Add(this.label62);
            this.Veg_S_gpCover.Location = new System.Drawing.Point(183, 46);
            this.Veg_S_gpCover.Name = "Veg_S_gpCover";
            this.Veg_S_gpCover.Size = new System.Drawing.Size(168, 221);
            this.Veg_S_gpCover.TabIndex = 5;
            this.Veg_S_gpCover.TabStop = false;
            this.Veg_S_gpCover.Text = "Deckung [%]";
            // 
            // Veg_S_C_Crypto
            // 
            this.Veg_S_C_Crypto.Location = new System.Drawing.Point(115, 134);
            this.Veg_S_C_Crypto.Name = "Veg_S_C_Crypto";
            this.Veg_S_C_Crypto.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Crypto.TabIndex = 50;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 186);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(74, 13);
            this.label35.TabIndex = 11;
            this.label35.Text = "offener Boden";
            // 
            // Veg_S_C_Herbs
            // 
            this.Veg_S_C_Herbs.Location = new System.Drawing.Point(115, 110);
            this.Veg_S_C_Herbs.Name = "Veg_S_C_Herbs";
            this.Veg_S_C_Herbs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Herbs.TabIndex = 49;
            // 
            // Veg_S_C_oSoil
            // 
            this.Veg_S_C_oSoil.Location = new System.Drawing.Point(115, 182);
            this.Veg_S_C_oSoil.Name = "Veg_S_C_oSoil";
            this.Veg_S_C_oSoil.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_oSoil.TabIndex = 10;
            // 
            // Veg_S_C_Shrubs
            // 
            this.Veg_S_C_Shrubs.Location = new System.Drawing.Point(115, 86);
            this.Veg_S_C_Shrubs.Name = "Veg_S_C_Shrubs";
            this.Veg_S_C_Shrubs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Shrubs.TabIndex = 48;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 162);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Streu";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 138);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(103, 13);
            this.label14.TabIndex = 47;
            this.label14.Text = "Kryptogamenschicht";
            // 
            // Veg_S_C_Litter
            // 
            this.Veg_S_C_Litter.Location = new System.Drawing.Point(115, 158);
            this.Veg_S_C_Litter.Name = "Veg_S_C_Litter";
            this.Veg_S_C_Litter.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Litter.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 114);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 13);
            this.label16.TabIndex = 46;
            this.label16.Text = "Krautschicht";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(6, 42);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(77, 13);
            this.label63.TabIndex = 41;
            this.label63.Text = "Baumschicht 1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 90);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 13);
            this.label21.TabIndex = 45;
            this.label21.Text = "Strauchschicht";
            // 
            // Veg_S_C_Total
            // 
            this.Veg_S_C_Total.Location = new System.Drawing.Point(115, 14);
            this.Veg_S_C_Total.Name = "Veg_S_C_Total";
            this.Veg_S_C_Total.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Total.TabIndex = 40;
            // 
            // Veg_S_C_Trees2
            // 
            this.Veg_S_C_Trees2.Location = new System.Drawing.Point(115, 62);
            this.Veg_S_C_Trees2.Name = "Veg_S_C_Trees2";
            this.Veg_S_C_Trees2.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Trees2.TabIndex = 44;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 18);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(43, 13);
            this.label64.TabIndex = 39;
            this.label64.Text = "Gesamt";
            // 
            // Veg_S_C_Trees1
            // 
            this.Veg_S_C_Trees1.Location = new System.Drawing.Point(115, 38);
            this.Veg_S_C_Trees1.Name = "Veg_S_C_Trees1";
            this.Veg_S_C_Trees1.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_C_Trees1.TabIndex = 43;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 66);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(77, 13);
            this.label62.TabIndex = 42;
            this.label62.Text = "Baumschicht 2";
            // 
            // Veg_S_gpHeights
            // 
            this.Veg_S_gpHeights.AutoSize = true;
            this.Veg_S_gpHeights.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Veg_S_gpHeights.Controls.Add(this.Veg_S_H_Herbs);
            this.Veg_S_gpHeights.Controls.Add(this.label61);
            this.Veg_S_gpHeights.Controls.Add(this.Veg_S_H_Shrubs);
            this.Veg_S_gpHeights.Controls.Add(this.label60);
            this.Veg_S_gpHeights.Controls.Add(this.Veg_S_H_Trees2);
            this.Veg_S_gpHeights.Controls.Add(this.label23);
            this.Veg_S_gpHeights.Controls.Add(this.Veg_S_H_Trees1);
            this.Veg_S_gpHeights.Controls.Add(this.label24);
            this.Veg_S_gpHeights.Location = new System.Drawing.Point(357, 46);
            this.Veg_S_gpHeights.Name = "Veg_S_gpHeights";
            this.Veg_S_gpHeights.Size = new System.Drawing.Size(148, 149);
            this.Veg_S_gpHeights.TabIndex = 4;
            this.Veg_S_gpHeights.TabStop = false;
            this.Veg_S_gpHeights.Text = "Höhe [m]";
            // 
            // Veg_S_H_Herbs
            // 
            this.Veg_S_H_Herbs.Location = new System.Drawing.Point(95, 110);
            this.Veg_S_H_Herbs.Name = "Veg_S_H_Herbs";
            this.Veg_S_H_Herbs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_H_Herbs.TabIndex = 42;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(6, 42);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(77, 13);
            this.label61.TabIndex = 5;
            this.label61.Text = "Baumschicht 1";
            // 
            // Veg_S_H_Shrubs
            // 
            this.Veg_S_H_Shrubs.Location = new System.Drawing.Point(95, 86);
            this.Veg_S_H_Shrubs.Name = "Veg_S_H_Shrubs";
            this.Veg_S_H_Shrubs.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_H_Shrubs.TabIndex = 41;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 66);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(77, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "Baumschicht 2";
            // 
            // Veg_S_H_Trees2
            // 
            this.Veg_S_H_Trees2.Location = new System.Drawing.Point(95, 62);
            this.Veg_S_H_Trees2.Name = "Veg_S_H_Trees2";
            this.Veg_S_H_Trees2.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_H_Trees2.TabIndex = 40;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 90);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(78, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Strauchschicht";
            // 
            // Veg_S_H_Trees1
            // 
            this.Veg_S_H_Trees1.Location = new System.Drawing.Point(95, 38);
            this.Veg_S_H_Trees1.Name = "Veg_S_H_Trees1";
            this.Veg_S_H_Trees1.Size = new System.Drawing.Size(47, 20);
            this.Veg_S_H_Trees1.TabIndex = 39;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 114);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(66, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Krautschicht";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btCANCEL);
            this.panel1.Controls.Add(this.btINSERT);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 469);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(828, 144);
            this.panel1.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RTB);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(729, 137);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SQL-Kommando";
            // 
            // RTB
            // 
            this.RTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RTB.Location = new System.Drawing.Point(3, 16);
            this.RTB.Name = "RTB";
            this.RTB.ReadOnly = true;
            this.RTB.Size = new System.Drawing.Size(723, 118);
            this.RTB.TabIndex = 4;
            this.RTB.Text = "";
            // 
            // btCANCEL
            // 
            this.btCANCEL.Location = new System.Drawing.Point(738, 50);
            this.btCANCEL.Name = "btCANCEL";
            this.btCANCEL.Size = new System.Drawing.Size(87, 29);
            this.btCANCEL.TabIndex = 33;
            this.btCANCEL.Text = "Abbrechen";
            this.btCANCEL.UseVisualStyleBackColor = true;
            this.btCANCEL.Click += new System.EventHandler(this.btCANCEL_Click);
            // 
            // btINSERT
            // 
            this.btINSERT.Location = new System.Drawing.Point(738, 85);
            this.btINSERT.Name = "btINSERT";
            this.btINSERT.Size = new System.Drawing.Size(87, 52);
            this.btINSERT.TabIndex = 32;
            this.btINSERT.Text = "Einfügen";
            this.btINSERT.UseVisualStyleBackColor = true;
            this.btINSERT.Click += new System.EventHandler(this.btINSERT_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 616);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(834, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(61, 17);
            this.toolStripStatusLabel1.Text = "Fortschritt";
            // 
            // TSL_Progressbar
            // 
            this.TSL_Progressbar.Name = "TSL_Progressbar";
            this.TSL_Progressbar.Size = new System.Drawing.Size(100, 16);
            // 
            // TSL_StatusLabel
            // 
            this.TSL_StatusLabel.Name = "TSL_StatusLabel";
            this.TSL_StatusLabel.Size = new System.Drawing.Size(118, 17);
            this.TSL_StatusLabel.Text = "toolStripStatusLabel1";
            // 
            // timer
            // 
            this.timer.Interval = 500;
            // 
            // SubInsertObservationArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 638);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "SubInsertObservationArea";
            this.Text = "GFZ Beobachtungsfläche einfügen";
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbLOC_new.ResumeLayout(false);
            this.gbLOC_new.PerformLayout();
            this.gbLOC_select.ResumeLayout(false);
            this.gbLOC_select.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.gb_SOIL.ResumeLayout(false);
            this.gb_SOIL.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.gb_SOIL_select.ResumeLayout(false);
            this.gb_SOIL_select.PerformLayout();
            this.gb_SOIL_new.ResumeLayout(false);
            this.gb_SOIL_new.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.gb_PlantSociety.ResumeLayout(false);
            this.gb_PlantSociety.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.gb_PS_select.ResumeLayout(false);
            this.gb_PS_select.PerformLayout();
            this.gb_PS_new.ResumeLayout(false);
            this.gb_PS_new.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.Veg_S_gpSpecies.ResumeLayout(false);
            this.Veg_S_gpSpecies.PerformLayout();
            this.Veg_S_gpCover.ResumeLayout(false);
            this.Veg_S_gpCover.PerformLayout();
            this.Veg_S_gpHeights.ResumeLayout(false);
            this.Veg_S_gpHeights.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox Loc_Slope;
        private System.Windows.Forms.TextBox Loc_Locname;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox area_tb_Area_Notes;
        private System.Windows.Forms.TextBox area_aspect_nosw;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox area_tb_area;
        private System.Windows.Forms.TextBox area_tb_width;
        private System.Windows.Forms.TextBox area_tb_length;
        private System.Windows.Forms.TextBox area_gfz_id_tb;
        private System.Windows.Forms.TextBox Loc_ID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbLOC_New;
        private System.Windows.Forms.RadioButton rbLOC_select;
        private System.Windows.Forms.RadioButton rbLOC_none;
        private System.Windows.Forms.Button Loc_ID_Select_bt;
        private System.Windows.Forms.ToolTip TT;
        private System.Windows.Forms.GroupBox gbLOC_new;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Loc_Aspect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Loc_Notes;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox Loc_ID_description;
        private System.Windows.Forms.GroupBox gbLOC_select;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btCANCEL;
        private System.Windows.Forms.Button btINSERT;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox RTB;
        private System.Windows.Forms.ComboBox area_cb_LU_CLC;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.ComboBox area_cb_LU_KA5;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox area_tb_Methodic;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox area_tb_Bio_EUNIS;
        private System.Windows.Forms.TextBox area_tb_Bio_BB;
        private System.Windows.Forms.Button bt_Bio_EUNIS;
        private System.Windows.Forms.Button bt_Bio_BB;
        private System.Windows.Forms.Panel Loc_Coord;
        private System.Windows.Forms.GroupBox gb_SOIL;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton gb_SOIL_rb_new;
        private System.Windows.Forms.RadioButton gb_SOIL_rb_none;
        private System.Windows.Forms.RadioButton gb_SOIL_rb_select;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.GroupBox gb_SOIL_select;
        private System.Windows.Forms.TextBox soil_tb_iddescription;
        private System.Windows.Forms.TextBox soil_tb_id;
        private System.Windows.Forms.Button soil_bt_select_id;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gb_SOIL_new;
        private System.Windows.Forms.TextBox Soil_GE_Bezeichnung;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.ComboBox Soil_WRB_Specifier;
        private System.Windows.Forms.ComboBox Soil_WRB_Qualifier;
        private System.Windows.Forms.ComboBox Soil_WRB_SoilGroup;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.ComboBox Soil_moisturetext_cb;
        private System.Windows.Forms.TextBox Soil_colortext;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox Soil_munsell_chroma;
        private System.Windows.Forms.TextBox Soil_munsell_value;
        private System.Windows.Forms.TextBox Soil_munsell_hue;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox Soil_notes;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox area_cb_Reliefeinheit_KA5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox area_name_tb;
        private System.Windows.Forms.MonthCalendar soil_monthCalendar1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar TSL_Progressbar;
        private System.Windows.Forms.ToolStripStatusLabel TSL_StatusLabel;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button AREA_BFNCode_bt;
        private System.Windows.Forms.TextBox AREA_BFNCode_tb;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Soil_ge_bodenform_tb;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.GroupBox gb_PlantSociety;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton gb_PS_rb_new;
        private System.Windows.Forms.RadioButton gb_PS_rb_none;
        private System.Windows.Forms.RadioButton gb_PS_rb_select;
        private System.Windows.Forms.GroupBox gb_PS_select;
        private System.Windows.Forms.TextBox gb_PS_select_tbIDdescription;
        private System.Windows.Forms.TextBox gb_PS_select_tbID;
        private System.Windows.Forms.Button gb_PS_bt_select;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox gb_PS_new;
        private System.Windows.Forms.GroupBox Veg_S_gpSpecies;
        private System.Windows.Forms.TextBox Veg_S_NOS_Crypto;
        private System.Windows.Forms.TextBox Veg_S_NOS_Herbs;
        private System.Windows.Forms.TextBox Veg_S_NOS_Shrubs;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox Veg_S_NOS_Trees2;
        private System.Windows.Forms.TextBox Veg_S_NOS_Trees1;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox Veg_S_NOS_Total;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox Veg_S_Notes;
        private System.Windows.Forms.GroupBox Veg_S_gpCover;
        private System.Windows.Forms.TextBox Veg_S_C_Crypto;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox Veg_S_C_Herbs;
        private System.Windows.Forms.TextBox Veg_S_C_oSoil;
        private System.Windows.Forms.TextBox Veg_S_C_Shrubs;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Veg_S_C_Litter;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox Veg_S_C_Total;
        private System.Windows.Forms.TextBox Veg_S_C_Trees2;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox Veg_S_C_Trees1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox Veg_S_gpHeights;
        private System.Windows.Forms.TextBox Veg_S_H_Herbs;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox Veg_S_H_Shrubs;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox Veg_S_H_Trees2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox Veg_S_H_Trees1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.MonthCalendar gb_PS_dateCalendar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bt_Bio_BB_remove;
        private System.Windows.Forms.Button bt_Bio_EUNIS_btRemove;
        private System.Windows.Forms.Button AREA_BFNCode_bt_remove;
    }
}