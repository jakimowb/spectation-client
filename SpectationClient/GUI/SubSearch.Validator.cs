﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Windows.Forms;
using SpectationClient;
using SpectationClient.DataBaseDescription;
//using ToolLibrary.Forms;
using SpectationClient.SpectralTools;
using Npgsql;

namespace SpectationClient.GUI {
    partial class SubSearch {
        #region Initialise Validators
        private void initValidators() {
            LOC_LAT_Y.TextChanged += new EventHandler(VAL_Location);
            LOC_LONG_X.TextChanged += new EventHandler(VAL_Location);
            LOC_Buffer.TextChanged += new EventHandler(VAL_Location);
            LOC_name.TextChanged += new EventHandler(VAL_Location);
            LOC_matchAll.CheckedChanged +=new EventHandler(VAL_Location);

            CORE_Date_Begin.ValueChanged += new EventHandler(Val_Required);
            CORE_Date_End.ValueChanged += new EventHandler(Val_Required);
            CORE_Datatype.SelectedValueChanged += new EventHandler(Val_Required);
            CORE_SpecType.SelectedValueChanged += new EventHandler(Val_Required);
            CORE_Source.SelectedValueChanged += new EventHandler(Val_Required);
            //CORE_Datatype.DataSourceChanged += new EventHandler(CORE_Datatype_DataSourceChanged);
            gb_CORE_CAMP_clb.SelectedValueChanged += new EventHandler(Val_Required);
            gb_CORE_WREF_PANELS_clb.SelectedValueChanged += new EventHandler(Val_Required);
            gb_CORE_SENS_clb.SelectedValueChanged += new EventHandler(Val_Required);
            gb_CORE_USER_clb.SelectedValueChanged += new EventHandler(Val_Required);
            VGFZ_PS_BB_DGV.RowsAdded += new DataGridViewRowsAddedEventHandler(Val_Required);
            VGFZ_PS_BB_DGV.RowsRemoved += new DataGridViewRowsRemovedEventHandler(Val_Required);

            VGFZ_PS_EUNIS_DGV.RowsAdded += new DataGridViewRowsAddedEventHandler(Val_Required);
            VGFZ_PS_EUNIS_DGV.RowsRemoved += new DataGridViewRowsRemovedEventHandler(Val_Required);

            VGFZ_SP_DGV.RowsAdded += new DataGridViewRowsAddedEventHandler(Val_Required);
            VGFZ_SP_DGV.RowsRemoved += new DataGridViewRowsRemovedEventHandler(Val_Required);
            VGFZ_SP_musthaveall.CheckedChanged += new EventHandler(Val_Required);
        }

        
        private void EL_ErrorListIsEmpty(object sender, EventArgs e) {
            SEARCH_bt.Enabled = true;
            Val_Required(null, null);
        }

        private void EL_ErrorAdded(object sender, EventArgs e) {
            SEARCH_bt.Enabled = false;
            RTB_CMD.Text = C_RTF_CMDERROR;
        }

        #endregion

        #region Validators

        private void VAL_Location(object sender, EventArgs e) {
            
            bool has_long = LOC_LONG_X.TextLength > 0;
            bool has_lat = LOC_LAT_Y.TextLength > 0;
            bool has_buff = LOC_Buffer.TextLength > 0;
            if (!has_long && !has_lat) {
                EL.removeError(LOC_LAT_Y);
                EL.removeError(LOC_LONG_X);
                EL.removeError(LOC_Buffer);

            } else {
                EL.validation_Number(LOC_LAT_Y, true, LOC_BUFFER_UC.COORD_LATY_MIN, LOC_BUFFER_UC.COORD_LATY_MAX);
                EL.validation_Number(LOC_LONG_X,true, LOC_BUFFER_UC.COORD_LONGX_MIN, LOC_BUFFER_UC.COORD_LONGX_MAX);
                EL.validation_Number(LOC_Buffer, true, 0, 1e20);//new EventArgsValidator(true, "Benötige Bufferdistanz als Zahl >= 0"));
            }
            Val_Required(null, null);
            
        } 

       
        #endregion
    }

    internal class EventArgsValidator : EventArgs {
        private Boolean _IsRequired;
        private String _EP_Message = "";
        public EventArgsValidator(bool valuerequired) {
            _IsRequired = valuerequired;
        }
        public EventArgsValidator(bool valuerequired, String ep_message) {
            _IsRequired = valuerequired;
            _EP_Message = ep_message;
        }
        public Boolean IsRequired {
            get { return _IsRequired; }
        }

        public String EP_Message {
            get { return _EP_Message; }
        }
    }
}
