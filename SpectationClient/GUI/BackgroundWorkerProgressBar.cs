﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SpectationClient.GUI {
    public partial class BackgroundWorkerProgressBar : Form {

        /// <summary>
        /// Gets or set the title text of the BackgroundWorkerProgressForm.
        /// </summary>
        public String Title {
            get { return this.Text; }
            set { this.Text = value; }
        }

        /// <summary>
        /// A short information label abouve the progress bar.
        /// </summary>
        public String InfoText {
            get { return this.labelInfoText.Text; }
            set { this.labelInfoText.Text = value; }
        }

        /// <summary>
        /// The BackgroundWorker object this BackgroundWorkerProgressForm is linked to.
        /// </summary>
        public BackgroundWorker BackgroundWorker { get { return this.bgw; } }
        private BackgroundWorker bgw;

        /// <summary>
        /// Returns the BackgroundWorker linked to this Form.
        /// </summary>
        //public BackgroundWorker BackgroundWorker { get { return this.bgw; } }

        public BackgroundWorkerProgressBar() {
            init(new BackgroundWorker());
            
        }

        public BackgroundWorkerProgressBar(ref BackgroundWorker bgw) {
            this.init(bgw);
          
        }

        private void init(BackgroundWorker bgw) {
            InitializeComponent();
            this.labelInfoText.Text = "";
            this.bgw = bgw;
            this.bgw.WorkerSupportsCancellation = this.bgw.WorkerReportsProgress = true;
            this.bgw.ProgressChanged +=new ProgressChangedEventHandler(bgw_ProgressChanged);
            this.bgw.RunWorkerCompleted +=new RunWorkerCompletedEventHandler(bgw_RunWorkerCompleted);

            this.BringToFront();
            this.Show();
        }

        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Close();
        }

        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            this.progressBar.Value = e.ProgressPercentage;
            this.labelInfoText.Text = e.UserState as String;
        }

        /// <summary>
        /// Is called when the cancelation button was pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btCancel_Click(object sender, EventArgs e) {
            this.labelInfoText.Text = "Cancel operation...";
            this.btCancel.Enabled = false;
            this.bgw.CancelAsync();
        }
    }
}
