﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;

using SpectationClient.Stuff;


namespace SpectationClient.GUI {
    public partial class SaveFilesDialog : Form {

        DataGridView dgv;
        ErrorList EL;

        /// <summary>
        /// Returns a specified file extension, e.g. "jpg" or "png". Removes leading/trailing blanks or dots.
        /// </summary>
        public String FileExtension { 
            get {
                return this.tbFileExtension.Text.Trim(new char[]{' ','.'});
            }
            set { this.tbFileExtension.Text = value; }
        }
        public String[] FileNameColumns {
            get { return getSelectedColumns(); } 
        }
        public String Folder { get { return this.tbFolder.Text; } }

        public bool UseRowNumbers { get { return rbFileName_RowNumber.Checked; } }

        public SaveFilesDialog() {
            InitializeComponent();
            init();
        }

        public SaveFilesDialog(DataGridView dgv) {
            InitializeComponent();
            this.dgv = dgv;
            init();
        }

        private void init() {
            EL = new ErrorList();
            this.Icon = Resources.Icon;
            if(this.dgv != null) {
                foreach(DataGridViewColumn c in this.dgv.Columns) {
                    if(c.ValueType != typeof(Byte[])) {
                        String display = c.Name;
                       
                        BoxItem item = new BoxItem(display, c.Name);
                        clbColumns.Items.Add(item, false);
                    }
                
                }
               
            }

        }

        private void btFolder_Click(object sender, EventArgs e) {
            FBD.Description = "Bitte Speicherort angeben";
            if(FBD.ShowDialog() == System.Windows.Forms.DialogResult.OK){
                tbFolder.Text = FBD.SelectedPath;
            }
        }

        //Returns all checked DGV column names
        private String[] getSelectedColumns() {
            return (from BoxItem item in clbColumns.CheckedItems
                             select item.Value as String).ToArray();
         
            
           
        }
        private void validation_event(object sender, EventArgs e) {
            validation();
        }
        private void validation() {
            if(!Directory.Exists(tbFolder.Text)) {
                EL.addError(tbFolder, "Ordner existiert nicht");
            } else {
                EL.removeError(tbFolder);
            }

            //Preview
            btOK.Enabled = EL.Count == 0 && tbFolder.Text.Trim().Length > 0;
        }

        private void btOK_Click(object sender, EventArgs e) {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void rbFileName_ColumnValues_CheckedChanged(object sender, EventArgs e) {
            this.clbColumns.Enabled = rbFileName_ColumnValues.Checked;
        }
    }
}
