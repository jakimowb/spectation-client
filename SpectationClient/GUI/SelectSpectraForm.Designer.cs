﻿using SpectationClient;
using SpectationClient.GUI.UC;
namespace SpectationClient.GUI {

    partial class SelectSpectraForm {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.UC_SpectralViewer = new UC_SpectralViewer();
            this.DGV = new System.Windows.Forms.DataGridView();
            this.SPEC = new DGVExtensions.DataGridViewSpectrumColumn();
            this.COLOR = new DGVExtensions.DataGridViewColorColumn();
            this.FILE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FLP = new System.Windows.Forms.FlowLayoutPanel();
            this.btOK = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).BeginInit();
            this.FLP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.splitContainer1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.FLP, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(850, 501);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // UC_SpectralViewer
            // 
            this.UC_SpectralViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UC_SpectralViewer.Location = new System.Drawing.Point(0, 0);
            this.UC_SpectralViewer.Name = "UC_SpectralViewer";
            this.UC_SpectralViewer.Size = new System.Drawing.Size(842, 251);
            this.UC_SpectralViewer.TabIndex = 0;
            // 
            // DGV
            // 
            this.DGV.AllowUserToAddRows = false;
            this.DGV.AllowUserToDeleteRows = false;
            this.DGV.AllowUserToOrderColumns = true;
            this.DGV.AllowUserToResizeRows = false;
            this.DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SPEC,
            this.COLOR,
            this.FILE});
            this.DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV.Location = new System.Drawing.Point(0, 0);
            this.DGV.Name = "DGV";
            this.DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV.Size = new System.Drawing.Size(842, 203);
            this.DGV.TabIndex = 1;
            // 
            // SPEC
            // 
            this.SPEC.HeaderText = "Spektren";
            this.SPEC.Name = "SPEC";
            this.SPEC.ReadOnly = true;
            this.SPEC.ToolStripLabel_ShowSpectra = false;
            this.SPEC.Width = 75;
            // 
            // COLOR
            // 
            this.COLOR.HeaderText = "Farbe";
            this.COLOR.Name = "COLOR";
            this.COLOR.ReadOnly = true;
            this.COLOR.Width = 59;
            // 
            // FILE
            // 
            this.FILE.HeaderText = "Datei";
            this.FILE.Name = "FILE";
            this.FILE.ReadOnly = true;
            this.FILE.Width = 57;
            // 
            // FLP
            // 
            this.FLP.Controls.Add(this.btOK);
            this.FLP.Controls.Add(this.btCancel);
            this.FLP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FLP.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.FLP.Location = new System.Drawing.Point(3, 471);
            this.FLP.Name = "FLP";
            this.FLP.Size = new System.Drawing.Size(844, 27);
            this.FLP.TabIndex = 2;
            // 
            // btOK
            // 
            this.btOK.Location = new System.Drawing.Point(766, 3);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(75, 23);
            this.btOK.TabIndex = 0;
            this.btOK.Text = "Ok";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(685, 3);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 1;
            this.btCancel.Text = "Abbruch";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.UC_SpectralViewer);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.DGV);
            this.splitContainer1.Size = new System.Drawing.Size(844, 462);
            this.splitContainer1.SplitterDistance = 253;
            this.splitContainer1.TabIndex = 1;
            // 
            // SelectSpectraForm
            // 
            this.AcceptButton = this.btOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(850, 501);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectSpectraForm";
            this.ShowInTaskbar = false;
            this.Text = "Bitte Spektren auswählen";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).EndInit();
            this.FLP.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private UC_SpectralViewer UC_SpectralViewer;
        private System.Windows.Forms.DataGridView DGV;
        private System.Windows.Forms.FlowLayoutPanel FLP;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btCancel;
        private DGVExtensions.DataGridViewSpectrumColumn SPEC;
        private DGVExtensions.DataGridViewColorColumn COLOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn FILE;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}