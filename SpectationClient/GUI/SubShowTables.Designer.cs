﻿namespace SpectationClient.GUI {
    partial class SubShowTables {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tslSchema = new System.Windows.Forms.ToolStripLabel();
            this.tsCBSchema = new System.Windows.Forms.ToolStripComboBox();
            this.tslTable = new System.Windows.Forms.ToolStripLabel();
            this.tsCBTable = new System.Windows.Forms.ToolStripComboBox();
            this.tsBtShow = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tsTBMax = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsTBOFFSET = new System.Windows.Forms.ToolStripTextBox();
            this.tsBtRowsBack = new System.Windows.Forms.ToolStripButton();
            this.tsBtRowsMore = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.prog_bar = new System.Windows.Forms.ToolStripProgressBar();
            this.prog_statuslabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.prog_status = new System.Windows.Forms.ToolStripStatusLabel();
            this.DGV = new System.Windows.Forms.DataGridView();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tsddBt_FindRelatedTables = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbUpdate = new System.Windows.Forms.ToolStripButton();
            this.tsbSaveDGV = new System.Windows.Forms.ToolStripButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RTB_CMD = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbHideBigBLOBs = new System.Windows.Forms.CheckBox();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.TT = new System.Windows.Forms.ToolTip(this.components);
            this.toolStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslSchema,
            this.tsCBSchema,
            this.tslTable,
            this.tsCBTable,
            this.tsBtShow,
            this.toolStripSeparator1,
            this.toolStripLabel2,
            this.tsTBMax,
            this.toolStripLabel1,
            this.tsTBOFFSET,
            this.tsBtRowsBack,
            this.tsBtRowsMore});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(974, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tslSchema
            // 
            this.tslSchema.Name = "tslSchema";
            this.tslSchema.Size = new System.Drawing.Size(52, 22);
            this.tslSchema.Text = "Schema:";
            // 
            // tsCBSchema
            // 
            this.tsCBSchema.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsCBSchema.Name = "tsCBSchema";
            this.tsCBSchema.Size = new System.Drawing.Size(121, 25);
            this.tsCBSchema.SelectedIndexChanged += new System.EventHandler(this.tsCBSchema_SelectedIndexChanged);
            // 
            // tslTable
            // 
            this.tslTable.Name = "tslTable";
            this.tslTable.Size = new System.Drawing.Size(78, 22);
            this.tslTable.Text = "Tabelle/View:";
            this.tslTable.ToolTipText = "Auswahl einer Datenbanktabelle oder einer Sicht (Präfix \"VIEW\") darauf.";
            // 
            // tsCBTable
            // 
            this.tsCBTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsCBTable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tsCBTable.Name = "tsCBTable";
            this.tsCBTable.Size = new System.Drawing.Size(175, 25);
            this.tsCBTable.SelectedIndexChanged += new System.EventHandler(this.tsCBTable_SelectedIndexChanged);
            // 
            // tsBtShow
            // 
            this.tsBtShow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtShow.Enabled = false;
            this.tsBtShow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tsBtShow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtShow.Name = "tsBtShow";
            this.tsBtShow.Size = new System.Drawing.Size(86, 22);
            this.tsBtShow.Text = "Zeige Tabelle";
            this.tsBtShow.Click += new System.EventHandler(this.toolStripBtShow_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(70, 22);
            this.toolStripLabel2.Text = "max. Zeilen:";
            this.toolStripLabel2.ToolTipText = "Maximale Anzahl angezeigter Zeilen";
            // 
            // tsTBMax
            // 
            this.tsTBMax.Name = "tsTBMax";
            this.tsTBMax.Size = new System.Drawing.Size(60, 25);
            this.tsTBMax.TextChanged += new System.EventHandler(this.tsTBMax_TextChanged);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(57, 22);
            this.toolStripLabel1.Text = "Startzeile:";
            // 
            // tsTBOFFSET
            // 
            this.tsTBOFFSET.Name = "tsTBOFFSET";
            this.tsTBOFFSET.Size = new System.Drawing.Size(60, 25);
            this.tsTBOFFSET.Text = "0";
            this.tsTBOFFSET.TextChanged += new System.EventHandler(this.tsTBMax_TextChanged);
            // 
            // tsBtRowsBack
            // 
            this.tsBtRowsBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtRowsBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtRowsBack.Name = "tsBtRowsBack";
            this.tsBtRowsBack.Size = new System.Drawing.Size(54, 22);
            this.tsBtRowsBack.Text = "<zurück";
            this.tsBtRowsBack.Click += new System.EventHandler(this.tsBtRowsBack_Click);
            // 
            // tsBtRowsMore
            // 
            this.tsBtRowsMore.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtRowsMore.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtRowsMore.Name = "tsBtRowsMore";
            this.tsBtRowsMore.Size = new System.Drawing.Size(51, 22);
            this.tsBtRowsMore.Text = "weiter>";
            this.tsBtRowsMore.Click += new System.EventHandler(this.tsBtRowsMore_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.86037F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.13963F));
            this.tableLayoutPanel1.Controls.Add(this.statusStrip1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.DGV, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.toolStrip2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 126F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(974, 428);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // statusStrip1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.statusStrip1, 2);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prog_bar,
            this.prog_statuslabel,
            this.prog_status});
            this.statusStrip1.Location = new System.Drawing.Point(0, 408);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(974, 20);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // prog_bar
            // 
            this.prog_bar.Name = "prog_bar";
            this.prog_bar.Size = new System.Drawing.Size(100, 14);
            // 
            // prog_statuslabel
            // 
            this.prog_statuslabel.Name = "prog_statuslabel";
            this.prog_statuslabel.Size = new System.Drawing.Size(42, 15);
            this.prog_statuslabel.Text = "Status:";
            // 
            // prog_status
            // 
            this.prog_status.Name = "prog_status";
            this.prog_status.Size = new System.Drawing.Size(0, 15);
            // 
            // DGV
            // 
            this.DGV.AllowDrop = true;
            this.DGV.AllowUserToResizeRows = false;
            this.DGV.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.EP.SetIconPadding(this.DGV, 5);
            this.DGV.Location = new System.Drawing.Point(138, 3);
            this.DGV.Name = "DGV";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.tableLayoutPanel1.SetRowSpan(this.DGV, 2);
            this.DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV.Size = new System.Drawing.Size(833, 308);
            this.DGV.TabIndex = 0;
            this.DGV.DataSourceChanged += new System.EventHandler(this.DGV_DataSourceChanged);
            this.DGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_CellClick);
            this.DGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_CellContentClick);
            this.DGV.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DGV_DataError);
            this.DGV.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DGV_RowHeaderMouseClick);
            this.DGV.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGV_RowsAdded);
            this.DGV.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.DGV_RowsRemoved);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsddBt_FindRelatedTables,
            this.toolStripSeparator2,
            this.tsbUpdate,
            this.tsbSaveDGV});
            this.toolStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(135, 126);
            this.toolStrip2.TabIndex = 2;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tsddBt_FindRelatedTables
            // 
            this.tsddBt_FindRelatedTables.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsddBt_FindRelatedTables.Enabled = false;
            this.tsddBt_FindRelatedTables.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsddBt_FindRelatedTables.Name = "tsddBt_FindRelatedTables";
            this.tsddBt_FindRelatedTables.Size = new System.Drawing.Size(133, 19);
            this.tsddBt_FindRelatedTables.Text = "Relation zu...";
            this.tsddBt_FindRelatedTables.ToolTipText = "Zeigt die Relation selektierter Zeilen zu anderen Tabelleneinträgen auf";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(133, 6);
            // 
            // tsbUpdate
            // 
            this.tsbUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbUpdate.Enabled = false;
            this.tsbUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbUpdate.Name = "tsbUpdate";
            this.tsbUpdate.Size = new System.Drawing.Size(133, 19);
            this.tsbUpdate.Text = "Update";
            this.tsbUpdate.ToolTipText = "Überführen der Änderungen in die Datenbank";
            this.tsbUpdate.Click += new System.EventHandler(this.tsbUpdate_Click);
            // 
            // tsbSaveDGV
            // 
            this.tsbSaveDGV.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbSaveDGV.Enabled = false;
            this.tsbSaveDGV.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSaveDGV.Name = "tsbSaveDGV";
            this.tsbSaveDGV.Size = new System.Drawing.Size(133, 19);
            this.tsbSaveDGV.Text = "Tabelle speichern";
            this.tsbSaveDGV.ToolTipText = "Tabelle auf lokalem Laufwerk abspeichern";
            this.tsbSaveDGV.Click += new System.EventHandler(this.tsbSaveDGV_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RTB_CMD);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(138, 317);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(833, 88);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SQL";
            // 
            // RTB_CMD
            // 
            this.RTB_CMD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RTB_CMD.Location = new System.Drawing.Point(3, 16);
            this.RTB_CMD.Name = "RTB_CMD";
            this.RTB_CMD.ReadOnly = true;
            this.RTB_CMD.Size = new System.Drawing.Size(827, 69);
            this.RTB_CMD.TabIndex = 4;
            this.RTB_CMD.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbHideBigBLOBs);
            this.groupBox1.Location = new System.Drawing.Point(3, 129);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(129, 100);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // cbHideBigBLOBs
            // 
            this.cbHideBigBLOBs.AutoSize = true;
            this.cbHideBigBLOBs.Checked = true;
            this.cbHideBigBLOBs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbHideBigBLOBs.Location = new System.Drawing.Point(7, 10);
            this.cbHideBigBLOBs.Name = "cbHideBigBLOBs";
            this.cbHideBigBLOBs.Size = new System.Drawing.Size(107, 17);
            this.cbHideBigBLOBs.TabIndex = 0;
            this.cbHideBigBLOBs.Text = "Nur Bildvorschau";
            this.TT.SetToolTip(this.cbHideBigBLOBs, "Falls aktiviert wird nur eine Bildvorschau geladen.");
            this.cbHideBigBLOBs.UseVisualStyleBackColor = true;
            this.cbHideBigBLOBs.CheckedChanged += new System.EventHandler(this.tsCBTable_SelectedIndexChanged);
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // OFD
            // 
            this.OFD.FileName = "openFileDialog1";
            // 
            // SubShowTables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 453);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "SubShowTables";
            this.Text = "Anzeigen von Datenbanktabellen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SubShowTables_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel tslTable;
        private System.Windows.Forms.ToolStripComboBox tsCBTable;
        private System.Windows.Forms.ToolStripButton tsBtShow;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView DGV;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox tsTBMax;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.ToolStripLabel tslSchema;
        private System.Windows.Forms.ToolStripComboBox tsCBSchema;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.ToolStripButton tsbUpdate;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.ToolStripButton tsbSaveDGV;
        private System.Windows.Forms.ToolTip TT;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripDropDownButton tsddBt_FindRelatedTables;
        private System.Windows.Forms.RichTextBox RTB_CMD;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox tsTBOFFSET;
        private System.Windows.Forms.ToolStripButton tsBtRowsBack;
        private System.Windows.Forms.ToolStripButton tsBtRowsMore;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar prog_bar;
        private System.Windows.Forms.ToolStripStatusLabel prog_statuslabel;
        private System.Windows.Forms.ToolStripStatusLabel prog_status;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbHideBigBLOBs;
    }
}