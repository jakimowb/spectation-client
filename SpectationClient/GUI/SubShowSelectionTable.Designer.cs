﻿namespace SpectationClient.GUI {
    partial class SubShowSelectionTable {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubShowSelectionTable));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tsTBMax = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsTBOFFSET = new System.Windows.Forms.ToolStripTextBox();
            this.tsb_back = new System.Windows.Forms.ToolStripButton();
            this.tsb_forward = new System.Windows.Forms.ToolStripButton();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.DGV = new System.Windows.Forms.DataGridView();
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.TT = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DGV_Constraints = new System.Windows.Forms.DataGridView();
            this.tbSearchALL = new System.Windows.Forms.TextBox();
            this.cbCaseSensitive = new System.Windows.Forms.CheckBox();
            this.cbExactMatch = new System.Windows.Forms.CheckBox();
            this.bt_SearchNew = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsl_status = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RTB = new System.Windows.Forms.RichTextBox();
            this.btOK = new System.Windows.Forms.Button();
            this.btCANCEL = new System.Windows.Forms.Button();
            this.TIMER = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Constraints)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.toolStripLabel2,
            this.tsTBMax,
            this.toolStripLabel1,
            this.tsTBOFFSET,
            this.tsb_back,
            this.tsb_forward});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(936, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(70, 22);
            this.toolStripLabel2.Text = "max. Zeilen:";
            this.toolStripLabel2.ToolTipText = "Maximale Anzahl angezeigter Zeilen";
            // 
            // tsTBMax
            // 
            this.tsTBMax.Name = "tsTBMax";
            this.tsTBMax.Size = new System.Drawing.Size(100, 25);
            this.tsTBMax.TextChanged += new System.EventHandler(this.tsTBMax_TextChanged);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(57, 22);
            this.toolStripLabel1.Text = "Startzeile:";
            // 
            // tsTBOFFSET
            // 
            this.tsTBOFFSET.Name = "tsTBOFFSET";
            this.tsTBOFFSET.Size = new System.Drawing.Size(100, 25);
            this.tsTBOFFSET.Text = "0";
            this.tsTBOFFSET.TextChanged += new System.EventHandler(this.tsTBMax_TextChanged);
            // 
            // tsb_back
            // 
            this.tsb_back.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsb_back.Image = ((System.Drawing.Image)(resources.GetObject("tsb_back.Image")));
            this.tsb_back.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_back.Name = "tsb_back";
            this.tsb_back.Size = new System.Drawing.Size(54, 22);
            this.tsb_back.Text = "<zurück";
            this.tsb_back.Click += new System.EventHandler(this.tsb_back_Click);
            // 
            // tsb_forward
            // 
            this.tsb_forward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsb_forward.Image = ((System.Drawing.Image)(resources.GetObject("tsb_forward.Image")));
            this.tsb_forward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_forward.Name = "tsb_forward";
            this.tsb_forward.Size = new System.Drawing.Size(51, 22);
            this.tsb_forward.Text = "weiter>";
            this.tsb_forward.Click += new System.EventHandler(this.tsb_forward_Click);
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // DGV
            // 
            this.DGV.AllowDrop = true;
            this.DGV.AllowUserToAddRows = false;
            this.DGV.AllowUserToDeleteRows = false;
            this.DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DGV.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.EP.SetIconPadding(this.DGV, 5);
            this.DGV.Location = new System.Drawing.Point(3, 3);
            this.DGV.MultiSelect = false;
            this.DGV.Name = "DGV";
            this.DGV.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV.Size = new System.Drawing.Size(930, 237);
            this.DGV.TabIndex = 0;
            // 
            // OFD
            // 
            this.OFD.FileName = "openFileDialog1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DGV_Constraints);
            this.groupBox1.Location = new System.Drawing.Point(6, 73);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 148);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Spalten auf Wert einschränken";
            this.TT.SetToolTip(this.groupBox1, "Wortanfang für gegebene Textpalten angeben.");
            // 
            // DGV_Constraints
            // 
            this.DGV_Constraints.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGV_Constraints.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Constraints.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV_Constraints.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DGV_Constraints.Location = new System.Drawing.Point(3, 16);
            this.DGV_Constraints.Name = "DGV_Constraints";
            this.DGV_Constraints.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DGV_Constraints.Size = new System.Drawing.Size(360, 129);
            this.DGV_Constraints.TabIndex = 0;
            this.DGV_Constraints.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_Const_Text_CellValueChanged);
            // 
            // tbSearchALL
            // 
            this.tbSearchALL.Location = new System.Drawing.Point(73, 47);
            this.tbSearchALL.Name = "tbSearchALL";
            this.tbSearchALL.Size = new System.Drawing.Size(296, 20);
            this.tbSearchALL.TabIndex = 0;
            this.TT.SetToolTip(this.tbSearchALL, "Suchbegriff der in allen Spalten gesucht wird");
            this.tbSearchALL.TextChanged += new System.EventHandler(this.EVENT_PrepareCommand);
            // 
            // cbCaseSensitive
            // 
            this.cbCaseSensitive.AutoSize = true;
            this.cbCaseSensitive.Location = new System.Drawing.Point(161, 24);
            this.cbCaseSensitive.Name = "cbCaseSensitive";
            this.cbCaseSensitive.Size = new System.Drawing.Size(94, 17);
            this.cbCaseSensitive.TabIndex = 1;
            this.cbCaseSensitive.Text = "Case sensitive";
            this.TT.SetToolTip(this.cbCaseSensitive, "Beachtet Groß- und Kleinschreibung");
            this.cbCaseSensitive.UseVisualStyleBackColor = true;
            this.cbCaseSensitive.CheckedChanged += new System.EventHandler(this.EVENT_PrepareCommand);
            // 
            // cbExactMatch
            // 
            this.cbExactMatch.AutoSize = true;
            this.cbExactMatch.Location = new System.Drawing.Point(9, 24);
            this.cbExactMatch.Name = "cbExactMatch";
            this.cbExactMatch.Size = new System.Drawing.Size(132, 17);
            this.cbExactMatch.TabIndex = 4;
            this.cbExactMatch.Text = "volle Übereinstimmung";
            this.TT.SetToolTip(this.cbExactMatch, "Der Wert in der Datenbank muss genau mit dem Suchbegriff übereinstimmen");
            this.cbExactMatch.UseVisualStyleBackColor = true;
            this.cbExactMatch.CheckedChanged += new System.EventHandler(this.EVENT_PrepareCommand);
            // 
            // bt_SearchNew
            // 
            this.bt_SearchNew.Location = new System.Drawing.Point(783, 14);
            this.bt_SearchNew.Name = "bt_SearchNew";
            this.bt_SearchNew.Size = new System.Drawing.Size(138, 59);
            this.bt_SearchNew.TabIndex = 3;
            this.bt_SearchNew.Text = "Suchergebnis\r\naktualisieren\r\n\r\n";
            this.TT.SetToolTip(this.bt_SearchNew, "Starten neue Datenbankabfrage, z.B. mit den getroffenene Einschränkungen");
            this.bt_SearchNew.UseVisualStyleBackColor = true;
            this.bt_SearchNew.Click += new System.EventHandler(this.bt_SearchNew_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tsl_status});
            this.statusStrip1.Location = new System.Drawing.Point(0, 507);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(936, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Status:";
            // 
            // tsl_status
            // 
            this.tsl_status.Name = "tsl_status";
            this.tsl_status.Size = new System.Drawing.Size(147, 17);
            this.tsl_status.Text = "warte auf Benutzereingabe";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.DGV, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 239F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(936, 482);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.bt_SearchNew);
            this.panel1.Controls.Add(this.btOK);
            this.panel1.Controls.Add(this.btCANCEL);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 246);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(930, 233);
            this.panel1.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbExactMatch);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.cbCaseSensitive);
            this.groupBox4.Controls.Add(this.groupBox1);
            this.groupBox4.Controls.Add(this.tbSearchALL);
            this.groupBox4.Location = new System.Drawing.Point(9, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(378, 224);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Einschränkungen";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Suchbegriff";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RTB);
            this.groupBox2.Location = new System.Drawing.Point(390, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(387, 224);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SQL";
            // 
            // RTB
            // 
            this.RTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RTB.Location = new System.Drawing.Point(3, 16);
            this.RTB.Name = "RTB";
            this.RTB.ReadOnly = true;
            this.RTB.Size = new System.Drawing.Size(381, 205);
            this.RTB.TabIndex = 4;
            this.RTB.Text = "";
            // 
            // btOK
            // 
            this.btOK.Enabled = false;
            this.btOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btOK.Location = new System.Drawing.Point(783, 165);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(138, 59);
            this.btOK.TabIndex = 1;
            this.btOK.Text = "Auswahl übernehmen";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // btCANCEL
            // 
            this.btCANCEL.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCANCEL.Location = new System.Drawing.Point(783, 136);
            this.btCANCEL.Name = "btCANCEL";
            this.btCANCEL.Size = new System.Drawing.Size(138, 23);
            this.btCANCEL.TabIndex = 0;
            this.btCANCEL.Text = "Abbruch";
            this.btCANCEL.UseVisualStyleBackColor = true;
            this.btCANCEL.Click += new System.EventHandler(this.btCANCEL_Click);
            // 
            // TIMER
            // 
            this.TIMER.Interval = 1000;
            // 
            // SubShowSelectionTable
            // 
            this.AcceptButton = this.btOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCANCEL;
            this.ClientSize = new System.Drawing.Size(936, 529);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "SubShowSelectionTable";
            this.Text = "Datenbanktabelle";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Constraints)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox tsTBMax;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.ToolTip TT;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox tsTBOFFSET;
        private System.Windows.Forms.ToolStripButton tsb_back;
        private System.Windows.Forms.ToolStripButton tsb_forward;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView DGV;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox RTB;
        private System.Windows.Forms.Button bt_SearchNew;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView DGV_Constraints;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btCANCEL;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tsl_status;
        private System.Windows.Forms.TextBox tbSearchALL;
        private System.Windows.Forms.CheckBox cbCaseSensitive;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbExactMatch;
        private System.Windows.Forms.Timer TIMER;
    }
}