﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using SpectationClient;
using GeoAPI.CoordinateSystems;
//using ProjNet.CoordinateSystems; 
//using ProjNet.CoordinateSystems;
//using ProjNet.CoordinateSystems;

namespace SpectationClient.GUI {
    public partial class SubSelectCoordinateSystem : Form {
        private DataView DV;
        private DataTable DT;
        private DBManager DBM;
        private List<String> rowFilterAttributes;
        public SubSelectCoordinateSystem(ref DBManager dbm) {
            InitializeComponent();


            this.Icon = Resources.Icon;
            rowFilterAttributes = new List<string>();
            rb_ALL.CheckedChanged += new EventHandler(rb_ALL_CheckedChanged);
            rb_GEOD.CheckedChanged += new EventHandler(rb_ALL_CheckedChanged);
            rb_GEOGR.CheckedChanged += new EventHandler(rb_ALL_CheckedChanged);
            rb_GEOZ.CheckedChanged += new EventHandler(rb_ALL_CheckedChanged);
            gb_KEYWORD_tb.TextChanged += new EventHandler(rb_ALL_CheckedChanged);
            DBM = dbm;
            DGV.SelectionChanged += new EventHandler(DGV_SelectionChanged);
            //DGV.AutoGenerateColumns = false;
            DT = DBM.getSRIDTable();
            DV = new DataView(DT);
            DGV.DataSource = DV;
        }

        public int SRID {
            get { int ridx = DGV.SelectedCells[0].RowIndex;
                  return (int) DGV.Rows[ridx].Cells["SRID"].Value;
            }
        }

        public String TYPE {
            get {
                int ridx = DGV.SelectedCells[0].RowIndex;
                return DGV.Rows[ridx].Cells["TYPE"].Value.ToString();
            }
        }

        public String NAME {
            get {
                int ridx = DGV.SelectedCells[0].RowIndex;
                return DGV.Rows[ridx].Cells["NAME"].Value.ToString();
            }
        }
        public String UNIT {
            get {
                int ridx = DGV.SelectedCells[0].RowIndex;
                return DGV.Rows[ridx].Cells["UNIT"].Value.ToString();
            }
        }

        void DGV_SelectionChanged(object sender, EventArgs e) {
            if (DGV.SelectedCells.Count == 0 || 
                DGV.SelectedRows.Count == 0) {
                bt_OK.Enabled = false;
            } else {
                bt_OK.Enabled = true;
            }
        }

        void rb_ALL_CheckedChanged(object sender, EventArgs e) {
            rowFilterAttributes.Clear();
            if(rb_ALL.Checked){
                //NOTHING DV.RowFilter = "";
            }else if(rb_GEOD.Checked){
                //DV.RowFilter = "TYPE = '"+typeof(ProjectedCoordinateSystem).Name+"'";
                rowFilterAttributes.Add("TYPE = '" + typeof(IProjectedCoordinateSystem).Name + "'");
            }else if(rb_GEOGR.Checked){
                //DV.RowFilter = "TYPE = '" + typeof(GeographicCoordinateSystem).Name + "'";
                rowFilterAttributes.Add("TYPE = '" + typeof(IGeographicCoordinateSystem).Name + "'");
            }else if(rb_GEOZ.Checked){
                //DV.RowFilter = "TYPE = '" + typeof(GeocentricCoordinateSystem).Name + "'";
                rowFilterAttributes.Add("TYPE = '" + typeof(IGeocentricCoordinateSystem).Name + "'");
            }
            String filterText = gb_KEYWORD_tb.Text.Trim();
            if(filterText.Length > 0) {
                //TODO remove inner white spaces
                //String p2 = R_RemoveWS.Replace(filterText, "*");
                //String p2 = R_TrimInnerWS.Replace(filterText, " ");
                String p2 = filterText;
                rowFilterAttributes.Add(String.Format("NAME LIKE '*{0}*'", p2));
            }
            //build filter
            try {
                DV.RowFilter = TextHelper.combine(rowFilterAttributes, " AND ");
            } catch(Exception ex) {
                Console.WriteLine(TextHelper.Exception2String(ex));
            }
                
        }

        private void bt_OK_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.OK;
        }

      
    }
}
