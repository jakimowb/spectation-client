﻿namespace SpectationClient {
    partial class SpectationClientInfo {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpectationClientInfo));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.logoPictureBox = new System.Windows.Forms.PictureBox();
            this.RTB = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ll_3rdParties = new System.Windows.Forms.LinkLabel();
            this.linkCopyRight = new System.Windows.Forms.LinkLabel();
            this.ll_ChangeLog = new System.Windows.Forms.LinkLabel();
            this.okButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.labelProductVersion = new System.Windows.Forms.Label();
            this.labelProductName = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.logoPictureBox, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.RTB, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.panel1, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(9, 9);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 72.93578F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.06422F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(464, 355);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // logoPictureBox
            // 
            this.logoPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("logoPictureBox.Image")));
            this.logoPictureBox.Location = new System.Drawing.Point(3, 3);
            this.logoPictureBox.Name = "logoPictureBox";
            this.logoPictureBox.Size = new System.Drawing.Size(144, 144);
            this.logoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logoPictureBox.TabIndex = 12;
            this.logoPictureBox.TabStop = false;
            // 
            // RTB
            // 
            this.RTB.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.RTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableLayoutPanel.SetColumnSpan(this.RTB, 2);
            this.RTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RTB.Location = new System.Drawing.Point(1, 160);
            this.RTB.Margin = new System.Windows.Forms.Padding(1, 10, 1, 10);
            this.RTB.Name = "RTB";
            this.RTB.ReadOnly = true;
            this.RTB.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.RTB.Size = new System.Drawing.Size(462, 129);
            this.RTB.TabIndex = 25;
            this.RTB.Text = "";
            // 
            // panel1
            // 
            this.tableLayoutPanel.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.ll_3rdParties);
            this.panel1.Controls.Add(this.linkCopyRight);
            this.panel1.Controls.Add(this.ll_ChangeLog);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 299);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(464, 56);
            this.panel1.TabIndex = 26;
            // 
            // ll_3rdParties
            // 
            this.ll_3rdParties.AutoSize = true;
            this.ll_3rdParties.Location = new System.Drawing.Point(188, 30);
            this.ll_3rdParties.Name = "ll_3rdParties";
            this.ll_3rdParties.Size = new System.Drawing.Size(103, 13);
            this.ll_3rdParties.TabIndex = 0;
            this.ll_3rdParties.TabStop = true;
            this.ll_3rdParties.Text = "Third Party Software";
            this.ll_3rdParties.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ll_3rdParties_LinkClicked);
            // 
            // linkCopyRight
            // 
            this.linkCopyRight.AutoSize = true;
            this.linkCopyRight.Location = new System.Drawing.Point(71, 30);
            this.linkCopyRight.Name = "linkCopyRight";
            this.linkCopyRight.Size = new System.Drawing.Size(111, 13);
            this.linkCopyRight.TabIndex = 25;
            this.linkCopyRight.TabStop = true;
            this.linkCopyRight.Text = "GNU LGPL 3 Licence";
            this.linkCopyRight.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkCopyRight_LinkClicked);
            // 
            // ll_ChangeLog
            // 
            this.ll_ChangeLog.AutoSize = true;
            this.ll_ChangeLog.Location = new System.Drawing.Point(3, 30);
            this.ll_ChangeLog.Name = "ll_ChangeLog";
            this.ll_ChangeLog.Size = new System.Drawing.Size(62, 13);
            this.ll_ChangeLog.TabIndex = 1;
            this.ll_ChangeLog.TabStop = true;
            this.ll_ChangeLog.Text = "ChangeLog";
            this.ll_ChangeLog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ll_ChangeLog_LinkClicked);
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.okButton.Location = new System.Drawing.Point(386, 30);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 24;
            this.okButton.Text = "&OK";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.labelProductVersion);
            this.panel2.Controls.Add(this.labelProductName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(150, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(314, 150);
            this.panel2.TabIndex = 27;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(198, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Copyright 2009-2012, Benjamin Jakimow";
            // 
            // labelProductVersion
            // 
            this.labelProductVersion.AutoSize = true;
            this.labelProductVersion.Location = new System.Drawing.Point(4, 30);
            this.labelProductVersion.Name = "labelProductVersion";
            this.labelProductVersion.Size = new System.Drawing.Size(45, 13);
            this.labelProductVersion.TabIndex = 1;
            this.labelProductVersion.Text = "Version ";
            // 
            // labelProductName
            // 
            this.labelProductName.AutoSize = true;
            this.labelProductName.Location = new System.Drawing.Point(4, 4);
            this.labelProductName.Name = "labelProductName";
            this.labelProductName.Size = new System.Drawing.Size(35, 13);
            this.labelProductName.TabIndex = 0;
            this.labelProductName.Text = "Name";
            // 
            // SpectationClientInfo
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 373);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SpectationClientInfo";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Info über SPECTATION Client";
            this.tableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.LinkLabel ll_ChangeLog;
        private System.Windows.Forms.LinkLabel ll_3rdParties;
        private System.Windows.Forms.RichTextBox RTB;
        private System.Windows.Forms.LinkLabel linkCopyRight;
        private System.Windows.Forms.PictureBox logoPictureBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelProductVersion;
        private System.Windows.Forms.Label labelProductName;
    }
}
