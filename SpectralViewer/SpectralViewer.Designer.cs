﻿namespace SpectralViewer {
    partial class SpectralViewer {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("ENVI Spectral Libraries", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("ASD Binary Spectra", System.Windows.Forms.HorizontalAlignment.Left);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpectralViewer));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btRemove = new System.Windows.Forms.Button();
            this.btAdd = new System.Windows.Forms.Button();
            this.listViewSpectralFile = new System.Windows.Forms.ListView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.DGVSpectra = new System.Windows.Forms.DataGridView();
            this.CSPECTRA = new SpectationClient.DGVExtensions.DataGridViewSpectrumColumn();
            this.CCOLORS = new SpectationClient.DGVExtensions.DataGridViewColorColumn();
            this.CFILENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemLink = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewSpectrumColumn1 = new SpectationClient.DGVExtensions.DataGridViewSpectrumColumn();
            this.dataGridViewColorColumn1 = new SpectationClient.DGVExtensions.DataGridViewColorColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVSpectra)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 472);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(836, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(836, 472);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btRemove);
            this.tabPage1.Controls.Add(this.btAdd);
            this.tabPage1.Controls.Add(this.listViewSpectralFile);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(828, 446);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Files";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btRemove
            // 
            this.btRemove.Enabled = false;
            this.btRemove.Location = new System.Drawing.Point(90, 5);
            this.btRemove.Name = "btRemove";
            this.btRemove.Size = new System.Drawing.Size(75, 23);
            this.btRemove.TabIndex = 2;
            this.btRemove.Text = "Remove";
            this.btRemove.UseVisualStyleBackColor = true;
            this.btRemove.Click += new System.EventHandler(this.btRemove_Click);
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(8, 6);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(75, 23);
            this.btAdd.TabIndex = 1;
            this.btAdd.Text = "Add";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // listViewSpectralFile
            // 
            this.listViewSpectralFile.FullRowSelect = true;
            this.listViewSpectralFile.GridLines = true;
            listViewGroup1.Header = "ENVI Spectral Libraries";
            listViewGroup1.Name = "groupESL";
            listViewGroup2.Header = "ASD Binary Spectra";
            listViewGroup2.Name = "groupASD";
            this.listViewSpectralFile.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.listViewSpectralFile.HideSelection = false;
            this.listViewSpectralFile.LabelEdit = true;
            this.listViewSpectralFile.Location = new System.Drawing.Point(8, 34);
            this.listViewSpectralFile.Name = "listViewSpectralFile";
            this.listViewSpectralFile.Size = new System.Drawing.Size(536, 384);
            this.listViewSpectralFile.TabIndex = 0;
            this.listViewSpectralFile.UseCompatibleStateImageBehavior = false;
            this.listViewSpectralFile.View = System.Windows.Forms.View.Details;
            this.listViewSpectralFile.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listViewSpectralFile_ItemSelectionChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(828, 446);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Spectra";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.DGVSpectra);
            this.splitContainer1.Size = new System.Drawing.Size(822, 440);
            this.splitContainer1.SplitterDistance = 264;
            this.splitContainer1.TabIndex = 0;
            // 
            // DGVSpectra
            // 
            this.DGVSpectra.AllowUserToAddRows = false;
            this.DGVSpectra.AllowUserToDeleteRows = false;
            this.DGVSpectra.AllowUserToOrderColumns = true;
            this.DGVSpectra.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVSpectra.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGVSpectra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVSpectra.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CSPECTRA,
            this.CCOLORS,
            this.CFILENAME,
            this.ItemLink});
            this.DGVSpectra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVSpectra.Location = new System.Drawing.Point(0, 0);
            this.DGVSpectra.Name = "DGVSpectra";
            this.DGVSpectra.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVSpectra.Size = new System.Drawing.Size(820, 170);
            this.DGVSpectra.TabIndex = 0;
            // 
            // CSPECTRA
            // 
            this.CSPECTRA.FillWeight = 111.9289F;
            this.CSPECTRA.HeaderText = "Spectrum";
            this.CSPECTRA.Name = "CSPECTRA";
            this.CSPECTRA.ReadOnly = true;
            this.CSPECTRA.ToolStripLabel_ShowSpectra = false;
            // 
            // CCOLORS
            // 
            this.CCOLORS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CCOLORS.FillWeight = 76.14214F;
            this.CCOLORS.HeaderText = "Color";
            this.CCOLORS.MaxInputLength = 50;
            this.CCOLORS.Name = "CCOLORS";
            this.CCOLORS.ReadOnly = true;
            this.CCOLORS.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CCOLORS.Width = 50;
            // 
            // CFILENAME
            // 
            this.CFILENAME.FillWeight = 111.9289F;
            this.CFILENAME.HeaderText = "Filename";
            this.CFILENAME.Name = "CFILENAME";
            // 
            // ItemLink
            // 
            this.ItemLink.HeaderText = "ItemLink";
            this.ItemLink.Name = "ItemLink";
            this.ItemLink.Visible = false;
            // 
            // dataGridViewSpectrumColumn1
            // 
            this.dataGridViewSpectrumColumn1.FillWeight = 111.9289F;
            this.dataGridViewSpectrumColumn1.HeaderText = "Spectrum";
            this.dataGridViewSpectrumColumn1.Name = "dataGridViewSpectrumColumn1";
            this.dataGridViewSpectrumColumn1.ReadOnly = true;
            this.dataGridViewSpectrumColumn1.ToolStripLabel_ShowSpectra = false;
            this.dataGridViewSpectrumColumn1.Width = 291;
            // 
            // dataGridViewColorColumn1
            // 
            this.dataGridViewColorColumn1.FillWeight = 76.14214F;
            this.dataGridViewColorColumn1.HeaderText = "Color";
            this.dataGridViewColorColumn1.MaxInputLength = 50;
            this.dataGridViewColorColumn1.Name = "dataGridViewColorColumn1";
            this.dataGridViewColorColumn1.ReadOnly = true;
            this.dataGridViewColorColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewColorColumn1.Width = 197;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.FillWeight = 111.9289F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Filename";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 291;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "ItemLink";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // SpectralViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(836, 494);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SpectralViewer";
            this.Text = "SpectralViewer";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVSpectra)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView DGVSpectra;
        private System.Windows.Forms.ListView listViewSpectralFile;
        private System.Windows.Forms.Button btRemove;
        private System.Windows.Forms.Button btAdd;
        private SpectationClient.DGVExtensions.DataGridViewSpectrumColumn CSPECTRA;
        private SpectationClient.DGVExtensions.DataGridViewColorColumn CCOLORS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CFILENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemLink;
        private SpectationClient.DGVExtensions.DataGridViewSpectrumColumn dataGridViewSpectrumColumn1;
        private SpectationClient.DGVExtensions.DataGridViewColorColumn dataGridViewColorColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}

